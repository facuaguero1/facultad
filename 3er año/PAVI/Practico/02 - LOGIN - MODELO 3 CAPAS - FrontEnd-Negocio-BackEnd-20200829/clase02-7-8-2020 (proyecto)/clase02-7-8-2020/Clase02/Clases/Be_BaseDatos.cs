﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;


namespace clase02.Clases
{
    class Be_BaseDatos
    {
        SqlConnection Conexion = new SqlConnection();
        SqlCommand Cmd = new  SqlCommand();
        string Cadena_conexion = "Data Source=DESKTOP-V2CUC0T\\SQLEXPRESS;Initial Catalog=_TRATAMIENTO_ERRORES;Integrated Security=True";

        private void Conectar()
        {
            Conexion.ConnectionString = Cadena_conexion;
            Conexion.Open();
            Cmd.Connection = Conexion;
            Cmd.CommandType = System.Data.CommandType.Text;
        }
        private void Desconectar()
        {
            Conexion.Close();
        }
        public DataTable Consulta (string sql)
        {
            Conectar();
            Cmd.CommandText = sql;
            DataTable tabla = new DataTable();
            tabla.Load(Cmd.ExecuteReader());
            Desconectar();
            return tabla;
        }
    }
}
