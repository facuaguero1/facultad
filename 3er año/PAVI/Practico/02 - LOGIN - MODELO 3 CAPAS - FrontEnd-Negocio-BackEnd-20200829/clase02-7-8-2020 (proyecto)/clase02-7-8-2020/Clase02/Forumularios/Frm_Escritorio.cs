﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Clase02
{
    public partial class Frm_Escritorio : Form
    {
        public Frm_Escritorio()
        {
            InitializeComponent();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close(); 
        }

        private void Frm_Escritorio_Load(object sender, EventArgs e)
        {
            Frm_login login = new Frm_login();
            login.ShowDialog();

            MessageBox.Show("el usuario es: " + login.Usuario + "\n" + "el password es:" + login.Password);
            login.Dispose();

        }
    }
}
