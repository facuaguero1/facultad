USE [Pymes]
GO
/****** Object:  Table [dbo].[Articulos]    Script Date: 05/03/2021 17:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Articulos](
	[IdArticulo] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](55) NULL,
	[Precio] [decimal](18, 2) NULL,
	[CodigoDeBarra] [nchar](13) NULL,
	[IdArticuloFamilia] [int] NULL,
	[Stock] [int] NULL,
	[FechaAlta] [date] NULL,
	[Activo] [bit] NULL,
 CONSTRAINT [PK_Articulos] PRIMARY KEY CLUSTERED 
(
	[IdArticulo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ArticulosFamilias]    Script Date: 05/03/2021 17:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ArticulosFamilias](
	[IdArticuloFamilia] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](50) NULL,
 CONSTRAINT [PK_ArticulosFamilias] PRIMARY KEY CLUSTERED 
(
	[IdArticuloFamilia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AuditoriasABM]    Script Date: 05/03/2021 17:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuditoriasABM](
	[IdAuditoriaABM] [int] IDENTITY(1,1) NOT NULL,
	[Fecha] [datetime] NULL,
	[Tabla] [varchar](50) NULL,
	[IdRegistro] [int] NULL,
	[AccionABM] [char](1) NULL,
	[Observaciones] [varchar](100) NULL,
	[IdSession] [nchar](25) NULL,
 CONSTRAINT [PK_AuditoriasABM] PRIMARY KEY CLUSTERED 
(
	[IdAuditoriaABM] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categorias]    Script Date: 05/03/2021 17:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categorias](
	[IdCategoria] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](50) NULL,
 CONSTRAINT [PK_Categorias] PRIMARY KEY CLUSTERED 
(
	[IdCategoria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Clientes]    Script Date: 05/03/2021 17:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clientes](
	[IdCliente] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
	[Cuit] [bigint] NULL,
	[IdTipoDocumento] [nvarchar](3) NULL,
	[NumeroDocumento] [bigint] NULL,
	[FechaNacimiento] [datetime] NULL,
	[IdSexo] [nchar](1) NULL,
	[IdEstadoCivil] [nchar](1) NULL,
	[Calle] [nvarchar](50) NULL,
	[NumeroCalle] [int] NULL,
	[Localidad] [nvarchar](255) NULL,
	[IdDepartamento] [int] NULL,
	[IdProvincia] [int] NULL,
	[IdPais] [int] NULL,
	[FechaIngreso] [datetime] NULL,
	[FechaEgreso] [datetime] NULL,
	[TieneTrabajo] [bit] NULL,
	[TieneAuto] [bit] NULL,
	[TieneCasa] [bit] NULL,
	[CreditoMaximo] [decimal](18, 2) NULL,
	[Mail] [nvarchar](50) NULL,
	[Clave] [nvarchar](15) NULL,
	[IdUsuario] [int] NULL,
	[Activo] [bit] NULL,
	[Observaciones] [nvarchar](500) NULL,
 CONSTRAINT [PK_Clientes] PRIMARY KEY CLUSTERED 
(
	[IdCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Contactos]    Script Date: 05/03/2021 17:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contactos](
	[IdContacto] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
	[FechaNacimiento] [datetime] NULL,
	[Telefono] [int] NULL,
	[IdCategoria] [int] NULL,
 CONSTRAINT [PK_Contactos] PRIMARY KEY CLUSTERED 
(
	[IdContacto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Departamentos]    Script Date: 05/03/2021 17:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Departamentos](
	[IdDepartamento] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](50) NULL,
	[IdProvincia] [int] NULL,
 CONSTRAINT [PK_Departamentos] PRIMARY KEY CLUSTERED 
(
	[IdDepartamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Empresas]    Script Date: 05/03/2021 17:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Empresas](
	[IdEmpresa] [int] IDENTITY(1,1) NOT NULL,
	[RazonSocial] [nvarchar](50) NOT NULL,
	[CantidadEmpleados] [int] NOT NULL,
	[FechaFundacion] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_Empresas] PRIMARY KEY CLUSTERED 
(
	[IdEmpresa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EstadosCiviles]    Script Date: 05/03/2021 17:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EstadosCiviles](
	[IdEstadoCivil] [nchar](1) NOT NULL,
	[Nombre] [varchar](30) NULL,
 CONSTRAINT [PK_EstadosCiviles] PRIMARY KEY CLUSTERED 
(
	[IdEstadoCivil] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Grupos]    Script Date: 05/03/2021 17:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Grupos](
	[IdGrupo] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](30) NULL,
 CONSTRAINT [PK_Grupos] PRIMARY KEY CLUSTERED 
(
	[IdGrupo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GruposPermisos]    Script Date: 05/03/2021 17:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GruposPermisos](
	[IdGrupo] [int] NOT NULL,
	[IdPermiso] [int] NOT NULL,
 CONSTRAINT [PK_PerfilesProcesos] PRIMARY KEY CLUSTERED 
(
	[IdGrupo] ASC,
	[IdPermiso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IvasTipos]    Script Date: 05/03/2021 17:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IvasTipos](
	[IvaTipoId] [tinyint] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](30) NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_IvasTipos] PRIMARY KEY CLUSTERED 
(
	[IvaTipoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Menus]    Script Date: 05/03/2021 17:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Menus](
	[IdMenu] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NULL,
	[Url] [varchar](100) NULL,
	[IdMenuPadre] [int] NULL,
	[Orden] [tinyint] NULL,
	[IdPermiso] [int] NULL,
 CONSTRAINT [PK_Menus] PRIMARY KEY CLUSTERED 
(
	[IdMenu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Paises]    Script Date: 05/03/2021 17:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Paises](
	[IdPais] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
	[Poblacion] [int] NULL,
	[Continente] [nvarchar](50) NULL,
 CONSTRAINT [PK_Paises] PRIMARY KEY CLUSTERED 
(
	[IdPais] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Permisos]    Script Date: 05/03/2021 17:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permisos](
	[IdPermisos] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NULL,
 CONSTRAINT [PK_Permisos] PRIMARY KEY CLUSTERED 
(
	[IdPermisos] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Personas]    Script Date: 05/03/2021 17:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Personas](
	[IdPersona] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](50) NULL,
 CONSTRAINT [PK_Personas] PRIMARY KEY CLUSTERED 
(
	[IdPersona] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Provincias]    Script Date: 05/03/2021 17:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Provincias](
	[IdProvincia] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](30) NOT NULL,
	[IdPais] [int] NULL,
 CONSTRAINT [PK_Provincias] PRIMARY KEY CLUSTERED 
(
	[IdProvincia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sesiones]    Script Date: 05/03/2021 17:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sesiones](
	[IdSesion] [varchar](25) NOT NULL,
	[Maquina] [varchar](50) NULL,
	[FechaInicio] [datetime] NULL,
	[FechaFin] [datetime] NULL,
	[IdUsuario] [int] NULL,
 CONSTRAINT [PK_Sesiones] PRIMARY KEY CLUSTERED 
(
	[IdSesion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sexos]    Script Date: 05/03/2021 17:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sexos](
	[IdSexo] [nchar](1) NOT NULL,
	[Nombre] [nvarchar](50) NULL,
 CONSTRAINT [PK_Sexos] PRIMARY KEY CLUSTERED 
(
	[IdSexo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TelefonoXCliente]    Script Date: 05/03/2021 17:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TelefonoXCliente](
	[IdTelefono] [int] IDENTITY(1,1) NOT NULL,
	[IdCliente] [int] NOT NULL,
	[Telefono] [nchar](50) NOT NULL,
 CONSTRAINT [PK_TelefonoXCliente] PRIMARY KEY CLUSTERED 
(
	[IdTelefono] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TiposDocumentos]    Script Date: 05/03/2021 17:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TiposDocumentos](
	[IdTipoDocumento] [nvarchar](3) NOT NULL,
	[Nombre] [nvarchar](50) NULL,
 CONSTRAINT [PK_TipoDocumentos] PRIMARY KEY CLUSTERED 
(
	[IdTipoDocumento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 05/03/2021 17:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[IdUsuario] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](30) NOT NULL,
	[Clave] [varchar](50) NOT NULL,
	[Bloqueado] [bit] NULL,
	[IdRegistroRelacion] [int] NULL,
	[IdTablaRelacion] [varchar](50) NULL,
 CONSTRAINT [PK_Usuarios] PRIMARY KEY CLUSTERED 
(
	[IdUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UsuariosGrupos]    Script Date: 05/03/2021 17:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsuariosGrupos](
	[IdUsuario] [int] NOT NULL,
	[IdGrupo] [int] NOT NULL,
 CONSTRAINT [PK_UsuariosGrupos] PRIMARY KEY CLUSTERED 
(
	[IdUsuario] ASC,
	[IdGrupo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ventas]    Script Date: 05/03/2021 17:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ventas](
	[IdVenta] [int] IDENTITY(1,1) NOT NULL,
	[IdCliente] [int] NULL,
	[Fecha] [datetime] NULL,
	[Total] [decimal](18, 2) NULL,
 CONSTRAINT [PK_Ventas] PRIMARY KEY CLUSTERED 
(
	[IdVenta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VentasDetalle]    Script Date: 05/03/2021 17:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VentasDetalle](
	[IdVentaDetalle] [int] IDENTITY(1,1) NOT NULL,
	[IdVenta] [int] NOT NULL,
	[IdArticulo] [int] NULL,
	[Cantidad] [int] NULL,
	[Precio] [decimal](18, 2) NULL,
 CONSTRAINT [PK_VentasDetalle] PRIMARY KEY CLUSTERED 
(
	[IdVentaDetalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Articulos] ON 

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (1, N'KIT DIRECT TV PREPA 0.60MT', CAST(299.00 AS Decimal(18, 2)), N'0779815559001', 10, 329, CAST(N'2017-01-19' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (2, N'KIT DIRECT TV PREPA 0.90MT', CAST(349.00 AS Decimal(18, 2)), N'0779815559002', 10, 468, CAST(N'2017-01-31' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (3, N'LED 22" LG FHD 22MN42APM', CAST(2669.00 AS Decimal(18, 2)), N'0779808338808', 10, 536, CAST(N'2017-01-12' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (4, N'LED 24" ILO HD DIGITAL MOD LDH24ILO02', CAST(2999.00 AS Decimal(18, 2)), N'0779696260024', 10, 169, CAST(N'2017-01-30' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (5, N'LED 24" LG HD 24MN42A-PM', CAST(3129.00 AS Decimal(18, 2)), N'0779808338809', 10, 296, CAST(N'2016-12-28' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (7, N'LED 32" BGH HD BLE3214D', CAST(4830.00 AS Decimal(18, 2)), N'0779688540133', 10, 998, CAST(N'2017-01-01' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (8, N'LED 32" BGH SMART TV BLE3213RT', CAST(5405.00 AS Decimal(18, 2)), N'0779688540117', 10, 650, CAST(N'2017-01-18' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (9, N'LED 32" HISENSE IPTV HLE3213RT', CAST(5290.00 AS Decimal(18, 2)), N'0779688540119', 10, 51, CAST(N'2017-02-03' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (10, N'LED 32" HITACHI HD CDHLE32FD10', CAST(4837.00 AS Decimal(18, 2)), N'0779694109973', 10, 838, CAST(N'2016-12-25' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (11, N'LED 32" ILO HD DIGITAL LDH32ILO02', CAST(4199.00 AS Decimal(18, 2)), N'0779696260132', 10, 501, CAST(N'2017-01-25' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (12, N'LED 32" JVC HD IPTV LT32DR930', CAST(6699.00 AS Decimal(18, 2)), N'0779818058057', 10, 906, CAST(N'2017-01-25' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (13, N'LED 32" JVC HD LT32DA330', CAST(4499.00 AS Decimal(18, 2)), N'0779696266323', 10, 435, CAST(N'2017-02-07' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (14, N'LED 32" LG 3D 32LA613B', CAST(6299.00 AS Decimal(18, 2)), N'0779808338816', 10, 329, CAST(N'2017-02-06' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (15, N'LED 32" PHILIPS FHD 32PFL3018D/77', CAST(6799.00 AS Decimal(18, 2)), N'0871258168715', 10, 971, CAST(N'2016-12-25' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (16, N'LED 32" PHILIPS FHD IPTV 32PFL4508G/77', CAST(7699.00 AS Decimal(18, 2)), N'0871258167198', 10, 636, CAST(N'2017-02-07' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (17, N'LED 32" PHILIPS HD 32PFL3008D/77', CAST(5799.00 AS Decimal(18, 2)), N'0871258167218', 10, 67, CAST(N'2016-12-27' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (18, N'LED 32" PHILIPS SMART TV 32PFL3518G/77', CAST(7399.00 AS Decimal(18, 2)), N'0871258167225', 10, 250, CAST(N'2017-01-08' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (19, N'LED 32" RCA HD L32S80DIGI', CAST(4499.00 AS Decimal(18, 2)), N'0779694101214', 10, 857, CAST(N'2017-01-23' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (20, N'LED 32" SAMSUNG FHD UN32F5000', CAST(6094.00 AS Decimal(18, 2)), N'0880608543154', 10, 636, CAST(N'2016-12-30' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (21, N'LED 32" SAMSUNG HD UN32F4000', CAST(5519.00 AS Decimal(18, 2)), N'0880608543153', 10, 37, CAST(N'2017-01-23' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (22, N'LED 32" SAMSUNG SMART UN32F5500', CAST(6899.00 AS Decimal(18, 2)), N'0880608548607', 10, 214, CAST(N'2017-01-24' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (23, N'LED 32" SONY HD KDL32R425', CAST(6199.00 AS Decimal(18, 2)), N'0490552491740', 10, 642, CAST(N'2017-01-17' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (24, N'LED 32" SONY SMART TV KDL32W655', CAST(6999.00 AS Decimal(18, 2)), N'0490552491687', 10, 50, CAST(N'2017-02-04' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (25, N'LED 39" ILO DIG FHD LDF39ILO2', CAST(5699.00 AS Decimal(18, 2)), N'0779696260394', 10, 951, CAST(N'2017-01-19' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (26, N'LED 39" PHILIPS FHD IPTV 39PFL3508G/77', CAST(8799.00 AS Decimal(18, 2)), N'0871258168717', 10, 889, CAST(N'2017-02-03' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (27, N'LED 39" RCA FHD L39S85DIGIFHD', CAST(6499.00 AS Decimal(18, 2)), N'0779694101215', 10, 487, CAST(N'2016-12-25' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (28, N'LED 40" BGH FHD BLE4014D', CAST(7245.00 AS Decimal(18, 2)), N'0779688540132', 10, 480, CAST(N'2016-12-27' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (29, N'LED 40" SAMSUNG 3D SMART UN40F6800', CAST(13224.00 AS Decimal(18, 2)), N'0880608565606', 10, 734, CAST(N'2017-01-26' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (30, N'LED 40" SAMSUNG 3D UN40F6100', CAST(9999.00 AS Decimal(18, 2)), N'0880608544958', 10, 835, CAST(N'2017-01-19' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (31, N'LED 40" SAMSUNG FHD UN40F5000', CAST(8164.00 AS Decimal(18, 2)), N'0880608543156', 10, 436, CAST(N'2017-02-01' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (32, N'LED 40" SAMSUNG SMART UN40F5500', CAST(9774.00 AS Decimal(18, 2)), N'0880608565438', 10, 639, CAST(N'2017-01-20' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (33, N'LED 40" SONY FHD KDL40R485', CAST(7499.00 AS Decimal(18, 2)), N'0490552493532', 10, 862, CAST(N'2017-01-07' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (34, N'LED 42" LG 3D 42LA6130', CAST(9199.00 AS Decimal(18, 2)), N'0779808338817', 10, 560, CAST(N'2017-01-05' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (35, N'LED 42" LG FHD 42LN5400', CAST(8099.00 AS Decimal(18, 2)), N'0779808338818', 10, 48, CAST(N'2017-01-28' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (36, N'LED 42" LG SMART TV 42LN5700', CAST(9799.00 AS Decimal(18, 2)), N'0779808338823', 10, 967, CAST(N'2017-01-27' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (37, N'LED 42" PANASONIC 3D SMART TV TCL42ET60', CAST(11249.00 AS Decimal(18, 2)), N'0779805518074', 10, 570, CAST(N'2017-01-19' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (38, N'LED 42" PHILIPS 3D SMART TV 42PFL5008G/7', CAST(11599.00 AS Decimal(18, 2)), N'0871258167039', 10, 802, CAST(N'2017-02-04' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (39, N'LED 42" PHILIPS FHD 42PFL3008D/77', CAST(8499.00 AS Decimal(18, 2)), N'0871258167221', 10, 193, CAST(N'2017-02-04' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (40, N'LED 42" PHILIPS SMART TV 42PFL3508G/77', CAST(9499.00 AS Decimal(18, 2)), N'0871258167227', 10, 693, CAST(N'2016-12-30' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (41, N'LED 42" PIONEER 3D SMART PLE42FZP2', CAST(12299.00 AS Decimal(18, 2)), N'0498802821943', 10, 907, CAST(N'2017-02-01' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (42, N'LED 42" SONY FHD KDL42R475', CAST(7999.00 AS Decimal(18, 2)), N'0490552491728', 10, 140, CAST(N'2017-01-13' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (43, N'LED 46" PHILIPS SMART TV 46PFL4508G/7', CAST(13999.00 AS Decimal(18, 2)), N'0871258168718', 10, 236, CAST(N'2017-01-31' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (44, N'LED 46" SAMSUNG 3D SMART TV UN46F7500', CAST(23574.00 AS Decimal(18, 2)), N'0880608565943', 10, 143, CAST(N'2016-12-25' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (45, N'LED 46" SAMSUNG SMART UN46F5500', CAST(13224.00 AS Decimal(18, 2)), N'0880608548610', 10, 345, CAST(N'2017-01-07' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (46, N'LED 46" SANYO SMART TV LCE46IF12', CAST(10599.00 AS Decimal(18, 2)), N'0779696260612', 10, 557, CAST(N'2017-02-03' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (47, N'LED 47" LG SMART TV 47LN5700', CAST(13199.00 AS Decimal(18, 2)), N'0779808338824', 10, 599, CAST(N'2017-01-20' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (48, N'LED 47" PIONEER 3D SMART PLE47FZP1', CAST(15999.00 AS Decimal(18, 2)), N'0498802821947', 10, 310, CAST(N'2017-02-07' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (49, N'LED 47" SONY 3D SMART TV KDL47W805', CAST(17199.00 AS Decimal(18, 2)), N'0490552494098', 10, 526, CAST(N'2017-01-31' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (50, N'LED 55" NOBLEX 3D IPTV 55LD856DI', CAST(20799.00 AS Decimal(18, 2)), N'0779696260000', 10, 362, CAST(N'2017-01-26' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (51, N'LED 55" PHILIPS 3D SMART TV 55PFL8008G/77', CAST(29999.00 AS Decimal(18, 2)), N'0871258166949', 10, 841, CAST(N'2017-01-06' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (52, N'SOPORTE LCD / LED DE 14" A 42" TANGWOOD', CAST(599.00 AS Decimal(18, 2)), N'0779814176493', 10, 527, CAST(N'2017-02-07' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (53, N'SOPORTE LCD / LED DE 17 '''' A 40 ''''', CAST(499.00 AS Decimal(18, 2)), N'0779814176654', 10, 588, CAST(N'2016-12-23' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (54, N'SOPORTE LCD / LED DE 17" A 37" TANGWOOD', CAST(225.00 AS Decimal(18, 2)), N'0779814176489', 10, 687, CAST(N'2017-01-29' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (55, N'SOPORTE LCD / LED DE 23 '''' A 50 ''''', CAST(350.00 AS Decimal(18, 2)), N'0779814176652', 10, 519, CAST(N'2016-12-25' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (56, N'SOPORTE LCD / LED DE 26" A 47" TANGWOOD', CAST(350.00 AS Decimal(18, 2)), N'0779814176442', 10, 81, CAST(N'2017-01-28' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (57, N'SOPORTE LCD / LED TGW DE 17 '''' A 37 ''''', CAST(199.00 AS Decimal(18, 2)), N'0779814176648', 10, 164, CAST(N'2017-01-17' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (58, N'SOPORTE LCD 10" TAGWOOD', CAST(375.00 AS Decimal(18, 2)), N'0779814176490', 10, 217, CAST(N'2017-01-31' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (59, N'SOPORTE LCD 32" NAKAN', CAST(199.00 AS Decimal(18, 2)), N'0779803504550', 10, 873, CAST(N'2017-01-01' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (60, N'SOPORTE LCD 32" ONE FOR ALL', CAST(259.00 AS Decimal(18, 2)), N'0871618404213', 10, 585, CAST(N'2017-01-30' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (61, N'SOPORTE LCD 40" ONE FOR ALL', CAST(519.00 AS Decimal(18, 2)), N'0871618404215', 10, 809, CAST(N'2017-01-22' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (62, N'SOPORTE LCD/LED 23 A 46"', CAST(399.00 AS Decimal(18, 2)), N'0779814176617', 10, 470, CAST(N'2017-01-21' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (68, N'SOPORTE GPS', CAST(119.00 AS Decimal(18, 2)), N'0779814176084', 8, 524, CAST(N'2017-01-14' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (69, N'SOPORTE GPS NEGRO MOTO 3,5" - 5,5"', CAST(259.00 AS Decimal(18, 2)), N'0779808004535', 8, 800, CAST(N'2017-02-05' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (70, N'GPS GARMIN NUVI 2595', CAST(2899.00 AS Decimal(18, 2)), N'0075375999226', 8, 745, CAST(N'2017-02-07' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (71, N'GPS GARMIN NUVI 52', CAST(2149.00 AS Decimal(18, 2)), N'0075375999808', 8, 274, CAST(N'2016-12-22' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (72, N'GPS X VIEW VENTURA TV 7"', CAST(1849.00 AS Decimal(18, 2)), N'0779804220262', 8, 150, CAST(N'2016-12-30' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (73, N'GPS XVIEW VENTURA TV', CAST(1509.00 AS Decimal(18, 2)), N'0779804220220', 8, 183, CAST(N'2017-01-05' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (74, N'MOUSE HP 2.4G SILVER WIRELESS OPT CAN/EN', CAST(199.00 AS Decimal(18, 2)), N'0088496276058', 9, 40, CAST(N'2017-02-03' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (75, N'PENDRIVE KINGSTONE DT101G2 8GB', CAST(129.00 AS Decimal(18, 2)), N'0074061716983', 9, 537, CAST(N'2016-12-21' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (76, N'PENDRIVE SANDISK BLADE 4GB', CAST(129.00 AS Decimal(18, 2)), N'0061965900041', 9, 340, CAST(N'2017-02-02' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (77, N'PENDRIVE SANDISK CRUZAR ORBIT 8GB', CAST(159.00 AS Decimal(18, 2)), N'0061965909040', 9, 696, CAST(N'2017-02-07' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (78, N'PENDRIVE SANDISK POP BLACK 8GB', CAST(159.00 AS Decimal(18, 2)), N'0061965908448', 9, 431, CAST(N'2017-01-08' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (79, N'PENDRIVE SANDISK POP PAIN 8GB', CAST(159.00 AS Decimal(18, 2)), N'0061965908156', 9, 521, CAST(N'2017-02-01' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (80, N'CARTUCHO EPSON 732 CYAN', CAST(10290.00 AS Decimal(18, 2)), N'0001034385887', 9, 234, CAST(N'2017-01-26' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (81, N'CARTUCHO EPSON T133120-AL MAGENTA', CAST(9690.00 AS Decimal(18, 2)), N'0001034387695', 9, 374, CAST(N'2016-12-26' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (82, N'CARTUCHO EPSON T133120-AL NEGRO', CAST(8479.00 AS Decimal(18, 2)), N'0001034387692', 9, 836, CAST(N'2017-01-25' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (83, N'CARTUCHO EPSON T133420-AL AMARILLO', CAST(9690.00 AS Decimal(18, 2)), N'0001034387696', 9, 796, CAST(N'2016-12-28' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (84, N'CARTUCHO HP 122 NEGRO', CAST(149.00 AS Decimal(18, 2)), N'0088496298354', 9, 373, CAST(N'2017-02-05' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (85, N'CARTUCHO HP 22 COLOR', CAST(299.00 AS Decimal(18, 2)), N'0082916090222', 9, 199, CAST(N'2017-01-01' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (86, N'CARTUCHO HP 60 COLOR', CAST(289.00 AS Decimal(18, 2)), N'0088358598319', 9, 801, CAST(N'2017-01-31' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (87, N'CARTUCHO HP 60 NEGRO', CAST(199.00 AS Decimal(18, 2)), N'0088358598317', 9, 655, CAST(N'2017-01-08' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (88, N'PC ALL IN ONE 120-1156LA + TECLADO INAL + MOUSE', CAST(5499.00 AS Decimal(18, 2)), N'0088611278012', 9, 331, CAST(N'2017-01-19' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (90, N'IMPRESORA MULTIFUNCION EPSON L355', CAST(3999.00 AS Decimal(18, 2)), N'0001034390469', 9, 293, CAST(N'2017-01-01' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (91, N'MULTIFUNCION EPSON L210 + SISTEMA CONTINUO', CAST(3399.00 AS Decimal(18, 2)), N'0001034390433', 9, 689, CAST(N'2017-01-09' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (92, N'MULTIFUNCION EPSON XP211', CAST(1199.00 AS Decimal(18, 2)), N'0001034390754', 9, 693, CAST(N'2017-01-08' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (93, N'MULTIFUNCION EPSON XP401', CAST(1799.00 AS Decimal(18, 2)), N'0001034390348', 9, 363, CAST(N'2017-01-17' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (94, N'NOTEBOOK BGH C-530 3D', CAST(4999.00 AS Decimal(18, 2)), N'0779816664067', 9, 401, CAST(N'2017-01-30' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (95, N'NOTEBOOK BGH C-550', CAST(5799.00 AS Decimal(18, 2)), N'0779816664065', 9, 230, CAST(N'2017-01-04' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (96, N'NOTEBOOK BGH C-565', CAST(6299.00 AS Decimal(18, 2)), N'0779816664069', 9, 876, CAST(N'2017-02-06' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (97, N'NOTEBOOK BGH C-570', CAST(7299.00 AS Decimal(18, 2)), N'0779816664070', 9, 929, CAST(N'2017-01-17' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (98, N'NOTEBOOK BGH QL 300 MINI', CAST(3699.00 AS Decimal(18, 2)), N'0779816664101', 9, 176, CAST(N'2017-01-28' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (99, N'NOTEBOOK DELL INSPIRON 14 3421 I14I32_45', CAST(6599.00 AS Decimal(18, 2)), N'0789948950198', 9, 758, CAST(N'2016-12-31' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (100, N'NOTEBOOK DELL INSPIRON 14 3421 I14V997_4', CAST(5999.00 AS Decimal(18, 2)), N'0779801657005', 9, 666, CAST(N'2016-12-20' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (101, N'NOTEBOOK LENOVO G485 C-70', CAST(4399.00 AS Decimal(18, 2)), N'0088761972842', 9, 115, CAST(N'2017-01-21' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (102, N'NOTEBOOK NOBLEX CEVEN GFAST', CAST(4499.00 AS Decimal(18, 2)), N'0779808041201', 9, 853, CAST(N'2017-02-07' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (103, N'NOTEBOOK POSITIVO BGH F-810N NEGRA', CAST(4999.00 AS Decimal(18, 2)), N'0779816664059', 9, 48, CAST(N'2017-01-21' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (104, N'NOTEBOOK SAMSUNG NP300E4C', CAST(6999.00 AS Decimal(18, 2)), N'0880608528173', 9, 272, CAST(N'2017-01-08' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (105, N'NOTEBOOK SAMSUNG NP300E5A AD4AR', CAST(4799.00 AS Decimal(18, 2)), N'0880608500428', 9, 194, CAST(N'2017-01-18' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (106, N'ULTRABOOK ACER S3-391-6867', CAST(9793.00 AS Decimal(18, 2)), N'0471219655495', 9, 974, CAST(N'2017-01-23' AS Date), 1)
GO
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (107, N'ADAPTADOR PCI WIFI TL-WN751ND', CAST(259.00 AS Decimal(18, 2)), N'0693536405056', 9, 171, CAST(N'2017-01-15' AS Date), 0)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (110, N'ANTENA TP-LINK TL-ANT2408C', CAST(249.00 AS Decimal(18, 2)), N'0693536405216', 9, 689, CAST(N'2016-12-26' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (111, N'MINI ADAPATADOR USB TP LINK WN723N', CAST(185.00 AS Decimal(18, 2)), N'0693536405055', 9, 382, CAST(N'2016-12-31' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (112, N'ROUTER MR3420 3G TP-LINK', CAST(649.00 AS Decimal(18, 2)), N'0693536405149', 9, 143, CAST(N'2016-12-21' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (113, N'ROUTER PORTATIL TP LINK TL-MR3020', CAST(499.00 AS Decimal(18, 2)), N'0693536405170', 9, 594, CAST(N'2017-02-01' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (114, N'ROUTER TL-WR941ND TP LINK', CAST(759.00 AS Decimal(18, 2)), N'0693536405127', 9, 646, CAST(N'2017-02-06' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (115, N'ROUTER TP-LINK TL-WR720N', CAST(309.00 AS Decimal(18, 2)), N'0693536405198', 9, 867, CAST(N'2017-01-01' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (116, N'ROUTER WR740 TP-LINK', CAST(389.00 AS Decimal(18, 2)), N'0693536405133', 9, 925, CAST(N'2017-01-28' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (117, N'ROUTER WR841 TP-LINK', CAST(469.00 AS Decimal(18, 2)), N'0693536405124', 9, 624, CAST(N'2017-01-29' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (118, N'TABLET MAGNUM TECH 7"', CAST(2599.00 AS Decimal(18, 2)), N'0779813546539', 9, 344, CAST(N'2016-12-26' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (119, N'TABLET 10" MAGNUM TECH 8GB 1GBM', CAST(3799.00 AS Decimal(18, 2)), N'0779813546540', 9, 751, CAST(N'2017-01-24' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (120, N'TABLET 10" NOBLEX NB1012', CAST(3549.00 AS Decimal(18, 2)), N'0779696292015', 9, 319, CAST(N'2017-01-13' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (121, N'TABLET ALCATEL AB10', CAST(1799.00 AS Decimal(18, 2)), N'0695508989953', 9, 939, CAST(N'2017-02-01' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (122, N'TABLET EUROCASE ARS 708', CAST(1099.00 AS Decimal(18, 2)), N'0779813546928', 9, 534, CAST(N'2017-01-26' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (123, N'TABLET FUNTAB PRO', CAST(1699.00 AS Decimal(18, 2)), N'0081770701101', 9, 869, CAST(N'2017-01-23' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (124, N'TABLET IDEAPAD LENOVO A1000L', CAST(2799.00 AS Decimal(18, 2)), N'0088794260611', 9, 597, CAST(N'2017-01-05' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (125, N'TABLET LENOVO IDEAPAD A1000 7"', CAST(2299.00 AS Decimal(18, 2)), N'0088777046041', 9, 510, CAST(N'2017-02-04' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (126, N'TABLET MAGNUM MG-701', CAST(1499.00 AS Decimal(18, 2)), N'0779813546946', 9, 645, CAST(N'2017-02-05' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (127, N'TABLET NOBLEX-8013 8''''', CAST(2149.00 AS Decimal(18, 2)), N'0779696291801', 9, 850, CAST(N'2017-01-17' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (130, N'TABLET OLIPAD SMART 7" 3G', CAST(1499.00 AS Decimal(18, 2)), N'0802033432056', 9, 489, CAST(N'2017-02-07' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (131, N'TABLET PC 7001 TITAN', CAST(999.00 AS Decimal(18, 2)), N'0076113310158', 9, 850, CAST(N'2016-12-24' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (132, N'TABLET PC BOX T700U 7" DUAL CORE', CAST(1999.00 AS Decimal(18, 2)), N'0779815876409', 9, 769, CAST(N'2017-02-06' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (133, N'TABLET PC FIRSTAR MID070A 8650', CAST(799.00 AS Decimal(18, 2)), N'0779815467080', 9, 9, CAST(N'2017-01-23' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (134, N'TABLET PCBOX MOD T900', CAST(2799.00 AS Decimal(18, 2)), N'0779815876410', 9, 501, CAST(N'2017-01-25' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (135, N'TABLET POLAROID MID1000 10', CAST(4299.00 AS Decimal(18, 2)), N'0358417655560', 9, 151, CAST(N'2016-12-23' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (136, N'TABLET SYNKOM 7"', CAST(2499.00 AS Decimal(18, 2)), N'0779816920041', 9, 695, CAST(N'2016-12-23' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (137, N'TABLET XVIEW ALPHA2 8GB', CAST(1899.00 AS Decimal(18, 2)), N'0779804220264', 9, 565, CAST(N'2017-02-05' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (138, N'TABLET XVIEW PROTON', CAST(1699.00 AS Decimal(18, 2)), N'0779804220247', 9, 3, CAST(N'2016-12-28' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (139, N'AIRE ACONDICIONADO DAEWOO 3200FC DWT23200FC', CAST(5898.00 AS Decimal(18, 2)), N'0779816944014', 7, 668, CAST(N'2018-01-04' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (140, N'AIRE ACONDICIONADO DURABRAND 3500FC DUS35WCL4', CAST(5499.00 AS Decimal(18, 2)), N'0779688543933', 7, 945, CAST(N'2017-01-20' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (141, N'AIRE ACONDICIONADO DURABRAND 4500FC DUS53WCL4', CAST(7499.00 AS Decimal(18, 2)), N'0779688543937', 7, 962, CAST(N'2016-12-29' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (142, N'AIRE ACONDICIONADO KELVINATOR 2500WFC COD1056', CAST(4499.00 AS Decimal(18, 2)), N'0779694101056', 7, 670, CAST(N'2017-01-03' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (143, N'AIRE ACONDICIONADO LG 3000 FC H126TNW0', CAST(7499.00 AS Decimal(18, 2)), N'0779808338858', 7, 441, CAST(N'2017-01-09' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (144, N'AIRE ACONDICIONADO LG 4500 FC H1865NW0', CAST(10399.00 AS Decimal(18, 2)), N'0779808338859', 7, 971, CAST(N'2016-12-23' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (145, N'AIRE ACONDICIONADO LG 5500 FC H2465NW0', CAST(12699.00 AS Decimal(18, 2)), N'0779808338860', 7, 648, CAST(N'2017-01-15' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (146, N'AIRE ACONDICIONADO LG ARTCOOL 2300FC H096EFT0', CAST(7999.00 AS Decimal(18, 2)), N'0779808338853', 7, 659, CAST(N'2017-01-01' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (147, N'AIRE ACONDICIONADO LG ARTCOOL 4500FC H1868FT0', CAST(12899.00 AS Decimal(18, 2)), N'0779808338855', 7, 712, CAST(N'2016-12-25' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (148, N'AIRE ACONDICIONADO PHILCO 3200W FC PHS32H13X', CAST(6199.00 AS Decimal(18, 2)), N'0779696244974', 7, 588, CAST(N'2017-01-09' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (149, N'AIRE ACONDICIONADO PHILCO 5000W FC PHS50H13X', CAST(9099.00 AS Decimal(18, 2)), N'0779696242975', 7, 275, CAST(N'2016-12-22' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (150, N'AIRE ACONDICIONADO PORTATIL DURABRAND 2500FS LGACD01', CAST(4999.00 AS Decimal(18, 2)), N'0073621119267', 7, 995, CAST(N'2017-01-26' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (151, N'AIRE ACONDICIONADO SAMSUNG 3000FC AR12FQFTAUR', CAST(7949.00 AS Decimal(18, 2)), N'0880608575497', 7, 34, CAST(N'2017-01-03' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (152, N'AIRE ACONDICIONADO SANYO 2600W FC KC913HSAN', CAST(6099.00 AS Decimal(18, 2)), N'0779696244956', 7, 372, CAST(N'2017-01-23' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (153, N'AIRE ACONDICIONADO SANYO 3200W FC KC1213HSAN', CAST(6899.00 AS Decimal(18, 2)), N'0779696242957', 7, 260, CAST(N'2017-02-02' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (154, N'AIRE ACONDICIONADO SURREYPRIA 2250FC 553EPQ0913F', CAST(6929.00 AS Decimal(18, 2)), N'0779708708630', 7, 38, CAST(N'2016-12-30' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (155, N'AIRE ACONDICIONADO SURREYPRIA 3000FC 553EPQ1213F', CAST(7949.00 AS Decimal(18, 2)), N'0779708708631', 7, 180, CAST(N'2017-01-04' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (156, N'AIRE ACONDICIONADO SURREYPRIA 4500FC 553EPQ1813F', CAST(11849.00 AS Decimal(18, 2)), N'0779708708632', 7, 232, CAST(N'2017-01-07' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (157, N'AIRE ACONDICIONADO SURREYPRIA 5500FC 553EPQ2213F', CAST(14329.00 AS Decimal(18, 2)), N'0779708708633', 7, 909, CAST(N'2017-01-10' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (158, N'CALEFACTOR SIN SALIDA 4000 KCAL VOLCAN', CAST(1159.00 AS Decimal(18, 2)), N'0779703781219', 7, 598, CAST(N'2016-12-23' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (159, N'CALEFACTOR SIN SALIDA ORBIS 4200 KCAL', CAST(1469.00 AS Decimal(18, 2)), N'0779703781123', 7, 504, CAST(N'2017-01-11' AS Date), 0)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (160, N'ESTUFA ORBIS TIRO BALANCEADO 5000 K', CAST(2019.00 AS Decimal(18, 2)), N'0779703781129', 7, 600, CAST(N'2017-01-17' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (161, N'ESTUFA VOLCAN TIRO BALANCEADO 2000 KCAL 42312V', CAST(1439.00 AS Decimal(18, 2)), N'0779703781220', 7, 602, CAST(N'2016-12-28' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (162, N'ESTUFA VOLCAN TIRO BALANCEADO NEGRO 3800 43712V', CAST(1679.00 AS Decimal(18, 2)), N'0779703781221', 7, 650, CAST(N'2017-02-04' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (163, N'TIRO BALANCEADO 3500 KCAL EMEGE', CAST(1605.00 AS Decimal(18, 2)), N'0779135400180', 7, 474, CAST(N'2017-01-29' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (164, N'CALEFACTOR ELECTRICO CLEVER VIDRIO H1107', CAST(1950.00 AS Decimal(18, 2)), N'0779815957117', 7, 459, CAST(N'2016-12-29' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (165, N'CALEFACTOR ELECTRICO CONVECCION CON-1800', CAST(1599.00 AS Decimal(18, 2)), N'0779814958212', 7, 10, CAST(N'2017-01-13' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (166, N'CALEFACTOR ELECTRICO CONVECCION CON-2000N', CAST(790.00 AS Decimal(18, 2)), N'0779815957180', 7, 112, CAST(N'2017-01-11' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (167, N'CALEFACTOR ELECTRICO CONVECCION CON-2000R', CAST(790.00 AS Decimal(18, 2)), N'0779815957181', 7, 141, CAST(N'2017-01-26' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (168, N'CALEFACTOR LILIANA INFRARROJO CI062', CAST(345.00 AS Decimal(18, 2)), N'0779386200687', 7, 516, CAST(N'2016-12-27' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (169, N'CALEFACTOR PANEL 500 WATTS', CAST(769.00 AS Decimal(18, 2)), N'0779813482002', 7, 804, CAST(N'2017-01-03' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (170, N'CALOVENTOR 2000 W AXEL AX-CA100', CAST(249.00 AS Decimal(18, 2)), N'0779811896139', 7, 780, CAST(N'2017-01-10' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (171, N'CALOVENTOR DE PARED 2000 W KENBROWN', CAST(839.00 AS Decimal(18, 2)), N'0779811320136', 7, 737, CAST(N'2016-12-28' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (172, N'CALOVENTOR DE PARED PROTALIA CP200A', CAST(799.00 AS Decimal(18, 2)), N'0779811559131', 7, 833, CAST(N'2017-01-30' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (173, N'CALOVENTOR ELECTRICO BLANCO 1500W LE1500B', CAST(599.00 AS Decimal(18, 2)), N'0779815957245', 7, 492, CAST(N'2017-01-04' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (174, N'CALOVENTOR ELECTRICO LE1500ROJO', CAST(599.00 AS Decimal(18, 2)), N'0779815957247', 7, 437, CAST(N'2017-01-29' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (175, N'CALOVENTOR ELECTRICO NEGRO 1500W LE1500N', CAST(599.00 AS Decimal(18, 2)), N'0779815957246', 7, 875, CAST(N'2017-01-09' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (176, N'CALOVENTOR ELECTROLUX SPLIT CONTROL REMOTO', CAST(999.00 AS Decimal(18, 2)), N'0779386200613', 7, 675, CAST(N'2016-12-20' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (177, N'CALOVENTOR KEN BROWN 2000 W', CAST(319.00 AS Decimal(18, 2)), N'0779811320075', 7, 76, CAST(N'2017-01-23' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (178, N'CALOVENTOR RESISTENCIA CERAMICA', CAST(319.00 AS Decimal(18, 2)), N'0557306319076', 7, 243, CAST(N'2017-01-08' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (179, N'CIRCULADOR DE AIRE FRIO CALOR DURABRAND', CAST(1049.00 AS Decimal(18, 2)), N'0073621119287', 7, 121, CAST(N'2017-01-30' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (180, N'CONVECTOR AXEL 2000 W AX-COT100', CAST(689.00 AS Decimal(18, 2)), N'0779811896141', 7, 357, CAST(N'2016-12-24' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (181, N'CONVECTOR AXEL 2000 W CON TURBO AX-COT', CAST(609.00 AS Decimal(18, 2)), N'0779811896131', 7, 246, CAST(N'2017-01-16' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (182, N'CONVECTOR CLEVER CLEVERBLANCO CON2000B', CAST(790.00 AS Decimal(18, 2)), N'0779815957179', 7, 229, CAST(N'2017-01-09' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (183, N'CONVECTOR TELEFUNKEN 2000 WATT C1009', CAST(479.00 AS Decimal(18, 2)), N'0779724533114', 7, 642, CAST(N'2016-12-29' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (184, N'ESTUFA ELECTROLUX HALOGENAS HAL18G', CAST(549.00 AS Decimal(18, 2)), N'0779386200254', 7, 295, CAST(N'2017-01-15' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (185, N'ESTUFA ELECTRICA KEN BROWN 2 VELAS 800 KB 22', CAST(245.00 AS Decimal(18, 2)), N'0779811320288', 7, 598, CAST(N'2016-12-24' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (186, N'ESTUFA HALOGENA 3 VELAS KEN BROWN', CAST(409.00 AS Decimal(18, 2)), N'0779811320134', 7, 580, CAST(N'2016-12-24' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (187, N'ESTUFA HALOGENA 4 VELAS KEN BROWN', CAST(449.00 AS Decimal(18, 2)), N'0779811320135', 7, 741, CAST(N'2017-01-28' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (188, N'ESTUFA HALOGENA ELECTROLUX 1600W SIN OSCILACION HAL18A', CAST(499.00 AS Decimal(18, 2)), N'0779386200253', 7, 632, CAST(N'2016-12-23' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (189, N'ESTUFA HALOGENA MAGIC 1200 W C1007', CAST(189.00 AS Decimal(18, 2)), N'0779724533112', 7, 518, CAST(N'2016-12-26' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (190, N'PANEL 1000W ATMA', CAST(99999.00 AS Decimal(18, 2)), N'0779696280631', 7, 951, CAST(N'2017-01-17' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (191, N'PANEL 2000 W NEGRO ENERGY SAVE', CAST(1499.00 AS Decimal(18, 2)), N'0779814951036', 7, 647, CAST(N'2016-12-20' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (192, N'PANEL 500 W ECOSOL', CAST(1119.00 AS Decimal(18, 2)), N'0779813482029', 7, 805, CAST(N'2017-01-18' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (193, N'PANEL 900W ECOSOL 1-502', CAST(1869.00 AS Decimal(18, 2)), N'0779813482031', 7, 726, CAST(N'2017-02-01' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (194, N'PANEL MICA ELECTROLUX RMIC15', CAST(999.00 AS Decimal(18, 2)), N'0779386200256', 7, 331, CAST(N'2016-12-26' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (195, N'PANEL PIETRA 500 W PEISA', CAST(699.00 AS Decimal(18, 2)), N'0779808116284', 7, 171, CAST(N'2017-01-27' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (196, N'RADIADOR DE MICA ELECTROLUX 1000W RALU01', CAST(699.00 AS Decimal(18, 2)), N'0779817317015', 7, 987, CAST(N'2017-01-24' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (197, N'TURBO CALENTADOR 2000W TCAL2000', CAST(590.00 AS Decimal(18, 2)), N'0779815957248', 7, 539, CAST(N'2017-01-03' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (198, N'VENTILADOR DE PIE DURABRAND 18" VP21', CAST(122.00 AS Decimal(18, 2)), N'0779797170650', 7, 318, CAST(N'2017-01-31' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (199, N'CAMARA DIGITAL C1433 SLVER GE', CAST(899.00 AS Decimal(18, 2)), N'0084695100018', 6, 528, CAST(N'2017-02-02' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (500, N'LIMPIADOR CD SV 8336 ONE FOR ALL', CAST(55.00 AS Decimal(18, 2)), N'0871618404342', 1, 508, CAST(N'2016-12-27' AS Date), 1)
INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (501, N'LIMPIADOR LCD SV 8410 ONE FOR ALL', CAST(102.00 AS Decimal(18, 2)), N'0871618404333', 1, 186, CAST(N'2017-02-02' AS Date), 1)
SET IDENTITY_INSERT [dbo].[Articulos] OFF
SET IDENTITY_INSERT [dbo].[ArticulosFamilias] ON 

INSERT [dbo].[ArticulosFamilias] ([IdArticuloFamilia], [Nombre]) VALUES (1, N'ACCESORIOS')
INSERT [dbo].[ArticulosFamilias] ([IdArticuloFamilia], [Nombre]) VALUES (2, N'AUDIO')
INSERT [dbo].[ArticulosFamilias] ([IdArticuloFamilia], [Nombre]) VALUES (3, N'CELULARES')
INSERT [dbo].[ArticulosFamilias] ([IdArticuloFamilia], [Nombre]) VALUES (4, N'CUIDADO PERSONAL')
INSERT [dbo].[ArticulosFamilias] ([IdArticuloFamilia], [Nombre]) VALUES (5, N'DVD')
INSERT [dbo].[ArticulosFamilias] ([IdArticuloFamilia], [Nombre]) VALUES (6, N'FOTOGRAFIA')
INSERT [dbo].[ArticulosFamilias] ([IdArticuloFamilia], [Nombre]) VALUES (7, N'FRIO-CALOR')
INSERT [dbo].[ArticulosFamilias] ([IdArticuloFamilia], [Nombre]) VALUES (8, N'GPS')
INSERT [dbo].[ArticulosFamilias] ([IdArticuloFamilia], [Nombre]) VALUES (9, N'INFORMATICA')
INSERT [dbo].[ArticulosFamilias] ([IdArticuloFamilia], [Nombre]) VALUES (10, N'LED - LCD')
SET IDENTITY_INSERT [dbo].[ArticulosFamilias] OFF
SET IDENTITY_INSERT [dbo].[AuditoriasABM] ON 

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (3, CAST(N'2001-11-15T00:00:00.000' AS DateTime), N'CONTACTOS', 1, N'M', NULL, NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (4, CAST(N'2012-04-20T00:00:00.000' AS DateTime), N'CLIENTES', 775, N'A', NULL, NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (5, CAST(N'2012-04-20T11:03:04.737' AS DateTime), N'CLIENTES', 782, N'A', NULL, NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (6, CAST(N'2012-04-24T11:44:18.017' AS DateTime), N'CLIENTES', 788, N'A', NULL, NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (7, CAST(N'2012-04-24T11:46:53.323' AS DateTime), N'CLIENTES', 789, N'A', NULL, NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (8, CAST(N'2012-04-24T12:00:54.887' AS DateTime), N'CLIENTES', 792, N'A', NULL, NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (9, CAST(N'2012-04-24T12:03:51.860' AS DateTime), N'CLIENTES', 794, N'A', NULL, NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (10, CAST(N'2012-04-24T12:07:32.020' AS DateTime), N'CLIENTES', 796, N'A', NULL, NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (11, CAST(N'2012-04-24T12:10:29.490' AS DateTime), N'CLIENTES', 802, N'A', NULL, NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (12, CAST(N'2012-04-24T12:13:06.673' AS DateTime), N'', 803, N'A', NULL, NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (13, CAST(N'2012-04-24T12:16:11.350' AS DateTime), N'CLIENTES', 807, N'A', NULL, NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (20, CAST(N'2013-04-09T11:59:22.207' AS DateTime), N'CLIENTES', 898, N'B', N'facu', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (21, CAST(N'2013-04-09T12:00:02.600' AS DateTime), N'CLIENTES', 896, N'B', N'facu', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (22, CAST(N'2013-04-09T12:01:55.800' AS DateTime), N'CLIENTES', 912, N'B', N'facu', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (23, CAST(N'2013-04-09T12:01:58.333' AS DateTime), N'CLIENTES', 913, N'B', N'facu', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (24, CAST(N'2013-04-09T12:04:10.020' AS DateTime), N'CLIENTES', 914, N'B', N'facu', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (26, CAST(N'2003-03-03T00:00:00.000' AS DateTime), N'CLIENTES', 900, N'D', NULL, NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (37, CAST(N'2013-04-09T12:16:37.003' AS DateTime), N'CLIENTES', 911, N'D', N'Marcelo', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (38, CAST(N'2013-04-09T12:16:38.190' AS DateTime), N'CLIENTES', 911, N'D', N'Marcelo', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (39, CAST(N'2013-04-09T12:18:21.090' AS DateTime), N'CLIENTES', 915, N'D', N'Marcelo', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (40, CAST(N'2013-04-09T12:32:51.017' AS DateTime), N'CLIENTES', 925, N'B', N'facu', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (41, CAST(N'2013-04-09T12:38:38.717' AS DateTime), N'CLIENTES', 936, N'D', N'Marcelo', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (42, CAST(N'2013-04-09T12:39:54.863' AS DateTime), N'CLIENTES', 938, N'D', N'Marcelo', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (43, CAST(N'2013-04-09T12:41:42.393' AS DateTime), N'CLIENTES', 942, N'D', N'Marcelo', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (44, CAST(N'2013-04-09T12:42:45.700' AS DateTime), N'CLIENTES', 939, N'D', N'Marcelo', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (45, CAST(N'2013-04-09T12:43:57.207' AS DateTime), N'CLIENTES', 947, N'D', N'Marcelo', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (46, CAST(N'2013-04-09T12:48:03.337' AS DateTime), N'CLIENTES', 950, N'D', N'Marcelo950', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (47, CAST(N'2013-04-09T12:48:37.997' AS DateTime), N'CLIENTES', 946, N'B', N'sol', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (48, CAST(N'2013-04-09T12:50:41.410' AS DateTime), N'CLIENTES', 956, N'B', N'sol', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (49, CAST(N'2013-04-12T08:24:03.423' AS DateTime), N'CLIENTES', 959, N'D', N'Marcelo959', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (64, CAST(N'2013-04-12T08:37:03.747' AS DateTime), N'CLIENTES', 966, N'B', N'asd', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (80, CAST(N'2013-04-12T08:55:34.650' AS DateTime), N'CLIENTES', 978, N'B', N'Gonzalo', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (81, CAST(N'2013-04-16T09:55:08.457' AS DateTime), N'CLIENTES', 979, N'D', N'Marcelo979', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (82, CAST(N'2013-04-16T10:11:30.107' AS DateTime), N'CLIENTES', 989, N'D', N'Marcelo989', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (83, CAST(N'2013-04-16T10:19:13.650' AS DateTime), N'CLIENTES', 1005, N'A', N'edt716', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (84, CAST(N'2013-04-16T10:19:22.790' AS DateTime), N'CLIENTES', 1006, N'A', N'edt716', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (85, CAST(N'2013-04-16T10:19:43.980' AS DateTime), N'CLIENTES', 995, N'D', N'Marcelo995', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (86, CAST(N'2013-04-16T10:19:47.677' AS DateTime), N'CLIENTES', 991, N'D', N'pablo991', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (87, CAST(N'2013-04-16T10:19:52.417' AS DateTime), N'CLIENTES', 992, N'D', N'Marcelo992', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (88, CAST(N'2013-04-16T10:19:53.617' AS DateTime), N'CLIENTES', 1005, N'B', N'Gonzalo', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (89, CAST(N'2013-04-16T10:20:08.353' AS DateTime), N'CLIENTES', 997, N'D', N'Marcelo997', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (90, CAST(N'2013-04-16T10:20:12.100' AS DateTime), N'CLIENTES', 994, N'D', N'Marcelo994', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (91, CAST(N'2013-04-16T10:20:13.940' AS DateTime), N'CLIENTES', 998, N'D', N'pablo998', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (92, CAST(N'2013-04-16T10:20:18.493' AS DateTime), N'CLIENTES', 1006, N'D', N'Marcelo1006', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (93, CAST(N'2013-04-16T10:20:19.320' AS DateTime), N'CLIENTES', 1004, N'D', N'Marcelo1004', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (94, CAST(N'2013-04-16T10:20:20.320' AS DateTime), N'CLIENTES', 1000, N'D', N'Marcelo1000', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (95, CAST(N'2013-04-16T10:20:23.003' AS DateTime), N'CLIENTES', 1001, N'D', N'pablo1001', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (96, CAST(N'2013-04-16T10:20:24.063' AS DateTime), N'CLIENTES', 1003, N'D', N'pablo1003', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (97, CAST(N'2013-04-16T10:20:25.227' AS DateTime), N'CLIENTES', 996, N'D', N'Marcelo996', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (98, CAST(N'2013-04-16T10:20:26.253' AS DateTime), N'CLIENTES', 1007, N'D', N'pablo1007', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (99, CAST(N'2013-04-16T10:20:27.313' AS DateTime), N'CLIENTES', 1002, N'D', N'pablo1002', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (100, CAST(N'2013-04-16T10:20:29.110' AS DateTime), N'CLIENTES', 999, N'D', N'pablo999', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (101, CAST(N'2013-04-16T10:21:21.560' AS DateTime), N'CLIENTES', 1008, N'D', N'Marcelo1008', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (102, CAST(N'2013-04-16T10:21:45.587' AS DateTime), N'CLIENTES', 1012, N'A', N'edt716', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (103, CAST(N'2013-04-16T10:22:06.917' AS DateTime), N'CLIENTES', 1014, N'A', N'sebas', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (104, CAST(N'2013-04-16T10:22:55.247' AS DateTime), N'CLIENTES', 1018, N'D', N'Alexis1018', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (105, CAST(N'2013-04-16T10:23:24.353' AS DateTime), N'CLIENTES', 1012, N'D', N'Alexis1012', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (106, CAST(N'2013-04-16T10:26:42.753' AS DateTime), N'CLIENTES', 1019, N'D', N'Alexis1019', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (107, CAST(N'2013-04-16T10:26:44.830' AS DateTime), N'CLIENTES', 1024, N'D', N'Alexis1024', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (108, CAST(N'2013-04-16T10:26:46.047' AS DateTime), N'CLIENTES', 1021, N'D', N'Alexis1021', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (109, CAST(N'2013-04-16T10:26:47.157' AS DateTime), N'CLIENTES', 1009, N'D', N'Alexis1009', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (110, CAST(N'2013-04-16T10:26:48.030' AS DateTime), N'CLIENTES', 1014, N'D', N'Alexis1014', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (111, CAST(N'2013-04-16T10:29:36.590' AS DateTime), N'CLIENTES', 1066, N'A', N'edt716', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (112, CAST(N'2013-04-16T10:35:43.563' AS DateTime), N'CLIENTES', 1087, N'A', N'edt716', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (113, CAST(N'2013-04-16T10:36:25.723' AS DateTime), N'CLIENTES', 1086, N'B', N'Gonzalo', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (114, CAST(N'2013-04-16T10:37:31.337' AS DateTime), N'CLIENTES', 1092, N'B', N'Gonzalo', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (115, CAST(N'2013-04-16T10:38:16.047' AS DateTime), N'CLIENTES', 1102, N'A', N'Emi', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (116, CAST(N'2013-04-16T10:40:31.063' AS DateTime), N'CLIENTES', 1131, N'A', N'sebas', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (117, CAST(N'2013-04-16T10:42:13.783' AS DateTime), N'CLIENTES', 1102, N'B', N'Gonzalo', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (118, CAST(N'2013-04-16T10:42:14.007' AS DateTime), N'CLIENTES', 1089, N'D', N'Marcelo1089', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (119, CAST(N'2013-04-16T10:42:25.647' AS DateTime), N'CLIENTES', 1090, N'B', N'Gonzalo', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (120, CAST(N'2013-04-16T10:44:20.923' AS DateTime), N'CLIENTES', 1145, N'A', N'Emi', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (121, CAST(N'2013-04-16T10:46:03.457' AS DateTime), N'CLIENTES', 1150, N'A', N'edt716', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (122, CAST(N'2013-04-16T10:49:58.160' AS DateTime), N'CLIENTES', 1162, N'A', N'sebas', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (123, CAST(N'2013-04-16T10:52:42.477' AS DateTime), N'CLIENTES', 1167, N'A', N'Emi', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (124, CAST(N'2013-04-16T10:52:56.770' AS DateTime), N'CLIENTES', 1170, N'A', N'sebas', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (125, CAST(N'2013-04-16T10:53:17.240' AS DateTime), N'CLIENTES', 1167, N'B', N'sebas', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (126, CAST(N'2013-04-16T10:53:21.240' AS DateTime), N'CLIENTES', 1166, N'B', N'sebas', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (127, CAST(N'2013-04-16T10:53:25.117' AS DateTime), N'CLIENTES', 1138, N'B', N'sebas', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (128, CAST(N'2013-04-16T10:54:53.367' AS DateTime), N'CLIENTES', 1176, N'A', N'edt716', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (129, CAST(N'2013-04-16T10:59:03.620' AS DateTime), N'CLIENTES', 1192, N'A', N'Emi', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (130, CAST(N'2013-04-16T11:14:11.810' AS DateTime), N'CLIENTES', 1209, N'B', N'sebas', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (131, CAST(N'2013-04-16T11:14:14.530' AS DateTime), N'CLIENTES', 1233, N'B', N'sebas', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (132, CAST(N'2013-04-16T11:53:57.123' AS DateTime), N'CLIENTES', 1285, N'D', N'pablo1285', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (133, CAST(N'2013-04-16T11:55:36.457' AS DateTime), N'CLIENTES', 1287, N'D', N'pablo1287', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (134, CAST(N'2013-04-16T12:17:43.217' AS DateTime), N'CLIENTES', NULL, N'B', N'Santiago', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (135, CAST(N'2013-04-16T12:18:00.030' AS DateTime), N'CLIENTES', NULL, N'B', N'Santiago', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (136, CAST(N'2013-04-16T12:18:08.047' AS DateTime), N'CLIENTES', NULL, N'B', N'Santiago', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (137, CAST(N'2013-04-16T12:33:05.860' AS DateTime), N'CLIENTES', NULL, N'B', N'Santiago', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (138, CAST(N'2013-04-23T12:08:27.150' AS DateTime), N'CLIENTES', 1290, N'D', N'Marcelo1290', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (139, CAST(N'2013-04-23T12:13:43.993' AS DateTime), N'CLIENTES', 1283, N'D', N'Marcelo1283', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (140, CAST(N'2013-04-30T10:00:37.897' AS DateTime), N'CLIENTES', 1282, N'D', N'Marcelo1282', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (141, CAST(N'2013-04-30T10:06:59.473' AS DateTime), N'CLIENTES', 1295, N'D', N'Marcelo1295', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (142, CAST(N'2013-04-30T10:07:19.393' AS DateTime), N'CLIENTES', 1294, N'D', N'Marcelo1294', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (143, CAST(N'2013-04-30T10:45:09.640' AS DateTime), N'CLIENTES', 1298, N'D', N'Marcelo1298', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (144, CAST(N'2013-05-07T10:50:36.753' AS DateTime), N'CLIENTES', 1301, N'D', N'Marcelo1301', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (145, CAST(N'2013-05-07T10:52:53.837' AS DateTime), N'CLIENTES', 1303, N'D', N'Marcelo1303', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (146, CAST(N'2013-05-07T10:59:04.973' AS DateTime), N'CLIENTES', 1300, N'D', N'Marcelo1300', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (147, CAST(N'2013-05-07T10:59:32.843' AS DateTime), N'CLIENTES', 1311, N'D', N'Marcelo1311', NULL)
GO
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (148, CAST(N'2013-05-07T12:15:13.120' AS DateTime), N'CLIENTES', 1312, N'D', N'Marcelo1312', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (149, CAST(N'2013-05-07T12:32:21.040' AS DateTime), N'CLIENTES', 1318, N'D', N'Marcelo1318', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (150, CAST(N'2013-07-09T16:25:04.407' AS DateTime), N'CLIENTES', 1189, N'D', N'Marcelo1189', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (151, CAST(N'2013-07-09T17:10:23.987' AS DateTime), N'CLIENTES', 1314, N'D', N'Marcelo1314', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (152, CAST(N'2013-07-09T17:10:27.057' AS DateTime), N'CLIENTES', 1302, N'D', N'Marcelo1302', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (153, CAST(N'2013-07-09T17:35:54.337' AS DateTime), N'CLIENTES', 1316, N'D', N'Marcelo1316', NULL)
INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (154, CAST(N'2014-09-30T20:02:06.143' AS DateTime), N'CLIENTES', 9858, N'B', N'', NULL)
SET IDENTITY_INSERT [dbo].[AuditoriasABM] OFF
SET IDENTITY_INSERT [dbo].[Categorias] ON 

INSERT [dbo].[Categorias] ([IdCategoria], [Nombre]) VALUES (1, N'FAMILIA')
INSERT [dbo].[Categorias] ([IdCategoria], [Nombre]) VALUES (2, N'AMIGOS')
INSERT [dbo].[Categorias] ([IdCategoria], [Nombre]) VALUES (3, N'NEGOCIOS')
SET IDENTITY_INSERT [dbo].[Categorias] OFF
SET IDENTITY_INSERT [dbo].[Clientes] ON 

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (1, N'SANCHEZ, JUAN CARLOS', 20079758583, N'DNI', 7975858, CAST(N'1944-01-18T03:00:00.000' AS DateTime), N'M', N'S', N'SANCHEZ', 797, N'SIN ASIGNAR', 66, 4, 1, CAST(N'1951-01-16T03:00:00.000' AS DateTime), CAST(N'1955-01-15T03:00:00.000' AS DateTime), 1, 1, 0, CAST(123.45 AS Decimal(18, 2)), N'sanchezjuancarlos@hotmail.com', NULL, NULL, 1, N'Observacion de SANCHEZ, JUAN CARLOS')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (2, N'ROBLEDO, LUIS ARMANDO', 23763028397, N'DNI', 7630283, CAST(N'1949-01-06T00:00:00.000' AS DateTime), N'M', N'Z', N'ROBLEDO', 763, N'CORDOBA CAPITAL', 67, 1, NULL, CAST(N'1956-01-05T00:00:00.000' AS DateTime), CAST(N'1960-01-04T00:00:00.000' AS DateTime), 0, 1, 1, CAST(1188151.00 AS Decimal(18, 2)), N'robledoluisarmando@gmail.com', NULL, NULL, 1, N'Observacion de ROBLEDO, LUIS ARMANDO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (3, N'SOSA, SIMONA', 27500830852, N'DNI', 5008308, CAST(N'1945-01-01T00:00:00.000' AS DateTime), N'F', N'S', N'SOSA', 500, N'CORDOBA CAPITAL', 68, 2, NULL, CAST(N'1951-12-31T00:00:00.000' AS DateTime), CAST(N'1955-12-30T00:00:00.000' AS DateTime), 1, 0, 0, CAST(916694.00 AS Decimal(18, 2)), N'sosasimona@hotmail.com', NULL, NULL, 1, N'Observacion de SOSA, SIMONA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (4, N'BRITO, MIGUEL F', 20651529251, N'DNI', 6515292, CAST(N'1940-09-04T00:00:00.000' AS DateTime), N'M', N'S', N'BRITO', 651, N'CORDOBA CAPITAL', 69, 3, NULL, CAST(N'1947-09-03T00:00:00.000' AS DateTime), CAST(N'1951-09-02T00:00:00.000' AS DateTime), 0, 1, 0, CAST(516288.00 AS Decimal(18, 2)), N'britomiguelf@gmail.com', NULL, NULL, 1, N'Observacion de BRITO, MIGUEL F')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (5, N'GONZALEZ, RAMON ELADIO', 20800962090, N'DNI', 8009620, CAST(N'1950-06-10T00:00:00.000' AS DateTime), N'M', N'Z', N'NZALEZ', 800, N'CORDOBA CAPITAL', 70, 4, NULL, CAST(N'1957-06-08T00:00:00.000' AS DateTime), CAST(N'1961-06-07T00:00:00.000' AS DateTime), 1, 0, 0, CAST(416019.00 AS Decimal(18, 2)), N'gonzalezramoneladio@hotmail.com', NULL, NULL, 1, N'Observacion de NZALEZ, RAMON ELADIO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (6, N'ECHENIQUE, EFRAIN', 23800091196, N'DNI', 8000911, CAST(N'1948-06-18T00:00:00.000' AS DateTime), N'M', N'C', N'ECHENIQUE', 800, N'CORDOBA CAPITAL', 71, 5, NULL, CAST(N'1955-06-17T00:00:00.000' AS DateTime), CAST(N'1959-06-16T00:00:00.000' AS DateTime), 0, 0, 1, CAST(396668.00 AS Decimal(18, 2)), N'echeniqueefrain@gmail.com', NULL, NULL, 1, N'Observacion de ECHENIQUE, EFRAIN')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (7, N'VALDEZ, VICENTA', 27547866415, N'CF', 5478664, CAST(N'1946-10-27T00:00:00.000' AS DateTime), N'F', N'Z', N'VALDEZ', 547, N'CORDOBA CAPITAL', 72, 6, NULL, CAST(N'1953-10-25T00:00:00.000' AS DateTime), CAST(N'1957-10-24T00:00:00.000' AS DateTime), 1, 1, 0, CAST(393540.00 AS Decimal(18, 2)), N'valdezvicenta@hotmail.com', NULL, NULL, 1, N'Observacion de VALDEZ, VICENTA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (8, N'ALBORNOZ, SILVIA BEATRIZ', 24540989916, N'CF', 5409899, CAST(N'1946-08-15T00:00:00.000' AS DateTime), N'F', N'C', N'ALBORNOZ', 540, N'CORDOBA CAPITAL', 73, NULL, NULL, CAST(N'1953-08-13T00:00:00.000' AS DateTime), CAST(N'1957-08-12T00:00:00.000' AS DateTime), 0, 0, 1, CAST(306762.00 AS Decimal(18, 2)), N'albornozsilviabeatriz@gmail.com', NULL, NULL, 1, N'Observacion de ALBORNOZ, SILVIA BEATRIZ')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (9, N'SOSA, CLEOFO MERCEDES', 20639199868, N'CF', 6391998, CAST(N'1943-09-25T00:00:00.000' AS DateTime), N'F', N'Z', N'SOSA', 639, N'CORDOBA CAPITAL', 74, NULL, NULL, CAST(N'1950-09-23T00:00:00.000' AS DateTime), CAST(N'1954-09-22T00:00:00.000' AS DateTime), 1, 0, 0, CAST(229324.00 AS Decimal(18, 2)), N'sosacleofomercedes@hotmail.com', NULL, NULL, 1, N'Observacion de SOSA, CLEOFO MERCEDES')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (10, N'MARTINEZ, JULIA AZUCENA', 23203492042, N'CP', 2034920, CAST(N'1935-07-21T00:00:00.000' AS DateTime), N'F', N'S', N'MARTINEZ', 203, N'CORDOBA CAPITAL', 75, NULL, NULL, CAST(N'1942-07-19T00:00:00.000' AS DateTime), CAST(N'1946-07-18T00:00:00.000' AS DateTime), 0, 0, 0, CAST(232034.00 AS Decimal(18, 2)), N'martinezjuliaazucena@gmail.com', NULL, NULL, 1, N'Observacion de MARTINEZ, JULIA AZUCENA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (11, N'FERNANDEZ, ANGELA YOLANDA', 27482082884, N'CP', 4820828, CAST(N'1944-09-12T00:00:00.000' AS DateTime), N'F', N'Z', N'FERNANDEZ', 482, N'EL FORTIN', 76, NULL, NULL, CAST(N'1951-09-11T00:00:00.000' AS DateTime), CAST(N'1955-09-10T00:00:00.000' AS DateTime), 1, 0, 0, CAST(249837.00 AS Decimal(18, 2)), N'fernandezangelayolanda@hotmail.com', NULL, NULL, 1, N'Observacion de FERNANDEZ, ANGELA YOLANDA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (12, N'ALIENDRO, OLGA', 27358497562, N'CP', 3584975, CAST(N'1938-01-11T00:00:00.000' AS DateTime), N'F', N'S', N'ALIENDRO', 358, N'CRUZ DEL EJE', 77, NULL, NULL, CAST(N'1945-01-09T00:00:00.000' AS DateTime), CAST(N'1949-01-08T00:00:00.000' AS DateTime), 0, 0, 1, CAST(227987.00 AS Decimal(18, 2)), N'aliendroolga@gmail.com', NULL, NULL, 1, N'Observacion de ALIENDRO, OLGA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (13, N'AMERI, OLGA ESTHER', 27526019253, N'LE', 5260192, CAST(N'1971-01-01T00:00:00.000' AS DateTime), N'F', N'Z', N'AMERI', 526, N'LABOULAYE', 78, NULL, NULL, CAST(N'1977-12-30T00:00:00.000' AS DateTime), CAST(N'1981-12-29T00:00:00.000' AS DateTime), 1, 1, 0, CAST(211738.00 AS Decimal(18, 2)), N'ameriolgaesther@hotmail.com', NULL, NULL, 1, N'Observacion de AMERI, OLGA ESTHER')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (14, N'OVIEDO, AIDEE FELIPA', 23429454041, N'LE', 4294540, CAST(N'1944-08-23T00:00:00.000' AS DateTime), N'F', N'S', N'OVIEDO', 429, N'CAMINIAGA', 79, NULL, NULL, CAST(N'1951-08-22T00:00:00.000' AS DateTime), CAST(N'1955-08-21T00:00:00.000' AS DateTime), 0, 1, 0, CAST(167353.00 AS Decimal(18, 2)), N'oviedoaideefelipa@gmail.com', NULL, NULL, 1, N'Observacion de OVIEDO, AIDEE FELIPA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (15, N'MORENO, ELISA CRISTINA', 27573824790, N'PSP', 5738247, CAST(N'1948-07-06T00:00:00.000' AS DateTime), N'F', N'C', N'MORENO', 573, N'DEAN FUNES', 80, NULL, NULL, CAST(N'1955-07-05T00:00:00.000' AS DateTime), CAST(N'1959-07-04T00:00:00.000' AS DateTime), 1, 0, 1, CAST(183825.00 AS Decimal(18, 2)), N'morenoelisacristina@hotmail.com', NULL, NULL, 1, N'Observacion de MORENO, ELISA CRISTINA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (16, N'MEZ, CANDELARIA ALICIA', 27410899768, N'DNI', 4108997, CAST(N'1940-06-11T00:00:00.000' AS DateTime), N'F', N'Z', N'MEZ', 410, N'DEAN FUNES', 81, NULL, NULL, CAST(N'1947-06-10T00:00:00.000' AS DateTime), CAST(N'1951-06-09T00:00:00.000' AS DateTime), 0, 0, 1, CAST(171318.00 AS Decimal(18, 2)), N'MEZcandelariaalicia@gmail.com', NULL, NULL, 1, N'Observacion de MEZ, CANDELARIA ALICIA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (17, N'ORTIZ, EMILIA V', 27466964043, N'DNI', 4669640, CAST(N'1946-10-03T00:00:00.000' AS DateTime), N'F', N'Z', N'ORTIZ', 466, N'BENJAMIN GOULD', 82, NULL, NULL, CAST(N'1953-10-01T00:00:00.000' AS DateTime), CAST(N'1957-09-30T00:00:00.000' AS DateTime), 1, 1, 0, CAST(161570.00 AS Decimal(18, 2)), N'ortizemiliav@hotmail.com', NULL, NULL, 1, N'Observacion de ORTIZ, EMILIA V')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (18, N'ROJAS, JORGE CORNELIO', 20655007536, N'DNI', 6550075, CAST(N'1941-09-19T00:00:00.000' AS DateTime), N'M', N'Z', N'ROJAS', 655, N'SAN MARCOS SUD', 83, NULL, NULL, CAST(N'1948-09-17T00:00:00.000' AS DateTime), CAST(N'1952-09-16T00:00:00.000' AS DateTime), 0, 0, 1, CAST(114750.00 AS Decimal(18, 2)), N'rojasjorgecornelio@gmail.com', NULL, NULL, 1, N'Observacion de ROJAS, JORGE CORNELIO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (19, N'BUENO, GERARDO', 20654416337, N'DNI', 6544163, CAST(N'1937-02-21T00:00:00.000' AS DateTime), N'M', N'Z', N'BUENO', 654, N'SIN ASIGNAR', 84, NULL, NULL, CAST(N'1944-02-20T00:00:00.000' AS DateTime), CAST(N'1948-02-19T00:00:00.000' AS DateTime), 1, 1, 1, CAST(108707.00 AS Decimal(18, 2)), N'buenogerardo@hotmail.com', NULL, NULL, 1, N'Observacion de BUENO, GERARDO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (20, N'STREET, SUSANA MARGARITA', 23488956540, N'DNI', 4889565, CAST(N'1947-06-05T00:00:00.000' AS DateTime), N'F', N'Z', N'STREET', 488, N'LABOULAYE', 85, NULL, NULL, CAST(N'1954-06-03T00:00:00.000' AS DateTime), CAST(N'1958-06-02T00:00:00.000' AS DateTime), 0, 0, 1, CAST(117444.00 AS Decimal(18, 2)), N'streetsusanamargarita@gmail.com', NULL, NULL, 1, N'Observacion de STREET, SUSANA MARGARITA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (21, N'SANCHEZ, GRISELDA ELINA', 27263738204, N'DNI', 26373820, CAST(N'1975-10-09T00:00:00.000' AS DateTime), N'F', N'S', N'SANCHEZ', 263, N'VILLA GUTIERREZ', 86, NULL, NULL, CAST(N'1982-10-07T00:00:00.000' AS DateTime), CAST(N'1986-10-06T00:00:00.000' AS DateTime), 1, 0, 0, CAST(129827.00 AS Decimal(18, 2)), N'sanchezgriseldaelina@hotmail.com', NULL, NULL, 1, N'Observacion de SANCHEZ, GRISELDA ELINA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (22, N'CENA, MARGARITA ROSA', 27995449408, N'DNI', 9954494, CAST(N'1939-07-21T00:00:00.000' AS DateTime), N'F', N'V', N'CENA', 995, N'LABOULAYE', 87, NULL, NULL, CAST(N'1946-07-19T00:00:00.000' AS DateTime), CAST(N'1950-07-18T00:00:00.000' AS DateTime), 0, 0, 0, CAST(127252.00 AS Decimal(18, 2)), N'cenamargaritarosa@gmail.com', NULL, NULL, 1, N'Observacion de CENA, MARGARITA ROSA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (23, N'CRISTALDO MARTINEZ, DEONILDA', 27928244024, N'CF', 9282440, CAST(N'1960-04-08T00:00:00.000' AS DateTime), N'F', N'Z', N'CRISTALDO MARTINEZ', 928, N'SIN ASIGNAR', 88, NULL, NULL, CAST(N'1967-04-07T00:00:00.000' AS DateTime), CAST(N'1971-04-06T00:00:00.000' AS DateTime), 1, 0, 0, CAST(121427.00 AS Decimal(18, 2)), N'cristaldomartinezdeonilda@hotmail.com', NULL, NULL, 1, N'Observacion de CRISTALDO MARTINEZ, DEONILDA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (24, N'PETRELLI, FABIANA', 27162750644, N'CF', 1627506, CAST(N'1962-12-03T00:00:00.000' AS DateTime), N'F', N'Z', N'PETRELLI', 162, N'RIO CEBALLOS', 89, NULL, NULL, CAST(N'1969-12-01T00:00:00.000' AS DateTime), CAST(N'1973-11-30T00:00:00.000' AS DateTime), 0, 0, 0, CAST(113178.00 AS Decimal(18, 2)), N'petrellifabiana@gmail.com', NULL, NULL, 1, N'Observacion de PETRELLI, FABIANA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (25, N'MARTINEZ, GINES', 20637709075, N'CF', 6377090, CAST(N'1935-05-25T00:00:00.000' AS DateTime), N'M', N'S', N'MARTINEZ', 637, N'CORDOBA CAPITAL', 90, NULL, NULL, CAST(N'1942-05-23T00:00:00.000' AS DateTime), CAST(N'1946-05-22T00:00:00.000' AS DateTime), 1, 1, 0, CAST(82550.00 AS Decimal(18, 2)), N'martinezgines@hotmail.com', NULL, NULL, 1, N'Observacion de MARTINEZ, GINES')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (26, N'BARRIOS, AGUSTIN', 27665428882, N'CP', 6654288, CAST(N'1945-04-02T00:00:00.000' AS DateTime), N'M', N'Z', N'BARRIOS', 665, N'LABOULAYE', 91, NULL, NULL, CAST(N'1952-03-31T00:00:00.000' AS DateTime), CAST(N'1956-03-30T00:00:00.000' AS DateTime), 0, 0, 0, CAST(106405.00 AS Decimal(18, 2)), N'barriosagustin@gmail.com', NULL, NULL, 1, N'Observacion de BARRIOS, AGUSTIN')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (27, N'RIVERO, SUSANA ELENA', 27591843723, N'CP', 5918437, CAST(N'1948-08-04T00:00:00.000' AS DateTime), N'F', N'Z', N'RIVERO', 591, N'ARROYITO', 92, NULL, NULL, CAST(N'1955-08-03T00:00:00.000' AS DateTime), CAST(N'1959-08-02T00:00:00.000' AS DateTime), 1, 1, 1, CAST(102192.00 AS Decimal(18, 2)), N'riverosusanaelena@hotmail.com', NULL, NULL, 1, N'Observacion de RIVERO, SUSANA ELENA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (28, N'BLETRAMO, LEANDRO EMANUEL', 27626507831, N'CP', 34273940, CAST(N'1989-05-17T00:00:00.000' AS DateTime), N'M', N'S', N'BLETRAMO', 342, N'SIN ASIGNAR', 93, NULL, NULL, CAST(N'1996-05-15T00:00:00.000' AS DateTime), CAST(N'2000-05-14T00:00:00.000' AS DateTime), 0, 1, 0, CAST(98666.00 AS Decimal(18, 2)), N'bletramoleandroemanuel@gmail.com', NULL, NULL, 1, N'Observacion de BLETRAMO, LEANDRO EMANUEL')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (29, N'VINCENZINI, NORMA IRIS', 27595598130, N'LE', 5955981, CAST(N'1950-03-05T00:00:00.000' AS DateTime), N'F', N'C', N'VINCENZINI', 595, N'JUSTINIANO POSSE', 94, NULL, NULL, CAST(N'1957-03-03T00:00:00.000' AS DateTime), CAST(N'1961-03-02T00:00:00.000' AS DateTime), 1, 0, 1, CAST(95157.00 AS Decimal(18, 2)), N'vincenzininormairis@hotmail.com', NULL, NULL, 1, N'Observacion de VINCENZINI, NORMA IRIS')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (30, N'FERREYRA, MARTA GRACIELA', 27566081638, N'LE', 5660816, CAST(N'1947-11-12T00:00:00.000' AS DateTime), N'F', N'C', N'FERREYRA', 566, N'ALCIRA', 95, NULL, NULL, CAST(N'1954-11-10T00:00:00.000' AS DateTime), CAST(N'1958-11-09T00:00:00.000' AS DateTime), 0, 0, 0, CAST(91886.00 AS Decimal(18, 2)), N'ferreyramartagraciela@gmail.com', NULL, NULL, 1, N'Observacion de FERREYRA, MARTA GRACIELA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (31, N'VERON, MANUEL VALLE', 20795662260, N'PSP', 7956622, CAST(N'1946-10-11T00:00:00.000' AS DateTime), N'M', N'S', N'VERON', 795, N'LUCIO V MANSILLA', 96, NULL, NULL, CAST(N'1953-10-09T00:00:00.000' AS DateTime), CAST(N'1957-10-08T00:00:00.000' AS DateTime), 1, 0, 0, CAST(67082.00 AS Decimal(18, 2)), N'veronmanuelvalle@hotmail.com', NULL, NULL, 1, N'Observacion de VERON, MANUEL VALLE')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (32, N'RUIZ, ELENA VICENTA', 27532766070, N'DNI', 5327660, CAST(N'1949-12-08T00:00:00.000' AS DateTime), N'F', N'S', N'RUIZ', 532, N'SIN ASIGNAR', 97, NULL, NULL, CAST(N'1956-12-06T00:00:00.000' AS DateTime), CAST(N'1960-12-05T00:00:00.000' AS DateTime), 0, 0, 0, CAST(86039.00 AS Decimal(18, 2)), N'ruizelenavicenta@gmail.com', NULL, NULL, 1, N'Observacion de RUIZ, ELENA VICENTA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (33, N'SAGUILAN, LUICIANA', 27448698914, N'DNI', 4486989, CAST(N'1944-01-16T00:00:00.000' AS DateTime), N'F', N'S', N'SAGUILAN', 448, N'SIN ASIGNAR', 98, NULL, NULL, CAST(N'1951-01-14T00:00:00.000' AS DateTime), CAST(N'1955-01-13T00:00:00.000' AS DateTime), 1, 0, 1, CAST(83177.00 AS Decimal(18, 2)), N'saguilanluiciana@hotmail.com', NULL, NULL, 1, N'Observacion de SAGUILAN, LUICIANA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (34, N'BRIZUELA, JUAN AGUSTIN', 20712112165, N'DNI', 7121121, CAST(N'1943-08-29T00:00:00.000' AS DateTime), N'M', N'C', N'BRIZUELA', 712, N'LUCIO V MANSILLA', 99, NULL, NULL, CAST(N'1950-08-27T00:00:00.000' AS DateTime), CAST(N'1954-08-26T00:00:00.000' AS DateTime), 0, 1, 1, CAST(60917.00 AS Decimal(18, 2)), N'brizuelajuanagustin@gmail.com', NULL, NULL, 1, N'Observacion de BRIZUELA, JUAN AGUSTIN')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (35, N'LOBO, SILVESTRE AMARANTO', 23638043395, N'DNI', 6380433, CAST(N'1936-12-20T00:00:00.000' AS DateTime), N'M', N'S', N'LOBO', 638, N'SIN ASIGNAR', 100, NULL, NULL, CAST(N'1943-12-19T00:00:00.000' AS DateTime), CAST(N'1947-12-18T00:00:00.000' AS DateTime), 1, 1, 1, CAST(67537.00 AS Decimal(18, 2)), N'lobosilvestreamaranto@hotmail.com', NULL, NULL, 1, N'Observacion de LOBO, SILVESTRE AMARANTO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (36, N'JUAREZ, JOAQUIN AVELINO', 20083634100, N'DNI', 8363410, CAST(N'1951-03-04T00:00:00.000' AS DateTime), N'M', N'S', N'JUAREZ', 836, N'SIN ASIGNAR', 101, NULL, NULL, CAST(N'1958-03-02T00:00:00.000' AS DateTime), CAST(N'1962-03-01T00:00:00.000' AS DateTime), 0, 0, 0, CAST(55787.00 AS Decimal(18, 2)), N'juarezjoaquinavelino@gmail.com', NULL, NULL, 1, N'Observacion de JUAREZ, JOAQUIN AVELINO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (37, N'HEREDIA, CARLOS', 20784359428, N'DNI', 7843594, CAST(N'1949-11-18T00:00:00.000' AS DateTime), N'M', N'S', N'HEREDIA', 784, N'CORDOBA CAPITAL', 102, NULL, NULL, CAST(N'1956-11-16T00:00:00.000' AS DateTime), CAST(N'1960-11-15T00:00:00.000' AS DateTime), 1, 0, 0, CAST(56173.00 AS Decimal(18, 2)), N'herediacarlos@hotmail.com', NULL, NULL, 1, N'Observacion de HEREDIA, CARLOS')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (38, N'FUENTES, DIEGO HERNAN', 20227968314, N'DNI', 22796831, CAST(N'1972-12-11T00:00:00.000' AS DateTime), N'M', N'Z', N'FUENTES', 227, N'CORDOBA CAPITAL', 103, NULL, NULL, CAST(N'1979-12-10T00:00:00.000' AS DateTime), CAST(N'1983-12-09T00:00:00.000' AS DateTime), 0, 0, 1, CAST(53231.00 AS Decimal(18, 2)), N'fuentesdiegohernan@gmail.com', NULL, NULL, 1, N'Observacion de FUENTES, DIEGO HERNAN')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (39, N'RIVERO, JOSE', 20668590628, N'CF', 6685906, CAST(N'1938-01-01T00:00:00.000' AS DateTime), N'M', N'C', N'RIVERO', 668, N'SIN ASIGNAR', 104, NULL, NULL, CAST(N'1944-12-30T00:00:00.000' AS DateTime), CAST(N'1948-12-29T00:00:00.000' AS DateTime), 1, 0, 0, CAST(52996.00 AS Decimal(18, 2)), N'riverojose@hotmail.com', NULL, NULL, 1, N'Observacion de RIVERO, JOSE')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (40, N'MACCHI, MARIA DE LUJAN', 27572149168, N'CF', 5721491, CAST(N'1947-10-28T00:00:00.000' AS DateTime), N'F', N'V', N'MACCHI', 572, N'SIN ASIGNAR', 105, NULL, NULL, CAST(N'1954-10-26T00:00:00.000' AS DateTime), CAST(N'1958-10-25T00:00:00.000' AS DateTime), 0, 0, 1, CAST(68930.00 AS Decimal(18, 2)), N'macchimariadelujan@gmail.com', NULL, NULL, 1, N'Observacion de MACCHI, MARIA DE LUJAN')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (41, N'SANCHEZ, CRISTINA DORA', 27450689062, N'CF', 4506890, CAST(N'1942-09-15T00:00:00.000' AS DateTime), N'F', N'S', N'SANCHEZ', 450, N'SIN ASIGNAR', 106, NULL, NULL, CAST(N'1949-09-13T00:00:00.000' AS DateTime), CAST(N'1953-09-12T00:00:00.000' AS DateTime), 1, 0, 0, CAST(66952.00 AS Decimal(18, 2)), N'sanchezcristinadora@hotmail.com', NULL, NULL, 1, N'Observacion de SANCHEZ, CRISTINA DORA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (42, N'ARCE, YESICA J', 27281138084, N'CP', 28113808, CAST(N'1980-03-31T00:00:00.000' AS DateTime), N'F', N'S', N'ARCE', 281, N'SIN ASIGNAR', 107, NULL, NULL, CAST(N'1987-03-30T00:00:00.000' AS DateTime), CAST(N'1991-03-29T00:00:00.000' AS DateTime), 0, 0, 0, CAST(64955.00 AS Decimal(18, 2)), N'arceyesicaj@gmail.com', NULL, NULL, 0, N'Observacion de ARCE, YESICA J')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (43, N'URQUIZA, CESAR DANIEL', 22639321708, N'CP', 26393217, CAST(N'1978-01-21T00:00:00.000' AS DateTime), N'M', N'Z', N'URQUIZA', 263, N'CORDOBA CAPITAL', 108, NULL, NULL, CAST(N'1985-01-19T00:00:00.000' AS DateTime), CAST(N'1989-01-18T00:00:00.000' AS DateTime), 1, 0, 1, CAST(52649.00 AS Decimal(18, 2)), N'urquizacesardaniel@hotmail.com', NULL, NULL, 1, N'Observacion de URQUIZA, CESAR DANIEL')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (44, N'RODRIGUEZ, DARIO MARTIN', 20219020007, N'CP', 21902000, CAST(N'1970-11-06T00:00:00.000' AS DateTime), N'M', N'S', N'RODRIGUEZ', 219, N'CORDOBA CAPITAL', 109, NULL, NULL, CAST(N'1977-11-04T00:00:00.000' AS DateTime), CAST(N'1981-11-03T00:00:00.000' AS DateTime), 0, 1, 0, CAST(45952.00 AS Decimal(18, 2)), N'rodriguezdariomartin@gmail.com', NULL, NULL, 1, N'Observacion de RODRIGUEZ, DARIO MARTIN')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (45, N'GULLI, JORGE DANIEL', 20184298942, N'LE', 18429894, CAST(N'1967-10-18T00:00:00.000' AS DateTime), N'M', N'Z', N'GULLI', 184, N'CORDOBA CAPITAL', 110, NULL, NULL, CAST(N'1974-10-16T00:00:00.000' AS DateTime), CAST(N'1978-10-15T00:00:00.000' AS DateTime), 1, 0, 0, CAST(44853.00 AS Decimal(18, 2)), N'gullijorgedaniel@hotmail.com', NULL, NULL, 1, N'Observacion de GULLI, JORGE DANIEL')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (46, N'NAVARRO, DANIEL ALEJANDRO', 20206227870, N'LE', 20622787, CAST(N'1969-02-14T00:00:00.000' AS DateTime), N'M', N'C', N'NAVARRO', 206, N'CORDOBA CAPITAL', 111, NULL, NULL, CAST(N'1976-02-13T00:00:00.000' AS DateTime), CAST(N'1980-02-12T00:00:00.000' AS DateTime), 0, 0, 1, CAST(43926.00 AS Decimal(18, 2)), N'navarrodanielalejandro@gmail.com', NULL, NULL, 1, N'Observacion de NAVARRO, DANIEL ALEJANDRO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (47, N'RAMONDA, FRANCISCA MIGUELA', 27474045041, N'PSP', 4740450, CAST(N'1947-03-02T00:00:00.000' AS DateTime), N'F', N'C', N'RAMONDA', 474, N'LA CESIRA', 112, NULL, NULL, CAST(N'1954-02-28T00:00:00.000' AS DateTime), CAST(N'1958-02-27T00:00:00.000' AS DateTime), 1, 1, 0, CAST(58455.00 AS Decimal(18, 2)), N'ramondafranciscamiguela@hotmail.com', NULL, NULL, 1, N'Observacion de RAMONDA, FRANCISCA MIGUELA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (48, N'BOTTI, AMELIA HAYDEE', 27451000877, N'DNI', 4510008, CAST(N'1942-03-24T00:00:00.000' AS DateTime), N'F', N'C', N'BOTTI', 451, N'LA CESIRA', 113, NULL, NULL, CAST(N'1949-03-22T00:00:00.000' AS DateTime), CAST(N'1953-03-21T00:00:00.000' AS DateTime), 0, 1, 0, CAST(57189.00 AS Decimal(18, 2)), N'bottiameliahaydee@gmail.com', NULL, NULL, 1, N'Observacion de BOTTI, AMELIA HAYDEE')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (49, N'MORENO, DELIA', 27456452616, N'DNI', 4564526, CAST(N'1943-08-06T00:00:00.000' AS DateTime), N'F', N'Z', N'MORENO', 456, N'CORDOBA CAPITAL', 114, NULL, NULL, CAST(N'1950-08-04T00:00:00.000' AS DateTime), CAST(N'1954-08-03T00:00:00.000' AS DateTime), 1, 0, 0, CAST(56033.00 AS Decimal(18, 2)), N'morenodelia@hotmail.com', NULL, NULL, 1, N'Observacion de MORENO, DELIA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (50, N'MAROSTICA, CARLOS ERNESTO', 20769344021, N'DNI', 7693440, CAST(N'1949-04-03T00:00:00.000' AS DateTime), N'M', N'Z', N'MAROSTICA', 769, N'CORDOBA CAPITAL', 115, NULL, NULL, CAST(N'1956-04-01T00:00:00.000' AS DateTime), CAST(N'1960-03-31T00:00:00.000' AS DateTime), 0, 1, 0, CAST(41538.00 AS Decimal(18, 2)), N'marosticacarlosernesto@gmail.com', NULL, NULL, 1, N'Observacion de MAROSTICA, CARLOS ERNESTO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (51, N'LEDESMA, MERCEDES DEL CARMEN', 27370343507, N'DNI', 3703435, CAST(N'1936-11-15T00:00:00.000' AS DateTime), N'F', N'Z', N'LEDESMA', 370, N'VILLA ALLENDE', 116, NULL, NULL, CAST(N'1943-11-14T00:00:00.000' AS DateTime), CAST(N'1947-11-13T00:00:00.000' AS DateTime), 1, 1, 1, CAST(53667.00 AS Decimal(18, 2)), N'ledesmamercedesdelcarmen@hotmail.com', NULL, NULL, 1, N'Observacion de LEDESMA, MERCEDES DEL CARMEN')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (52, N'FLORES, MERCEDES YOLANDA', 27509792916, N'DNI', 5097929, CAST(N'1944-05-20T00:00:00.000' AS DateTime), N'F', N'Z', N'FLORES', 509, N'CORDOBA CAPITAL', 117, NULL, NULL, CAST(N'1951-05-19T00:00:00.000' AS DateTime), CAST(N'1955-05-18T00:00:00.000' AS DateTime), 0, 0, 1, CAST(52903.00 AS Decimal(18, 2)), N'floresmercedesyolanda@gmail.com', NULL, NULL, 1, N'Observacion de FLORES, MERCEDES YOLANDA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (53, N'MENDOZA, MARTIN ELEUTERIO', 20804194236, N'DNI', 8041942, CAST(N'1946-11-03T00:00:00.000' AS DateTime), N'M', N'S', N'MENDOZA', 804, N'CORDOBA CAPITAL', 118, NULL, NULL, CAST(N'1953-11-01T00:00:00.000' AS DateTime), CAST(N'1957-10-31T00:00:00.000' AS DateTime), 1, 0, 0, CAST(39253.00 AS Decimal(18, 2)), N'mendozamartineleuterio@hotmail.com', NULL, NULL, 1, N'Observacion de MENDOZA, MARTIN ELEUTERIO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (54, N'RIVADERO, NILDA MIRTA', 27450779130, N'DNI', 4507791, CAST(N'1942-08-17T00:00:00.000' AS DateTime), N'F', N'C', N'RIVADERO', 450, N'CORDOBA CAPITAL', 119, NULL, NULL, CAST(N'1949-08-15T00:00:00.000' AS DateTime), CAST(N'1953-08-14T00:00:00.000' AS DateTime), 0, 0, 1, CAST(50834.00 AS Decimal(18, 2)), N'rivaderonildamirta@gmail.com', NULL, NULL, 1, N'Observacion de RIVADERO, NILDA MIRTA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (55, N'LUBARY, ELENA DEL CARMEN', 27664750056, N'CF', 6647500, CAST(N'1951-09-14T00:00:00.000' AS DateTime), N'F', N'Z', N'LUBARY', 664, N'ALICIA', 120, NULL, NULL, CAST(N'1958-09-12T00:00:00.000' AS DateTime), CAST(N'1962-09-11T00:00:00.000' AS DateTime), 1, 0, 0, CAST(50299.00 AS Decimal(18, 2)), N'lubaryelenadelcarmen@hotmail.com', NULL, NULL, 1, N'Observacion de LUBARY, ELENA DEL CARMEN')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (56, N'MALDONADO, AURORA DEL VALLE', 27573076394, N'CF', 5730763, CAST(N'1948-07-04T00:00:00.000' AS DateTime), N'F', N'V', N'MALDONADO', 573, N'VILLA CARLOS PAZ', 121, NULL, NULL, CAST(N'1955-07-03T00:00:00.000' AS DateTime), CAST(N'1959-07-02T00:00:00.000' AS DateTime), 0, 0, 1, CAST(49237.00 AS Decimal(18, 2)), N'maldonadoauroradelvalle@gmail.com', NULL, NULL, 1, N'Observacion de MALDONADO, AURORA DEL VALLE')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (57, N'CARRANZA, FRANCISCA CORINA', 27157244850, N'CF', 1572448, CAST(N'1932-10-16T00:00:00.000' AS DateTime), N'F', N'V', N'CARRANZA', 157, N'VILLA CARLOS PAZ', 122, NULL, NULL, CAST(N'1939-10-15T00:00:00.000' AS DateTime), CAST(N'1943-10-14T00:00:00.000' AS DateTime), 1, 0, 0, CAST(47644.00 AS Decimal(18, 2)), N'carranzafranciscacorina@hotmail.com', NULL, NULL, 1, N'Observacion de CARRANZA, FRANCISCA CORINA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (58, N'DELGADO, RAMONA', 27626457982, N'CP', 6264579, CAST(N'1950-01-25T00:00:00.000' AS DateTime), N'F', N'C', N'DELGADO', 626, N'VILLA SANTA CRUZ DEL LAGO', 123, NULL, NULL, CAST(N'1957-01-23T00:00:00.000' AS DateTime), CAST(N'1961-01-22T00:00:00.000' AS DateTime), 0, 0, 1, CAST(47631.00 AS Decimal(18, 2)), N'delgadoramona@gmail.com', NULL, NULL, 1, N'Observacion de DELGADO, RAMONA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (59, N'LUCERO, ROSARIO DEL CARMEN', 27388759522, N'CP', 3887595, CAST(N'1940-07-06T00:00:00.000' AS DateTime), N'F', N'S', N'LUCERO', 388, N'CORDOBA CAPITAL', 124, NULL, NULL, CAST(N'1947-07-05T00:00:00.000' AS DateTime), CAST(N'1951-07-04T00:00:00.000' AS DateTime), 1, 0, 1, CAST(46421.00 AS Decimal(18, 2)), N'lucerorosariodelcarmen@hotmail.com', NULL, NULL, 1, N'Observacion de LUCERO, ROSARIO DEL CARMEN')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (60, N'DOMINGUEZ, MARTHA AMALIA', 27563688988, N'CP', 5636889, CAST(N'1950-06-27T00:00:00.000' AS DateTime), N'F', N'Z', N'DOMINGUEZ', 563, N'SIN ASIGNAR', 125, NULL, NULL, CAST(N'1957-06-25T00:00:00.000' AS DateTime), CAST(N'1961-06-24T00:00:00.000' AS DateTime), 0, 0, 1, CAST(45939.00 AS Decimal(18, 2)), N'dominguezmarthaamalia@gmail.com', NULL, NULL, 1, N'Observacion de DOMINGUEZ, MARTHA AMALIA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (61, N'PIZZUTO, SEBASTIAN', 20841186497, N'LE', 8411864, CAST(N'1950-09-07T00:00:00.000' AS DateTime), N'M', N'Z', N'PIZZUTO', 841, N'CORDOBA CAPITAL', 126, NULL, NULL, CAST(N'1957-09-05T00:00:00.000' AS DateTime), CAST(N'1961-09-04T00:00:00.000' AS DateTime), 1, 1, 0, CAST(34165.00 AS Decimal(18, 2)), N'pizzutosebastian@hotmail.com', NULL, NULL, 1, N'Observacion de PIZZUTO, SEBASTIAN')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (62, N'GIGENA, OFELIA DORA', 27668031772, N'LE', 6680317, CAST(N'1949-04-17T00:00:00.000' AS DateTime), N'F', N'Z', N'GIGENA', 668, N'CORDOBA CAPITAL', 127, NULL, NULL, CAST(N'1956-04-15T00:00:00.000' AS DateTime), CAST(N'1960-04-14T00:00:00.000' AS DateTime), 0, 0, 1, CAST(44625.00 AS Decimal(18, 2)), N'gigenaofeliadora@gmail.com', NULL, NULL, 1, N'Observacion de GIGENA, OFELIA DORA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (63, N'ROMERO, HAYDEE', 27573070353, N'PSP', 5730703, CAST(N'1948-07-11T00:00:00.000' AS DateTime), N'F', N'S', N'ROMERO', 573, N'VILLA CARLOS PAZ', 128, NULL, NULL, CAST(N'1955-07-10T00:00:00.000' AS DateTime), CAST(N'1959-07-09T00:00:00.000' AS DateTime), 1, 1, 1, CAST(43766.00 AS Decimal(18, 2)), N'romerohaydee@hotmail.com', NULL, NULL, 1, N'Observacion de ROMERO, HAYDEE')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (64, N'SAUR, ELENA MARIA', 27524744728, N'DNI', 5247447, CAST(N'1947-01-04T00:00:00.000' AS DateTime), N'F', N'Z', N'SAUR', 524, N'CORRAL DE BUSTOS', 129, NULL, NULL, CAST(N'1954-01-02T00:00:00.000' AS DateTime), CAST(N'1958-01-01T00:00:00.000' AS DateTime), 0, 0, 1, CAST(43007.00 AS Decimal(18, 2)), N'saurelenamaria@gmail.com', NULL, NULL, 1, N'Observacion de SAUR, ELENA MARIA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (65, N'PIRIS, MARGARITA AURELIA', 27524744807, N'DNI', 5247448, CAST(N'1947-04-15T00:00:00.000' AS DateTime), N'F', N'S', N'PIRIS', 524, N'ISLA VERDE', 130, NULL, NULL, CAST(N'1954-04-13T00:00:00.000' AS DateTime), CAST(N'1958-04-12T00:00:00.000' AS DateTime), 1, 1, 0, CAST(42345.00 AS Decimal(18, 2)), N'pirismargaritaaurelia@hotmail.com', NULL, NULL, 1, N'Observacion de PIRIS, MARGARITA AURELIA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (66, N'GUZMAN, ALEJANDRINA', 27279896894, N'DNI', 2798968, CAST(N'1936-02-26T00:00:00.000' AS DateTime), N'F', N'Z', N'GUZMAN', 279, N'VILLA CARLOS PAZ', 131, NULL, NULL, CAST(N'1943-02-24T00:00:00.000' AS DateTime), CAST(N'1947-02-23T00:00:00.000' AS DateTime), 0, 0, 0, CAST(41333.00 AS Decimal(18, 2)), N'guzmanalejandrina@gmail.com', NULL, NULL, 1, N'Observacion de GUZMAN, ALEJANDRINA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (67, N'ZAPATA, ALICIA ANGELICA', 27485027392, N'DNI', 4850273, CAST(N'1944-07-14T00:00:00.000' AS DateTime), N'F', N'Z', N'ZAPATA', 485, N'ISLA VERDE', 132, NULL, NULL, CAST(N'1951-07-13T00:00:00.000' AS DateTime), CAST(N'1955-07-12T00:00:00.000' AS DateTime), 1, 0, 1, CAST(41022.00 AS Decimal(18, 2)), N'zapataaliciaangelica@hotmail.com', NULL, NULL, 1, N'Observacion de ZAPATA, ALICIA ANGELICA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (68, N'RODRIGUEZ, MATEO ELADIO', 20642911154, N'DNI', 6429111, CAST(N'1939-11-08T00:00:00.000' AS DateTime), N'M', N'C', N'RODRIGUEZ', 642, N'CORDOBA CAPITAL', 133, NULL, NULL, CAST(N'1946-11-06T00:00:00.000' AS DateTime), CAST(N'1950-11-05T00:00:00.000' AS DateTime), 0, 0, 1, CAST(30357.00 AS Decimal(18, 2)), N'rodriguezmateoeladio@gmail.com', NULL, NULL, 1, N'Observacion de RODRIGUEZ, MATEO ELADIO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (69, N'AGUIRRE, ANDRES RAFAEL', 20785607987, N'DNI', 7856079, CAST(N'1949-09-09T00:00:00.000' AS DateTime), N'M', N'Z', N'AGUIRRE', 785, N'SIN ASIGNAR', 134, NULL, NULL, CAST(N'1956-09-07T00:00:00.000' AS DateTime), CAST(N'1960-09-06T00:00:00.000' AS DateTime), 1, 1, 1, CAST(30124.00 AS Decimal(18, 2)), N'aguirreandresrafael@hotmail.com', NULL, NULL, 1, N'Observacion de AGUIRRE, ANDRES RAFAEL')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (70, N'FIGUEROA, JUAN SANTOS', 23639408796, N'DNI', 6394087, CAST(N'1943-11-29T00:00:00.000' AS DateTime), N'M', N'C', N'FIGUEROA', 639, N'DEAN FUNES', 135, NULL, NULL, CAST(N'1950-11-27T00:00:00.000' AS DateTime), CAST(N'1954-11-26T00:00:00.000' AS DateTime), 0, 0, 1, CAST(33770.00 AS Decimal(18, 2)), N'figueroajuansantos@gmail.com', NULL, NULL, 1, N'Observacion de FIGUEROA, JUAN SANTOS')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (71, N'MARRONI, MARTA OFELIA', 23497993146, N'CF', 4979931, CAST(N'1944-09-10T00:00:00.000' AS DateTime), N'F', N'Z', N'MARRONI', 497, N'CORDOBA CAPITAL', 136, NULL, NULL, CAST(N'1951-09-09T00:00:00.000' AS DateTime), CAST(N'1955-09-08T00:00:00.000' AS DateTime), 1, 0, 1, CAST(33095.00 AS Decimal(18, 2)), N'marronimartaofelia@hotmail.com', NULL, NULL, 1, N'Observacion de MARRONI, MARTA OFELIA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (72, N'FRONTERA, CARLOS', 20648727461, N'CF', 6487274, CAST(N'1934-05-07T00:00:00.000' AS DateTime), N'M', N'Z', N'FRONTERA', 648, N'CORDOBA CAPITAL', 137, NULL, NULL, CAST(N'1941-05-05T00:00:00.000' AS DateTime), CAST(N'1945-05-04T00:00:00.000' AS DateTime), 0, 1, 0, CAST(28678.00 AS Decimal(18, 2)), N'fronteracarlos@gmail.com', NULL, NULL, 1, N'Observacion de FRONTERA, CARLOS')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (73, N'GUERRERO, RAMONA IMELDA', 27545552245, N'CF', 5455522, CAST(N'1947-01-18T00:00:00.000' AS DateTime), N'F', N'Z', N'GUERRERO', 545, N'CORDOBA CAPITAL', 138, NULL, NULL, CAST(N'1954-01-16T00:00:00.000' AS DateTime), CAST(N'1958-01-15T00:00:00.000' AS DateTime), 1, 1, 0, CAST(37733.00 AS Decimal(18, 2)), N'guerreroramonaimelda@hotmail.com', NULL, NULL, 1, N'Observacion de GUERRERO, RAMONA IMELDA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (74, N'GIMENEZ, GRACIELA BERNARDITA', 27639703236, N'CP', 6397032, CAST(N'1950-08-20T00:00:00.000' AS DateTime), N'F', N'Z', N'GIMENEZ', 639, N'CORDOBA CAPITAL', 139, NULL, NULL, CAST(N'1957-08-18T00:00:00.000' AS DateTime), CAST(N'1961-08-17T00:00:00.000' AS DateTime), 0, 0, 0, CAST(37350.00 AS Decimal(18, 2)), N'gimenezgracielabernardita@gmail.com', NULL, NULL, 1, N'Observacion de GIMENEZ, GRACIELA BERNARDITA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (75, N'SALEVA, ROSA', 27500824967, N'CP', 5008249, CAST(N'1945-01-23T00:00:00.000' AS DateTime), N'F', N'Z', N'SALEVA', 500, N'CORDOBA CAPITAL', 140, NULL, NULL, CAST(N'1952-01-22T00:00:00.000' AS DateTime), CAST(N'1956-01-21T00:00:00.000' AS DateTime), 1, 1, 1, CAST(36667.00 AS Decimal(18, 2)), N'salevarosa@hotmail.com', NULL, NULL, 1, N'Observacion de SALEVA, ROSA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (76, N'CEBALLO, RAMON MARCIANO', 20849888136, N'CP', 8498881, CAST(N'1951-05-09T00:00:00.000' AS DateTime), N'M', N'S', N'CEBALLO', 849, N'CORDOBA CAPITAL', 141, NULL, NULL, CAST(N'1958-05-07T00:00:00.000' AS DateTime), CAST(N'1962-05-06T00:00:00.000' AS DateTime), 0, 0, 1, CAST(27434.00 AS Decimal(18, 2)), N'ceballoramonmarciano@gmail.com', NULL, NULL, 1, N'Observacion de CEBALLO, RAMON MARCIANO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (77, N'FERNANDEZ, IRMA BEATRIZ', 27574818761, N'LE', 5748187, CAST(N'1948-09-15T00:00:00.000' AS DateTime), N'F', N'Z', N'FERNANDEZ', 574, N'CORDOBA CAPITAL', 142, NULL, NULL, CAST(N'1955-09-14T00:00:00.000' AS DateTime), CAST(N'1959-09-13T00:00:00.000' AS DateTime), 1, 1, 1, CAST(35811.00 AS Decimal(18, 2)), N'fernandezirmabeatriz@hotmail.com', NULL, NULL, 1, N'Observacion de FERNANDEZ, IRMA BEATRIZ')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (78, N'ANDRADA, IRMA DEL VALLE', 21287418974, N'LE', 12874189, CAST(N'1955-01-15T00:00:00.000' AS DateTime), N'F', N'Z', N'ANDRADA', 128, N'CORDOBA CAPITAL', 143, NULL, NULL, CAST(N'1962-01-13T00:00:00.000' AS DateTime), CAST(N'1966-01-12T00:00:00.000' AS DateTime), 0, 0, 1, CAST(27291.00 AS Decimal(18, 2)), N'andradairmadelvalle@gmail.com', NULL, NULL, 1, N'Observacion de ANDRADA, IRMA DEL VALLE')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (79, N'PALUMBO, CLAUDIO GERARDO', 21784488704, N'PSP', 17844887, CAST(N'1966-11-19T00:00:00.000' AS DateTime), N'M', N'Z', N'PALUMBO', 178, N'CORDOBA CAPITAL', 144, NULL, NULL, CAST(N'1973-11-17T00:00:00.000' AS DateTime), CAST(N'1977-11-16T00:00:00.000' AS DateTime), 1, 0, 1, CAST(27575.00 AS Decimal(18, 2)), N'palumboclaudiogerardo@hotmail.com', NULL, NULL, 1, N'Observacion de PALUMBO, CLAUDIO GERARDO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (80, N'PERALTA, SILVIA PATRICIA', 22222018877, N'DNI', 22220188, CAST(N'1971-04-08T00:00:00.000' AS DateTime), N'F', N'Z', N'PERALTA', 222, N'CORDOBA CAPITAL', 145, NULL, NULL, CAST(N'1978-04-06T00:00:00.000' AS DateTime), CAST(N'1982-04-05T00:00:00.000' AS DateTime), 0, 1, 0, CAST(27777.00 AS Decimal(18, 2)), N'peraltasilviapatricia@gmail.com', NULL, NULL, 1, N'Observacion de PERALTA, SILVIA PATRICIA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (81, N'BACILOPULOS, CATALINA', 23649356744, N'DNI', 6493567, CAST(N'1954-06-06T00:00:00.000' AS DateTime), N'F', N'Z', N'BACILOPULOS', 649, N'LA FALDA', 146, NULL, NULL, CAST(N'1961-06-04T00:00:00.000' AS DateTime), CAST(N'1965-06-03T00:00:00.000' AS DateTime), 1, 0, 1, CAST(29196.00 AS Decimal(18, 2)), N'bacilopuloscatalina@hotmail.com', NULL, NULL, 1, N'Observacion de BACILOPULOS, CATALINA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (82, N'FARAIG, OLGA ANA', 27734671236, N'DNI', 7346712, CAST(N'1931-10-02T00:00:00.000' AS DateTime), N'F', N'S', N'FARAIG', 734, N'CORDOBA CAPITAL', 147, NULL, NULL, CAST(N'1938-09-30T00:00:00.000' AS DateTime), CAST(N'1942-09-29T00:00:00.000' AS DateTime), 0, 0, 0, CAST(33822.00 AS Decimal(18, 2)), N'faraigolgaana@gmail.com', NULL, NULL, 1, N'Observacion de FARAIG, OLGA ANA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (83, N'GUEVARA, MARTA N', 27624907438, N'DNI', 6249074, CAST(N'1949-11-30T00:00:00.000' AS DateTime), N'F', N'Z', N'GUEVARA', 624, N'LA FALDA', 148, NULL, NULL, CAST(N'1956-11-28T00:00:00.000' AS DateTime), CAST(N'1960-11-27T00:00:00.000' AS DateTime), 1, 0, 0, CAST(33283.00 AS Decimal(18, 2)), N'guevaramartan@hotmail.com', NULL, NULL, 1, N'Observacion de GUEVARA, MARTA N')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (84, N'BARBIERI, YOLANDA ROSA', 27459145484, N'DNI', 4591454, CAST(N'1943-06-16T00:00:00.000' AS DateTime), N'F', N'S', N'BARBIERI', 459, N'CORDOBA CAPITAL', 149, NULL, NULL, CAST(N'1950-06-14T00:00:00.000' AS DateTime), CAST(N'1954-06-13T00:00:00.000' AS DateTime), 0, 0, 0, CAST(32689.00 AS Decimal(18, 2)), N'barbieriyolandarosa@gmail.com', NULL, NULL, 1, N'Observacion de BARBIERI, YOLANDA ROSA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (85, N'ZARATE, NORMA AGUSTINA', 27521555995, N'DNI', 5215559, CAST(N'1971-01-01T00:00:00.000' AS DateTime), N'F', N'S', N'ZARATE', 521, N'LA FALDA', 150, NULL, NULL, CAST(N'1977-12-30T00:00:00.000' AS DateTime), CAST(N'1981-12-29T00:00:00.000' AS DateTime), 1, 1, 1, CAST(32378.00 AS Decimal(18, 2)), N'zaratenormaagustina@hotmail.com', NULL, NULL, 1, N'Observacion de ZARATE, NORMA AGUSTINA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (86, N'LOPEZ, RENE ANTONIO', 20659177866, N'DNI', 6591778, CAST(N'1938-11-30T00:00:00.000' AS DateTime), N'M', N'Z', N'LOPEZ', 659, N'HERNANDO', 151, NULL, NULL, CAST(N'1945-11-28T00:00:00.000' AS DateTime), CAST(N'1949-11-27T00:00:00.000' AS DateTime), 0, 0, 0, CAST(24022.00 AS Decimal(18, 2)), N'lopezreneantonio@gmail.com', NULL, NULL, 1, N'Observacion de LOPEZ, RENE ANTONIO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (87, N'BANEGA, TELEFORO ENRIQUE', 20795397096, N'CF', 7953970, CAST(N'1945-02-20T00:00:00.000' AS DateTime), N'M', N'S', N'BANEGA', 795, N'KILOMETRO 658', 152, NULL, NULL, CAST(N'1952-02-19T00:00:00.000' AS DateTime), CAST(N'1956-02-18T00:00:00.000' AS DateTime), 1, 0, 0, CAST(23902.00 AS Decimal(18, 2)), N'banegateleforoenrique@hotmail.com', NULL, NULL, 1, N'Observacion de BANEGA, TELEFORO ENRIQUE')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (88, N'SUAREZ, VICENTE', 20787085585, N'CF', 7870855, CAST(N'1950-03-27T00:00:00.000' AS DateTime), N'M', N'Z', N'SUAREZ', 787, N'HERNANDO', 153, NULL, NULL, CAST(N'1957-03-25T00:00:00.000' AS DateTime), CAST(N'1961-03-24T00:00:00.000' AS DateTime), 0, 1, 1, CAST(23621.00 AS Decimal(18, 2)), N'suarezvicente@gmail.com', NULL, NULL, 1, N'Observacion de SUAREZ, VICENTE')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (89, N'MURUA, MARIA', 27625678277, N'CF', 6256782, CAST(N'1949-08-20T00:00:00.000' AS DateTime), N'F', N'Z', N'MURUA', 625, N'SIN ASIGNAR', 154, NULL, NULL, CAST(N'1956-08-18T00:00:00.000' AS DateTime), CAST(N'1960-08-17T00:00:00.000' AS DateTime), 1, 1, 0, CAST(31040.00 AS Decimal(18, 2)), N'muruamaria@hotmail.com', NULL, NULL, 1, N'Observacion de MURUA, MARIA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (90, N'PALMIERIS, MIGUEL ANGEL', 20660819916, N'CP', 6608199, CAST(N'1946-12-22T00:00:00.000' AS DateTime), N'M', N'C', N'PALMIERIS', 660, N'HERNANDO', 155, NULL, NULL, CAST(N'1953-12-20T00:00:00.000' AS DateTime), CAST(N'1957-12-19T00:00:00.000' AS DateTime), 0, 0, 1, CAST(22956.00 AS Decimal(18, 2)), N'palmierismiguelangel@gmail.com', NULL, NULL, 1, N'Observacion de PALMIERIS, MIGUEL ANGEL')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (91, N'VIVAS, CRISTINA DEL VALLE', 27568285885, N'CP', 5682858, CAST(N'1948-12-19T00:00:00.000' AS DateTime), N'F', N'S', N'VIVAS', 568, N'CORDOBA CAPITAL', 156, NULL, NULL, CAST(N'1955-12-18T00:00:00.000' AS DateTime), CAST(N'1959-12-17T00:00:00.000' AS DateTime), 1, 1, 0, CAST(30294.00 AS Decimal(18, 2)), N'vivascristinadelvalle@hotmail.com', NULL, NULL, 1, N'Observacion de VIVAS, CRISTINA DEL VALLE')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (92, N'MARQUEZ, NICOLASA Z', 27544334714, N'CP', 5443347, CAST(N'1947-08-24T00:00:00.000' AS DateTime), N'F', N'Z', N'MARQUEZ', 544, N'SIN ASIGNAR', 157, NULL, NULL, CAST(N'1954-08-22T00:00:00.000' AS DateTime), CAST(N'1958-08-21T00:00:00.000' AS DateTime), 0, 0, 1, CAST(29939.00 AS Decimal(18, 2)), N'marqueznicolasaz@gmail.com', NULL, NULL, 1, N'Observacion de MARQUEZ, NICOLASA Z')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (93, N'MONSERRAT, ERNESTO NAZARENO', 20785685782, N'LE', 7856857, CAST(N'1950-03-02T00:00:00.000' AS DateTime), N'M', N'C', N'MONSERRAT', 785, N'CORDOBA CAPITAL', 158, NULL, NULL, CAST(N'1957-02-28T00:00:00.000' AS DateTime), CAST(N'1961-02-27T00:00:00.000' AS DateTime), 1, 0, 1, CAST(22350.00 AS Decimal(18, 2)), N'monserraternestonazareno@hotmail.com', NULL, NULL, 1, N'Observacion de MONSERRAT, ERNESTO NAZARENO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (94, N'CEBALLOS, ETELVINA  SEFERINA', 27115018648, N'LE', 1150186, CAST(N'1942-01-09T00:00:00.000' AS DateTime), N'F', N'S', N'CEBALLOS', 115, N'CORDOBA CAPITAL', 159, NULL, NULL, CAST(N'1949-01-07T00:00:00.000' AS DateTime), CAST(N'1953-01-06T00:00:00.000' AS DateTime), 0, 0, 0, CAST(28845.00 AS Decimal(18, 2)), N'ceballosetelvinaseferina@gmail.com', NULL, NULL, 1, N'Observacion de CEBALLOS, ETELVINA  SEFERINA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (95, N'SOSA, FELIX ENRIQUE', 20669190491, N'PSP', 6691904, CAST(N'1942-03-01T00:00:00.000' AS DateTime), N'M', N'Z', N'SOSA', 669, N'CORDOBA CAPITAL', 160, NULL, NULL, CAST(N'1949-02-27T00:00:00.000' AS DateTime), CAST(N'1953-02-26T00:00:00.000' AS DateTime), 1, 1, 0, CAST(21757.00 AS Decimal(18, 2)), N'sosafelixenrique@hotmail.com', NULL, NULL, 1, N'Observacion de SOSA, FELIX ENRIQUE')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (96, N'CARRIZO, CLARA ROSA', 27321777494, N'DNI', 3217774, CAST(N'1935-03-15T00:00:00.000' AS DateTime), N'F', N'Z', N'CARRIZO', 321, N'DEAN FUNES', 161, NULL, NULL, CAST(N'1942-03-13T00:00:00.000' AS DateTime), CAST(N'1946-03-12T00:00:00.000' AS DateTime), 0, 0, 0, CAST(28460.00 AS Decimal(18, 2)), N'carrizoclararosa@gmail.com', NULL, NULL, 1, N'Observacion de CARRIZO, CLARA ROSA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (97, N'ROSS, LETICIA', 21462617430, N'DNI', 14626174, CAST(N'1961-09-12T00:00:00.000' AS DateTime), N'F', N'Z', N'ROSS', 146, N'CORDOBA CAPITAL', 162, NULL, NULL, CAST(N'1968-09-10T00:00:00.000' AS DateTime), CAST(N'1972-09-09T00:00:00.000' AS DateTime), 1, 0, 0, CAST(22126.00 AS Decimal(18, 2)), N'rossleticia@hotmail.com', NULL, NULL, 1, N'Observacion de ROSS, LETICIA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (98, N'OLMOS, RAMONA', 27497455975, N'DNI', 4974559, CAST(N'1946-01-31T00:00:00.000' AS DateTime), N'F', N'Z', N'OLMOS', 497, N'CORDOBA CAPITAL', 163, NULL, NULL, CAST(N'1953-01-29T00:00:00.000' AS DateTime), CAST(N'1957-01-28T00:00:00.000' AS DateTime), 0, 1, 1, CAST(28058.00 AS Decimal(18, 2)), N'olmosramona@gmail.com', NULL, NULL, 1, N'Observacion de OLMOS, RAMONA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (99, N'BANCHI, MARIA DEL CARMEN', 27588175841, N'DNI', 5881758, CAST(N'1949-02-21T00:00:00.000' AS DateTime), N'F', N'Z', N'BANCHI', 588, N'CORDOBA CAPITAL', 164, NULL, NULL, CAST(N'1956-02-20T00:00:00.000' AS DateTime), CAST(N'1960-02-19T00:00:00.000' AS DateTime), 1, 1, 0, CAST(27866.00 AS Decimal(18, 2)), N'banchimariadelcarmen@hotmail.com', NULL, NULL, 1, N'Observacion de BANCHI, MARIA DEL CARMEN')
GO
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (100, N'YONSON, ROBERTO ENRIQUE', 20675461228, N'DNI', 6754612, CAST(N'1935-02-18T00:00:00.000' AS DateTime), N'M', N'S', N'YONSON', 675, N'CORDOBA CAPITAL', 165, NULL, NULL, CAST(N'1942-02-16T00:00:00.000' AS DateTime), CAST(N'1946-02-15T00:00:00.000' AS DateTime), 0, 0, 0, CAST(20675.00 AS Decimal(18, 2)), N'yonsonrobertoenrique@gmail.com', NULL, NULL, 1, N'Observacion de YONSON, ROBERTO ENRIQUE')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (101, N'CORREA, MARIA ISABEL', 27667987187, N'DNI', 6679871, CAST(N'1950-11-15T00:00:00.000' AS DateTime), N'F', N'S', N'CORREA', 667, N'CORDOBA CAPITAL', 166, NULL, NULL, CAST(N'1957-11-13T00:00:00.000' AS DateTime), CAST(N'1961-11-12T00:00:00.000' AS DateTime), 1, 1, 1, CAST(27394.00 AS Decimal(18, 2)), N'correamariaisabel@hotmail.com', NULL, NULL, 1, N'Observacion de CORREA, MARIA ISABEL')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (102, N'VERGARA, HUGO HENRIQUE', 20798967813, N'DNI', 7989678, CAST(N'1946-07-10T00:00:00.000' AS DateTime), N'M', N'C', N'VERGARA', 798, N'CORDOBA CAPITAL', 167, NULL, NULL, CAST(N'1953-07-08T00:00:00.000' AS DateTime), CAST(N'1957-07-07T00:00:00.000' AS DateTime), 0, 1, 0, CAST(20391.00 AS Decimal(18, 2)), N'vergarahugohenrique@gmail.com', NULL, NULL, 1, N'Observacion de VERGARA, HUGO HENRIQUE')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (103, N'ROCHA, CARMEN ILDA', 27245328074, N'CF', 2453280, CAST(N'1937-10-11T00:00:00.000' AS DateTime), N'F', N'Z', N'ROCHA', 245, N'CORDOBA CAPITAL', 168, NULL, NULL, CAST(N'1944-10-09T00:00:00.000' AS DateTime), CAST(N'1948-10-08T00:00:00.000' AS DateTime), 1, 0, 0, CAST(26451.00 AS Decimal(18, 2)), N'rochacarmenilda@hotmail.com', NULL, NULL, 1, N'Observacion de ROCHA, CARMEN ILDA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (104, N'SANCHEZ, RINA', 27606259517, N'CF', 6062595, CAST(N'1948-07-25T00:00:00.000' AS DateTime), N'F', N'S', N'SANCHEZ', 606, N'CORDOBA CAPITAL', 169, NULL, NULL, CAST(N'1955-07-24T00:00:00.000' AS DateTime), CAST(N'1959-07-23T00:00:00.000' AS DateTime), 0, 1, 1, CAST(26544.00 AS Decimal(18, 2)), N'sanchezrina@gmail.com', NULL, NULL, 1, N'Observacion de SANCHEZ, RINA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (105, N'SISTERNA, ANTONIO RAMON', 20650913818, N'CF', 6509138, CAST(N'1939-08-14T00:00:00.000' AS DateTime), N'M', N'S', N'SISTERNA', 650, N'CORDOBA CAPITAL', 170, NULL, NULL, CAST(N'1946-08-12T00:00:00.000' AS DateTime), CAST(N'1950-08-11T00:00:00.000' AS DateTime), 1, 0, 0, CAST(19667.00 AS Decimal(18, 2)), N'sisternaantonioramon@hotmail.com', NULL, NULL, 1, N'Observacion de SISTERNA, ANTONIO RAMON')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (106, N'BENENCIO, TERESA BEATRIZ', 27625715302, N'CP', 6257153, CAST(N'1950-01-06T00:00:00.000' AS DateTime), N'F', N'Z', N'BENENCIO', 625, N'CORDOBA CAPITAL', 171, NULL, NULL, CAST(N'1957-01-04T00:00:00.000' AS DateTime), CAST(N'1961-01-03T00:00:00.000' AS DateTime), 0, 0, 1, CAST(26061.00 AS Decimal(18, 2)), N'benencioteresabeatriz@gmail.com', NULL, NULL, 1, N'Observacion de BENENCIO, TERESA BEATRIZ')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (107, N'FLORES, CARLOS MARTIN', 20794530813, N'CP', 7945308, CAST(N'1945-09-17T00:00:00.000' AS DateTime), N'M', N'C', N'FLORES', 794, N'CORDOBA CAPITAL', 172, NULL, NULL, CAST(N'1952-09-15T00:00:00.000' AS DateTime), CAST(N'1956-09-14T00:00:00.000' AS DateTime), 1, 1, 0, CAST(19434.00 AS Decimal(18, 2)), N'florescarlosmartin@hotmail.com', NULL, NULL, 1, N'Observacion de FLORES, CARLOS MARTIN')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (108, N'HINNI, MARGARITA MARIA', 27639439975, N'CP', 6394399, CAST(N'1951-01-15T00:00:00.000' AS DateTime), N'F', N'Z', N'HINNI', 639, N'CORDOBA CAPITAL', 173, NULL, NULL, CAST(N'1958-01-13T00:00:00.000' AS DateTime), CAST(N'1962-01-12T00:00:00.000' AS DateTime), 0, 1, 1, CAST(25592.00 AS Decimal(18, 2)), N'hinnimargaritamaria@gmail.com', NULL, NULL, 1, N'Observacion de HINNI, MARGARITA MARIA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (109, N'RODRIGUEZ, ROGELIO MANUEL', 20660938255, N'LE', 6609382, CAST(N'1947-04-30T00:00:00.000' AS DateTime), N'M', N'Z', N'RODRIGUEZ', 660, N'CORDOBA CAPITAL', 174, NULL, NULL, CAST(N'1954-04-28T00:00:00.000' AS DateTime), CAST(N'1958-04-27T00:00:00.000' AS DateTime), 1, 1, 0, CAST(18954.00 AS Decimal(18, 2)), N'rodriguezrogeliomanuel@hotmail.com', NULL, NULL, 1, N'Observacion de RODRIGUEZ, ROGELIO MANUEL')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (110, N'MERLO, LEOPOLDO ARGENTINO', 20799258974, N'LE', 7992589, CAST(N'1946-10-25T00:00:00.000' AS DateTime), N'M', N'S', N'MERLO', 799, N'CORDOBA CAPITAL', 175, NULL, NULL, CAST(N'1953-10-23T00:00:00.000' AS DateTime), CAST(N'1957-10-22T00:00:00.000' AS DateTime), 0, 0, 1, CAST(18908.00 AS Decimal(18, 2)), N'merloleopoldoargentino@gmail.com', NULL, NULL, 1, N'Observacion de MERLO, LEOPOLDO ARGENTINO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (111, N'MONGE, TRANSITO ELVIO', 27796426492, N'PSP', 7964264, CAST(N'1941-08-15T00:00:00.000' AS DateTime), N'M', N'C', N'MONGE', 796, N'CORDOBA CAPITAL', 176, NULL, NULL, CAST(N'1948-08-13T00:00:00.000' AS DateTime), CAST(N'1952-08-12T00:00:00.000' AS DateTime), 1, 0, 0, CAST(25041.00 AS Decimal(18, 2)), N'mongetransitoelvio@hotmail.com', NULL, NULL, 1, N'Observacion de MONGE, TRANSITO ELVIO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (112, N'CUELLO, LUIS EDUARDO', 20660789744, N'DNI', 6607897, CAST(N'1946-11-11T00:00:00.000' AS DateTime), N'M', N'Z', N'CUELLO', 660, N'CORDOBA CAPITAL', 177, NULL, NULL, CAST(N'1953-11-09T00:00:00.000' AS DateTime), CAST(N'1957-11-08T00:00:00.000' AS DateTime), 0, 0, 1, CAST(18447.00 AS Decimal(18, 2)), N'cuelloluiseduardo@gmail.com', NULL, NULL, 1, N'Observacion de CUELLO, LUIS EDUARDO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (113, N'GUZMAN, ALFONSO', 20798224784, N'DNI', 7982247, CAST(N'1945-04-02T00:00:00.000' AS DateTime), N'M', N'C', N'GUZMAN', 798, N'CORDOBA CAPITAL', 178, NULL, NULL, CAST(N'1952-03-31T00:00:00.000' AS DateTime), CAST(N'1956-03-30T00:00:00.000' AS DateTime), 1, 0, 1, CAST(18405.00 AS Decimal(18, 2)), N'guzmanalfonso@hotmail.com', NULL, NULL, 1, N'Observacion de GUZMAN, ALFONSO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (114, N'JUHEL, JERONIMO ADRIAN', 20648921354, N'DNI', 6489213, CAST(N'1934-09-30T00:00:00.000' AS DateTime), N'M', N'Z', N'JUHEL', 648, N'CORDOBA CAPITAL', 179, NULL, NULL, CAST(N'1941-09-28T00:00:00.000' AS DateTime), CAST(N'1945-09-27T00:00:00.000' AS DateTime), 0, 0, 1, CAST(18113.00 AS Decimal(18, 2)), N'juheljeronimoadrian@gmail.com', NULL, NULL, 1, N'Observacion de JUHEL, JERONIMO ADRIAN')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (115, N'MONJE, CARLOS HUGO', 20796098477, N'DNI', 7960984, CAST(N'1949-01-11T00:00:00.000' AS DateTime), N'M', N'Z', N'MONJE', 796, N'VILLA ALLENDE', 180, NULL, NULL, CAST(N'1956-01-10T00:00:00.000' AS DateTime), CAST(N'1960-01-09T00:00:00.000' AS DateTime), 1, 1, 0, CAST(18083.00 AS Decimal(18, 2)), N'monjecarloshugo@hotmail.com', NULL, NULL, 1, N'Observacion de MONJE, CARLOS HUGO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (116, N'SALOME, MARIA M', 27500828793, N'DNI', 5008287, CAST(N'1944-12-05T00:00:00.000' AS DateTime), N'F', N'Z', N'SALOME', 500, N'CORDOBA CAPITAL', 181, NULL, NULL, CAST(N'1951-12-04T00:00:00.000' AS DateTime), CAST(N'1955-12-03T00:00:00.000' AS DateTime), 0, 1, 1, CAST(23707.00 AS Decimal(18, 2)), N'salomemariam@gmail.com', NULL, NULL, 1, N'Observacion de SALOME, MARIA M')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (117, N'PUGLIAFITO, BENEDICTA', 27373403542, N'DNI', 3734035, CAST(N'1938-04-05T00:00:00.000' AS DateTime), N'F', N'Z', N'PUGLIAFITO', 373, N'CORDOBA CAPITAL', 182, NULL, NULL, CAST(N'1945-04-03T00:00:00.000' AS DateTime), CAST(N'1949-04-02T00:00:00.000' AS DateTime), 1, 0, 1, CAST(23396.00 AS Decimal(18, 2)), N'pugliafitobenedicta@hotmail.com', NULL, NULL, 1, N'Observacion de PUGLIAFITO, BENEDICTA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (118, N'NUÑEZ, REBECA  INES', 27827215683, N'DNI', 28272156, CAST(N'1980-08-21T00:00:00.000' AS DateTime), N'F', N'Z', N'NUÑEZ', 282, N'SIN ASIGNAR', 183, NULL, NULL, CAST(N'1987-08-20T00:00:00.000' AS DateTime), CAST(N'1991-08-19T00:00:00.000' AS DateTime), 0, 1, 0, CAST(23582.00 AS Decimal(18, 2)), N'nuñezrebecaines@gmail.com', NULL, NULL, 1, N'Observacion de NUÑEZ, REBECA  INES')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (119, N'CAMINO, ELIO PEDRO', 20639061325, N'CF', 6390613, CAST(N'1942-01-28T00:00:00.000' AS DateTime), N'M', N'Z', N'CAMINO', 639, N'SIN ASIGNAR', 184, NULL, NULL, CAST(N'1949-01-26T00:00:00.000' AS DateTime), CAST(N'1953-01-25T00:00:00.000' AS DateTime), 1, 1, 1, CAST(17343.00 AS Decimal(18, 2)), N'caminoeliopedro@hotmail.com', NULL, NULL, 1, N'Observacion de CAMINO, ELIO PEDRO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (120, N'VILLARREAL, MERCEDES EULOGIA', 27463763024, N'CF', 4637630, CAST(N'1943-09-11T00:00:00.000' AS DateTime), N'F', N'S', N'VILLARREAL', 463, N'CORDOBA CAPITAL', 185, NULL, NULL, CAST(N'1950-09-09T00:00:00.000' AS DateTime), CAST(N'1954-09-08T00:00:00.000' AS DateTime), 0, 0, 0, CAST(22886.00 AS Decimal(18, 2)), N'villarrealmercedeseulogia@gmail.com', NULL, NULL, 1, N'Observacion de VILLARREAL, MERCEDES EULOGIA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (121, N'MURUA, NORMA FELIPA', 27569840095, N'CF', 5698400, CAST(N'1948-02-17T00:00:00.000' AS DateTime), N'F', N'Z', N'MURUA', 569, N'CORDOBA CAPITAL', 186, NULL, NULL, CAST(N'1955-02-15T00:00:00.000' AS DateTime), CAST(N'1959-02-14T00:00:00.000' AS DateTime), 1, 1, 0, CAST(22784.00 AS Decimal(18, 2)), N'muruanormafelipa@hotmail.com', NULL, NULL, 1, N'Observacion de MURUA, NORMA FELIPA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (122, N'ROJAS, LUCIA ROSA', 27543148637, N'CP', 5431486, CAST(N'1945-11-13T00:00:00.000' AS DateTime), N'F', N'Z', N'ROJAS', 543, N'CORDOBA CAPITAL', 187, NULL, NULL, CAST(N'1952-11-11T00:00:00.000' AS DateTime), CAST(N'1956-11-10T00:00:00.000' AS DateTime), 0, 1, 0, CAST(22576.00 AS Decimal(18, 2)), N'rojasluciarosa@gmail.com', NULL, NULL, 1, N'Observacion de ROJAS, LUCIA ROSA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (123, N'BULACIO, MARTA ADELMA', 27426143575, N'CP', 4261435, CAST(N'1941-07-22T00:00:00.000' AS DateTime), N'F', N'Z', N'BULACIO', 426, N'CORDOBA CAPITAL', 188, NULL, NULL, CAST(N'1948-07-20T00:00:00.000' AS DateTime), CAST(N'1952-07-19T00:00:00.000' AS DateTime), 1, 1, 1, CAST(22297.00 AS Decimal(18, 2)), N'bulaciomartaadelma@hotmail.com', NULL, NULL, 1, N'Observacion de BULACIO, MARTA ADELMA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (124, N'LOPEZ, JOSE VITALINO', 20796683477, N'CP', 7966834, CAST(N'1941-09-20T00:00:00.000' AS DateTime), N'M', N'Z', N'LOPEZ', 796, N'SIN ASIGNAR', 189, NULL, NULL, CAST(N'1948-09-18T00:00:00.000' AS DateTime), CAST(N'1952-09-17T00:00:00.000' AS DateTime), 0, 1, 0, CAST(16771.00 AS Decimal(18, 2)), N'lopezjosevitalino@gmail.com', NULL, NULL, 1, N'Observacion de LOPEZ, JOSE VITALINO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (125, N'PALMA, GLADYS MAGDALENA', 27639765854, N'LE', 6397658, CAST(N'1950-08-11T00:00:00.000' AS DateTime), N'F', N'Z', N'PALMA', 639, N'SIN ASIGNAR', 190, NULL, NULL, CAST(N'1957-08-09T00:00:00.000' AS DateTime), CAST(N'1961-08-08T00:00:00.000' AS DateTime), 1, 0, 0, CAST(22111.00 AS Decimal(18, 2)), N'palmagladysmagdalena@hotmail.com', NULL, NULL, 1, N'Observacion de PALMA, GLADYS MAGDALENA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (126, N'MOLINA, RUMUALDA MERCEDES', 27528379907, N'LE', 5283799, CAST(N'1946-02-05T00:00:00.000' AS DateTime), N'F', N'S', N'MOLINA', 528, N'CORDOBA CAPITAL', 191, NULL, NULL, CAST(N'1953-02-03T00:00:00.000' AS DateTime), CAST(N'1957-02-02T00:00:00.000' AS DateTime), 0, 1, 1, CAST(21847.00 AS Decimal(18, 2)), N'molinarumualdamercedes@gmail.com', NULL, NULL, 1, N'Observacion de MOLINA, RUMUALDA MERCEDES')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (127, N'MOYA, AMELIA', 27527951204, N'PSP', 5279512, CAST(N'1945-10-12T00:00:00.000' AS DateTime), N'F', N'S', N'MOYA', 527, N'CORDOBA CAPITAL', 192, NULL, NULL, CAST(N'1952-10-10T00:00:00.000' AS DateTime), CAST(N'1956-10-09T00:00:00.000' AS DateTime), 1, 0, 0, CAST(21675.00 AS Decimal(18, 2)), N'moyaamelia@hotmail.com', NULL, NULL, 1, N'Observacion de MOYA, AMELIA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (128, N'SORIA, MANUEL S', 20644479808, N'DNI', 6444798, CAST(N'1946-06-20T00:00:00.000' AS DateTime), N'M', N'Z', N'SORIA', 644, N'CORDOBA CAPITAL', 193, NULL, NULL, CAST(N'1953-06-18T00:00:00.000' AS DateTime), CAST(N'1957-06-17T00:00:00.000' AS DateTime), 0, 0, 0, CAST(16128.00 AS Decimal(18, 2)), N'soriamanuels@gmail.com', NULL, NULL, 1, N'Observacion de SORIA, MANUEL S')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (129, N'FIGUEROA, ANA DEL VALLE', 27572004944, N'DNI', 5720049, CAST(N'1948-06-13T00:00:00.000' AS DateTime), N'F', N'Z', N'FIGUEROA', 572, N'CORDOBA CAPITAL', 194, NULL, NULL, CAST(N'1955-06-12T00:00:00.000' AS DateTime), CAST(N'1959-06-11T00:00:00.000' AS DateTime), 1, 0, 1, CAST(21373.00 AS Decimal(18, 2)), N'figueroaanadelvalle@hotmail.com', NULL, NULL, 1, N'Observacion de FIGUEROA, ANA DEL VALLE')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (130, N'CORVALAN, TOMAS DE LA CRUZ', 20817933107, N'DNI', 8179331, CAST(N'1946-12-28T00:00:00.000' AS DateTime), N'M', N'Z', N'CORVALAN', 817, N'CORDOBA CAPITAL', 195, NULL, NULL, CAST(N'1953-12-26T00:00:00.000' AS DateTime), CAST(N'1957-12-25T00:00:00.000' AS DateTime), 0, 1, 1, CAST(16013.00 AS Decimal(18, 2)), N'corvalantomasdelacruz@gmail.com', NULL, NULL, 1, N'Observacion de CORVALAN, TOMAS DE LA CRUZ')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (131, N'BUSTO, FRANCISCA NILDA', 27602553644, N'DNI', 6025536, CAST(N'1949-10-07T00:00:00.000' AS DateTime), N'F', N'S', N'BUSTO', 602, N'CORDOBA CAPITAL', 196, NULL, NULL, CAST(N'1956-10-05T00:00:00.000' AS DateTime), CAST(N'1960-10-04T00:00:00.000' AS DateTime), 1, 0, 0, CAST(21070.00 AS Decimal(18, 2)), N'bustofranciscanilda@hotmail.com', NULL, NULL, 1, N'Observacion de BUSTO, FRANCISCA NILDA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (132, N'SARMIENTO, MARGARITA DEL MILAGRO', 27525818643, N'DNI', 5258186, CAST(N'1945-10-07T00:00:00.000' AS DateTime), N'F', N'Z', N'SARMIENTO', 525, N'SIN ASIGNAR', 197, NULL, NULL, CAST(N'1952-10-05T00:00:00.000' AS DateTime), CAST(N'1956-10-04T00:00:00.000' AS DateTime), 0, 1, 0, CAST(20852.00 AS Decimal(18, 2)), N'sarmientomargaritadelmilagro@gmail.com', NULL, NULL, 1, N'Observacion de SARMIENTO, MARGARITA DEL MILAGRO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (133, N'GALINDEZ, OSCAR', 20650794118, N'DNI', 6507941, CAST(N'1938-08-17T00:00:00.000' AS DateTime), N'M', N'Z', N'GALINDEZ', 650, N'CORDOBA CAPITAL', 198, NULL, NULL, CAST(N'1945-08-15T00:00:00.000' AS DateTime), CAST(N'1949-08-14T00:00:00.000' AS DateTime), 1, 0, 1, CAST(15526.00 AS Decimal(18, 2)), N'galindezoscar@hotmail.com', NULL, NULL, 1, N'Observacion de GALINDEZ, OSCAR')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (134, N'SCAMPONE, ALEJANDRO PLACIDO', 20779897318, N'DNI', 7798973, CAST(N'1949-05-27T00:00:00.000' AS DateTime), N'M', N'S', N'SCAMPONE', 779, N'CORDOBA CAPITAL', 199, NULL, NULL, CAST(N'1956-05-25T00:00:00.000' AS DateTime), CAST(N'1960-05-24T00:00:00.000' AS DateTime), 0, 0, 1, CAST(15507.00 AS Decimal(18, 2)), N'scamponealejandroplacido@gmail.com', NULL, NULL, 1, N'Observacion de SCAMPONE, ALEJANDRO PLACIDO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (135, N'FERNANDEZ, CARMEN ANIBAL', 20643830280, N'CF', 6438302, CAST(N'1943-11-14T00:00:00.000' AS DateTime), N'M', N'C', N'FERNANDEZ', 643, N'SIN ASIGNAR', 65, NULL, NULL, CAST(N'1950-11-12T00:00:00.000' AS DateTime), CAST(N'1954-11-11T00:00:00.000' AS DateTime), 1, 0, 0, CAST(15291.00 AS Decimal(18, 2)), N'fernandezcarmenanibal@hotmail.com', NULL, NULL, 1, N'Observacion de FERNANDEZ, CARMEN ANIBAL')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (136, N'NIETO, ANGEL ANTONIO', 20669914596, N'CF', 6699145, CAST(N'1944-01-22T00:00:00.000' AS DateTime), N'M', N'Z', N'NIETO', 669, N'CORDOBA CAPITAL', 66, NULL, NULL, CAST(N'1951-01-20T00:00:00.000' AS DateTime), CAST(N'1955-01-19T00:00:00.000' AS DateTime), 0, 0, 1, CAST(15198.00 AS Decimal(18, 2)), N'nietoangelantonio@gmail.com', NULL, NULL, 1, N'Observacion de NIETO, ANGEL ANTONIO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (137, N'VIGNOLA, RUBEN BENITO', 20643159284, N'CF', 6431592, CAST(N'1941-01-12T00:00:00.000' AS DateTime), N'M', N'Z', N'VIGNOLA', 643, N'SAN FRANCISCO', 67, NULL, NULL, CAST(N'1948-01-11T00:00:00.000' AS DateTime), CAST(N'1952-01-10T00:00:00.000' AS DateTime), 1, 0, 0, CAST(15067.00 AS Decimal(18, 2)), N'vignolarubenbenito@hotmail.com', NULL, NULL, 1, N'Observacion de VIGNOLA, RUBEN BENITO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (138, N'ACHERVI, MIGUEL ANGELA7', 20644355407, N'DNI', 64435540, CAST(N'1975-08-01T03:00:00.000' AS DateTime), N'F', N'C', N'ACHERVI', 644, N'santa rosa de calamuchita', 68, 4, 1, CAST(N'1958-03-06T03:00:00.000' AS DateTime), NULL, 0, 1, 0, CAST(14957.00 AS Decimal(18, 2)), N'achervimiguelangel7@gmail.com', NULL, NULL, 1, N'Observacion de ACHERVI, MIGUEL ANGELA7')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (139, N'LENCINAS, CARLOS ALBERTO', 20642881228, N'CP', 6428812, CAST(N'1939-08-29T00:00:00.000' AS DateTime), N'M', N'Z', N'LENCINAS', 642, N'SAN FRANCISCO', 69, NULL, NULL, CAST(N'1946-08-27T00:00:00.000' AS DateTime), CAST(N'1950-08-26T00:00:00.000' AS DateTime), 1, 0, 0, CAST(14850.00 AS Decimal(18, 2)), N'lencinascarlosalberto@hotmail.com', NULL, NULL, 1, N'Observacion de LENCINAS, CARLOS ALBERTO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (140, N'NADALIN, OTILIO JOSE', 20642603253, N'CP', 6426032, CAST(N'1938-08-02T00:00:00.000' AS DateTime), N'M', N'C', N'NADALIN', 642, N'MARULL', 70, NULL, NULL, CAST(N'1945-07-31T00:00:00.000' AS DateTime), CAST(N'1949-07-30T00:00:00.000' AS DateTime), 0, 1, 0, CAST(14744.00 AS Decimal(18, 2)), N'nadalinotiliojose@gmail.com', NULL, NULL, 1, N'Observacion de NADALIN, OTILIO JOSE')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (141, N'MOYANO, RAUL ANTONIO', 23643455591, N'LE', 6434555, CAST(N'1942-04-03T00:00:00.000' AS DateTime), N'M', N'S', N'MOYANO', 643, N'SAN FRANCISCO', 71, NULL, NULL, CAST(N'1949-04-01T00:00:00.000' AS DateTime), CAST(N'1953-03-31T00:00:00.000' AS DateTime), 1, 1, 1, CAST(16768.00 AS Decimal(18, 2)), N'moyanoraulantonio@hotmail.com', NULL, NULL, 1, N'Observacion de MOYANO, RAUL ANTONIO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (142, N'MOYANO, LINA MARGARITA', 27878539487, N'LE', 8785394, CAST(N'1939-12-10T00:00:00.000' AS DateTime), N'F', N'Z', N'MOYANO', 878, N'CORDOBA CAPITAL', 72, NULL, NULL, CAST(N'1946-12-08T00:00:00.000' AS DateTime), CAST(N'1950-12-07T00:00:00.000' AS DateTime), 0, 1, 0, CAST(19632.00 AS Decimal(18, 2)), N'moyanolinamargarita@gmail.com', NULL, NULL, 1, N'Observacion de MOYANO, LINA MARGARITA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (143, N'ABRATE, VICENTE BARTOLO', 20642466677, N'CF', 64246667, CAST(N'1943-08-08T04:00:00.000' AS DateTime), N'M', N'Z', N'ABRATE', 642, N'cordoba capital', 73, 5, 1, CAST(N'1944-08-08T03:00:00.000' AS DateTime), CAST(N'2020-10-01T03:00:00.000' AS DateTime), 1, 1, 1, CAST(123.54 AS Decimal(18, 2)), N'abratevicentebartolo@hotmail.com', NULL, NULL, 1, N'Observacion de ABRATE, VICENTE BARTOLO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (144, N'MEZ, JUAN CARLOS', 20794386868, N'DNI', 7943868, CAST(N'1945-03-03T00:00:00.000' AS DateTime), N'M', N'Z', N'MEZ', 794, N'SAN FRANCISCO', 74, NULL, NULL, CAST(N'1952-03-01T00:00:00.000' AS DateTime), CAST(N'1956-02-29T00:00:00.000' AS DateTime), 0, 0, 0, CAST(14440.00 AS Decimal(18, 2)), N'MEZjuancarlos@gmail.com', NULL, NULL, 1, N'Observacion de MEZ, JUAN CARLOS')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (145, N'HEREDIA, SARA ANTONIA', 27375677155, N'DNI', 3756771, CAST(N'1938-08-10T00:00:00.000' AS DateTime), N'F', N'S', N'HEREDIA', 375, N'CORDOBA CAPITAL', 75, NULL, NULL, CAST(N'1945-08-08T00:00:00.000' AS DateTime), CAST(N'1949-08-07T00:00:00.000' AS DateTime), 1, 1, 1, CAST(18879.00 AS Decimal(18, 2)), N'herediasaraantonia@hotmail.com', NULL, NULL, 1, N'Observacion de HEREDIA, SARA ANTONIA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (146, N'CONTRERAS, NORMA BEATRIZ', 22078512775, N'DNI', 20785127, CAST(N'1969-08-08T00:00:00.000' AS DateTime), N'F', N'S', N'CONTRERAS', 207, N'CORDOBA CAPITAL', 76, NULL, NULL, CAST(N'1976-08-06T00:00:00.000' AS DateTime), CAST(N'1980-08-05T00:00:00.000' AS DateTime), 0, 1, 1, CAST(15122.00 AS Decimal(18, 2)), N'contrerasnormabeatriz@gmail.com', NULL, NULL, 1, N'Observacion de CONTRERAS, NORMA BEATRIZ')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (147, N'ORTEGA, JESUS', 20841075742, N'DNI', 8410757, CAST(N'1948-12-25T00:00:00.000' AS DateTime), N'M', N'Z', N'ORTEGA', 841, N'CORDOBA CAPITAL', 77, NULL, NULL, CAST(N'1955-12-24T00:00:00.000' AS DateTime), CAST(N'1959-12-23T00:00:00.000' AS DateTime), 1, 0, 1, CAST(14177.00 AS Decimal(18, 2)), N'ortegajesus@hotmail.com', NULL, NULL, 1, N'Observacion de ORTEGA, JESUS')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (148, N'DOMINGUEZ, AURORA', 27597976334, N'DNI', 5979763, CAST(N'1949-12-21T00:00:00.000' AS DateTime), N'F', N'C', N'DOMINGUEZ', 597, N'CORDOBA CAPITAL', 78, NULL, NULL, CAST(N'1956-12-19T00:00:00.000' AS DateTime), CAST(N'1960-12-18T00:00:00.000' AS DateTime), 0, 0, 1, CAST(18647.00 AS Decimal(18, 2)), N'dominguezaurora@gmail.com', NULL, NULL, 1, N'Observacion de DOMINGUEZ, AURORA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (149, N'SILVESTRE, CRISTINA BARBARIANA', 27599597651, N'DNI', 5995976, CAST(N'1949-06-25T00:00:00.000' AS DateTime), N'F', N'Z', N'SILVESTRE', 599, N'CORDOBA CAPITAL', 79, NULL, NULL, CAST(N'1956-06-23T00:00:00.000' AS DateTime), CAST(N'1960-06-22T00:00:00.000' AS DateTime), 1, 1, 0, CAST(18523.00 AS Decimal(18, 2)), N'silvestrecristinabarbariana@hotmail.com', NULL, NULL, 1, N'Observacion de SILVESTRE, CRISTINA BARBARIANA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (150, N'SANTOS, ESTELA ELVIRA', 27545233814, N'DNI', 5452338, CAST(N'1946-10-11T00:00:00.000' AS DateTime), N'F', N'Z', N'SANTOS', 545, N'CORDOBA CAPITAL', 80, NULL, NULL, CAST(N'1953-10-09T00:00:00.000' AS DateTime), CAST(N'1957-10-08T00:00:00.000' AS DateTime), 0, 0, 0, CAST(18363.00 AS Decimal(18, 2)), N'santosestelaelvira@gmail.com', NULL, NULL, 1, N'Observacion de SANTOS, ESTELA ELVIRA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (151, N'BARRIONUEVO, AIDA NORA', 27562853226, N'CF', 5628532, CAST(N'1948-01-01T00:00:00.000' AS DateTime), N'F', N'S', N'BARRIONUEVO', 562, N'CORDOBA CAPITAL', 81, NULL, NULL, CAST(N'1954-12-30T00:00:00.000' AS DateTime), CAST(N'1958-12-29T00:00:00.000' AS DateTime), 1, 0, 0, CAST(18253.00 AS Decimal(18, 2)), N'barrionuevoaidanora@hotmail.com', NULL, NULL, 1, N'Observacion de BARRIONUEVO, AIDA NORA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (152, N'GUZMAN, RAMONA  ESPERANZA', 27613276288, N'CF', 6132762, CAST(N'1949-11-14T00:00:00.000' AS DateTime), N'F', N'Z', N'GUZMAN', 613, N'CORDOBA CAPITAL', 82, NULL, NULL, CAST(N'1956-11-12T00:00:00.000' AS DateTime), CAST(N'1960-11-11T00:00:00.000' AS DateTime), 0, 0, 0, CAST(18166.00 AS Decimal(18, 2)), N'guzmanramonaesperanza@gmail.com', NULL, NULL, 1, N'Observacion de GUZMAN, RAMONA  ESPERANZA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (153, N'VIDELA, CONSTANCIA', 27569816976, N'CF', 5698169, CAST(N'1947-11-21T00:00:00.000' AS DateTime), N'F', N'Z', N'VIDELA', 569, N'CORDOBA CAPITAL', 83, NULL, NULL, CAST(N'1954-11-19T00:00:00.000' AS DateTime), CAST(N'1958-11-18T00:00:00.000' AS DateTime), 1, 0, 1, CAST(18019.00 AS Decimal(18, 2)), N'videlaconstancia@hotmail.com', NULL, NULL, 1, N'Observacion de VIDELA, CONSTANCIA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (154, N'CONTRERAS, MIRTA NILDA', 27558722823, N'CP', 5587228, CAST(N'1947-02-08T00:00:00.000' AS DateTime), N'F', N'S', N'CONTRERAS', 558, N'CORDOBA CAPITAL', 84, NULL, NULL, CAST(N'1954-02-06T00:00:00.000' AS DateTime), CAST(N'1958-02-05T00:00:00.000' AS DateTime), 0, 1, 0, CAST(17895.00 AS Decimal(18, 2)), N'contrerasmirtanilda@gmail.com', NULL, NULL, 1, N'Observacion de CONTRERAS, MIRTA NILDA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (155, N'LESCANO, TEODORA ANTONIA', 27568904117, N'CP', 5689041, CAST(N'1948-06-06T00:00:00.000' AS DateTime), N'F', N'Z', N'LESCANO', 568, N'SIN ASIGNAR', 85, NULL, NULL, CAST(N'1955-06-05T00:00:00.000' AS DateTime), CAST(N'1959-06-04T00:00:00.000' AS DateTime), 1, 1, 1, CAST(17786.00 AS Decimal(18, 2)), N'lescanoteodoraantonia@hotmail.com', NULL, NULL, 1, N'Observacion de LESCANO, TEODORA ANTONIA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (156, N'BAIGORRIA, BLANCA ALICIA', 27170068678, N'CP', 1700686, CAST(N'1964-10-25T00:00:00.000' AS DateTime), N'F', N'Z', N'BAIGORRIA', 170, N'CORDOBA CAPITAL', 86, NULL, NULL, CAST(N'1971-10-24T00:00:00.000' AS DateTime), CAST(N'1975-10-23T00:00:00.000' AS DateTime), 0, 0, 0, CAST(17416.00 AS Decimal(18, 2)), N'baigorriablancaalicia@gmail.com', NULL, NULL, 1, N'Observacion de BAIGORRIA, BLANCA ALICIA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (157, N'PERALTA, HILARIA LEONIDES', 23445898345, N'LE', 4458983, CAST(N'1944-01-14T00:00:00.000' AS DateTime), N'F', N'Z', N'PERALTA', 445, N'CORDOBA CAPITAL', 87, NULL, NULL, CAST(N'1951-01-12T00:00:00.000' AS DateTime), CAST(N'1955-01-11T00:00:00.000' AS DateTime), 1, 1, 1, CAST(14933.00 AS Decimal(18, 2)), N'peraltahilarialeonides@hotmail.com', NULL, NULL, 1, N'Observacion de PERALTA, HILARIA LEONIDES')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (158, N'FERREYRA, MARIA INES', 23649485040, N'LE', 6494850, CAST(N'1951-04-01T00:00:00.000' AS DateTime), N'F', N'Z', N'FERREYRA', 649, N'CORDOBA CAPITAL', 88, NULL, NULL, CAST(N'1958-03-30T00:00:00.000' AS DateTime), CAST(N'1962-03-29T00:00:00.000' AS DateTime), 0, 0, 0, CAST(14968.00 AS Decimal(18, 2)), N'ferreyramariaines@gmail.com', NULL, NULL, 1, N'Observacion de FERREYRA, MARIA INES')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (159, N'ROMERO, PETRONA ROSA', 27544339517, N'PSP', 5443395, CAST(N'1947-12-04T00:00:00.000' AS DateTime), N'F', N'Z', N'ROMERO', 544, N'QUILINO', 89, NULL, NULL, CAST(N'1954-12-02T00:00:00.000' AS DateTime), CAST(N'1958-12-01T00:00:00.000' AS DateTime), 1, 1, 1, CAST(17323.00 AS Decimal(18, 2)), N'romeropetronarosa@hotmail.com', NULL, NULL, 1, N'Observacion de ROMERO, PETRONA ROSA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (160, N'SANCHEZ, RAMON ESTEBAN', 20795728972, N'DNI', 7957289, CAST(N'1947-02-13T00:00:00.000' AS DateTime), N'M', N'Z', N'SANCHEZ', 795, N'QUILINO', 90, NULL, NULL, CAST(N'1954-02-11T00:00:00.000' AS DateTime), CAST(N'1958-02-10T00:00:00.000' AS DateTime), 0, 0, 1, CAST(12997.00 AS Decimal(18, 2)), N'sanchezramonesteban@gmail.com', NULL, NULL, 1, N'Observacion de SANCHEZ, RAMON ESTEBAN')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (161, N'DROZCO, RAMONA ELISA', 27595652016, N'DNI', 5956520, CAST(N'1949-12-30T00:00:00.000' AS DateTime), N'F', N'S', N'DROZCO', 595, N'VILLA VALERIA', 91, NULL, NULL, CAST(N'1956-12-28T00:00:00.000' AS DateTime), CAST(N'1960-12-27T00:00:00.000' AS DateTime), 1, 0, 0, CAST(17140.00 AS Decimal(18, 2)), N'drozcoramonaelisa@hotmail.com', NULL, NULL, 1, N'Observacion de DROZCO, RAMONA ELISA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (162, N'SUAREZ, ANGELA LUCRECIA', 27625386148, N'DNI', 6253861, CAST(N'1949-12-18T00:00:00.000' AS DateTime), N'F', N'Z', N'SUAREZ', 625, N'QUILINO', 92, NULL, NULL, CAST(N'1956-12-16T00:00:00.000' AS DateTime), CAST(N'1960-12-15T00:00:00.000' AS DateTime), 0, 0, 1, CAST(17052.00 AS Decimal(18, 2)), N'suarezangelalucrecia@gmail.com', NULL, NULL, 1, N'Observacion de SUAREZ, ANGELA LUCRECIA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (163, N'VILLALBA, BENICIA DEOLINDA', 27496876004, N'DNI', 4968760, CAST(N'1945-09-23T00:00:00.000' AS DateTime), N'F', N'C', N'VILLALBA', 496, N'QUILINO', 93, NULL, NULL, CAST(N'1952-09-21T00:00:00.000' AS DateTime), CAST(N'1956-09-20T00:00:00.000' AS DateTime), 1, 0, 0, CAST(16869.00 AS Decimal(18, 2)), N'villalbabeniciadeolinda@hotmail.com', NULL, NULL, 1, N'Observacion de VILLALBA, BENICIA DEOLINDA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (164, N'ROTH, MARIA ESTELA', 23466879145, N'DNI', 4668791, CAST(N'1945-11-11T00:00:00.000' AS DateTime), N'F', N'S', N'ROTH', 466, N'SUCO', 94, NULL, NULL, CAST(N'1952-11-09T00:00:00.000' AS DateTime), CAST(N'1956-11-08T00:00:00.000' AS DateTime), 0, 1, 1, CAST(14309.00 AS Decimal(18, 2)), N'rothmariaestela@gmail.com', NULL, NULL, 1, N'Observacion de ROTH, MARIA ESTELA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (165, N'GOGONZALES, OLGA CRISTINA', 27574258522, N'DNI', 5742585, CAST(N'1948-04-24T00:00:00.000' AS DateTime), N'F', N'C', N'GOGONZALES', 574, N'CRUZ DEL EJE', 95, NULL, NULL, CAST(N'1955-04-23T00:00:00.000' AS DateTime), CAST(N'1959-04-22T00:00:00.000' AS DateTime), 1, 0, 1, CAST(16711.00 AS Decimal(18, 2)), N'goGONZALESolgacristina@hotmail.com', NULL, NULL, 1, N'Observacion de GONZALES, OLGA CRISTINA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (166, N'SAINT, GIRONS MIRTA', 27474216546, N'DNI', 4742165, CAST(N'1944-03-08T00:00:00.000' AS DateTime), N'F', N'C', N'SAINT', 474, N'COSQUIN', 96, NULL, NULL, CAST(N'1951-03-07T00:00:00.000' AS DateTime), CAST(N'1955-03-06T00:00:00.000' AS DateTime), 0, 0, 1, CAST(16550.00 AS Decimal(18, 2)), N'saintgironsmirta@gmail.com', NULL, NULL, 1, N'Observacion de SAINT, GIRONS MIRTA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (167, N'GALLARDO, VALERIA ISABEL', 27517863655, N'CF', 5178636, CAST(N'1946-05-05T00:00:00.000' AS DateTime), N'F', N'V', N'GALLARDO', 517, N'LUYABA', 97, NULL, NULL, CAST(N'1953-05-03T00:00:00.000' AS DateTime), CAST(N'1957-05-02T00:00:00.000' AS DateTime), 1, 1, 0, CAST(16477.00 AS Decimal(18, 2)), N'gallardovaleriaisabel@hotmail.com', NULL, NULL, 1, N'Observacion de GALLARDO, VALERIA ISABEL')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (168, N'ALVAREZ, AIDA MIRIAN', 23636575146, N'CF', 6365751, CAST(N'1950-08-01T00:00:00.000' AS DateTime), N'F', N'C', N'ALVAREZ', 636, N'CORDOBA CAPITAL', 98, NULL, NULL, CAST(N'1957-07-30T00:00:00.000' AS DateTime), CAST(N'1961-07-29T00:00:00.000' AS DateTime), 0, 0, 1, CAST(14069.00 AS Decimal(18, 2)), N'alvarezaidamirian@gmail.com', NULL, NULL, 1, N'Observacion de ALVAREZ, AIDA MIRIAN')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (169, N'ALTAMIRA, YOLANDA NELIDA', 27581850731, N'CF', 5818507, CAST(N'1948-09-30T00:00:00.000' AS DateTime), N'F', N'S', N'ALTAMIRA', 581, N'CORDOBA CAPITAL', 99, NULL, NULL, CAST(N'1955-09-29T00:00:00.000' AS DateTime), CAST(N'1959-09-28T00:00:00.000' AS DateTime), 1, 1, 1, CAST(16320.00 AS Decimal(18, 2)), N'altamirayolandanelida@hotmail.com', NULL, NULL, 1, N'Observacion de ALTAMIRA, YOLANDA NELIDA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (170, N'VILLAFAÑE, DAVID CARLOS', 23857867091, N'CP', 8578670, CAST(N'1951-08-09T00:00:00.000' AS DateTime), N'M', N'S', N'VILLAFAÑE', 857, N'RIO TERCERO', 100, NULL, NULL, CAST(N'1958-08-07T00:00:00.000' AS DateTime), CAST(N'1962-08-06T00:00:00.000' AS DateTime), 0, 1, 0, CAST(14034.00 AS Decimal(18, 2)), N'villafañedavidcarlos@gmail.com', NULL, NULL, 1, N'Observacion de VILLAFAÑE, DAVID CARLOS')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (171, N'PAEZ, MARTINA NILDA', 27546182853, N'CP', 5461828, CAST(N'1947-01-30T00:00:00.000' AS DateTime), N'F', N'S', N'PAEZ', 546, N'CORDOBA CAPITAL', 101, NULL, NULL, CAST(N'1954-01-28T00:00:00.000' AS DateTime), CAST(N'1958-01-27T00:00:00.000' AS DateTime), 1, 1, 0, CAST(16108.00 AS Decimal(18, 2)), N'paezmartinanilda@hotmail.com', NULL, NULL, 1, N'Observacion de PAEZ, MARTINA NILDA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (172, N'LAFON, MIGUEL ANGEL', 20799244675, N'CP', 7992446, CAST(N'1946-12-31T00:00:00.000' AS DateTime), N'M', N'Z', N'LAFON', 799, N'CORDOBA CAPITAL', 102, NULL, NULL, CAST(N'1953-12-29T00:00:00.000' AS DateTime), CAST(N'1957-12-28T00:00:00.000' AS DateTime), 0, 1, 0, CAST(12092.00 AS Decimal(18, 2)), N'lafonmiguelangel@gmail.com', NULL, NULL, 1, N'Observacion de LAFON, MIGUEL ANGEL')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (173, N'ROMERO, MARIA ANA', 23634210148, N'LE', 6342101, CAST(N'1941-08-03T00:00:00.000' AS DateTime), N'F', N'Z', N'ROMERO', 634, N'CORDOBA CAPITAL', 103, NULL, NULL, CAST(N'1948-08-01T00:00:00.000' AS DateTime), CAST(N'1952-07-31T00:00:00.000' AS DateTime), 1, 0, 1, CAST(13661.00 AS Decimal(18, 2)), N'romeromariaana@hotmail.com', NULL, NULL, 1, N'Observacion de ROMERO, MARIA ANA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (174, N'ACOSTA, LIDIA', 27606233553, N'LE', 6062335, CAST(N'1949-07-01T00:00:00.000' AS DateTime), N'F', N'Z', N'ACOSTA', 606, N'LA PUERTA', 104, NULL, NULL, CAST(N'1956-06-29T00:00:00.000' AS DateTime), CAST(N'1960-06-28T00:00:00.000' AS DateTime), 0, 1, 1, CAST(15865.00 AS Decimal(18, 2)), N'acostalidia@gmail.com', NULL, NULL, 0, N'Observacion de ACOSTA, LIDIA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (175, N'CHAVEZ, BLANCA GRACIELA', 27160820537, N'PSP', 1608205, CAST(N'1962-10-24T00:00:00.000' AS DateTime), N'F', N'Z', N'CHAVEZ', 160, N'CORDOBA CAPITAL', 105, NULL, NULL, CAST(N'1969-10-22T00:00:00.000' AS DateTime), CAST(N'1973-10-21T00:00:00.000' AS DateTime), 1, 1, 1, CAST(15520.00 AS Decimal(18, 2)), N'chavezblancagraciela@hotmail.com', NULL, NULL, 1, N'Observacion de CHAVEZ, BLANCA GRACIELA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (176, N'GONZALEZ, EDUARDO FELICIANO', 20634808364, N'DNI', 6348083, CAST(N'1940-08-16T00:00:00.000' AS DateTime), N'M', N'C', N'NZALEZ', 634, N'BIALET MASSE', 106, NULL, NULL, CAST(N'1947-08-15T00:00:00.000' AS DateTime), CAST(N'1951-08-14T00:00:00.000' AS DateTime), 0, 0, 1, CAST(11724.00 AS Decimal(18, 2)), N'gonzalezeduardofeliciano@gmail.com', NULL, NULL, 1, N'Observacion de NZALEZ, EDUARDO FELICIANO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (177, N'GONZALEZ, NICOLAS RAMON', 20763904116, N'DNI', 7639041, CAST(N'1949-09-18T00:00:00.000' AS DateTime), N'M', N'Z', N'NZALEZ', 763, N'BIALET MASSE', 107, NULL, NULL, CAST(N'1956-09-16T00:00:00.000' AS DateTime), CAST(N'1960-09-15T00:00:00.000' AS DateTime), 1, 0, 1, CAST(11731.00 AS Decimal(18, 2)), N'gonzaleznicolasramon@hotmail.com', NULL, NULL, 1, N'Observacion de NZALEZ, NICOLAS RAMON')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (178, N'ROBLEDO, ELIDA MABEL', 27426669265, N'DNI', 4266692, CAST(N'1971-01-01T00:00:00.000' AS DateTime), N'F', N'Z', N'ROBLEDO', 426, N'GENERAL LEVALLE', 108, NULL, NULL, CAST(N'1977-12-30T00:00:00.000' AS DateTime), CAST(N'1981-12-29T00:00:00.000' AS DateTime), 0, 1, 0, CAST(15408.00 AS Decimal(18, 2)), N'robledoelidamabel@gmail.com', NULL, NULL, 1, N'Observacion de ROBLEDO, ELIDA MABEL')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (179, N'SALDAÑO, MARIA LIDIA', 27546207783, N'DNI', 5462077, CAST(N'1947-02-19T00:00:00.000' AS DateTime), N'F', N'Z', N'SALDAÑO', 546, N'CORDOBA CAPITAL', 109, NULL, NULL, CAST(N'1954-02-17T00:00:00.000' AS DateTime), CAST(N'1958-02-16T00:00:00.000' AS DateTime), 1, 1, 1, CAST(15388.00 AS Decimal(18, 2)), N'saldañomarialidia@hotmail.com', NULL, NULL, 1, N'Observacion de SALDAÑO, MARIA LIDIA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (180, N'RIVERO, PASTORA', 21400512974, N'DNI', 14005129, CAST(N'1960-10-27T00:00:00.000' AS DateTime), N'F', N'Z', N'RIVERO', 140, N'BIALET MASSE', 110, NULL, NULL, CAST(N'1967-10-26T00:00:00.000' AS DateTime), CAST(N'1971-10-25T00:00:00.000' AS DateTime), 0, 0, 1, CAST(11889.00 AS Decimal(18, 2)), N'riveropastora@gmail.com', NULL, NULL, 1, N'Observacion de RIVERO, PASTORA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (181, N'LAZO, JUAN', 20655653400, N'DNI', 6556534, CAST(N'1945-09-16T00:00:00.000' AS DateTime), N'M', N'Z', N'LAZO', 655, N'CHILIBROSTE', 111, NULL, NULL, CAST(N'1952-09-14T00:00:00.000' AS DateTime), CAST(N'1956-09-13T00:00:00.000' AS DateTime), 1, 0, 0, CAST(11411.00 AS Decimal(18, 2)), N'lazojuan@hotmail.com', NULL, NULL, 1, N'Observacion de LAZO, JUAN')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (182, N'BRUNOTTO, AZUCENA LUCIA', 27133120967, N'DNI', 13312096, CAST(N'1957-04-02T00:00:00.000' AS DateTime), N'F', N'S', N'BRUNOTTO', 133, N'LA FRANCIA', 112, NULL, NULL, CAST(N'1964-03-31T00:00:00.000' AS DateTime), CAST(N'1968-03-30T00:00:00.000' AS DateTime), 0, 1, 0, CAST(14908.00 AS Decimal(18, 2)), N'brunottoazucenalucia@gmail.com', NULL, NULL, 1, N'Observacion de BRUNOTTO, AZUCENA LUCIA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (183, N'DIAZ, CELIA', 27388526264, N'CF', 3885262, CAST(N'1940-02-08T00:00:00.000' AS DateTime), N'F', N'S', N'DIAZ', 388, N'CHILIBROSTE', 113, NULL, NULL, CAST(N'1947-02-06T00:00:00.000' AS DateTime), CAST(N'1951-02-05T00:00:00.000' AS DateTime), 1, 0, 0, CAST(14966.00 AS Decimal(18, 2)), N'diazcelia@hotmail.com', NULL, NULL, 1, N'Observacion de DIAZ, CELIA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (184, N'PAEZ, ROSA', 27478944123, N'CF', 4789441, CAST(N'1944-05-17T00:00:00.000' AS DateTime), N'F', N'Z', N'PAEZ', 478, N'BIALET MASSE', 114, NULL, NULL, CAST(N'1951-05-16T00:00:00.000' AS DateTime), CAST(N'1955-05-15T00:00:00.000' AS DateTime), 0, 1, 1, CAST(14934.00 AS Decimal(18, 2)), N'paezrosa@gmail.com', NULL, NULL, 1, N'Observacion de PAEZ, ROSA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (185, N'PERALTA, TERESA DE JESUS', 27388357648, N'CF', 3883576, CAST(N'1939-10-15T00:00:00.000' AS DateTime), N'F', N'Z', N'PERALTA', 388, N'MARULL', 115, NULL, NULL, CAST(N'1946-10-13T00:00:00.000' AS DateTime), CAST(N'1950-10-12T00:00:00.000' AS DateTime), 1, 0, 0, CAST(14804.00 AS Decimal(18, 2)), N'peraltateresadejesus@hotmail.com', NULL, NULL, 1, N'Observacion de PERALTA, TERESA DE JESUS')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (186, N'JUNCOS, IRMA JOSEFA', 27487279438, N'CP', 4872794, CAST(N'1948-03-19T00:00:00.000' AS DateTime), N'F', N'Z', N'JUNCOS', 487, N'MARULL', 116, NULL, NULL, CAST(N'1955-03-18T00:00:00.000' AS DateTime), CAST(N'1959-03-17T00:00:00.000' AS DateTime), 0, 0, 0, CAST(14778.00 AS Decimal(18, 2)), N'juncosirmajosefa@gmail.com', NULL, NULL, 1, N'Observacion de JUNCOS, IRMA JOSEFA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (187, N'MASOERO, ENRIQUE ALFREDO', 20604056714, N'CP', 6040567, CAST(N'1941-07-15T00:00:00.000' AS DateTime), N'M', N'Z', N'MASOERO', 604, N'VILLA SANTA CRUZ DEL LAGO', 117, NULL, NULL, CAST(N'1948-07-13T00:00:00.000' AS DateTime), CAST(N'1952-07-12T00:00:00.000' AS DateTime), 1, 0, 1, CAST(11018.00 AS Decimal(18, 2)), N'masoeroenriquealfredo@hotmail.com', NULL, NULL, 1, N'Observacion de MASOERO, ENRIQUE ALFREDO')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (188, N'SANCHEZ, TOMASA PETRONA', 27361059985, N'CP', 3610599, CAST(N'1938-09-20T00:00:00.000' AS DateTime), N'F', N'Z', N'SANCHEZ', 361, N'SIN ASIGNAR', 118, NULL, NULL, CAST(N'1945-09-18T00:00:00.000' AS DateTime), CAST(N'1949-09-17T00:00:00.000' AS DateTime), 0, 1, 1, CAST(14553.00 AS Decimal(18, 2)), N'sancheztomasapetrona@gmail.com', NULL, NULL, 1, N'Observacion de SANCHEZ, TOMASA PETRONA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (189, N'ROMERO, ROSA EVA', 27484389336, N'LE', 4843893, CAST(N'1947-08-30T00:00:00.000' AS DateTime), N'F', N'S', N'ROMERO', 484, N'SIN ASIGNAR', 119, NULL, NULL, CAST(N'1954-08-28T00:00:00.000' AS DateTime), CAST(N'1958-08-27T00:00:00.000' AS DateTime), 1, 0, 1, CAST(14542.00 AS Decimal(18, 2)), N'romerorosaeva@hotmail.com', NULL, NULL, 1, N'Observacion de ROMERO, ROSA EVA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (190, N'TORRES, CLAUDINA S', 27412989581, N'LE', 4129895, CAST(N'1941-12-08T00:00:00.000' AS DateTime), N'F', N'Z', N'TORRES', 412, N'SANTA ANA', 120, NULL, NULL, CAST(N'1948-12-06T00:00:00.000' AS DateTime), CAST(N'1952-12-05T00:00:00.000' AS DateTime), 0, 1, 1, CAST(14427.00 AS Decimal(18, 2)), N'torresclaudinas@gmail.com', NULL, NULL, 1, N'Observacion de TORRES, CLAUDINA S')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (191, N'CARRIZO, ELIA DEL VALLE', 23467557745, N'PSP', 4675577, CAST(N'1943-08-12T00:00:00.000' AS DateTime), N'F', N'Z', N'CARRIZO', 467, N'CORDOBA CAPITAL', 121, NULL, NULL, CAST(N'1950-08-10T00:00:00.000' AS DateTime), CAST(N'1954-08-09T00:00:00.000' AS DateTime), 1, 1, 1, CAST(12286.00 AS Decimal(18, 2)), N'carrizoeliadelvalle@hotmail.com', NULL, NULL, 1, N'Observacion de CARRIZO, ELIA DEL VALLE')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (192, N'RODRIGUEZ, MARIA ANTONIA', 22087011071, N'DNI', 20870110, CAST(N'1969-05-03T00:00:00.000' AS DateTime), N'F', N'Z', N'RODRIGUEZ', 208, N'CORDOBA CAPITAL', 122, NULL, NULL, CAST(N'1976-05-01T00:00:00.000' AS DateTime), CAST(N'1980-04-30T00:00:00.000' AS DateTime), 0, 1, 0, CAST(11503.00 AS Decimal(18, 2)), N'rodriguezmariaantonia@gmail.com', NULL, NULL, 1, N'Observacion de RODRIGUEZ, MARIA ANTONIA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (193, N'MUZO, MIRTA EDITH', 27639729112, N'DNI', 6397291, CAST(N'1950-11-08T00:00:00.000' AS DateTime), N'F', N'S', N'MUZO', 639, N'CORDOBA CAPITAL', 123, NULL, NULL, CAST(N'1957-11-06T00:00:00.000' AS DateTime), CAST(N'1961-11-05T00:00:00.000' AS DateTime), 1, 0, 1, CAST(14321.00 AS Decimal(18, 2)), N'muzomirtaedith@hotmail.com', NULL, NULL, 1, N'Observacion de MUZO, MIRTA EDITH')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (194, N'BENETTO, MIRTA DEL VALLE', 27581302798, N'DNI', 5813027, CAST(N'1948-04-03T00:00:00.000' AS DateTime), N'F', N'Z', N'BENETTO', 581, N'SIN ASIGNAR', 124, NULL, NULL, CAST(N'1955-04-02T00:00:00.000' AS DateTime), CAST(N'1959-04-01T00:00:00.000' AS DateTime), 0, 0, 1, CAST(14217.00 AS Decimal(18, 2)), N'benettomirtadelvalle@gmail.com', NULL, NULL, 1, N'Observacion de BENETTO, MIRTA DEL VALLE')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (195, N'OROPEL, NELIDA ROSA', 27568164914, N'DNI', 5681649, CAST(N'1946-08-31T00:00:00.000' AS DateTime), N'F', N'S', N'OROPEL', 568, N'CORDOBA CAPITAL', 125, NULL, NULL, CAST(N'1953-08-29T00:00:00.000' AS DateTime), CAST(N'1957-08-28T00:00:00.000' AS DateTime), 1, 0, 1, CAST(14137.00 AS Decimal(18, 2)), N'oropelnelidarosa@hotmail.com', NULL, NULL, 1, N'Observacion de OROPEL, NELIDA ROSA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (196, N'MOLINA, ROSA ESPERANZA', 27423003521, N'DNI', 4230035, CAST(N'1941-12-08T00:00:00.000' AS DateTime), N'F', N'Z', N'MOLINA', 423, N'CORDOBA CAPITAL', 126, NULL, NULL, CAST(N'1948-12-06T00:00:00.000' AS DateTime), CAST(N'1952-12-05T00:00:00.000' AS DateTime), 0, 1, 1, CAST(13991.00 AS Decimal(18, 2)), N'molinarosaesperanza@gmail.com', NULL, NULL, 1, N'Observacion de MOLINA, ROSA ESPERANZA')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (197, N'BAZAN, ROQUE', 20799556806, N'DNI', 7995568, CAST(N'1947-04-05T00:00:00.000' AS DateTime), N'M', N'Z', N'BAZAN', 799, N'CORDOBA CAPITAL', 127, NULL, NULL, CAST(N'1954-04-03T00:00:00.000' AS DateTime), CAST(N'1958-04-02T00:00:00.000' AS DateTime), 1, 0, 0, CAST(10558.00 AS Decimal(18, 2)), N'bazanroque@hotmail.com', NULL, NULL, 1, N'Observacion de BAZAN, ROQUE')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (198, N'PEREYRA, OLGA BEATRIZ', 27548992923, N'DNI', 5489929, CAST(N'1947-01-09T00:00:00.000' AS DateTime), N'F', N'Z', N'PEREYRA', 548, N'CORDOBA CAPITAL', 128, NULL, NULL, CAST(N'1954-01-07T00:00:00.000' AS DateTime), CAST(N'1958-01-06T00:00:00.000' AS DateTime), 0, 1, 1, CAST(13913.00 AS Decimal(18, 2)), N'pereyraolgabeatriz@gmail.com', NULL, NULL, 1, N'Observacion de PEREYRA, OLGA BEATRIZ')
INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (199, N'MORENO, MARIA ESTHER', 27625858334, N'CF', 6258583, CAST(N'1950-03-04T00:00:00.000' AS DateTime), N'F', N'Z', N'MORENO', 625, N'CORDOBA CAPITAL', 129, NULL, NULL, CAST(N'1957-03-02T00:00:00.000' AS DateTime), CAST(N'1961-03-01T00:00:00.000' AS DateTime), 1, 0, 1, CAST(13882.00 AS Decimal(18, 2)), N'morenomariaesther@hotmail.com', NULL, NULL, 1, N'Observacion de MORENO, MARIA ESTHER')
GO
SET IDENTITY_INSERT [dbo].[Clientes] OFF
SET IDENTITY_INSERT [dbo].[Contactos] ON 

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (1, N'JUAREZ, JOAQUIN AVELINO', CAST(N'1951-03-04T00:00:00.000' AS DateTime), 152008363, 1)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (2, N'HEREDIA, CARLOS', CAST(N'1949-11-18T00:00:00.000' AS DateTime), 152078435, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (3, N'FUENTES, DIEGO HERNAN', CAST(N'1972-12-11T00:00:00.000' AS DateTime), 152022796, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (4, N'RIVERO, JOSE', CAST(N'1938-01-01T00:00:00.000' AS DateTime), 152066859, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (5, N'MACCHI, MARIA DE LUJAN', CAST(N'1947-10-28T00:00:00.000' AS DateTime), 152757214, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (6, N'SANCHEZ, CRISTINA DORA', CAST(N'1942-09-15T00:00:00.000' AS DateTime), 152745068, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (7, N'ARCE, YESICA J', CAST(N'1980-03-31T00:00:00.000' AS DateTime), 152728113, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (8, N'URQUIZA, CESAR DANIEL', CAST(N'1978-01-21T00:00:00.000' AS DateTime), 152263932, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (9, N'RODRIGUEZ, DARIO MARTIN', CAST(N'1970-11-06T00:00:00.000' AS DateTime), 152021902, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (10, N'GULLI, JORGE DANIEL', CAST(N'1967-10-18T00:00:00.000' AS DateTime), 152018429, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (11, N'NAVARRO, DANIEL ALEJANDRO', CAST(N'1969-02-14T00:00:00.000' AS DateTime), 152020622, 1)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (12, N'RAMONDA, FRANCISCA MIGUELA', CAST(N'1947-03-02T00:00:00.000' AS DateTime), 152747404, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (13, N'BOTTI, AMELIA HAYDEE', CAST(N'1942-03-24T00:00:00.000' AS DateTime), 152745100, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (14, N'MORENO, DELIA', CAST(N'1943-08-06T00:00:00.000' AS DateTime), 152745645, 1)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (15, N'NIEVAS, JULIA AZUCENA', CAST(N'1946-07-27T00:00:00.000' AS DateTime), 152754099, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (16, N'MAROSTICA, CARLOS ERNESTO', CAST(N'1949-04-03T00:00:00.000' AS DateTime), 152076934, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (17, N'LEDESMA, MERCEDES DEL CARMEN', CAST(N'1936-11-15T00:00:00.000' AS DateTime), 152737034, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (18, N'FRULLINGUI, MIGUEL ANGEL', CAST(N'1945-05-08T00:00:00.000' AS DateTime), 152060606, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (19, N'FLORES, MERCEDES YOLANDA', CAST(N'1944-05-20T00:00:00.000' AS DateTime), 152750979, 1)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (20, N'MENDOZA, MARTIN ELEUTERIO', CAST(N'1946-11-03T00:00:00.000' AS DateTime), 152080419, 1)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (21, N'RIVADERO, NILDA MIRTA', CAST(N'1942-08-17T00:00:00.000' AS DateTime), 152745077, 1)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (22, N'LUBARY, ELENA DEL CARMEN', CAST(N'1951-09-14T00:00:00.000' AS DateTime), 152766475, 1)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (23, N'MALDONADO, AURORA DEL VALLE', CAST(N'1948-07-04T00:00:00.000' AS DateTime), 152757307, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (24, N'CARRANZA, FRANCISCA CORINA', CAST(N'1932-10-16T00:00:00.000' AS DateTime), 152715724, 1)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (25, N'DELGADO, RAMONA', CAST(N'1950-01-25T00:00:00.000' AS DateTime), 152762645, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (26, N'LUCERO, ROSARIO DEL CARMEN', CAST(N'1940-07-06T00:00:00.000' AS DateTime), 152738875, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (27, N'DOMINGUEZ, MARTHA AMALIA', CAST(N'1950-06-27T00:00:00.000' AS DateTime), 152756368, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (28, N'PIZZUTO, SEBASTIAN', CAST(N'1950-09-07T00:00:00.000' AS DateTime), 152084118, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (29, N'GIGENA, OFELIA DORA', CAST(N'1949-04-17T00:00:00.000' AS DateTime), 152766803, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (30, N'ROMERO, HAYDEE', CAST(N'1948-07-11T00:00:00.000' AS DateTime), 152757307, 1)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (31, N'SAUR, ELENA MARIA', CAST(N'1947-01-04T00:00:00.000' AS DateTime), 152752474, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (32, N'PIRIS, MARGARITA AURELIA', CAST(N'1947-04-15T00:00:00.000' AS DateTime), 152752474, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (33, N'GUZMAN, ALEJANDRINA', CAST(N'1936-02-26T00:00:00.000' AS DateTime), 152727989, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (34, N'ZAPATA, ALICIA ANGELICA', CAST(N'1944-07-14T00:00:00.000' AS DateTime), 152748502, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (35, N'RODRIGUEZ, MATEO ELADIO', CAST(N'1939-11-08T00:00:00.000' AS DateTime), 152064291, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (36, N'AGUIRRE, ANDRES RAFAEL', CAST(N'1949-09-09T00:00:00.000' AS DateTime), 152078560, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (37, N'VIGNOLA, RUBEN BENITO', CAST(N'1941-01-12T00:00:00.000' AS DateTime), 152064315, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (38, N'ACHERVI, MIGUEL ANGEL', CAST(N'1946-03-08T00:00:00.000' AS DateTime), 152064435, 1)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (39, N'LENCINAS, CARLOS ALBERTO', CAST(N'1939-08-29T00:00:00.000' AS DateTime), 152064288, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (40, N'NADALIN, OTILIO JOSE', CAST(N'1938-08-02T00:00:00.000' AS DateTime), 152064260, 1)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (41, N'MOYANO, RAUL ANTONIO', CAST(N'1942-04-03T00:00:00.000' AS DateTime), 152364345, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (42, N'MOYANO, LINA MARGARITA', CAST(N'1939-12-10T00:00:00.000' AS DateTime), 152787853, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (43, N'ABRATE, VICENTE BARTOLO', CAST(N'1937-08-10T00:00:00.000' AS DateTime), 152064246, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (44, N'MEZ, JUAN CARLOS', CAST(N'1945-03-03T00:00:00.000' AS DateTime), 152079438, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (45, N'HEREDIA, SARA ANTONIA', CAST(N'1938-08-10T00:00:00.000' AS DateTime), 152737567, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (46, N'CONTRERAS, NORMA BEATRIZ', CAST(N'1969-08-08T00:00:00.000' AS DateTime), 152207851, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (47, N'ORTEGA, JESUS', CAST(N'1948-12-25T00:00:00.000' AS DateTime), 152084107, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (48, N'DOMINGUEZ, AURORA', CAST(N'1949-12-21T00:00:00.000' AS DateTime), 152759797, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (49, N'SILVESTRE, CRISTINA BARBARIANA', CAST(N'1949-06-25T00:00:00.000' AS DateTime), 152759959, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (50, N'SANTOS, ESTELA ELVIRA', CAST(N'1946-10-11T00:00:00.000' AS DateTime), 152754523, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (51, N'BARRIONUEVO, AIDA NORA', CAST(N'1948-01-01T00:00:00.000' AS DateTime), 152756285, 1)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (52, N'GUZMAN, RAMONA  ESPERANZA', CAST(N'1949-11-14T00:00:00.000' AS DateTime), 152761327, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (53, N'VIDELA, CONSTANCIA', CAST(N'1947-11-21T00:00:00.000' AS DateTime), 152756981, 1)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (54, N'SANCHEZ, JUAN CARLOS', CAST(N'1944-01-18T00:00:00.000' AS DateTime), 152079758, 1)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (55, N'ROBLEDO, LUIS ARMANDO', CAST(N'1949-01-06T00:00:00.000' AS DateTime), 152376302, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (56, N'SOSA, SIMONA', CAST(N'1945-01-01T00:00:00.000' AS DateTime), 152750083, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (57, N'BRITO, MIGUEL F', CAST(N'1940-09-04T00:00:00.000' AS DateTime), 152065152, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (58, N'GONZALEZ, RAMON ELADIO', CAST(N'1950-06-10T00:00:00.000' AS DateTime), 152080096, 1)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (59, N'ECHENIQUE, EFRAIN', CAST(N'1948-06-18T00:00:00.000' AS DateTime), 152380009, 1)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (60, N'VALDEZ, VICENTA', CAST(N'1946-10-27T00:00:00.000' AS DateTime), 152754786, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (61, N'ALBORNOZ, SILVIA BEATRIZ', CAST(N'1946-08-15T00:00:00.000' AS DateTime), 152454098, 1)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (62, N'SOSA, CLEOFO MERCEDES', CAST(N'1943-09-25T00:00:00.000' AS DateTime), 152063919, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (63, N'MARTINEZ, JULIA AZUCENA', CAST(N'1935-07-21T00:00:00.000' AS DateTime), 152320349, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (64, N'FERNANDEZ, ANGELA YOLANDA', CAST(N'1944-09-12T00:00:00.000' AS DateTime), 152748208, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (65, N'ALIENDRO, OLGA', CAST(N'1938-01-11T00:00:00.000' AS DateTime), 152735849, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (66, N'AMERI, OLGA ESTHER', CAST(N'1971-01-01T00:00:00.000' AS DateTime), 152752601, 1)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (67, N'ORONA, ROSA ZULEMA', CAST(N'1934-01-04T00:00:00.000' AS DateTime), 152724405, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (68, N'OVIEDO, AIDEE FELIPA', CAST(N'1944-08-23T00:00:00.000' AS DateTime), 152342945, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (69, N'MORENO, ELISA CRISTINA', CAST(N'1948-07-06T00:00:00.000' AS DateTime), 152757382, 1)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (70, N'MEZ, CANDELARIA ALICIA', CAST(N'1940-06-11T00:00:00.000' AS DateTime), 152741089, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (71, N'ORTIZ, EMILIA V', CAST(N'1946-10-03T00:00:00.000' AS DateTime), 152746696, 1)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (72, N'ROJAS, JORGE CORNELIO', CAST(N'1941-09-19T00:00:00.000' AS DateTime), 152065500, 1)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (73, N'BUENO, GERARDO', CAST(N'1937-02-21T00:00:00.000' AS DateTime), 152065441, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (74, N'STREET, SUSANA MARGARITA', CAST(N'1947-06-05T00:00:00.000' AS DateTime), 152348895, 1)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (75, N'SANCHEZ, GRISELDA ELINA', CAST(N'1975-10-09T00:00:00.000' AS DateTime), 152726373, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (76, N'CENA, MARGARITA ROSA', CAST(N'1939-07-21T00:00:00.000' AS DateTime), 152799544, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (77, N'CRISTALDO MARTINEZ, DEONILDA', CAST(N'1960-04-08T00:00:00.000' AS DateTime), 152792824, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (78, N'PETRELLI, FABIANA', CAST(N'1962-12-03T00:00:00.000' AS DateTime), 152716275, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (79, N'MARTINEZ, GINES', CAST(N'1935-05-25T00:00:00.000' AS DateTime), 152063770, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (80, N'BARRIOS, AGUSTIN', CAST(N'1945-04-02T00:00:00.000' AS DateTime), 152766542, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (81, N'RIVERO, SUSANA ELENA', CAST(N'1948-08-04T00:00:00.000' AS DateTime), 152759184, 1)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (82, N'BLETRAMO, LEANDRO EMANUEL', CAST(N'1989-05-17T00:00:00.000' AS DateTime), 152762650, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (83, N'VINCENZINI, NORMA IRIS', CAST(N'1950-03-05T00:00:00.000' AS DateTime), 152759559, 1)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (84, N'FERREYRA, MARTA GRACIELA', CAST(N'1947-11-12T00:00:00.000' AS DateTime), 152756608, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (85, N'VERON, MANUEL VALLE', CAST(N'1946-10-11T00:00:00.000' AS DateTime), 152079566, 1)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (86, N'RUIZ, ELENA VICENTA', CAST(N'1949-12-08T00:00:00.000' AS DateTime), 152753276, 1)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (87, N'SAGUILAN, LUICIANA', CAST(N'1944-01-16T00:00:00.000' AS DateTime), 152744869, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (88, N'BRIZUELA, JUAN AGUSTIN', CAST(N'1943-08-29T00:00:00.000' AS DateTime), 152071211, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (89, N'LOBO, SILVESTRE AMARANTO', CAST(N'1936-12-20T00:00:00.000' AS DateTime), 152363804, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (90, N'LOVERA, LUCIA MARIA', CAST(N'1945-01-22T00:00:00.000' AS DateTime), 152749905, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (91, N'VEGA, HERMINDA MIRTA', CAST(N'1944-08-16T00:00:00.000' AS DateTime), 152748901, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (92, N'FALCON, OLGA HERMENEGILDA', CAST(N'1934-05-06T00:00:00.000' AS DateTime), 152731890, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (93, N'SOLA, ARMINDA', CAST(N'1949-05-20T00:00:00.000' AS DateTime), 152748209, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (94, N'BARRIONUEVO, ROQUE RAUL', CAST(N'1949-08-18T00:00:00.000' AS DateTime), 152065611, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (95, N'NUÑEZ, CARLOS', CAST(N'1936-01-05T00:00:00.000' AS DateTime), 152066364, 3)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (96, N'PEREZ, EMMA CIRILA', CAST(N'1941-07-30T00:00:00.000' AS DateTime), 152741288, 1)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (97, N'MANSILLA, AMALIA C', CAST(N'1950-11-23T00:00:00.000' AS DateTime), 152762205, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (98, N'HEREDIA, TOMAS GREGORIO', CAST(N'1941-04-12T00:00:00.000' AS DateTime), 152064321, 2)
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (99, N'HEREDIA, OMAR CESAR', CAST(N'1942-03-17T00:00:00.000' AS DateTime), 152079672, 1)
GO
INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (100, N'MONSERRAT, YOLANDA ISABEL', CAST(N'1948-10-05T00:00:00.000' AS DateTime), 152757382, 2)
SET IDENTITY_INSERT [dbo].[Contactos] OFF
SET IDENTITY_INSERT [dbo].[Departamentos] ON 

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (65, N'ADOLFO ALSINA', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (66, N'ADOLFO GONZALES CHAVES', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (67, N'ALBERTI', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (68, N'ALMIRANTE BROWN', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (69, N'AVELLANEDA', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (70, N'AYACUCHO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (71, N'AZUL', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (72, N'BAHIA BLANCA', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (73, N'BALCARCE', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (74, N'BARADERO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (75, N'BARTOLOME MITRE', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (76, N'BENITO JUAREZ', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (77, N'BERAZATEGUI', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (78, N'BERISSO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (79, N'BOLIVAR', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (80, N'BRAGADO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (81, N'CAMPANA', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (82, N'CAÑUELAS', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (83, N'CAPITAL FEDERAL', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (84, N'CAPITAN SARMIENTO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (85, N'CARLOS CASARES', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (86, N'CARLOS TEJEDOR', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (87, N'CARMEN DE ARECO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (88, N'CASTELLI', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (89, N'CHACABUCO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (90, N'CHASCOMUS', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (91, N'CHIVILCOY', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (92, N'CIUDAD AUTONOMA DE BUENOS AIRES', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (93, N'COLON', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (94, N'CORONEL BRANDSEN', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (95, N'CORONEL DORREGO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (96, N'CORONEL PRINGLES', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (97, N'CORONEL ROSALES', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (98, N'CORONEL SUAREZ', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (99, N'DAIREAUX', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (100, N'DE LA COSTA', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (101, N'DOLORES', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (102, N'ENSENADA', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (103, N'ESCOBAR', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (104, N'ESTEBAN ECHEVERRIA', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (105, N'EXALTACION DE LA CRUZ', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (106, N'EZEIZA', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (107, N'FLORENCIO VARELA', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (108, N'FLORENTINO AMEGHINO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (109, N'GENERAL ALVARADO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (110, N'GENERAL ALVEAR', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (111, N'GENERAL ARENALES', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (112, N'GENERAL BELGRANO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (113, N'GENERAL GUIDO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (114, N'GENERAL LAMADRID', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (115, N'GENERAL LAS HERAS', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (116, N'GENERAL LAVALLE', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (117, N'GENERAL MADARIAGA', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (118, N'GENERAL PAZ', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (119, N'GENERAL PINTO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (120, N'GENERAL PUEYRREDON', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (121, N'GENERAL RODRIGUEZ', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (122, N'GENERAL SAN MARTIN', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (123, N'GENERAL VIAMONTE', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (124, N'GENERAL VILLEGAS', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (125, N'GUAMINI', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (126, N'HIPOLITO YRIYEN', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (127, N'HURLINGHAM', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (128, N'ITUZAIN', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (129, N'JOSE CLEMENTE PAZ', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (130, N'JUNIN', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (131, N'LA MATANZA', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (132, N'LA PLATA', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (133, N'LANUS', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (134, N'LAPRIDA', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (135, N'LAS FLORES', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (136, N'LEANDRO N  ALEM', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (137, N'LINCOLN', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (138, N'LOBERIA', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (139, N'LOBOS', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (140, N'LOMAS DE ZAMORA', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (141, N'LUJAN', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (142, N'MAGDALENA', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (143, N'MAIPU', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (144, N'MALVINAS ARGENTINAS', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (145, N'MAR CHIQUITA', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (146, N'MARCOS PAZ', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (147, N'MERCEDES', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (148, N'MERLO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (149, N'MONTE', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (150, N'MONTE HERMOSO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (151, N'MORENO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (152, N'MORON', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (153, N'NAVARRO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (154, N'NECOCHEA', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (155, N'OLAVARRIA', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (156, N'PATAGONES', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (157, N'PEHUAJO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (158, N'PELLEGRINI', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (159, N'PERGAMINO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (160, N'PILA', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (161, N'PILAR', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (162, N'PINAMAR', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (163, N'PRESIDENTE PERON', 5)
GO
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (164, N'PUAN', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (165, N'PUNTA INDIO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (166, N'QUILMES', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (167, N'RAMALLO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (168, N'RAUCH', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (169, N'RIVADAVIA', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (170, N'ROJAS', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (171, N'ROQUE PEREZ', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (172, N'SAAVEDRA', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (173, N'SALADILLO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (174, N'SALLIQUELO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (175, N'SALTO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (176, N'SAN ANDRES DE GILES', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (177, N'SAN ANTONIO DE ARECO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (178, N'SAN CAYETANO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (179, N'SAN FERNANDO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (180, N'SAN ISIDRO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (181, N'SAN MIGUEL', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (182, N'SAN NICOLAS', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (183, N'SAN PEDRO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (184, N'SAN VICENTE', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (185, N'SUIPACHA', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (186, N'TANDIL', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (187, N'TAPALQUE', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (188, N'TIGRE', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (189, N'TORDILLO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (190, N'TORNQUIST', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (191, N'TRENQUE LAUQUEN', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (192, N'TRES ARROYOS', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (193, N'TRES DE FEBRERO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (194, N'TRES LOMAS', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (195, N'VICENTE LOPEZ', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (196, N'VILLA GESELL', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (197, N'VILLARINO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (198, N'ZARATE', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (199, N'25 DE MAYO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (200, N'9 DE JULIO', 5)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (202, N'CALAMUCHITA', 4)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (203, N'CAPITAL', 4)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (204, N'COLON', 4)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (205, N'CRUZ DEL EJE', 4)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (206, N'GENERAL ROCA', 4)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (207, N'GENERAL SAN MARTIN', 4)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (208, N'ISCHILIN', 4)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (209, N'JUAREZ CELMAN', 4)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (210, N'MARCOS JUAREZ', 4)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (211, N'MINAS', 4)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (212, N'POCHO', 4)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (213, N'PTE. ROQUE SAENZ PEÑA', 4)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (214, N'PUNILLA', 4)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (215, N'RIO CUARTO', 4)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (216, N'RIO PRIMERO', 4)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (217, N'RIO SECO', 4)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (218, N'RIO SEGUNDO', 4)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (219, N'SAN ALBERTO', 4)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (220, N'SAN JAVIER', 4)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (221, N'SAN JUSTO', 4)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (222, N'SANTA MARIA', 4)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (224, N'SOBREMONTE', 4)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (225, N'TERCERO ARRIBA', 4)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (226, N'TOTORAL', 4)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (227, N'TULUMBA', 4)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (228, N'UNION', 4)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (230, N'BELGRANO', 6)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (231, N'CASEROS', 6)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (232, N'CASTELLANOS', 6)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (233, N'CONSTITUCION', 6)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (234, N'GARAY', 6)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (235, N'GENERAL LOPEZ', 6)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (236, N'GENERAL OBLIGADO', 6)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (237, N'IRIONDO', 6)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (238, N'LA CAPITAL', 6)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (239, N'LAS COLONIAS', 6)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (240, N'ROSARIO', 6)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (241, N'SAN CRISTOBAL', 6)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (242, N'SAN JAVIER', 6)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (243, N'SAN JERONIMO', 6)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (244, N'SAN JUSTO', 6)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (245, N'SAN LORENZO', 6)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (246, N'SAN MARTIN', 6)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (247, N'SIN INFORMAR', 6)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (248, N'VERA', 6)
INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (249, N'9 DE JULIO', 6)
SET IDENTITY_INSERT [dbo].[Departamentos] OFF
SET IDENTITY_INSERT [dbo].[Empresas] ON 

INSERT [dbo].[Empresas] ([IdEmpresa], [RazonSocial], [CantidadEmpleados], [FechaFundacion]) VALUES (1, N'SANTEX GROUP', 300, CAST(N'1976-07-09T00:00:00' AS SmallDateTime))
INSERT [dbo].[Empresas] ([IdEmpresa], [RazonSocial], [CantidadEmpleados], [FechaFundacion]) VALUES (2, N'COMPAÑIA SUR', 256, CAST(N'1974-04-09T00:00:00' AS SmallDateTime))
INSERT [dbo].[Empresas] ([IdEmpresa], [RazonSocial], [CantidadEmpleados], [FechaFundacion]) VALUES (3, N'SANTO DOMIN', 120, CAST(N'1999-10-04T00:00:00' AS SmallDateTime))
INSERT [dbo].[Empresas] ([IdEmpresa], [RazonSocial], [CantidadEmpleados], [FechaFundacion]) VALUES (4, N'COCA COLA', 234, CAST(N'2000-02-02T00:00:00' AS SmallDateTime))
INSERT [dbo].[Empresas] ([IdEmpresa], [RazonSocial], [CantidadEmpleados], [FechaFundacion]) VALUES (7, N'MAFRE', 125, CAST(N'2000-02-20T00:00:00' AS SmallDateTime))
INSERT [dbo].[Empresas] ([IdEmpresa], [RazonSocial], [CantidadEmpleados], [FechaFundacion]) VALUES (8, N'RIVADAVIA', 3, CAST(N'2000-12-12T00:00:00' AS SmallDateTime))
INSERT [dbo].[Empresas] ([IdEmpresa], [RazonSocial], [CantidadEmpleados], [FechaFundacion]) VALUES (9, N'ABC', 3, CAST(N'2001-10-10T00:00:00' AS SmallDateTime))
INSERT [dbo].[Empresas] ([IdEmpresa], [RazonSocial], [CantidadEmpleados], [FechaFundacion]) VALUES (10, N'CON FINES DE LUCRO', 3, CAST(N'2001-02-02T00:00:00' AS SmallDateTime))
INSERT [dbo].[Empresas] ([IdEmpresa], [RazonSocial], [CantidadEmpleados], [FechaFundacion]) VALUES (11, N'ARCOR', 3, CAST(N'2000-10-10T00:00:00' AS SmallDateTime))
INSERT [dbo].[Empresas] ([IdEmpresa], [RazonSocial], [CantidadEmpleados], [FechaFundacion]) VALUES (12, N'NOE AND COMPANY', 10, CAST(N'1900-11-11T00:00:00' AS SmallDateTime))
INSERT [dbo].[Empresas] ([IdEmpresa], [RazonSocial], [CantidadEmpleados], [FechaFundacion]) VALUES (13, N'SIN FIN DE LUCRO', 555, CAST(N'2010-01-01T00:00:00' AS SmallDateTime))
INSERT [dbo].[Empresas] ([IdEmpresa], [RazonSocial], [CantidadEmpleados], [FechaFundacion]) VALUES (14, N'MAXEMPRESA', 128, CAST(N'1900-01-01T00:00:00' AS SmallDateTime))
INSERT [dbo].[Empresas] ([IdEmpresa], [RazonSocial], [CantidadEmpleados], [FechaFundacion]) VALUES (16, N'EMPRESA PRIVADA BIS SRL', 50, CAST(N'2000-08-02T00:00:00' AS SmallDateTime))
INSERT [dbo].[Empresas] ([IdEmpresa], [RazonSocial], [CantidadEmpleados], [FechaFundacion]) VALUES (17, N'EMPRESA PRIVADA BIS', 52, CAST(N'2000-08-01T00:00:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[Empresas] OFF
INSERT [dbo].[EstadosCiviles] ([IdEstadoCivil], [Nombre]) VALUES (N'C', N'CASADO')
INSERT [dbo].[EstadosCiviles] ([IdEstadoCivil], [Nombre]) VALUES (N'Z', N'CELIBE')
INSERT [dbo].[EstadosCiviles] ([IdEstadoCivil], [Nombre]) VALUES (N'D', N'DIVORCIADO')
INSERT [dbo].[EstadosCiviles] ([IdEstadoCivil], [Nombre]) VALUES (N'S', N'SOLTERO')
INSERT [dbo].[EstadosCiviles] ([IdEstadoCivil], [Nombre]) VALUES (N'V', N'VIUDO')
SET IDENTITY_INSERT [dbo].[Grupos] ON 

INSERT [dbo].[Grupos] ([IdGrupo], [Nombre]) VALUES (1, N'ADMINISTRADORES')
INSERT [dbo].[Grupos] ([IdGrupo], [Nombre]) VALUES (2, N'SISTEMAS')
INSERT [dbo].[Grupos] ([IdGrupo], [Nombre]) VALUES (3, N'VENDEDORES')
INSERT [dbo].[Grupos] ([IdGrupo], [Nombre]) VALUES (4, N'CAJEROS')
INSERT [dbo].[Grupos] ([IdGrupo], [Nombre]) VALUES (5, N'REPARTIDORES')
INSERT [dbo].[Grupos] ([IdGrupo], [Nombre]) VALUES (6, N'CONTROLADORES')
SET IDENTITY_INSERT [dbo].[Grupos] OFF
SET IDENTITY_INSERT [dbo].[IvasTipos] ON 

INSERT [dbo].[IvasTipos] ([IvaTipoId], [Nombre]) VALUES (4, N'MONOTRIBUTO')
INSERT [dbo].[IvasTipos] ([IvaTipoId], [Nombre]) VALUES (5, N'RESPONSABLE INSCRIPTO')
INSERT [dbo].[IvasTipos] ([IvaTipoId], [Nombre]) VALUES (6, N'RESPONSABLE NO INSCRIPTO')
INSERT [dbo].[IvasTipos] ([IvaTipoId], [Nombre]) VALUES (7, N'EXENTO')
INSERT [dbo].[IvasTipos] ([IvaTipoId], [Nombre]) VALUES (8, N'CONSUMIDOR FINAL')
INSERT [dbo].[IvasTipos] ([IvaTipoId], [Nombre]) VALUES (9, N'OTRO REPONSABLE')
SET IDENTITY_INSERT [dbo].[IvasTipos] OFF
SET IDENTITY_INSERT [dbo].[Menus] ON 

INSERT [dbo].[Menus] ([IdMenu], [Nombre], [Url], [IdMenuPadre], [Orden], [IdPermiso]) VALUES (1, N'Sistema', NULL, NULL, 70, NULL)
INSERT [dbo].[Menus] ([IdMenu], [Nombre], [Url], [IdMenuPadre], [Orden], [IdPermiso]) VALUES (2, N'Ventas', N'', NULL, 10, NULL)
INSERT [dbo].[Menus] ([IdMenu], [Nombre], [Url], [IdMenuPadre], [Orden], [IdPermiso]) VALUES (3, N'Compras', NULL, NULL, 20, NULL)
INSERT [dbo].[Menus] ([IdMenu], [Nombre], [Url], [IdMenuPadre], [Orden], [IdPermiso]) VALUES (4, N'Articulos', NULL, NULL, 30, NULL)
INSERT [dbo].[Menus] ([IdMenu], [Nombre], [Url], [IdMenuPadre], [Orden], [IdPermiso]) VALUES (5, N'Clientes', N'', NULL, 40, NULL)
INSERT [dbo].[Menus] ([IdMenu], [Nombre], [Url], [IdMenuPadre], [Orden], [IdPermiso]) VALUES (6, N'Proveedores', NULL, NULL, 50, NULL)
INSERT [dbo].[Menus] ([IdMenu], [Nombre], [Url], [IdMenuPadre], [Orden], [IdPermiso]) VALUES (7, N'Administracion', NULL, NULL, 60, NULL)
INSERT [dbo].[Menus] ([IdMenu], [Nombre], [Url], [IdMenuPadre], [Orden], [IdPermiso]) VALUES (8, N'Login', NULL, 1, 10, 1)
INSERT [dbo].[Menus] ([IdMenu], [Nombre], [Url], [IdMenuPadre], [Orden], [IdPermiso]) VALUES (9, N'Gestion Usuarios', N'UsuariosWF.aspx', 1, 20, 2)
INSERT [dbo].[Menus] ([IdMenu], [Nombre], [Url], [IdMenuPadre], [Orden], [IdPermiso]) VALUES (10, N'Gestion Grupos', NULL, 1, 30, 3)
INSERT [dbo].[Menus] ([IdMenu], [Nombre], [Url], [IdMenuPadre], [Orden], [IdPermiso]) VALUES (11, N'Gestion Permisos', NULL, 1, 40, 4)
INSERT [dbo].[Menus] ([IdMenu], [Nombre], [Url], [IdMenuPadre], [Orden], [IdPermiso]) VALUES (12, N'Gestion Menus', NULL, 1, 50, 5)
INSERT [dbo].[Menus] ([IdMenu], [Nombre], [Url], [IdMenuPadre], [Orden], [IdPermiso]) VALUES (13, N'Configurar Sitio', NULL, 1, 60, 6)
INSERT [dbo].[Menus] ([IdMenu], [Nombre], [Url], [IdMenuPadre], [Orden], [IdPermiso]) VALUES (14, N'Gestionar Clientes', N'ClientesWF.aspx', NULL, 10, 7)
SET IDENTITY_INSERT [dbo].[Menus] OFF
SET IDENTITY_INSERT [dbo].[Paises] ON 

INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (1, N'ARGENTINA', 4400000, N'AMERICA DEL SUR')
INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (4, N'CHILE', 20000000, N'AMERICA DEL SUR')
INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (5, N'URUGUAY', 15000000, N'AMERICA DEL SUR')
INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (6, N'PARAGUAY', 20000000, N'AMERICA DEL SUR')
INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (7, N'BOLIVIA', 25000000, N'AMERICA DEL SUR')
INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (8, N'ESTADOS UNIDOS', 200000000, N'AMERICA DEL NORTE')
INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (9, N'FRANCIA', 80000000, N'EUROPA')
INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (10, N'SUD AFRICA', 35000000, N'AFRICA')
INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (12, N'ESPAÑA', 45000000, N'EUROPA')
INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (13, N'CANADA', 40000000, N'AMERICA DEL NORTE')
INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (14, N'PUERTO RICO', 10000000, N'AMERICA CENTRAL')
INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (15, N'COLOMBIA', NULL, N'AMERICA DEL SUR')
INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (18, N'JAPON', 70000000, N'ASIA')
INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (19, N'BRASIL', 150000000, N'AMERICA DEL SUR')
INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (20, N'COREA DEL SUR', 10000000, N'ASIA')
INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (22, N'COREA DEL NORTE', 40000000, N'ASIA')
SET IDENTITY_INSERT [dbo].[Paises] OFF
SET IDENTITY_INSERT [dbo].[Personas] ON 

INSERT [dbo].[Personas] ([IdPersona], [Nombre]) VALUES (1, N'JUAN')
INSERT [dbo].[Personas] ([IdPersona], [Nombre]) VALUES (2, N'PEDRO')
INSERT [dbo].[Personas] ([IdPersona], [Nombre]) VALUES (3, N'MARIA')
INSERT [dbo].[Personas] ([IdPersona], [Nombre]) VALUES (4, N'CELESTE')
INSERT [dbo].[Personas] ([IdPersona], [Nombre]) VALUES (5, N'FERNANDO')
SET IDENTITY_INSERT [dbo].[Personas] OFF
SET IDENTITY_INSERT [dbo].[Provincias] ON 

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (1, N'SALTA', 1)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (2, N'TUCUMAN', 1)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (3, N'SAN JUAN', 1)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (4, N'CORDOBA', 1)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (5, N'BUENOS AIRES', 1)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (6, N'SANTA FE', 1)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (7, N'SANTIA DEL ESTERO', 1)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (8, N'FORMOSA', 1)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (9, N'ENTRE RIOS', 1)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (10, N'CORRIENTES', 1)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (11, N'MISIONES', 1)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (12, N'LA PAMPA', 1)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (13, N'PERROCITY', 1)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (14, N'SAN LUIS', 1)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (15, N'CATAMARCA', 1)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (16, N'ACRE', 19)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (17, N'ALAAS', 19)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (18, N'AMAPA', 19)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (19, N'AMAZONAS', 19)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (20, N'BAHÍA', 19)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (21, N'CEARA', 19)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (22, N'DISTRITO FEDERAL', 19)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (23, N'ESPIRITO SANTO', 19)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (24, N'IAS', 19)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (25, N'MARANHAO', 19)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (26, N'MATO GROSSO', 19)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (27, N'MATO GROSSO DO SUL', 19)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (28, N'MINAS GERAIS', 19)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (29, N'PARA', 19)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (30, N'PARAIBA', 19)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (31, N'PARANÁ', 19)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (32, N'PERNAMBUCO', 19)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (33, N'PIAUI', 19)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (34, N'RÍO DE JANEIRO', 19)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (35, N'RÍO GRANDE DO NORTE', 19)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (36, N'RÍO GRANDE DO SUL', 19)
INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (37, N'RONDONIA', 19)
SET IDENTITY_INSERT [dbo].[Provincias] OFF
INSERT [dbo].[Sesiones] ([IdSesion], [Maquina], [FechaInicio], [FechaFin], [IdUsuario]) VALUES (N'1', N'LABSIS204', CAST(N'2011-06-07T10:13:00.000' AS DateTime), NULL, NULL)
INSERT [dbo].[Sesiones] ([IdSesion], [Maquina], [FechaInicio], [FechaFin], [IdUsuario]) VALUES (N'2', N'LABSIS704', CAST(N'2011-09-15T00:00:00.000' AS DateTime), CAST(N'2011-09-15T00:00:00.000' AS DateTime), 1)
INSERT [dbo].[Sexos] ([IdSexo], [Nombre]) VALUES (N'F', N'FEMENINO')
INSERT [dbo].[Sexos] ([IdSexo], [Nombre]) VALUES (N'M', N'MASCULINO')
INSERT [dbo].[TiposDocumentos] ([IdTipoDocumento], [Nombre]) VALUES (N'CF', N'CEDULA FEDERAL')
INSERT [dbo].[TiposDocumentos] ([IdTipoDocumento], [Nombre]) VALUES (N'CP', N'CEDULA PROVINCIAL')
INSERT [dbo].[TiposDocumentos] ([IdTipoDocumento], [Nombre]) VALUES (N'DNI', N'DOCUMENTO NACIONAL')
INSERT [dbo].[TiposDocumentos] ([IdTipoDocumento], [Nombre]) VALUES (N'LE', N'LIBRETA ENROLAMIENTO')
INSERT [dbo].[TiposDocumentos] ([IdTipoDocumento], [Nombre]) VALUES (N'PSP', N'PASAPORTE')
SET IDENTITY_INSERT [dbo].[Usuarios] ON 

INSERT [dbo].[Usuarios] ([IdUsuario], [Nombre], [Clave], [Bloqueado], [IdRegistroRelacion], [IdTablaRelacion]) VALUES (1, N'admin', N'admin123', NULL, NULL, NULL)
INSERT [dbo].[Usuarios] ([IdUsuario], [Nombre], [Clave], [Bloqueado], [IdRegistroRelacion], [IdTablaRelacion]) VALUES (2, N'juan', N'juan123', NULL, NULL, NULL)
INSERT [dbo].[Usuarios] ([IdUsuario], [Nombre], [Clave], [Bloqueado], [IdRegistroRelacion], [IdTablaRelacion]) VALUES (4, N'pedro', N'pedro123', NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Usuarios] OFF
SET IDENTITY_INSERT [dbo].[Ventas] ON 

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1, 1, CAST(N'2014-10-02T20:29:18.197' AS DateTime), CAST(3317.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (2, 2, CAST(N'2014-10-02T20:37:50.483' AS DateTime), CAST(25136.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (3, 3, CAST(N'2014-10-02T20:54:00.917' AS DateTime), CAST(23482.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (4, 4, CAST(N'2014-10-02T21:41:18.617' AS DateTime), CAST(6801.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (5, 5, CAST(N'2014-10-02T22:32:14.090' AS DateTime), CAST(6272.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (6, 6, CAST(N'2014-10-03T18:43:39.790' AS DateTime), CAST(12685.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (7, 1, CAST(N'2014-10-04T12:06:06.747' AS DateTime), CAST(7848.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (8, 1, CAST(N'2014-10-04T13:40:42.693' AS DateTime), CAST(15532.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (9, 1, CAST(N'2014-10-04T13:52:00.797' AS DateTime), CAST(32503.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1008, 1, CAST(N'2014-10-06T15:45:20.453' AS DateTime), CAST(1844.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1011, 1, CAST(N'2014-10-06T15:47:42.627' AS DateTime), CAST(349.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1012, 3, CAST(N'2020-11-20T20:46:52.923' AS DateTime), CAST(134454.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1013, 5, CAST(N'2020-11-20T20:55:26.170' AS DateTime), CAST(10197.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1014, 144, CAST(N'2020-11-20T21:44:07.347' AS DateTime), CAST(23235.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1015, 119, CAST(N'2020-11-20T22:35:16.707' AS DateTime), CAST(369.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1016, 70, CAST(N'2020-11-20T23:21:17.687' AS DateTime), CAST(5899.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1017, 112, CAST(N'2020-11-21T14:46:13.893' AS DateTime), CAST(649.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1018, 137, CAST(N'2020-11-01T15:00:00.000' AS DateTime), CAST(10399.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1019, 112, CAST(N'2020-11-21T20:20:05.177' AS DateTime), CAST(5899.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1020, 182, CAST(N'2020-11-23T03:00:00.000' AS DateTime), CAST(5899.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1021, 34, CAST(N'2020-11-24T21:47:57.210' AS DateTime), CAST(12498.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1022, 70, CAST(N'2020-11-24T21:50:00.000' AS DateTime), CAST(5899.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1023, 34, CAST(N'2020-11-24T21:52:53.067' AS DateTime), CAST(14998.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1024, 34, CAST(N'2020-11-24T22:17:38.283' AS DateTime), CAST(6548.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1025, 9, CAST(N'2021-02-01T22:50:31.990' AS DateTime), CAST(5597.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1026, 112, CAST(N'2021-02-08T20:37:44.820' AS DateTime), CAST(259.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1027, 112, CAST(N'2021-02-25T18:02:15.507' AS DateTime), CAST(5405.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1028, 100, CAST(N'2021-02-25T18:02:15.507' AS DateTime), CAST(15497.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1029, 119, CAST(N'2021-02-26T15:19:01.590' AS DateTime), CAST(1798.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1030, 82, CAST(N'2021-02-26T15:33:36.843' AS DateTime), CAST(2669.00 AS Decimal(18, 2)))
INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1031, 129, CAST(N'2021-02-26T16:32:47.373' AS DateTime), CAST(2669.00 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[Ventas] OFF
SET IDENTITY_INSERT [dbo].[VentasDetalle] ON 

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1, 1, 1, 1, CAST(299.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (2, 1, 2, 1, CAST(349.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (3, 1, 3, 1, CAST(2669.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (4, 2, 8, 1, CAST(5405.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (5, 2, 8, 1, CAST(5405.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (6, 2, 9, 1, CAST(5290.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (7, 2, 10, 1, CAST(4837.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (8, 2, 11, 1, CAST(4199.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (9, 3, 1, 1, CAST(299.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (10, 3, 4, 1, CAST(2999.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (11, 3, 7, 1, CAST(4830.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (12, 3, 298, 1, CAST(4389.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (13, 3, 301, 1, CAST(2999.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (14, 3, 5, 1, CAST(3129.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (15, 3, 10, 1, CAST(4837.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (16, 4, 198, 1, CAST(122.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (17, 4, 253, 1, CAST(631.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (18, 4, 285, 1, CAST(549.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (19, 4, 88, 1, CAST(5499.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (20, 5, 1, 1, CAST(299.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (21, 5, 4, 1, CAST(2999.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (22, 5, 63, 1, CAST(75.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (23, 5, 70, 1, CAST(2899.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (24, 6, 1, 1, CAST(299.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (25, 6, 2, 1, CAST(349.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (26, 6, 1, 1, CAST(299.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (27, 6, 295, 1, CAST(6239.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (28, 6, 296, 1, CAST(5499.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (29, 7, 2, 1, CAST(349.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (30, 7, 3, 1, CAST(2669.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (31, 7, 7, 1, CAST(4830.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (32, 8, 10, 1, CAST(4837.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (33, 8, 9, 1, CAST(5290.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (34, 8, 8, 1, CAST(5405.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (35, 9, 1, 1, CAST(299.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (36, 9, 2, 1, CAST(349.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (37, 9, 8, 1, CAST(5405.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (38, 9, 9, 5, CAST(5290.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1032, 1008, 2, 1, CAST(349.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1033, 1008, 1, 5, CAST(299.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1036, 1011, 2, 1, CAST(349.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1037, 1012, 107, 1, CAST(123456.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1038, 1012, 140, 2, CAST(5499.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1039, 1013, 74, 1, CAST(199.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1040, 1013, 150, 2, CAST(4999.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1041, 1014, 109, 2, CAST(369.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1042, 1014, 141, 3, CAST(7499.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1043, 1015, 109, 1, CAST(369.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1044, 1016, 139, 1, CAST(5899.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1045, 1017, 112, 1, CAST(649.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1046, 1018, 144, 1, CAST(10399.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1047, 1019, 139, 1, CAST(5899.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1048, 1020, 139, 1, CAST(5899.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1049, 1021, 150, 1, CAST(4999.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1050, 1021, 141, 1, CAST(7499.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1051, 1022, 139, 1, CAST(5899.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1052, 1023, 141, 2, CAST(7499.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1053, 1024, 112, 1, CAST(649.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1054, 1024, 139, 1, CAST(5899.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1055, 1025, 3, 2, CAST(2669.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1056, 1025, 107, 1, CAST(259.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1057, 1026, 107, 1, CAST(259.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1058, 1027, 8, 1, CAST(5405.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1059, 1028, 88, 1, CAST(5499.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1060, 1028, 150, 2, CAST(4999.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1061, 1029, 199, 2, CAST(899.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1062, 1030, 3, 1, CAST(2669.00 AS Decimal(18, 2)))
INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1063, 1031, 3, 1, CAST(2669.00 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[VentasDetalle] OFF
SET ANSI_PADDING ON
GO
/****** Object:  Index [UK_UsuariosNombre]    Script Date: 05/03/2021 17:46:25 ******/
ALTER TABLE [dbo].[Articulos] ADD  CONSTRAINT [UK_ArticulosNombre] UNIQUE NONCLUSTERED (	[Nombre] ASC )
go
ALTER TABLE [dbo].[Usuarios] ADD  CONSTRAINT [UK_UsuariosNombre] UNIQUE NONCLUSTERED (	[Nombre] ASC )
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Usuarios] ADD  CONSTRAINT [DF_Usuarios_Nombre]  DEFAULT ('') FOR [Nombre]
GO
ALTER TABLE [dbo].[Clientes]  WITH CHECK ADD  CONSTRAINT [FK_Clientes_Departamentos] FOREIGN KEY([IdDepartamento])
REFERENCES [dbo].[Departamentos] ([IdDepartamento])
GO
ALTER TABLE [dbo].[Clientes] CHECK CONSTRAINT [FK_Clientes_Departamentos]
GO
ALTER TABLE [dbo].[Clientes]  WITH CHECK ADD  CONSTRAINT [FK_Clientes_EstadosCiviles] FOREIGN KEY([IdEstadoCivil])
REFERENCES [dbo].[EstadosCiviles] ([IdEstadoCivil])
GO
ALTER TABLE [dbo].[Clientes] CHECK CONSTRAINT [FK_Clientes_EstadosCiviles]
GO
ALTER TABLE [dbo].[Clientes]  WITH CHECK ADD  CONSTRAINT [FK_Clientes_Paises] FOREIGN KEY([IdPais])
REFERENCES [dbo].[Paises] ([IdPais])
GO
ALTER TABLE [dbo].[Clientes] CHECK CONSTRAINT [FK_Clientes_Paises]
GO
ALTER TABLE [dbo].[Clientes]  WITH CHECK ADD  CONSTRAINT [FK_Clientes_Sexos] FOREIGN KEY([IdSexo])
REFERENCES [dbo].[Sexos] ([IdSexo])
GO
ALTER TABLE [dbo].[Clientes] CHECK CONSTRAINT [FK_Clientes_Sexos]
GO
ALTER TABLE [dbo].[Clientes]  WITH CHECK ADD  CONSTRAINT [FK_Clientes_TiposDocumentos] FOREIGN KEY([IdTipoDocumento])
REFERENCES [dbo].[TiposDocumentos] ([IdTipoDocumento])
GO
ALTER TABLE [dbo].[Clientes] CHECK CONSTRAINT [FK_Clientes_TiposDocumentos]
GO
ALTER TABLE [dbo].[Ventas]  WITH CHECK ADD  CONSTRAINT [FK_Ventas_ClienteId] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Clientes] ([IdCliente])
GO
ALTER TABLE [dbo].[Ventas] CHECK CONSTRAINT [FK_Ventas_ClienteId]
GO
ALTER TABLE [dbo].[Usuarios]  WITH CHECK ADD  CONSTRAINT [CK_UsuariosNombre] CHECK  ((len([nombre])>=(4)))
GO
ALTER TABLE [dbo].[Usuarios] CHECK CONSTRAINT [CK_UsuariosNombre]
GO
/****** Object:  StoredProcedure [dbo].[BaseRestaurar]    Script Date: 05/03/2021 17:46:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BaseRestaurar]
AS
BEGIN

truncate table [VentasDetalle]
truncate table [Ventas]

--truncate table [Clientes]
DELETE FROM [Clientes]
DBCC CHECKIDENT ('pymes.dbo.Clientes',RESEED, 0)

truncate table articulosfamilias
truncate table articulos
truncate table AuditoriasABM
truncate table [Categorias]
truncate table [Contactos]
--truncate table [Departamentos]
DELETE FROM Departamentos
DBCC CHECKIDENT ('pymes.dbo.Departamentos',RESEED, 0)

truncate table [Empresas]

--truncate table [EstadosCiviles]
DELETE FROM EstadosCiviles
--DBCC CHECKIDENT ('dbo.EstadosCiviles',RESEED, 0)

truncate table [Grupos]
truncate table [IvasTipos]
truncate table [Menus]

--truncate table [Paises]
DELETE FROM [Paises]
DBCC CHECKIDENT ('pymes.dbo.Paises',RESEED, 0)

truncate table [Personas]
truncate table [Provincias]
truncate table [Sesiones]

--truncate table [Sexos]
DELETE FROM Sexos

--truncate table [TiposDocumentos]
delete from [TiposDocumentos]

DELETE FROM Sexos
--DBCC CHECKIDENT ('pymes.dbo.Sexos',RESEED, 0)

truncate table [Usuarios]







SET IDENTITY_INSERT [dbo].[Articulos] ON 

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (1, N'KIT DIRECT TV PREPA 0.60MT', CAST(299.00 AS Decimal(18, 2)), N'0779815559001', 10, 329, CAST(N'2017-01-19' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (2, N'KIT DIRECT TV PREPA 0.90MT', CAST(349.00 AS Decimal(18, 2)), N'0779815559002', 10, 468, CAST(N'2017-01-31' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (3, N'LED 22" LG FHD 22MN42APM', CAST(2669.00 AS Decimal(18, 2)), N'0779808338808', 10, 536, CAST(N'2017-01-12' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (4, N'LED 24" ILO HD DIGITAL MOD LDH24ILO02', CAST(2999.00 AS Decimal(18, 2)), N'0779696260024', 10, 169, CAST(N'2017-01-30' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (5, N'LED 24" LG HD 24MN42A-PM', CAST(3129.00 AS Decimal(18, 2)), N'0779808338809', 10, 296, CAST(N'2016-12-28' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (7, N'LED 32" BGH HD BLE3214D', CAST(4830.00 AS Decimal(18, 2)), N'0779688540133', 10, 998, CAST(N'2017-01-01' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (8, N'LED 32" BGH SMART TV BLE3213RT', CAST(5405.00 AS Decimal(18, 2)), N'0779688540117', 10, 650, CAST(N'2017-01-18' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (9, N'LED 32" HISENSE IPTV HLE3213RT', CAST(5290.00 AS Decimal(18, 2)), N'0779688540119', 10, 51, CAST(N'2017-02-03' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (10, N'LED 32" HITACHI HD CDHLE32FD10', CAST(4837.00 AS Decimal(18, 2)), N'0779694109973', 10, 838, CAST(N'2016-12-25' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (11, N'LED 32" ILO HD DIGITAL LDH32ILO02', CAST(4199.00 AS Decimal(18, 2)), N'0779696260132', 10, 501, CAST(N'2017-01-25' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (12, N'LED 32" JVC HD IPTV LT32DR930', CAST(6699.00 AS Decimal(18, 2)), N'0779818058057', 10, 906, CAST(N'2017-01-25' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (13, N'LED 32" JVC HD LT32DA330', CAST(4499.00 AS Decimal(18, 2)), N'0779696266323', 10, 435, CAST(N'2017-02-07' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (14, N'LED 32" LG 3D 32LA613B', CAST(6299.00 AS Decimal(18, 2)), N'0779808338816', 10, 329, CAST(N'2017-02-06' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (15, N'LED 32" PHILIPS FHD 32PFL3018D/77', CAST(6799.00 AS Decimal(18, 2)), N'0871258168715', 10, 971, CAST(N'2016-12-25' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (16, N'LED 32" PHILIPS FHD IPTV 32PFL4508G/77', CAST(7699.00 AS Decimal(18, 2)), N'0871258167198', 10, 636, CAST(N'2017-02-07' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (17, N'LED 32" PHILIPS HD 32PFL3008D/77', CAST(5799.00 AS Decimal(18, 2)), N'0871258167218', 10, 67, CAST(N'2016-12-27' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (18, N'LED 32" PHILIPS SMART TV 32PFL3518G/77', CAST(7399.00 AS Decimal(18, 2)), N'0871258167225', 10, 250, CAST(N'2017-01-08' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (19, N'LED 32" RCA HD L32S80DIGI', CAST(4499.00 AS Decimal(18, 2)), N'0779694101214', 10, 857, CAST(N'2017-01-23' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (20, N'LED 32" SAMSUNG FHD UN32F5000', CAST(6094.00 AS Decimal(18, 2)), N'0880608543154', 10, 636, CAST(N'2016-12-30' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (21, N'LED 32" SAMSUNG HD UN32F4000', CAST(5519.00 AS Decimal(18, 2)), N'0880608543153', 10, 37, CAST(N'2017-01-23' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (22, N'LED 32" SAMSUNG SMART UN32F5500', CAST(6899.00 AS Decimal(18, 2)), N'0880608548607', 10, 214, CAST(N'2017-01-24' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (23, N'LED 32" SONY HD KDL32R425', CAST(6199.00 AS Decimal(18, 2)), N'0490552491740', 10, 642, CAST(N'2017-01-17' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (24, N'LED 32" SONY SMART TV KDL32W655', CAST(6999.00 AS Decimal(18, 2)), N'0490552491687', 10, 50, CAST(N'2017-02-04' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (25, N'LED 39" ILO DIG FHD LDF39ILO2', CAST(5699.00 AS Decimal(18, 2)), N'0779696260394', 10, 951, CAST(N'2017-01-19' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (26, N'LED 39" PHILIPS FHD IPTV 39PFL3508G/77', CAST(8799.00 AS Decimal(18, 2)), N'0871258168717', 10, 889, CAST(N'2017-02-03' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (27, N'LED 39" RCA FHD L39S85DIGIFHD', CAST(6499.00 AS Decimal(18, 2)), N'0779694101215', 10, 487, CAST(N'2016-12-25' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (28, N'LED 40" BGH FHD BLE4014D', CAST(7245.00 AS Decimal(18, 2)), N'0779688540132', 10, 480, CAST(N'2016-12-27' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (29, N'LED 40" SAMSUNG 3D SMART UN40F6800', CAST(13224.00 AS Decimal(18, 2)), N'0880608565606', 10, 734, CAST(N'2017-01-26' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (30, N'LED 40" SAMSUNG 3D UN40F6100', CAST(9999.00 AS Decimal(18, 2)), N'0880608544958', 10, 835, CAST(N'2017-01-19' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (31, N'LED 40" SAMSUNG FHD UN40F5000', CAST(8164.00 AS Decimal(18, 2)), N'0880608543156', 10, 436, CAST(N'2017-02-01' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (32, N'LED 40" SAMSUNG SMART UN40F5500', CAST(9774.00 AS Decimal(18, 2)), N'0880608565438', 10, 639, CAST(N'2017-01-20' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (33, N'LED 40" SONY FHD KDL40R485', CAST(7499.00 AS Decimal(18, 2)), N'0490552493532', 10, 862, CAST(N'2017-01-07' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (34, N'LED 42" LG 3D 42LA6130', CAST(9199.00 AS Decimal(18, 2)), N'0779808338817', 10, 560, CAST(N'2017-01-05' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (35, N'LED 42" LG FHD 42LN5400', CAST(8099.00 AS Decimal(18, 2)), N'0779808338818', 10, 48, CAST(N'2017-01-28' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (36, N'LED 42" LG SMART TV 42LN5700', CAST(9799.00 AS Decimal(18, 2)), N'0779808338823', 10, 967, CAST(N'2017-01-27' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (37, N'LED 42" PANASONIC 3D SMART TV TCL42ET60', CAST(11249.00 AS Decimal(18, 2)), N'0779805518074', 10, 570, CAST(N'2017-01-19' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (38, N'LED 42" PHILIPS 3D SMART TV 42PFL5008G/7', CAST(11599.00 AS Decimal(18, 2)), N'0871258167039', 10, 802, CAST(N'2017-02-04' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (39, N'LED 42" PHILIPS FHD 42PFL3008D/77', CAST(8499.00 AS Decimal(18, 2)), N'0871258167221', 10, 193, CAST(N'2017-02-04' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (40, N'LED 42" PHILIPS SMART TV 42PFL3508G/77', CAST(9499.00 AS Decimal(18, 2)), N'0871258167227', 10, 693, CAST(N'2016-12-30' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (41, N'LED 42" PIONEER 3D SMART PLE42FZP2', CAST(12299.00 AS Decimal(18, 2)), N'0498802821943', 10, 907, CAST(N'2017-02-01' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (42, N'LED 42" SONY FHD KDL42R475', CAST(7999.00 AS Decimal(18, 2)), N'0490552491728', 10, 140, CAST(N'2017-01-13' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (43, N'LED 46" PHILIPS SMART TV 46PFL4508G/7', CAST(13999.00 AS Decimal(18, 2)), N'0871258168718', 10, 236, CAST(N'2017-01-31' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (44, N'LED 46" SAMSUNG 3D SMART TV UN46F7500', CAST(23574.00 AS Decimal(18, 2)), N'0880608565943', 10, 143, CAST(N'2016-12-25' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (45, N'LED 46" SAMSUNG SMART UN46F5500', CAST(13224.00 AS Decimal(18, 2)), N'0880608548610', 10, 345, CAST(N'2017-01-07' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (46, N'LED 46" SANYO SMART TV LCE46IF12', CAST(10599.00 AS Decimal(18, 2)), N'0779696260612', 10, 557, CAST(N'2017-02-03' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (47, N'LED 47" LG SMART TV 47LN5700', CAST(13199.00 AS Decimal(18, 2)), N'0779808338824', 10, 599, CAST(N'2017-01-20' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (48, N'LED 47" PIONEER 3D SMART PLE47FZP1', CAST(15999.00 AS Decimal(18, 2)), N'0498802821947', 10, 310, CAST(N'2017-02-07' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (49, N'LED 47" SONY 3D SMART TV KDL47W805', CAST(17199.00 AS Decimal(18, 2)), N'0490552494098', 10, 526, CAST(N'2017-01-31' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (50, N'LED 55" NOBLEX 3D IPTV 55LD856DI', CAST(20799.00 AS Decimal(18, 2)), N'0779696260000', 10, 362, CAST(N'2017-01-26' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (51, N'LED 55" PHILIPS 3D SMART TV 55PFL8008G/77', CAST(29999.00 AS Decimal(18, 2)), N'0871258166949', 10, 841, CAST(N'2017-01-06' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (52, N'SOPORTE LCD / LED DE 14" A 42" TANGWOOD', CAST(599.00 AS Decimal(18, 2)), N'0779814176493', 10, 527, CAST(N'2017-02-07' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (53, N'SOPORTE LCD / LED DE 17 '''' A 40 ''''', CAST(499.00 AS Decimal(18, 2)), N'0779814176654', 10, 588, CAST(N'2016-12-23' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (54, N'SOPORTE LCD / LED DE 17" A 37" TANGWOOD', CAST(225.00 AS Decimal(18, 2)), N'0779814176489', 10, 687, CAST(N'2017-01-29' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (55, N'SOPORTE LCD / LED DE 23 '''' A 50 ''''', CAST(350.00 AS Decimal(18, 2)), N'0779814176652', 10, 519, CAST(N'2016-12-25' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (56, N'SOPORTE LCD / LED DE 26" A 47" TANGWOOD', CAST(350.00 AS Decimal(18, 2)), N'0779814176442', 10, 81, CAST(N'2017-01-28' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (57, N'SOPORTE LCD / LED TGW DE 17 '''' A 37 ''''', CAST(199.00 AS Decimal(18, 2)), N'0779814176648', 10, 164, CAST(N'2017-01-17' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (58, N'SOPORTE LCD 10" TAGWOOD', CAST(375.00 AS Decimal(18, 2)), N'0779814176490', 10, 217, CAST(N'2017-01-31' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (59, N'SOPORTE LCD 32" NAKAN', CAST(199.00 AS Decimal(18, 2)), N'0779803504550', 10, 873, CAST(N'2017-01-01' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (60, N'SOPORTE LCD 32" ONE FOR ALL', CAST(259.00 AS Decimal(18, 2)), N'0871618404213', 10, 585, CAST(N'2017-01-30' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (61, N'SOPORTE LCD 40" ONE FOR ALL', CAST(519.00 AS Decimal(18, 2)), N'0871618404215', 10, 809, CAST(N'2017-01-22' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (62, N'SOPORTE LCD/LED 23 A 46"', CAST(399.00 AS Decimal(18, 2)), N'0779814176617', 10, 470, CAST(N'2017-01-21' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (68, N'SOPORTE GPS', CAST(119.00 AS Decimal(18, 2)), N'0779814176084', 8, 524, CAST(N'2017-01-14' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (69, N'SOPORTE GPS NEGRO MOTO 3,5" - 5,5"', CAST(259.00 AS Decimal(18, 2)), N'0779808004535', 8, 800, CAST(N'2017-02-05' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (70, N'GPS GARMIN NUVI 2595', CAST(2899.00 AS Decimal(18, 2)), N'0075375999226', 8, 745, CAST(N'2017-02-07' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (71, N'GPS GARMIN NUVI 52', CAST(2149.00 AS Decimal(18, 2)), N'0075375999808', 8, 274, CAST(N'2016-12-22' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (72, N'GPS X VIEW VENTURA TV 7"', CAST(1849.00 AS Decimal(18, 2)), N'0779804220262', 8, 150, CAST(N'2016-12-30' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (73, N'GPS XVIEW VENTURA TV', CAST(1509.00 AS Decimal(18, 2)), N'0779804220220', 8, 183, CAST(N'2017-01-05' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (74, N'MOUSE HP 2.4G SILVER WIRELESS OPT CAN/EN', CAST(199.00 AS Decimal(18, 2)), N'0088496276058', 9, 40, CAST(N'2017-02-03' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (75, N'PENDRIVE KINGSTONE DT101G2 8GB', CAST(129.00 AS Decimal(18, 2)), N'0074061716983', 9, 537, CAST(N'2016-12-21' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (76, N'PENDRIVE SANDISK BLADE 4GB', CAST(129.00 AS Decimal(18, 2)), N'0061965900041', 9, 340, CAST(N'2017-02-02' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (77, N'PENDRIVE SANDISK CRUZAR ORBIT 8GB', CAST(159.00 AS Decimal(18, 2)), N'0061965909040', 9, 696, CAST(N'2017-02-07' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (78, N'PENDRIVE SANDISK POP BLACK 8GB', CAST(159.00 AS Decimal(18, 2)), N'0061965908448', 9, 431, CAST(N'2017-01-08' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (79, N'PENDRIVE SANDISK POP PAIN 8GB', CAST(159.00 AS Decimal(18, 2)), N'0061965908156', 9, 521, CAST(N'2017-02-01' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (80, N'CARTUCHO EPSON 732 CYAN', CAST(10290.00 AS Decimal(18, 2)), N'0001034385887', 9, 234, CAST(N'2017-01-26' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (81, N'CARTUCHO EPSON T133120-AL MAGENTA', CAST(9690.00 AS Decimal(18, 2)), N'0001034387695', 9, 374, CAST(N'2016-12-26' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (82, N'CARTUCHO EPSON T133120-AL NEGRO', CAST(8479.00 AS Decimal(18, 2)), N'0001034387692', 9, 836, CAST(N'2017-01-25' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (83, N'CARTUCHO EPSON T133420-AL AMARILLO', CAST(9690.00 AS Decimal(18, 2)), N'0001034387696', 9, 796, CAST(N'2016-12-28' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (84, N'CARTUCHO HP 122 NEGRO', CAST(149.00 AS Decimal(18, 2)), N'0088496298354', 9, 373, CAST(N'2017-02-05' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (85, N'CARTUCHO HP 22 COLOR', CAST(299.00 AS Decimal(18, 2)), N'0082916090222', 9, 199, CAST(N'2017-01-01' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (86, N'CARTUCHO HP 60 COLOR', CAST(289.00 AS Decimal(18, 2)), N'0088358598319', 9, 801, CAST(N'2017-01-31' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (87, N'CARTUCHO HP 60 NEGRO', CAST(199.00 AS Decimal(18, 2)), N'0088358598317', 9, 655, CAST(N'2017-01-08' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (88, N'PC ALL IN ONE 120-1156LA + TECLADO INAL + MOUSE', CAST(5499.00 AS Decimal(18, 2)), N'0088611278012', 9, 331, CAST(N'2017-01-19' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (90, N'IMPRESORA MULTIFUNCION EPSON L355', CAST(3999.00 AS Decimal(18, 2)), N'0001034390469', 9, 293, CAST(N'2017-01-01' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (91, N'MULTIFUNCION EPSON L210 + SISTEMA CONTINUO', CAST(3399.00 AS Decimal(18, 2)), N'0001034390433', 9, 689, CAST(N'2017-01-09' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (92, N'MULTIFUNCION EPSON XP211', CAST(1199.00 AS Decimal(18, 2)), N'0001034390754', 9, 693, CAST(N'2017-01-08' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (93, N'MULTIFUNCION EPSON XP401', CAST(1799.00 AS Decimal(18, 2)), N'0001034390348', 9, 363, CAST(N'2017-01-17' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (94, N'NOTEBOOK BGH C-530 3D', CAST(4999.00 AS Decimal(18, 2)), N'0779816664067', 9, 401, CAST(N'2017-01-30' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (95, N'NOTEBOOK BGH C-550', CAST(5799.00 AS Decimal(18, 2)), N'0779816664065', 9, 230, CAST(N'2017-01-04' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (96, N'NOTEBOOK BGH C-565', CAST(6299.00 AS Decimal(18, 2)), N'0779816664069', 9, 876, CAST(N'2017-02-06' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (97, N'NOTEBOOK BGH C-570', CAST(7299.00 AS Decimal(18, 2)), N'0779816664070', 9, 929, CAST(N'2017-01-17' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (98, N'NOTEBOOK BGH QL 300 MINI', CAST(3699.00 AS Decimal(18, 2)), N'0779816664101', 9, 176, CAST(N'2017-01-28' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (99, N'NOTEBOOK DELL INSPIRON 14 3421 I14I32_45', CAST(6599.00 AS Decimal(18, 2)), N'0789948950198', 9, 758, CAST(N'2016-12-31' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (100, N'NOTEBOOK DELL INSPIRON 14 3421 I14V997_4', CAST(5999.00 AS Decimal(18, 2)), N'0779801657005', 9, 666, CAST(N'2016-12-20' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (101, N'NOTEBOOK LENOVO G485 C-70', CAST(4399.00 AS Decimal(18, 2)), N'0088761972842', 9, 115, CAST(N'2017-01-21' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (102, N'NOTEBOOK NOBLEX CEVEN GFAST', CAST(4499.00 AS Decimal(18, 2)), N'0779808041201', 9, 853, CAST(N'2017-02-07' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (103, N'NOTEBOOK POSITIVO BGH F-810N NEGRA', CAST(4999.00 AS Decimal(18, 2)), N'0779816664059', 9, 48, CAST(N'2017-01-21' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (104, N'NOTEBOOK SAMSUNG NP300E4C', CAST(6999.00 AS Decimal(18, 2)), N'0880608528173', 9, 272, CAST(N'2017-01-08' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (105, N'NOTEBOOK SAMSUNG NP300E5A AD4AR', CAST(4799.00 AS Decimal(18, 2)), N'0880608500428', 9, 194, CAST(N'2017-01-18' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (106, N'ULTRABOOK ACER S3-391-6867', CAST(9793.00 AS Decimal(18, 2)), N'0471219655495', 9, 974, CAST(N'2017-01-23' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (107, N'ADAPTADOR PCI WIFI TL-WN751ND', CAST(259.00 AS Decimal(18, 2)), N'0693536405056', 9, 171, CAST(N'2017-01-15' AS Date), 0)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (110, N'ANTENA TP-LINK TL-ANT2408C', CAST(249.00 AS Decimal(18, 2)), N'0693536405216', 9, 689, CAST(N'2016-12-26' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (111, N'MINI ADAPATADOR USB TP LINK WN723N', CAST(185.00 AS Decimal(18, 2)), N'0693536405055', 9, 382, CAST(N'2016-12-31' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (112, N'ROUTER MR3420 3G TP-LINK', CAST(649.00 AS Decimal(18, 2)), N'0693536405149', 9, 143, CAST(N'2016-12-21' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (113, N'ROUTER PORTATIL TP LINK TL-MR3020', CAST(499.00 AS Decimal(18, 2)), N'0693536405170', 9, 594, CAST(N'2017-02-01' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (114, N'ROUTER TL-WR941ND TP LINK', CAST(759.00 AS Decimal(18, 2)), N'0693536405127', 9, 646, CAST(N'2017-02-06' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (115, N'ROUTER TP-LINK TL-WR720N', CAST(309.00 AS Decimal(18, 2)), N'0693536405198', 9, 867, CAST(N'2017-01-01' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (116, N'ROUTER WR740 TP-LINK', CAST(389.00 AS Decimal(18, 2)), N'0693536405133', 9, 925, CAST(N'2017-01-28' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (117, N'ROUTER WR841 TP-LINK', CAST(469.00 AS Decimal(18, 2)), N'0693536405124', 9, 624, CAST(N'2017-01-29' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (118, N'TABLET MAGNUM TECH 7"', CAST(2599.00 AS Decimal(18, 2)), N'0779813546539', 9, 344, CAST(N'2016-12-26' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (119, N'TABLET 10" MAGNUM TECH 8GB 1GBM', CAST(3799.00 AS Decimal(18, 2)), N'0779813546540', 9, 751, CAST(N'2017-01-24' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (120, N'TABLET 10" NOBLEX NB1012', CAST(3549.00 AS Decimal(18, 2)), N'0779696292015', 9, 319, CAST(N'2017-01-13' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (121, N'TABLET ALCATEL AB10', CAST(1799.00 AS Decimal(18, 2)), N'0695508989953', 9, 939, CAST(N'2017-02-01' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (122, N'TABLET EUROCASE ARS 708', CAST(1099.00 AS Decimal(18, 2)), N'0779813546928', 9, 534, CAST(N'2017-01-26' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (123, N'TABLET FUNTAB PRO', CAST(1699.00 AS Decimal(18, 2)), N'0081770701101', 9, 869, CAST(N'2017-01-23' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (124, N'TABLET IDEAPAD LENOVO A1000L', CAST(2799.00 AS Decimal(18, 2)), N'0088794260611', 9, 597, CAST(N'2017-01-05' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (125, N'TABLET LENOVO IDEAPAD A1000 7"', CAST(2299.00 AS Decimal(18, 2)), N'0088777046041', 9, 510, CAST(N'2017-02-04' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (126, N'TABLET MAGNUM MG-701', CAST(1499.00 AS Decimal(18, 2)), N'0779813546946', 9, 645, CAST(N'2017-02-05' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (127, N'TABLET NOBLEX-8013 8''''', CAST(2149.00 AS Decimal(18, 2)), N'0779696291801', 9, 850, CAST(N'2017-01-17' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (130, N'TABLET OLIPAD SMART 7" 3G', CAST(1499.00 AS Decimal(18, 2)), N'0802033432056', 9, 489, CAST(N'2017-02-07' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (131, N'TABLET PC 7001 TITAN', CAST(999.00 AS Decimal(18, 2)), N'0076113310158', 9, 850, CAST(N'2016-12-24' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (132, N'TABLET PC BOX T700U 7" DUAL CORE', CAST(1999.00 AS Decimal(18, 2)), N'0779815876409', 9, 769, CAST(N'2017-02-06' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (133, N'TABLET PC FIRSTAR MID070A 8650', CAST(799.00 AS Decimal(18, 2)), N'0779815467080', 9, 9, CAST(N'2017-01-23' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (134, N'TABLET PCBOX MOD T900', CAST(2799.00 AS Decimal(18, 2)), N'0779815876410', 9, 501, CAST(N'2017-01-25' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (135, N'TABLET POLAROID MID1000 10', CAST(4299.00 AS Decimal(18, 2)), N'0358417655560', 9, 151, CAST(N'2016-12-23' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (136, N'TABLET SYNKOM 7"', CAST(2499.00 AS Decimal(18, 2)), N'0779816920041', 9, 695, CAST(N'2016-12-23' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (137, N'TABLET XVIEW ALPHA2 8GB', CAST(1899.00 AS Decimal(18, 2)), N'0779804220264', 9, 565, CAST(N'2017-02-05' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (138, N'TABLET XVIEW PROTON', CAST(1699.00 AS Decimal(18, 2)), N'0779804220247', 9, 3, CAST(N'2016-12-28' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (139, N'AIRE ACONDICIONADO DAEWOO 3200FC DWT23200FC', CAST(5899.00 AS Decimal(18, 2)), N'0779816944014', 7, 668, CAST(N'2017-01-04' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (140, N'AIRE ACONDICIONADO DURABRAND 3500FC DUS35WCL4', CAST(5499.00 AS Decimal(18, 2)), N'0779688543933', 7, 945, CAST(N'2017-01-20' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (141, N'AIRE ACONDICIONADO DURABRAND 4500FC DUS53WCL4', CAST(7499.00 AS Decimal(18, 2)), N'0779688543937', 7, 962, CAST(N'2016-12-29' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (142, N'AIRE ACONDICIONADO KELVINATOR 2500WFC COD1056', CAST(4499.00 AS Decimal(18, 2)), N'0779694101056', 7, 670, CAST(N'2017-01-03' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (143, N'AIRE ACONDICIONADO LG 3000 FC H126TNW0', CAST(7499.00 AS Decimal(18, 2)), N'0779808338858', 7, 441, CAST(N'2017-01-09' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (144, N'AIRE ACONDICIONADO LG 4500 FC H1865NW0', CAST(10399.00 AS Decimal(18, 2)), N'0779808338859', 7, 971, CAST(N'2016-12-23' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (145, N'AIRE ACONDICIONADO LG 5500 FC H2465NW0', CAST(12699.00 AS Decimal(18, 2)), N'0779808338860', 7, 648, CAST(N'2017-01-15' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (146, N'AIRE ACONDICIONADO LG ARTCOOL 2300FC H096EFT0', CAST(7999.00 AS Decimal(18, 2)), N'0779808338853', 7, 659, CAST(N'2017-01-01' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (147, N'AIRE ACONDICIONADO LG ARTCOOL 4500FC H1868FT0', CAST(12899.00 AS Decimal(18, 2)), N'0779808338855', 7, 712, CAST(N'2016-12-25' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (148, N'AIRE ACONDICIONADO PHILCO 3200W FC PHS32H13X', CAST(6199.00 AS Decimal(18, 2)), N'0779696244974', 7, 588, CAST(N'2017-01-09' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (149, N'AIRE ACONDICIONADO PHILCO 5000W FC PHS50H13X', CAST(9099.00 AS Decimal(18, 2)), N'0779696242975', 7, 275, CAST(N'2016-12-22' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (150, N'AIRE ACONDICIONADO PORTATIL DURABRAND 2500FS LGACD01', CAST(4999.00 AS Decimal(18, 2)), N'0073621119267', 7, 995, CAST(N'2017-01-26' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (151, N'AIRE ACONDICIONADO SAMSUNG 3000FC AR12FQFTAUR', CAST(7949.00 AS Decimal(18, 2)), N'0880608575497', 7, 34, CAST(N'2017-01-03' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (152, N'AIRE ACONDICIONADO SANYO 2600W FC KC913HSAN', CAST(6099.00 AS Decimal(18, 2)), N'0779696244956', 7, 372, CAST(N'2017-01-23' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (153, N'AIRE ACONDICIONADO SANYO 3200W FC KC1213HSAN', CAST(6899.00 AS Decimal(18, 2)), N'0779696242957', 7, 260, CAST(N'2017-02-02' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (154, N'AIRE ACONDICIONADO SURREYPRIA 2250FC 553EPQ0913F', CAST(6929.00 AS Decimal(18, 2)), N'0779708708630', 7, 38, CAST(N'2016-12-30' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (155, N'AIRE ACONDICIONADO SURREYPRIA 3000FC 553EPQ1213F', CAST(7949.00 AS Decimal(18, 2)), N'0779708708631', 7, 180, CAST(N'2017-01-04' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (156, N'AIRE ACONDICIONADO SURREYPRIA 4500FC 553EPQ1813F', CAST(11849.00 AS Decimal(18, 2)), N'0779708708632', 7, 232, CAST(N'2017-01-07' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (157, N'AIRE ACONDICIONADO SURREYPRIA 5500FC 553EPQ2213F', CAST(14329.00 AS Decimal(18, 2)), N'0779708708633', 7, 909, CAST(N'2017-01-10' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (158, N'CALEFACTOR SIN SALIDA 4000 KCAL VOLCAN', CAST(1159.00 AS Decimal(18, 2)), N'0779703781219', 7, 598, CAST(N'2016-12-23' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (159, N'CALEFACTOR SIN SALIDA ORBIS 4200 KCAL', CAST(1469.00 AS Decimal(18, 2)), N'0779703781123', 7, 504, CAST(N'2017-01-11' AS Date), 0)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (160, N'ESTUFA ORBIS TIRO BALANCEADO 5000 K', CAST(2019.00 AS Decimal(18, 2)), N'0779703781129', 7, 600, CAST(N'2017-01-17' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (161, N'ESTUFA VOLCAN TIRO BALANCEADO 2000 KCAL 42312V', CAST(1439.00 AS Decimal(18, 2)), N'0779703781220', 7, 602, CAST(N'2016-12-28' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (162, N'ESTUFA VOLCAN TIRO BALANCEADO NEGRO 3800 43712V', CAST(1679.00 AS Decimal(18, 2)), N'0779703781221', 7, 650, CAST(N'2017-02-04' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (163, N'TIRO BALANCEADO 3500 KCAL EMEGE', CAST(1605.00 AS Decimal(18, 2)), N'0779135400180', 7, 474, CAST(N'2017-01-29' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (164, N'CALEFACTOR ELECTRICO CLEVER VIDRIO H1107', CAST(1950.00 AS Decimal(18, 2)), N'0779815957117', 7, 459, CAST(N'2016-12-29' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (165, N'CALEFACTOR ELECTRICO CONVECCION CON-1800', CAST(1599.00 AS Decimal(18, 2)), N'0779814958212', 7, 10, CAST(N'2017-01-13' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (166, N'CALEFACTOR ELECTRICO CONVECCION CON-2000N', CAST(790.00 AS Decimal(18, 2)), N'0779815957180', 7, 112, CAST(N'2017-01-11' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (167, N'CALEFACTOR ELECTRICO CONVECCION CON-2000R', CAST(790.00 AS Decimal(18, 2)), N'0779815957181', 7, 141, CAST(N'2017-01-26' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (168, N'CALEFACTOR LILIANA INFRARROJO CI062', CAST(345.00 AS Decimal(18, 2)), N'0779386200687', 7, 516, CAST(N'2016-12-27' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (169, N'CALEFACTOR PANEL 500 WATTS', CAST(769.00 AS Decimal(18, 2)), N'0779813482002', 7, 804, CAST(N'2017-01-03' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (170, N'CALOVENTOR 2000 W AXEL AX-CA100', CAST(249.00 AS Decimal(18, 2)), N'0779811896139', 7, 780, CAST(N'2017-01-10' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (171, N'CALOVENTOR DE PARED 2000 W KENBROWN', CAST(839.00 AS Decimal(18, 2)), N'0779811320136', 7, 737, CAST(N'2016-12-28' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (172, N'CALOVENTOR DE PARED PROTALIA CP200A', CAST(799.00 AS Decimal(18, 2)), N'0779811559131', 7, 833, CAST(N'2017-01-30' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (173, N'CALOVENTOR ELECTRICO BLANCO 1500W LE1500B', CAST(599.00 AS Decimal(18, 2)), N'0779815957245', 7, 492, CAST(N'2017-01-04' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (174, N'CALOVENTOR ELECTRICO LE1500ROJO', CAST(599.00 AS Decimal(18, 2)), N'0779815957247', 7, 437, CAST(N'2017-01-29' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (175, N'CALOVENTOR ELECTRICO NEGRO 1500W LE1500N', CAST(599.00 AS Decimal(18, 2)), N'0779815957246', 7, 875, CAST(N'2017-01-09' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (176, N'CALOVENTOR ELECTROLUX SPLIT CONTROL REMOTO', CAST(999.00 AS Decimal(18, 2)), N'0779386200613', 7, 675, CAST(N'2016-12-20' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (177, N'CALOVENTOR KEN BROWN 2000 W', CAST(319.00 AS Decimal(18, 2)), N'0779811320075', 7, 76, CAST(N'2017-01-23' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (178, N'CALOVENTOR RESISTENCIA CERAMICA', CAST(319.00 AS Decimal(18, 2)), N'0557306319076', 7, 243, CAST(N'2017-01-08' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (179, N'CIRCULADOR DE AIRE FRIO CALOR DURABRAND', CAST(1049.00 AS Decimal(18, 2)), N'0073621119287', 7, 121, CAST(N'2017-01-30' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (180, N'CONVECTOR AXEL 2000 W AX-COT100', CAST(689.00 AS Decimal(18, 2)), N'0779811896141', 7, 357, CAST(N'2016-12-24' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (181, N'CONVECTOR AXEL 2000 W CON TURBO AX-COT', CAST(609.00 AS Decimal(18, 2)), N'0779811896131', 7, 246, CAST(N'2017-01-16' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (182, N'CONVECTOR CLEVER CLEVERBLANCO CON2000B', CAST(790.00 AS Decimal(18, 2)), N'0779815957179', 7, 229, CAST(N'2017-01-09' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (183, N'CONVECTOR TELEFUNKEN 2000 WATT C1009', CAST(479.00 AS Decimal(18, 2)), N'0779724533114', 7, 642, CAST(N'2016-12-29' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (184, N'ESTUFA ELECTROLUX HALOGENAS HAL18G', CAST(549.00 AS Decimal(18, 2)), N'0779386200254', 7, 295, CAST(N'2017-01-15' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (185, N'ESTUFA ELECTRICA KEN BROWN 2 VELAS 800 KB 22', CAST(245.00 AS Decimal(18, 2)), N'0779811320288', 7, 598, CAST(N'2016-12-24' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (186, N'ESTUFA HALOGENA 3 VELAS KEN BROWN', CAST(409.00 AS Decimal(18, 2)), N'0779811320134', 7, 580, CAST(N'2016-12-24' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (187, N'ESTUFA HALOGENA 4 VELAS KEN BROWN', CAST(449.00 AS Decimal(18, 2)), N'0779811320135', 7, 741, CAST(N'2017-01-28' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (188, N'ESTUFA HALOGENA ELECTROLUX 1600W SIN OSCILACION HAL18A', CAST(499.00 AS Decimal(18, 2)), N'0779386200253', 7, 632, CAST(N'2016-12-23' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (189, N'ESTUFA HALOGENA MAGIC 1200 W C1007', CAST(189.00 AS Decimal(18, 2)), N'0779724533112', 7, 518, CAST(N'2016-12-26' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (190, N'PANEL 1000W ATMA', CAST(99999.00 AS Decimal(18, 2)), N'0779696280631', 7, 951, CAST(N'2017-01-17' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (191, N'PANEL 2000 W NEGRO ENERGY SAVE', CAST(1499.00 AS Decimal(18, 2)), N'0779814951036', 7, 647, CAST(N'2016-12-20' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (192, N'PANEL 500 W ECOSOL', CAST(1119.00 AS Decimal(18, 2)), N'0779813482029', 7, 805, CAST(N'2017-01-18' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (193, N'PANEL 900W ECOSOL 1-502', CAST(1869.00 AS Decimal(18, 2)), N'0779813482031', 7, 726, CAST(N'2017-02-01' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (194, N'PANEL MICA ELECTROLUX RMIC15', CAST(999.00 AS Decimal(18, 2)), N'0779386200256', 7, 331, CAST(N'2016-12-26' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (195, N'PANEL PIETRA 500 W PEISA', CAST(699.00 AS Decimal(18, 2)), N'0779808116284', 7, 171, CAST(N'2017-01-27' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (196, N'RADIADOR DE MICA ELECTROLUX 1000W RALU01', CAST(699.00 AS Decimal(18, 2)), N'0779817317015', 7, 987, CAST(N'2017-01-24' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (197, N'TURBO CALENTADOR 2000W TCAL2000', CAST(590.00 AS Decimal(18, 2)), N'0779815957248', 7, 539, CAST(N'2017-01-03' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (198, N'VENTILADOR DE PIE DURABRAND 18" VP21', CAST(122.00 AS Decimal(18, 2)), N'0779797170650', 7, 318, CAST(N'2017-01-31' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (199, N'CAMARA DIGITAL C1433 SLVER GE', CAST(899.00 AS Decimal(18, 2)), N'0084695100018', 6, 528, CAST(N'2017-02-02' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (500, N'LIMPIADOR CD SV 8336 ONE FOR ALL', CAST(55.00 AS Decimal(18, 2)), N'0871618404342', 1, 508, CAST(N'2016-12-27' AS Date), 1)

INSERT [dbo].[Articulos] ([IdArticulo], [Nombre], [Precio], [CodigoDeBarra], [IdArticuloFamilia], [Stock], [FechaAlta], [Activo]) VALUES (501, N'LIMPIADOR LCD SV 8410 ONE FOR ALL', CAST(102.00 AS Decimal(18, 2)), N'0871618404333', 1, 186, CAST(N'2017-02-02' AS Date), 1)

SET IDENTITY_INSERT [dbo].[Articulos] OFF


	

SET IDENTITY_INSERT [dbo].[ArticulosFamilias] ON 

INSERT [dbo].[ArticulosFamilias] ([IdArticuloFamilia], [Nombre]) VALUES (1, N'ACCESORIOS')

INSERT [dbo].[ArticulosFamilias] ([IdArticuloFamilia], [Nombre]) VALUES (2, N'AUDIO')

INSERT [dbo].[ArticulosFamilias] ([IdArticuloFamilia], [Nombre]) VALUES (3, N'CELULARES')

INSERT [dbo].[ArticulosFamilias] ([IdArticuloFamilia], [Nombre]) VALUES (4, N'CUIDADO PERSONAL')

INSERT [dbo].[ArticulosFamilias] ([IdArticuloFamilia], [Nombre]) VALUES (5, N'DVD')

INSERT [dbo].[ArticulosFamilias] ([IdArticuloFamilia], [Nombre]) VALUES (6, N'FOTOGRAFIA')

INSERT [dbo].[ArticulosFamilias] ([IdArticuloFamilia], [Nombre]) VALUES (7, N'FRIO-CALOR')

INSERT [dbo].[ArticulosFamilias] ([IdArticuloFamilia], [Nombre]) VALUES (8, N'GPS')

INSERT [dbo].[ArticulosFamilias] ([IdArticuloFamilia], [Nombre]) VALUES (9, N'INFORMATICA')

INSERT [dbo].[ArticulosFamilias] ([IdArticuloFamilia], [Nombre]) VALUES (10, N'LED - LCD')

SET IDENTITY_INSERT [dbo].[ArticulosFamilias] OFF

	




SET IDENTITY_INSERT [dbo].[Categorias] ON 

INSERT [dbo].[Categorias] ([IdCategoria], [Nombre]) VALUES (1, N'FAMILIA')

INSERT [dbo].[Categorias] ([IdCategoria], [Nombre]) VALUES (2, N'AMIGOS')

INSERT [dbo].[Categorias] ([IdCategoria], [Nombre]) VALUES (3, N'NEGOCIOS')

SET IDENTITY_INSERT [dbo].[Categorias] OFF






SET IDENTITY_INSERT [dbo].[Contactos] ON 

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (1, N'JUAREZ, JOAQUIN AVELINO', CAST(N'1951-03-04T00:00:00.000' AS DateTime), 152008363, 1)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (2, N'HEREDIA, CARLOS', CAST(N'1949-11-18T00:00:00.000' AS DateTime), 152078435, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (3, N'FUENTES, DIEGO HERNAN', CAST(N'1972-12-11T00:00:00.000' AS DateTime), 152022796, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (4, N'RIVERO, JOSE', CAST(N'1938-01-01T00:00:00.000' AS DateTime), 152066859, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (5, N'MACCHI, MARIA DE LUJAN', CAST(N'1947-10-28T00:00:00.000' AS DateTime), 152757214, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (6, N'SANCHEZ, CRISTINA DORA', CAST(N'1942-09-15T00:00:00.000' AS DateTime), 152745068, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (7, N'ARCE, YESICA J', CAST(N'1980-03-31T00:00:00.000' AS DateTime), 152728113, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (8, N'URQUIZA, CESAR DANIEL', CAST(N'1978-01-21T00:00:00.000' AS DateTime), 152263932, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (9, N'RODRIGUEZ, DARIO MARTIN', CAST(N'1970-11-06T00:00:00.000' AS DateTime), 152021902, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (10, N'GULLI, JORGE DANIEL', CAST(N'1967-10-18T00:00:00.000' AS DateTime), 152018429, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (11, N'NAVARRO, DANIEL ALEJANDRO', CAST(N'1969-02-14T00:00:00.000' AS DateTime), 152020622, 1)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (12, N'RAMONDA, FRANCISCA MIGUELA', CAST(N'1947-03-02T00:00:00.000' AS DateTime), 152747404, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (13, N'BOTTI, AMELIA HAYDEE', CAST(N'1942-03-24T00:00:00.000' AS DateTime), 152745100, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (14, N'MORENO, DELIA', CAST(N'1943-08-06T00:00:00.000' AS DateTime), 152745645, 1)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (15, N'NIEVAS, JULIA AZUCENA', CAST(N'1946-07-27T00:00:00.000' AS DateTime), 152754099, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (16, N'MAROSTICA, CARLOS ERNESTO', CAST(N'1949-04-03T00:00:00.000' AS DateTime), 152076934, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (17, N'LEDESMA, MERCEDES DEL CARMEN', CAST(N'1936-11-15T00:00:00.000' AS DateTime), 152737034, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (18, N'FRULLINGUI, MIGUEL ANGEL', CAST(N'1945-05-08T00:00:00.000' AS DateTime), 152060606, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (19, N'FLORES, MERCEDES YOLANDA', CAST(N'1944-05-20T00:00:00.000' AS DateTime), 152750979, 1)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (20, N'MENDOZA, MARTIN ELEUTERIO', CAST(N'1946-11-03T00:00:00.000' AS DateTime), 152080419, 1)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (21, N'RIVADERO, NILDA MIRTA', CAST(N'1942-08-17T00:00:00.000' AS DateTime), 152745077, 1)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (22, N'LUBARY, ELENA DEL CARMEN', CAST(N'1951-09-14T00:00:00.000' AS DateTime), 152766475, 1)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (23, N'MALDONADO, AURORA DEL VALLE', CAST(N'1948-07-04T00:00:00.000' AS DateTime), 152757307, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (24, N'CARRANZA, FRANCISCA CORINA', CAST(N'1932-10-16T00:00:00.000' AS DateTime), 152715724, 1)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (25, N'DELGADO, RAMONA', CAST(N'1950-01-25T00:00:00.000' AS DateTime), 152762645, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (26, N'LUCERO, ROSARIO DEL CARMEN', CAST(N'1940-07-06T00:00:00.000' AS DateTime), 152738875, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (27, N'DOMINGUEZ, MARTHA AMALIA', CAST(N'1950-06-27T00:00:00.000' AS DateTime), 152756368, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (28, N'PIZZUTO, SEBASTIAN', CAST(N'1950-09-07T00:00:00.000' AS DateTime), 152084118, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (29, N'GIGENA, OFELIA DORA', CAST(N'1949-04-17T00:00:00.000' AS DateTime), 152766803, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (30, N'ROMERO, HAYDEE', CAST(N'1948-07-11T00:00:00.000' AS DateTime), 152757307, 1)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (31, N'SAUR, ELENA MARIA', CAST(N'1947-01-04T00:00:00.000' AS DateTime), 152752474, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (32, N'PIRIS, MARGARITA AURELIA', CAST(N'1947-04-15T00:00:00.000' AS DateTime), 152752474, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (33, N'GUZMAN, ALEJANDRINA', CAST(N'1936-02-26T00:00:00.000' AS DateTime), 152727989, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (34, N'ZAPATA, ALICIA ANGELICA', CAST(N'1944-07-14T00:00:00.000' AS DateTime), 152748502, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (35, N'RODRIGUEZ, MATEO ELADIO', CAST(N'1939-11-08T00:00:00.000' AS DateTime), 152064291, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (36, N'AGUIRRE, ANDRES RAFAEL', CAST(N'1949-09-09T00:00:00.000' AS DateTime), 152078560, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (37, N'VIGNOLA, RUBEN BENITO', CAST(N'1941-01-12T00:00:00.000' AS DateTime), 152064315, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (38, N'ACHERVI, MIGUEL ANGEL', CAST(N'1946-03-08T00:00:00.000' AS DateTime), 152064435, 1)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (39, N'LENCINAS, CARLOS ALBERTO', CAST(N'1939-08-29T00:00:00.000' AS DateTime), 152064288, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (40, N'NADALIN, OTILIO JOSE', CAST(N'1938-08-02T00:00:00.000' AS DateTime), 152064260, 1)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (41, N'MOYANO, RAUL ANTONIO', CAST(N'1942-04-03T00:00:00.000' AS DateTime), 152364345, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (42, N'MOYANO, LINA MARGARITA', CAST(N'1939-12-10T00:00:00.000' AS DateTime), 152787853, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (43, N'ABRATE, VICENTE BARTOLO', CAST(N'1937-08-10T00:00:00.000' AS DateTime), 152064246, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (44, N'MEZ, JUAN CARLOS', CAST(N'1945-03-03T00:00:00.000' AS DateTime), 152079438, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (45, N'HEREDIA, SARA ANTONIA', CAST(N'1938-08-10T00:00:00.000' AS DateTime), 152737567, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (46, N'CONTRERAS, NORMA BEATRIZ', CAST(N'1969-08-08T00:00:00.000' AS DateTime), 152207851, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (47, N'ORTEGA, JESUS', CAST(N'1948-12-25T00:00:00.000' AS DateTime), 152084107, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (48, N'DOMINGUEZ, AURORA', CAST(N'1949-12-21T00:00:00.000' AS DateTime), 152759797, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (49, N'SILVESTRE, CRISTINA BARBARIANA', CAST(N'1949-06-25T00:00:00.000' AS DateTime), 152759959, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (50, N'SANTOS, ESTELA ELVIRA', CAST(N'1946-10-11T00:00:00.000' AS DateTime), 152754523, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (51, N'BARRIONUEVO, AIDA NORA', CAST(N'1948-01-01T00:00:00.000' AS DateTime), 152756285, 1)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (52, N'GUZMAN, RAMONA  ESPERANZA', CAST(N'1949-11-14T00:00:00.000' AS DateTime), 152761327, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (53, N'VIDELA, CONSTANCIA', CAST(N'1947-11-21T00:00:00.000' AS DateTime), 152756981, 1)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (54, N'SANCHEZ, JUAN CARLOS', CAST(N'1944-01-18T00:00:00.000' AS DateTime), 152079758, 1)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (55, N'ROBLEDO, LUIS ARMANDO', CAST(N'1949-01-06T00:00:00.000' AS DateTime), 152376302, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (56, N'SOSA, SIMONA', CAST(N'1945-01-01T00:00:00.000' AS DateTime), 152750083, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (57, N'BRITO, MIGUEL F', CAST(N'1940-09-04T00:00:00.000' AS DateTime), 152065152, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (58, N'GONZALEZ, RAMON ELADIO', CAST(N'1950-06-10T00:00:00.000' AS DateTime), 152080096, 1)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (59, N'ECHENIQUE, EFRAIN', CAST(N'1948-06-18T00:00:00.000' AS DateTime), 152380009, 1)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (60, N'VALDEZ, VICENTA', CAST(N'1946-10-27T00:00:00.000' AS DateTime), 152754786, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (61, N'ALBORNOZ, SILVIA BEATRIZ', CAST(N'1946-08-15T00:00:00.000' AS DateTime), 152454098, 1)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (62, N'SOSA, CLEOFO MERCEDES', CAST(N'1943-09-25T00:00:00.000' AS DateTime), 152063919, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (63, N'MARTINEZ, JULIA AZUCENA', CAST(N'1935-07-21T00:00:00.000' AS DateTime), 152320349, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (64, N'FERNANDEZ, ANGELA YOLANDA', CAST(N'1944-09-12T00:00:00.000' AS DateTime), 152748208, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (65, N'ALIENDRO, OLGA', CAST(N'1938-01-11T00:00:00.000' AS DateTime), 152735849, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (66, N'AMERI, OLGA ESTHER', CAST(N'1971-01-01T00:00:00.000' AS DateTime), 152752601, 1)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (67, N'ORONA, ROSA ZULEMA', CAST(N'1934-01-04T00:00:00.000' AS DateTime), 152724405, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (68, N'OVIEDO, AIDEE FELIPA', CAST(N'1944-08-23T00:00:00.000' AS DateTime), 152342945, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (69, N'MORENO, ELISA CRISTINA', CAST(N'1948-07-06T00:00:00.000' AS DateTime), 152757382, 1)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (70, N'MEZ, CANDELARIA ALICIA', CAST(N'1940-06-11T00:00:00.000' AS DateTime), 152741089, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (71, N'ORTIZ, EMILIA V', CAST(N'1946-10-03T00:00:00.000' AS DateTime), 152746696, 1)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (72, N'ROJAS, JORGE CORNELIO', CAST(N'1941-09-19T00:00:00.000' AS DateTime), 152065500, 1)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (73, N'BUENO, GERARDO', CAST(N'1937-02-21T00:00:00.000' AS DateTime), 152065441, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (74, N'STREET, SUSANA MARGARITA', CAST(N'1947-06-05T00:00:00.000' AS DateTime), 152348895, 1)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (75, N'SANCHEZ, GRISELDA ELINA', CAST(N'1975-10-09T00:00:00.000' AS DateTime), 152726373, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (76, N'CENA, MARGARITA ROSA', CAST(N'1939-07-21T00:00:00.000' AS DateTime), 152799544, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (77, N'CRISTALDO MARTINEZ, DEONILDA', CAST(N'1960-04-08T00:00:00.000' AS DateTime), 152792824, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (78, N'PETRELLI, FABIANA', CAST(N'1962-12-03T00:00:00.000' AS DateTime), 152716275, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (79, N'MARTINEZ, GINES', CAST(N'1935-05-25T00:00:00.000' AS DateTime), 152063770, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (80, N'BARRIOS, AGUSTIN', CAST(N'1945-04-02T00:00:00.000' AS DateTime), 152766542, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (81, N'RIVERO, SUSANA ELENA', CAST(N'1948-08-04T00:00:00.000' AS DateTime), 152759184, 1)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (82, N'BLETRAMO, LEANDRO EMANUEL', CAST(N'1989-05-17T00:00:00.000' AS DateTime), 152762650, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (83, N'VINCENZINI, NORMA IRIS', CAST(N'1950-03-05T00:00:00.000' AS DateTime), 152759559, 1)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (84, N'FERREYRA, MARTA GRACIELA', CAST(N'1947-11-12T00:00:00.000' AS DateTime), 152756608, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (85, N'VERON, MANUEL VALLE', CAST(N'1946-10-11T00:00:00.000' AS DateTime), 152079566, 1)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (86, N'RUIZ, ELENA VICENTA', CAST(N'1949-12-08T00:00:00.000' AS DateTime), 152753276, 1)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (87, N'SAGUILAN, LUICIANA', CAST(N'1944-01-16T00:00:00.000' AS DateTime), 152744869, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (88, N'BRIZUELA, JUAN AGUSTIN', CAST(N'1943-08-29T00:00:00.000' AS DateTime), 152071211, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (89, N'LOBO, SILVESTRE AMARANTO', CAST(N'1936-12-20T00:00:00.000' AS DateTime), 152363804, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (90, N'LOVERA, LUCIA MARIA', CAST(N'1945-01-22T00:00:00.000' AS DateTime), 152749905, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (91, N'VEGA, HERMINDA MIRTA', CAST(N'1944-08-16T00:00:00.000' AS DateTime), 152748901, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (92, N'FALCON, OLGA HERMENEGILDA', CAST(N'1934-05-06T00:00:00.000' AS DateTime), 152731890, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (93, N'SOLA, ARMINDA', CAST(N'1949-05-20T00:00:00.000' AS DateTime), 152748209, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (94, N'BARRIONUEVO, ROQUE RAUL', CAST(N'1949-08-18T00:00:00.000' AS DateTime), 152065611, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (95, N'NUÑEZ, CARLOS', CAST(N'1936-01-05T00:00:00.000' AS DateTime), 152066364, 3)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (96, N'PEREZ, EMMA CIRILA', CAST(N'1941-07-30T00:00:00.000' AS DateTime), 152741288, 1)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (97, N'MANSILLA, AMALIA C', CAST(N'1950-11-23T00:00:00.000' AS DateTime), 152762205, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (98, N'HEREDIA, TOMAS GREGORIO', CAST(N'1941-04-12T00:00:00.000' AS DateTime), 152064321, 2)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (99, N'HEREDIA, OMAR CESAR', CAST(N'1942-03-17T00:00:00.000' AS DateTime), 152079672, 1)

INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [FechaNacimiento], [Telefono], [IdCategoria]) VALUES (100, N'MONSERRAT, YOLANDA ISABEL', CAST(N'1948-10-05T00:00:00.000' AS DateTime), 152757382, 2)

SET IDENTITY_INSERT [dbo].[Contactos] OFF



SET IDENTITY_INSERT [dbo].[Departamentos] ON 

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (65, N'ADOLFO ALSINA', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (66, N'ADOLFO GONZALES CHAVES', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (67, N'ALBERTI', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (68, N'ALMIRANTE BROWN', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (69, N'AVELLANEDA', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (70, N'AYACUCHO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (71, N'AZUL', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (72, N'BAHIA BLANCA', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (73, N'BALCARCE', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (74, N'BARADERO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (75, N'BARTOLOME MITRE', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (76, N'BENITO JUAREZ', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (77, N'BERAZATEGUI', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (78, N'BERISSO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (79, N'BOLIVAR', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (80, N'BRAGADO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (81, N'CAMPANA', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (82, N'CAÑUELAS', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (83, N'CAPITAL FEDERAL', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (84, N'CAPITAN SARMIENTO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (85, N'CARLOS CASARES', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (86, N'CARLOS TEJEDOR', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (87, N'CARMEN DE ARECO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (88, N'CASTELLI', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (89, N'CHACABUCO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (90, N'CHASCOMUS', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (91, N'CHIVILCOY', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (92, N'CIUDAD AUTONOMA DE BUENOS AIRES', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (93, N'COLON', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (94, N'CORONEL BRANDSEN', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (95, N'CORONEL DORREGO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (96, N'CORONEL PRINGLES', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (97, N'CORONEL ROSALES', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (98, N'CORONEL SUAREZ', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (99, N'DAIREAUX', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (100, N'DE LA COSTA', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (101, N'DOLORES', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (102, N'ENSENADA', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (103, N'ESCOBAR', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (104, N'ESTEBAN ECHEVERRIA', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (105, N'EXALTACION DE LA CRUZ', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (106, N'EZEIZA', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (107, N'FLORENCIO VARELA', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (108, N'FLORENTINO AMEGHINO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (109, N'GENERAL ALVARADO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (110, N'GENERAL ALVEAR', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (111, N'GENERAL ARENALES', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (112, N'GENERAL BELGRANO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (113, N'GENERAL GUIDO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (114, N'GENERAL LAMADRID', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (115, N'GENERAL LAS HERAS', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (116, N'GENERAL LAVALLE', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (117, N'GENERAL MADARIAGA', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (118, N'GENERAL PAZ', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (119, N'GENERAL PINTO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (120, N'GENERAL PUEYRREDON', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (121, N'GENERAL RODRIGUEZ', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (122, N'GENERAL SAN MARTIN', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (123, N'GENERAL VIAMONTE', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (124, N'GENERAL VILLEGAS', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (125, N'GUAMINI', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (126, N'HIPOLITO YRIYEN', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (127, N'HURLINGHAM', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (128, N'ITUZAIN', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (129, N'JOSE CLEMENTE PAZ', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (130, N'JUNIN', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (131, N'LA MATANZA', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (132, N'LA PLATA', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (133, N'LANUS', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (134, N'LAPRIDA', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (135, N'LAS FLORES', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (136, N'LEANDRO N  ALEM', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (137, N'LINCOLN', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (138, N'LOBERIA', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (139, N'LOBOS', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (140, N'LOMAS DE ZAMORA', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (141, N'LUJAN', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (142, N'MAGDALENA', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (143, N'MAIPU', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (144, N'MALVINAS ARGENTINAS', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (145, N'MAR CHIQUITA', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (146, N'MARCOS PAZ', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (147, N'MERCEDES', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (148, N'MERLO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (149, N'MONTE', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (150, N'MONTE HERMOSO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (151, N'MORENO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (152, N'MORON', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (153, N'NAVARRO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (154, N'NECOCHEA', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (155, N'OLAVARRIA', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (156, N'PATAGONES', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (157, N'PEHUAJO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (158, N'PELLEGRINI', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (159, N'PERGAMINO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (160, N'PILA', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (161, N'PILAR', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (162, N'PINAMAR', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (163, N'PRESIDENTE PERON', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (164, N'PUAN', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (165, N'PUNTA INDIO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (166, N'QUILMES', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (167, N'RAMALLO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (168, N'RAUCH', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (169, N'RIVADAVIA', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (170, N'ROJAS', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (171, N'ROQUE PEREZ', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (172, N'SAAVEDRA', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (173, N'SALADILLO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (174, N'SALLIQUELO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (175, N'SALTO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (176, N'SAN ANDRES DE GILES', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (177, N'SAN ANTONIO DE ARECO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (178, N'SAN CAYETANO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (179, N'SAN FERNANDO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (180, N'SAN ISIDRO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (181, N'SAN MIGUEL', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (182, N'SAN NICOLAS', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (183, N'SAN PEDRO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (184, N'SAN VICENTE', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (185, N'SUIPACHA', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (186, N'TANDIL', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (187, N'TAPALQUE', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (188, N'TIGRE', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (189, N'TORDILLO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (190, N'TORNQUIST', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (191, N'TRENQUE LAUQUEN', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (192, N'TRES ARROYOS', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (193, N'TRES DE FEBRERO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (194, N'TRES LOMAS', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (195, N'VICENTE LOPEZ', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (196, N'VILLA GESELL', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (197, N'VILLARINO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (198, N'ZARATE', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (199, N'25 DE MAYO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (200, N'9 DE JULIO', 5)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (202, N'CALAMUCHITA', 4)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (203, N'CAPITAL', 4)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (204, N'COLON', 4)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (205, N'CRUZ DEL EJE', 4)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (206, N'GENERAL ROCA', 4)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (207, N'GENERAL SAN MARTIN', 4)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (208, N'ISCHILIN', 4)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (209, N'JUAREZ CELMAN', 4)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (210, N'MARCOS JUAREZ', 4)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (211, N'MINAS', 4)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (212, N'POCHO', 4)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (213, N'PTE. ROQUE SAENZ PEÑA', 4)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (214, N'PUNILLA', 4)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (215, N'RIO CUARTO', 4)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (216, N'RIO PRIMERO', 4)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (217, N'RIO SECO', 4)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (218, N'RIO SEGUNDO', 4)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (219, N'SAN ALBERTO', 4)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (220, N'SAN JAVIER', 4)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (221, N'SAN JUSTO', 4)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (222, N'SANTA MARIA', 4)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (224, N'SOBREMONTE', 4)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (225, N'TERCERO ARRIBA', 4)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (226, N'TOTORAL', 4)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (227, N'TULUMBA', 4)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (228, N'UNION', 4)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (230, N'BELGRANO', 6)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (231, N'CASEROS', 6)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (232, N'CASTELLANOS', 6)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (233, N'CONSTITUCION', 6)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (234, N'GARAY', 6)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (235, N'GENERAL LOPEZ', 6)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (236, N'GENERAL OBLIGADO', 6)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (237, N'IRIONDO', 6)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (238, N'LA CAPITAL', 6)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (239, N'LAS COLONIAS', 6)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (240, N'ROSARIO', 6)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (241, N'SAN CRISTOBAL', 6)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (242, N'SAN JAVIER', 6)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (243, N'SAN JERONIMO', 6)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (244, N'SAN JUSTO', 6)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (245, N'SAN LORENZO', 6)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (246, N'SAN MARTIN', 6)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (247, N'SIN INFORMAR', 6)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (248, N'VERA', 6)

INSERT [dbo].[Departamentos] ([IdDepartamento], [Nombre], [IdProvincia]) VALUES (249, N'9 DE JULIO', 6)

SET IDENTITY_INSERT [dbo].[Departamentos] OFF



SET IDENTITY_INSERT [dbo].[Empresas] ON 

INSERT [dbo].[Empresas] ([IdEmpresa], [RazonSocial], [CantidadEmpleados], [FechaFundacion]) VALUES (1, N'SANTEX GROUP', 300, CAST(N'1976-07-09T00:00:00' AS SmallDateTime))

INSERT [dbo].[Empresas] ([IdEmpresa], [RazonSocial], [CantidadEmpleados], [FechaFundacion]) VALUES (2, N'COMPAÑIA SUR', 256, CAST(N'1974-04-09T00:00:00' AS SmallDateTime))

INSERT [dbo].[Empresas] ([IdEmpresa], [RazonSocial], [CantidadEmpleados], [FechaFundacion]) VALUES (3, N'SANTO DOMIN', 120, CAST(N'1999-10-04T00:00:00' AS SmallDateTime))

INSERT [dbo].[Empresas] ([IdEmpresa], [RazonSocial], [CantidadEmpleados], [FechaFundacion]) VALUES (4, N'COCA COLA', 234, CAST(N'2000-02-02T00:00:00' AS SmallDateTime))

INSERT [dbo].[Empresas] ([IdEmpresa], [RazonSocial], [CantidadEmpleados], [FechaFundacion]) VALUES (7, N'MAFRE', 125, CAST(N'2000-02-20T00:00:00' AS SmallDateTime))

INSERT [dbo].[Empresas] ([IdEmpresa], [RazonSocial], [CantidadEmpleados], [FechaFundacion]) VALUES (8, N'RIVADAVIA', 3, CAST(N'2000-12-12T00:00:00' AS SmallDateTime))

INSERT [dbo].[Empresas] ([IdEmpresa], [RazonSocial], [CantidadEmpleados], [FechaFundacion]) VALUES (9, N'ABC', 3, CAST(N'2001-10-10T00:00:00' AS SmallDateTime))

INSERT [dbo].[Empresas] ([IdEmpresa], [RazonSocial], [CantidadEmpleados], [FechaFundacion]) VALUES (10, N'CON FINES DE LUCRO', 3, CAST(N'2001-02-02T00:00:00' AS SmallDateTime))

INSERT [dbo].[Empresas] ([IdEmpresa], [RazonSocial], [CantidadEmpleados], [FechaFundacion]) VALUES (11, N'ARCOR', 3, CAST(N'2000-10-10T00:00:00' AS SmallDateTime))

INSERT [dbo].[Empresas] ([IdEmpresa], [RazonSocial], [CantidadEmpleados], [FechaFundacion]) VALUES (12, N'NOE AND COMPANY', 10, CAST(N'1900-11-11T00:00:00' AS SmallDateTime))

INSERT [dbo].[Empresas] ([IdEmpresa], [RazonSocial], [CantidadEmpleados], [FechaFundacion]) VALUES (13, N'SIN FIN DE LUCRO', 555, CAST(N'2010-01-01T00:00:00' AS SmallDateTime))

INSERT [dbo].[Empresas] ([IdEmpresa], [RazonSocial], [CantidadEmpleados], [FechaFundacion]) VALUES (14, N'MAXEMPRESA', 128, CAST(N'1900-01-01T00:00:00' AS SmallDateTime))

INSERT [dbo].[Empresas] ([IdEmpresa], [RazonSocial], [CantidadEmpleados], [FechaFundacion]) VALUES (16, N'EMPRESA PRIVADA BIS SRL', 50, CAST(N'2000-08-02T00:00:00' AS SmallDateTime))

INSERT [dbo].[Empresas] ([IdEmpresa], [RazonSocial], [CantidadEmpleados], [FechaFundacion]) VALUES (17, N'EMPRESA PRIVADA BIS', 52, CAST(N'2000-08-01T00:00:00' AS SmallDateTime))

SET IDENTITY_INSERT [dbo].[Empresas] OFF



INSERT [dbo].[EstadosCiviles] ([IdEstadoCivil], [Nombre]) VALUES (N'C', N'CASADO')

INSERT [dbo].[EstadosCiviles] ([IdEstadoCivil], [Nombre]) VALUES (N'Z', N'CELIBE')

INSERT [dbo].[EstadosCiviles] ([IdEstadoCivil], [Nombre]) VALUES (N'D', N'DIVORCIADO')

INSERT [dbo].[EstadosCiviles] ([IdEstadoCivil], [Nombre]) VALUES (N'S', N'SOLTERO')

INSERT [dbo].[EstadosCiviles] ([IdEstadoCivil], [Nombre]) VALUES (N'V', N'VIUDO')



SET IDENTITY_INSERT [dbo].[Grupos] ON 

INSERT [dbo].[Grupos] ([IdGrupo], [Nombre]) VALUES (1, N'ADMINISTRADORES')

INSERT [dbo].[Grupos] ([IdGrupo], [Nombre]) VALUES (2, N'SISTEMAS')

INSERT [dbo].[Grupos] ([IdGrupo], [Nombre]) VALUES (3, N'VENDEDORES')

INSERT [dbo].[Grupos] ([IdGrupo], [Nombre]) VALUES (4, N'CAJEROS')

INSERT [dbo].[Grupos] ([IdGrupo], [Nombre]) VALUES (5, N'REPARTIDORES')

INSERT [dbo].[Grupos] ([IdGrupo], [Nombre]) VALUES (6, N'CONTROLADORES')

SET IDENTITY_INSERT [dbo].[Grupos] OFF



SET IDENTITY_INSERT [dbo].[IvasTipos] ON 

INSERT [dbo].[IvasTipos] ([IvaTipoId], [Nombre]) VALUES (4, N'MONOTRIBUTO')

INSERT [dbo].[IvasTipos] ([IvaTipoId], [Nombre]) VALUES (5, N'RESPONSABLE INSCRIPTO')

INSERT [dbo].[IvasTipos] ([IvaTipoId], [Nombre]) VALUES (6, N'RESPONSABLE NO INSCRIPTO')

INSERT [dbo].[IvasTipos] ([IvaTipoId], [Nombre]) VALUES (7, N'EXENTO')

INSERT [dbo].[IvasTipos] ([IvaTipoId], [Nombre]) VALUES (8, N'CONSUMIDOR FINAL')

INSERT [dbo].[IvasTipos] ([IvaTipoId], [Nombre]) VALUES (9, N'OTRO REPONSABLE')

SET IDENTITY_INSERT [dbo].[IvasTipos] OFF


SET IDENTITY_INSERT [dbo].[Menus] ON 

INSERT [dbo].[Menus] ([IdMenu], [Nombre], [Url], [IdMenuPadre], [Orden], [IdPermiso]) VALUES (1, N'Sistema', NULL, NULL, 70, NULL)

INSERT [dbo].[Menus] ([IdMenu], [Nombre], [Url], [IdMenuPadre], [Orden], [IdPermiso]) VALUES (2, N'Ventas', N'', NULL, 10, NULL)

INSERT [dbo].[Menus] ([IdMenu], [Nombre], [Url], [IdMenuPadre], [Orden], [IdPermiso]) VALUES (3, N'Compras', NULL, NULL, 20, NULL)

INSERT [dbo].[Menus] ([IdMenu], [Nombre], [Url], [IdMenuPadre], [Orden], [IdPermiso]) VALUES (4, N'Articulos', NULL, NULL, 30, NULL)

INSERT [dbo].[Menus] ([IdMenu], [Nombre], [Url], [IdMenuPadre], [Orden], [IdPermiso]) VALUES (5, N'Clientes', N'', NULL, 40, NULL)

INSERT [dbo].[Menus] ([IdMenu], [Nombre], [Url], [IdMenuPadre], [Orden], [IdPermiso]) VALUES (6, N'Proveedores', NULL, NULL, 50, NULL)

INSERT [dbo].[Menus] ([IdMenu], [Nombre], [Url], [IdMenuPadre], [Orden], [IdPermiso]) VALUES (7, N'Administracion', NULL, NULL, 60, NULL)

INSERT [dbo].[Menus] ([IdMenu], [Nombre], [Url], [IdMenuPadre], [Orden], [IdPermiso]) VALUES (8, N'Login', NULL, 1, 10, 1)

INSERT [dbo].[Menus] ([IdMenu], [Nombre], [Url], [IdMenuPadre], [Orden], [IdPermiso]) VALUES (9, N'Gestion Usuarios', N'UsuariosWF.aspx', 1, 20, 2)

INSERT [dbo].[Menus] ([IdMenu], [Nombre], [Url], [IdMenuPadre], [Orden], [IdPermiso]) VALUES (10, N'Gestion Grupos', NULL, 1, 30, 3)

INSERT [dbo].[Menus] ([IdMenu], [Nombre], [Url], [IdMenuPadre], [Orden], [IdPermiso]) VALUES (11, N'Gestion Permisos', NULL, 1, 40, 4)

INSERT [dbo].[Menus] ([IdMenu], [Nombre], [Url], [IdMenuPadre], [Orden], [IdPermiso]) VALUES (12, N'Gestion Menus', NULL, 1, 50, 5)

INSERT [dbo].[Menus] ([IdMenu], [Nombre], [Url], [IdMenuPadre], [Orden], [IdPermiso]) VALUES (13, N'Configurar Sitio', NULL, 1, 60, 6)

INSERT [dbo].[Menus] ([IdMenu], [Nombre], [Url], [IdMenuPadre], [Orden], [IdPermiso]) VALUES (14, N'Gestionar Clientes', N'ClientesWF.aspx', NULL, 10, 7)

SET IDENTITY_INSERT [dbo].[Menus] OFF



SET IDENTITY_INSERT [dbo].[Paises] ON 

INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (1, N'ARGENTINA', 4400000, N'AMERICA DEL SUR')

INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (4, N'CHILE', 20000000, N'AMERICA DEL SUR')

INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (5, N'URUGUAY', 15000000, N'AMERICA DEL SUR')

INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (6, N'PARAGUAY', 20000000, N'AMERICA DEL SUR')

INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (7, N'BOLIVIA', 25000000, N'AMERICA DEL SUR')

INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (8, N'ESTADOS UNIDOS', 200000000, N'AMERICA DEL NORTE')

INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (9, N'FRANCIA', 80000000, N'EUROPA')

INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (10, N'SUD AFRICA', 35000000, N'AFRICA')

INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (12, N'ESPAÑA', 45000000, N'EUROPA')

INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (13, N'CANADA', 40000000, N'AMERICA DEL NORTE')

INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (14, N'PUERTO RICO', 10000000, N'AMERICA CENTRAL')

INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (15, N'COLOMBIA', NULL, N'AMERICA DEL SUR')

INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (18, N'JAPON', 70000000, N'ASIA')

INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (19, N'BRASIL', 150000000, N'AMERICA DEL SUR')

INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (20, N'COREA DEL SUR', 10000000, N'ASIA')

INSERT [dbo].[Paises] ([IdPais], [Nombre], [Poblacion], [Continente]) VALUES (22, N'COREA DEL NORTE', 40000000, N'ASIA')

SET IDENTITY_INSERT [dbo].[Paises] OFF




SET IDENTITY_INSERT [dbo].[Personas] ON 

INSERT [dbo].[Personas] ([IdPersona], [Nombre]) VALUES (1, N'JUAN')

INSERT [dbo].[Personas] ([IdPersona], [Nombre]) VALUES (2, N'PEDRO')

INSERT [dbo].[Personas] ([IdPersona], [Nombre]) VALUES (3, N'MARIA')

INSERT [dbo].[Personas] ([IdPersona], [Nombre]) VALUES (4, N'CELESTE')

INSERT [dbo].[Personas] ([IdPersona], [Nombre]) VALUES (5, N'FERNANDO')

SET IDENTITY_INSERT [dbo].[Personas] OFF



SET IDENTITY_INSERT [dbo].[Provincias] ON 

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (1, N'SALTA', 1)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (2, N'TUCUMAN', 1)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (3, N'SAN JUAN', 1)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (4, N'CORDOBA', 1)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (5, N'BUENOS AIRES', 1)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (6, N'SANTA FE', 1)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (7, N'SANTIA DEL ESTERO', 1)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (8, N'FORMOSA', 1)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (9, N'ENTRE RIOS', 1)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (10, N'CORRIENTES', 1)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (11, N'MISIONES', 1)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (12, N'LA PAMPA', 1)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (13, N'PERROCITY', 1)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (14, N'SAN LUIS', 1)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (15, N'CATAMARCA', 1)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (16, N'ACRE', 19)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (17, N'ALAAS', 19)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (18, N'AMAPA', 19)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (19, N'AMAZONAS', 19)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (20, N'BAHÍA', 19)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (21, N'CEARA', 19)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (22, N'DISTRITO FEDERAL', 19)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (23, N'ESPIRITO SANTO', 19)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (24, N'IAS', 19)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (25, N'MARANHAO', 19)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (26, N'MATO GROSSO', 19)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (27, N'MATO GROSSO DO SUL', 19)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (28, N'MINAS GERAIS', 19)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (29, N'PARA', 19)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (30, N'PARAIBA', 19)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (31, N'PARANÁ', 19)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (32, N'PERNAMBUCO', 19)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (33, N'PIAUI', 19)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (34, N'RÍO DE JANEIRO', 19)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (35, N'RÍO GRANDE DO NORTE', 19)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (36, N'RÍO GRANDE DO SUL', 19)

INSERT [dbo].[Provincias] ([IdProvincia], [Nombre], [IdPais]) VALUES (37, N'RONDONIA', 19)

SET IDENTITY_INSERT [dbo].[Provincias] OFF



INSERT [dbo].[Sesiones] ([IdSesion], [Maquina], [FechaInicio], [FechaFin], [IdUsuario]) VALUES (N'1', N'LABSIS204', CAST(N'2011-06-07T10:13:00.000' AS DateTime), NULL, NULL)

INSERT [dbo].[Sesiones] ([IdSesion], [Maquina], [FechaInicio], [FechaFin], [IdUsuario]) VALUES (N'2', N'LABSIS704', CAST(N'2011-09-15T00:00:00.000' AS DateTime), CAST(N'2011-09-15T00:00:00.000' AS DateTime), 1)



INSERT [dbo].[Sexos] ([IdSexo], [Nombre]) VALUES (N'F', N'FEMENINO')

INSERT [dbo].[Sexos] ([IdSexo], [Nombre]) VALUES (N'M', N'MASCULINO')


INSERT [dbo].[TiposDocumentos] ([IdTipoDocumento], [Nombre]) VALUES (N'CF', N'CEDULA FEDERAL')

INSERT [dbo].[TiposDocumentos] ([IdTipoDocumento], [Nombre]) VALUES (N'CP', N'CEDULA PROVINCIAL')

INSERT [dbo].[TiposDocumentos] ([IdTipoDocumento], [Nombre]) VALUES (N'DNI', N'DOCUMENTO NACIONAL')

INSERT [dbo].[TiposDocumentos] ([IdTipoDocumento], [Nombre]) VALUES (N'LE', N'LIBRETA ENROLAMIENTO')

INSERT [dbo].[TiposDocumentos] ([IdTipoDocumento], [Nombre]) VALUES (N'PSP', N'PASAPORTE')



SET IDENTITY_INSERT [dbo].[Usuarios] ON 

INSERT [dbo].[Usuarios] ([IdUsuario], [Nombre], [Clave], [Bloqueado], [IdRegistroRelacion], [IdTablaRelacion]) VALUES (1, N'admin', N'admin123', NULL, NULL, NULL)

INSERT [dbo].[Usuarios] ([IdUsuario], [Nombre], [Clave], [Bloqueado], [IdRegistroRelacion], [IdTablaRelacion]) VALUES (2, N'juan', N'juan123', NULL, NULL, NULL)

INSERT [dbo].[Usuarios] ([IdUsuario], [Nombre], [Clave], [Bloqueado], [IdRegistroRelacion], [IdTablaRelacion]) VALUES (4, N'pedro', N'pedro123', NULL, NULL, NULL)

SET IDENTITY_INSERT [dbo].[Usuarios] OFF


SET IDENTITY_INSERT [dbo].[Clientes] ON 

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (1, N'SANCHEZ, JUAN CARLOS', 20079758583, N'DNI', 7975858, CAST(N'1944-01-18T03:00:00.000' AS DateTime), N'M', N'S', N'SANCHEZ', 797, N'SIN ASIGNAR', 66, 4, 1, CAST(N'1951-01-16T03:00:00.000' AS DateTime), CAST(N'1955-01-15T03:00:00.000' AS DateTime), 1, 1, 0, CAST(123.45 AS Decimal(18, 2)), N'sanchezjuancarlos@hotmail.com', NULL, NULL, 1, N'Observacion de SANCHEZ, JUAN CARLOS')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (2, N'ROBLEDO, LUIS ARMANDO', 23763028397, N'DNI', 7630283, CAST(N'1949-01-06T00:00:00.000' AS DateTime), N'M', N'Z', N'ROBLEDO', 763, N'CORDOBA CAPITAL', 67, 1, NULL, CAST(N'1956-01-05T00:00:00.000' AS DateTime), CAST(N'1960-01-04T00:00:00.000' AS DateTime), 0, 1, 1, CAST(1188151.00 AS Decimal(18, 2)), N'robledoluisarmando@gmail.com', NULL, NULL, 1, N'Observacion de ROBLEDO, LUIS ARMANDO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (3, N'SOSA, SIMONA', 27500830852, N'DNI', 5008308, CAST(N'1945-01-01T00:00:00.000' AS DateTime), N'F', N'S', N'SOSA', 500, N'CORDOBA CAPITAL', 68, 2, NULL, CAST(N'1951-12-31T00:00:00.000' AS DateTime), CAST(N'1955-12-30T00:00:00.000' AS DateTime), 1, 0, 0, CAST(916694.00 AS Decimal(18, 2)), N'sosasimona@hotmail.com', NULL, NULL, 1, N'Observacion de SOSA, SIMONA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (4, N'BRITO, MIGUEL F', 20651529251, N'DNI', 6515292, CAST(N'1940-09-04T00:00:00.000' AS DateTime), N'M', N'S', N'BRITO', 651, N'CORDOBA CAPITAL', 69, 3, NULL, CAST(N'1947-09-03T00:00:00.000' AS DateTime), CAST(N'1951-09-02T00:00:00.000' AS DateTime), 0, 1, 0, CAST(516288.00 AS Decimal(18, 2)), N'britomiguelf@gmail.com', NULL, NULL, 1, N'Observacion de BRITO, MIGUEL F')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (5, N'GONZALEZ, RAMON ELADIO', 20800962090, N'DNI', 8009620, CAST(N'1950-06-10T00:00:00.000' AS DateTime), N'M', N'Z', N'NZALEZ', 800, N'CORDOBA CAPITAL', 70, 4, NULL, CAST(N'1957-06-08T00:00:00.000' AS DateTime), CAST(N'1961-06-07T00:00:00.000' AS DateTime), 1, 0, 0, CAST(416019.00 AS Decimal(18, 2)), N'gonzalezramoneladio@hotmail.com', NULL, NULL, 1, N'Observacion de NZALEZ, RAMON ELADIO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (6, N'ECHENIQUE, EFRAIN', 23800091196, N'DNI', 8000911, CAST(N'1948-06-18T00:00:00.000' AS DateTime), N'M', N'C', N'ECHENIQUE', 800, N'CORDOBA CAPITAL', 71, 5, NULL, CAST(N'1955-06-17T00:00:00.000' AS DateTime), CAST(N'1959-06-16T00:00:00.000' AS DateTime), 0, 0, 1, CAST(396668.00 AS Decimal(18, 2)), N'echeniqueefrain@gmail.com', NULL, NULL, 1, N'Observacion de ECHENIQUE, EFRAIN')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (7, N'VALDEZ, VICENTA', 27547866415, N'CF', 5478664, CAST(N'1946-10-27T00:00:00.000' AS DateTime), N'F', N'Z', N'VALDEZ', 547, N'CORDOBA CAPITAL', 72, 6, NULL, CAST(N'1953-10-25T00:00:00.000' AS DateTime), CAST(N'1957-10-24T00:00:00.000' AS DateTime), 1, 1, 0, CAST(393540.00 AS Decimal(18, 2)), N'valdezvicenta@hotmail.com', NULL, NULL, 1, N'Observacion de VALDEZ, VICENTA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (8, N'ALBORNOZ, SILVIA BEATRIZ', 24540989916, N'CF', 5409899, CAST(N'1946-08-15T00:00:00.000' AS DateTime), N'F', N'C', N'ALBORNOZ', 540, N'CORDOBA CAPITAL', 73, NULL, NULL, CAST(N'1953-08-13T00:00:00.000' AS DateTime), CAST(N'1957-08-12T00:00:00.000' AS DateTime), 0, 0, 1, CAST(306762.00 AS Decimal(18, 2)), N'albornozsilviabeatriz@gmail.com', NULL, NULL, 1, N'Observacion de ALBORNOZ, SILVIA BEATRIZ')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (9, N'SOSA, CLEOFO MERCEDES', 20639199868, N'CF', 6391998, CAST(N'1943-09-25T00:00:00.000' AS DateTime), N'F', N'Z', N'SOSA', 639, N'CORDOBA CAPITAL', 74, NULL, NULL, CAST(N'1950-09-23T00:00:00.000' AS DateTime), CAST(N'1954-09-22T00:00:00.000' AS DateTime), 1, 0, 0, CAST(229324.00 AS Decimal(18, 2)), N'sosacleofomercedes@hotmail.com', NULL, NULL, 1, N'Observacion de SOSA, CLEOFO MERCEDES')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (10, N'MARTINEZ, JULIA AZUCENA', 23203492042, N'CP', 2034920, CAST(N'1935-07-21T00:00:00.000' AS DateTime), N'F', N'S', N'MARTINEZ', 203, N'CORDOBA CAPITAL', 75, NULL, NULL, CAST(N'1942-07-19T00:00:00.000' AS DateTime), CAST(N'1946-07-18T00:00:00.000' AS DateTime), 0, 0, 0, CAST(232034.00 AS Decimal(18, 2)), N'martinezjuliaazucena@gmail.com', NULL, NULL, 1, N'Observacion de MARTINEZ, JULIA AZUCENA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (11, N'FERNANDEZ, ANGELA YOLANDA', 27482082884, N'CP', 4820828, CAST(N'1944-09-12T00:00:00.000' AS DateTime), N'F', N'Z', N'FERNANDEZ', 482, N'EL FORTIN', 76, NULL, NULL, CAST(N'1951-09-11T00:00:00.000' AS DateTime), CAST(N'1955-09-10T00:00:00.000' AS DateTime), 1, 0, 0, CAST(249837.00 AS Decimal(18, 2)), N'fernandezangelayolanda@hotmail.com', NULL, NULL, 1, N'Observacion de FERNANDEZ, ANGELA YOLANDA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (12, N'ALIENDRO, OLGA', 27358497562, N'CP', 3584975, CAST(N'1938-01-11T00:00:00.000' AS DateTime), N'F', N'S', N'ALIENDRO', 358, N'CRUZ DEL EJE', 77, NULL, NULL, CAST(N'1945-01-09T00:00:00.000' AS DateTime), CAST(N'1949-01-08T00:00:00.000' AS DateTime), 0, 0, 1, CAST(227987.00 AS Decimal(18, 2)), N'aliendroolga@gmail.com', NULL, NULL, 1, N'Observacion de ALIENDRO, OLGA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (13, N'AMERI, OLGA ESTHER', 27526019253, N'LE', 5260192, CAST(N'1971-01-01T00:00:00.000' AS DateTime), N'F', N'Z', N'AMERI', 526, N'LABOULAYE', 78, NULL, NULL, CAST(N'1977-12-30T00:00:00.000' AS DateTime), CAST(N'1981-12-29T00:00:00.000' AS DateTime), 1, 1, 0, CAST(211738.00 AS Decimal(18, 2)), N'ameriolgaesther@hotmail.com', NULL, NULL, 1, N'Observacion de AMERI, OLGA ESTHER')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (14, N'OVIEDO, AIDEE FELIPA', 23429454041, N'LE', 4294540, CAST(N'1944-08-23T00:00:00.000' AS DateTime), N'F', N'S', N'OVIEDO', 429, N'CAMINIAGA', 79, NULL, NULL, CAST(N'1951-08-22T00:00:00.000' AS DateTime), CAST(N'1955-08-21T00:00:00.000' AS DateTime), 0, 1, 0, CAST(167353.00 AS Decimal(18, 2)), N'oviedoaideefelipa@gmail.com', NULL, NULL, 1, N'Observacion de OVIEDO, AIDEE FELIPA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (15, N'MORENO, ELISA CRISTINA', 27573824790, N'PSP', 5738247, CAST(N'1948-07-06T00:00:00.000' AS DateTime), N'F', N'C', N'MORENO', 573, N'DEAN FUNES', 80, NULL, NULL, CAST(N'1955-07-05T00:00:00.000' AS DateTime), CAST(N'1959-07-04T00:00:00.000' AS DateTime), 1, 0, 1, CAST(183825.00 AS Decimal(18, 2)), N'morenoelisacristina@hotmail.com', NULL, NULL, 1, N'Observacion de MORENO, ELISA CRISTINA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (16, N'MEZ, CANDELARIA ALICIA', 27410899768, N'DNI', 4108997, CAST(N'1940-06-11T00:00:00.000' AS DateTime), N'F', N'Z', N'MEZ', 410, N'DEAN FUNES', 81, NULL, NULL, CAST(N'1947-06-10T00:00:00.000' AS DateTime), CAST(N'1951-06-09T00:00:00.000' AS DateTime), 0, 0, 1, CAST(171318.00 AS Decimal(18, 2)), N'MEZcandelariaalicia@gmail.com', NULL, NULL, 1, N'Observacion de MEZ, CANDELARIA ALICIA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (17, N'ORTIZ, EMILIA V', 27466964043, N'DNI', 4669640, CAST(N'1946-10-03T00:00:00.000' AS DateTime), N'F', N'Z', N'ORTIZ', 466, N'BENJAMIN GOULD', 82, NULL, NULL, CAST(N'1953-10-01T00:00:00.000' AS DateTime), CAST(N'1957-09-30T00:00:00.000' AS DateTime), 1, 1, 0, CAST(161570.00 AS Decimal(18, 2)), N'ortizemiliav@hotmail.com', NULL, NULL, 1, N'Observacion de ORTIZ, EMILIA V')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (18, N'ROJAS, JORGE CORNELIO', 20655007536, N'DNI', 6550075, CAST(N'1941-09-19T00:00:00.000' AS DateTime), N'M', N'Z', N'ROJAS', 655, N'SAN MARCOS SUD', 83, NULL, NULL, CAST(N'1948-09-17T00:00:00.000' AS DateTime), CAST(N'1952-09-16T00:00:00.000' AS DateTime), 0, 0, 1, CAST(114750.00 AS Decimal(18, 2)), N'rojasjorgecornelio@gmail.com', NULL, NULL, 1, N'Observacion de ROJAS, JORGE CORNELIO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (19, N'BUENO, GERARDO', 20654416337, N'DNI', 6544163, CAST(N'1937-02-21T00:00:00.000' AS DateTime), N'M', N'Z', N'BUENO', 654, N'SIN ASIGNAR', 84, NULL, NULL, CAST(N'1944-02-20T00:00:00.000' AS DateTime), CAST(N'1948-02-19T00:00:00.000' AS DateTime), 1, 1, 1, CAST(108707.00 AS Decimal(18, 2)), N'buenogerardo@hotmail.com', NULL, NULL, 1, N'Observacion de BUENO, GERARDO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (20, N'STREET, SUSANA MARGARITA', 23488956540, N'DNI', 4889565, CAST(N'1947-06-05T00:00:00.000' AS DateTime), N'F', N'Z', N'STREET', 488, N'LABOULAYE', 85, NULL, NULL, CAST(N'1954-06-03T00:00:00.000' AS DateTime), CAST(N'1958-06-02T00:00:00.000' AS DateTime), 0, 0, 1, CAST(117444.00 AS Decimal(18, 2)), N'streetsusanamargarita@gmail.com', NULL, NULL, 1, N'Observacion de STREET, SUSANA MARGARITA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (21, N'SANCHEZ, GRISELDA ELINA', 27263738204, N'DNI', 26373820, CAST(N'1975-10-09T00:00:00.000' AS DateTime), N'F', N'S', N'SANCHEZ', 263, N'VILLA GUTIERREZ', 86, NULL, NULL, CAST(N'1982-10-07T00:00:00.000' AS DateTime), CAST(N'1986-10-06T00:00:00.000' AS DateTime), 1, 0, 0, CAST(129827.00 AS Decimal(18, 2)), N'sanchezgriseldaelina@hotmail.com', NULL, NULL, 1, N'Observacion de SANCHEZ, GRISELDA ELINA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (22, N'CENA, MARGARITA ROSA', 27995449408, N'DNI', 9954494, CAST(N'1939-07-21T00:00:00.000' AS DateTime), N'F', N'V', N'CENA', 995, N'LABOULAYE', 87, NULL, NULL, CAST(N'1946-07-19T00:00:00.000' AS DateTime), CAST(N'1950-07-18T00:00:00.000' AS DateTime), 0, 0, 0, CAST(127252.00 AS Decimal(18, 2)), N'cenamargaritarosa@gmail.com', NULL, NULL, 1, N'Observacion de CENA, MARGARITA ROSA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (23, N'CRISTALDO MARTINEZ, DEONILDA', 27928244024, N'CF', 9282440, CAST(N'1960-04-08T00:00:00.000' AS DateTime), N'F', N'Z', N'CRISTALDO MARTINEZ', 928, N'SIN ASIGNAR', 88, NULL, NULL, CAST(N'1967-04-07T00:00:00.000' AS DateTime), CAST(N'1971-04-06T00:00:00.000' AS DateTime), 1, 0, 0, CAST(121427.00 AS Decimal(18, 2)), N'cristaldomartinezdeonilda@hotmail.com', NULL, NULL, 1, N'Observacion de CRISTALDO MARTINEZ, DEONILDA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (24, N'PETRELLI, FABIANA', 27162750644, N'CF', 1627506, CAST(N'1962-12-03T00:00:00.000' AS DateTime), N'F', N'Z', N'PETRELLI', 162, N'RIO CEBALLOS', 89, NULL, NULL, CAST(N'1969-12-01T00:00:00.000' AS DateTime), CAST(N'1973-11-30T00:00:00.000' AS DateTime), 0, 0, 0, CAST(113178.00 AS Decimal(18, 2)), N'petrellifabiana@gmail.com', NULL, NULL, 1, N'Observacion de PETRELLI, FABIANA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (25, N'MARTINEZ, GINES', 20637709075, N'CF', 6377090, CAST(N'1935-05-25T00:00:00.000' AS DateTime), N'M', N'S', N'MARTINEZ', 637, N'CORDOBA CAPITAL', 90, NULL, NULL, CAST(N'1942-05-23T00:00:00.000' AS DateTime), CAST(N'1946-05-22T00:00:00.000' AS DateTime), 1, 1, 0, CAST(82550.00 AS Decimal(18, 2)), N'martinezgines@hotmail.com', NULL, NULL, 1, N'Observacion de MARTINEZ, GINES')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (26, N'BARRIOS, AGUSTIN', 27665428882, N'CP', 6654288, CAST(N'1945-04-02T00:00:00.000' AS DateTime), N'M', N'Z', N'BARRIOS', 665, N'LABOULAYE', 91, NULL, NULL, CAST(N'1952-03-31T00:00:00.000' AS DateTime), CAST(N'1956-03-30T00:00:00.000' AS DateTime), 0, 0, 0, CAST(106405.00 AS Decimal(18, 2)), N'barriosagustin@gmail.com', NULL, NULL, 1, N'Observacion de BARRIOS, AGUSTIN')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (27, N'RIVERO, SUSANA ELENA', 27591843723, N'CP', 5918437, CAST(N'1948-08-04T00:00:00.000' AS DateTime), N'F', N'Z', N'RIVERO', 591, N'ARROYITO', 92, NULL, NULL, CAST(N'1955-08-03T00:00:00.000' AS DateTime), CAST(N'1959-08-02T00:00:00.000' AS DateTime), 1, 1, 1, CAST(102192.00 AS Decimal(18, 2)), N'riverosusanaelena@hotmail.com', NULL, NULL, 1, N'Observacion de RIVERO, SUSANA ELENA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (28, N'BLETRAMO, LEANDRO EMANUEL', 27626507831, N'CP', 34273940, CAST(N'1989-05-17T00:00:00.000' AS DateTime), N'M', N'S', N'BLETRAMO', 342, N'SIN ASIGNAR', 93, NULL, NULL, CAST(N'1996-05-15T00:00:00.000' AS DateTime), CAST(N'2000-05-14T00:00:00.000' AS DateTime), 0, 1, 0, CAST(98666.00 AS Decimal(18, 2)), N'bletramoleandroemanuel@gmail.com', NULL, NULL, 1, N'Observacion de BLETRAMO, LEANDRO EMANUEL')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (29, N'VINCENZINI, NORMA IRIS', 27595598130, N'LE', 5955981, CAST(N'1950-03-05T00:00:00.000' AS DateTime), N'F', N'C', N'VINCENZINI', 595, N'JUSTINIANO POSSE', 94, NULL, NULL, CAST(N'1957-03-03T00:00:00.000' AS DateTime), CAST(N'1961-03-02T00:00:00.000' AS DateTime), 1, 0, 1, CAST(95157.00 AS Decimal(18, 2)), N'vincenzininormairis@hotmail.com', NULL, NULL, 1, N'Observacion de VINCENZINI, NORMA IRIS')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (30, N'FERREYRA, MARTA GRACIELA', 27566081638, N'LE', 5660816, CAST(N'1947-11-12T00:00:00.000' AS DateTime), N'F', N'C', N'FERREYRA', 566, N'ALCIRA', 95, NULL, NULL, CAST(N'1954-11-10T00:00:00.000' AS DateTime), CAST(N'1958-11-09T00:00:00.000' AS DateTime), 0, 0, 0, CAST(91886.00 AS Decimal(18, 2)), N'ferreyramartagraciela@gmail.com', NULL, NULL, 1, N'Observacion de FERREYRA, MARTA GRACIELA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (31, N'VERON, MANUEL VALLE', 20795662260, N'PSP', 7956622, CAST(N'1946-10-11T00:00:00.000' AS DateTime), N'M', N'S', N'VERON', 795, N'LUCIO V MANSILLA', 96, NULL, NULL, CAST(N'1953-10-09T00:00:00.000' AS DateTime), CAST(N'1957-10-08T00:00:00.000' AS DateTime), 1, 0, 0, CAST(67082.00 AS Decimal(18, 2)), N'veronmanuelvalle@hotmail.com', NULL, NULL, 1, N'Observacion de VERON, MANUEL VALLE')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (32, N'RUIZ, ELENA VICENTA', 27532766070, N'DNI', 5327660, CAST(N'1949-12-08T00:00:00.000' AS DateTime), N'F', N'S', N'RUIZ', 532, N'SIN ASIGNAR', 97, NULL, NULL, CAST(N'1956-12-06T00:00:00.000' AS DateTime), CAST(N'1960-12-05T00:00:00.000' AS DateTime), 0, 0, 0, CAST(86039.00 AS Decimal(18, 2)), N'ruizelenavicenta@gmail.com', NULL, NULL, 1, N'Observacion de RUIZ, ELENA VICENTA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (33, N'SAGUILAN, LUICIANA', 27448698914, N'DNI', 4486989, CAST(N'1944-01-16T00:00:00.000' AS DateTime), N'F', N'S', N'SAGUILAN', 448, N'SIN ASIGNAR', 98, NULL, NULL, CAST(N'1951-01-14T00:00:00.000' AS DateTime), CAST(N'1955-01-13T00:00:00.000' AS DateTime), 1, 0, 1, CAST(83177.00 AS Decimal(18, 2)), N'saguilanluiciana@hotmail.com', NULL, NULL, 1, N'Observacion de SAGUILAN, LUICIANA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (34, N'BRIZUELA, JUAN AGUSTIN', 20712112165, N'DNI', 7121121, CAST(N'1943-08-29T00:00:00.000' AS DateTime), N'M', N'C', N'BRIZUELA', 712, N'LUCIO V MANSILLA', 99, NULL, NULL, CAST(N'1950-08-27T00:00:00.000' AS DateTime), CAST(N'1954-08-26T00:00:00.000' AS DateTime), 0, 1, 1, CAST(60917.00 AS Decimal(18, 2)), N'brizuelajuanagustin@gmail.com', NULL, NULL, 1, N'Observacion de BRIZUELA, JUAN AGUSTIN')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (35, N'LOBO, SILVESTRE AMARANTO', 23638043395, N'DNI', 6380433, CAST(N'1936-12-20T00:00:00.000' AS DateTime), N'M', N'S', N'LOBO', 638, N'SIN ASIGNAR', 100, NULL, NULL, CAST(N'1943-12-19T00:00:00.000' AS DateTime), CAST(N'1947-12-18T00:00:00.000' AS DateTime), 1, 1, 1, CAST(67537.00 AS Decimal(18, 2)), N'lobosilvestreamaranto@hotmail.com', NULL, NULL, 1, N'Observacion de LOBO, SILVESTRE AMARANTO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (36, N'JUAREZ, JOAQUIN AVELINO', 20083634100, N'DNI', 8363410, CAST(N'1951-03-04T00:00:00.000' AS DateTime), N'M', N'S', N'JUAREZ', 836, N'SIN ASIGNAR', 101, NULL, NULL, CAST(N'1958-03-02T00:00:00.000' AS DateTime), CAST(N'1962-03-01T00:00:00.000' AS DateTime), 0, 0, 0, CAST(55787.00 AS Decimal(18, 2)), N'juarezjoaquinavelino@gmail.com', NULL, NULL, 1, N'Observacion de JUAREZ, JOAQUIN AVELINO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (37, N'HEREDIA, CARLOS', 20784359428, N'DNI', 7843594, CAST(N'1949-11-18T00:00:00.000' AS DateTime), N'M', N'S', N'HEREDIA', 784, N'CORDOBA CAPITAL', 102, NULL, NULL, CAST(N'1956-11-16T00:00:00.000' AS DateTime), CAST(N'1960-11-15T00:00:00.000' AS DateTime), 1, 0, 0, CAST(56173.00 AS Decimal(18, 2)), N'herediacarlos@hotmail.com', NULL, NULL, 1, N'Observacion de HEREDIA, CARLOS')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (38, N'FUENTES, DIEGO HERNAN', 20227968314, N'DNI', 22796831, CAST(N'1972-12-11T00:00:00.000' AS DateTime), N'M', N'Z', N'FUENTES', 227, N'CORDOBA CAPITAL', 103, NULL, NULL, CAST(N'1979-12-10T00:00:00.000' AS DateTime), CAST(N'1983-12-09T00:00:00.000' AS DateTime), 0, 0, 1, CAST(53231.00 AS Decimal(18, 2)), N'fuentesdiegohernan@gmail.com', NULL, NULL, 1, N'Observacion de FUENTES, DIEGO HERNAN')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (39, N'RIVERO, JOSE', 20668590628, N'CF', 6685906, CAST(N'1938-01-01T00:00:00.000' AS DateTime), N'M', N'C', N'RIVERO', 668, N'SIN ASIGNAR', 104, NULL, NULL, CAST(N'1944-12-30T00:00:00.000' AS DateTime), CAST(N'1948-12-29T00:00:00.000' AS DateTime), 1, 0, 0, CAST(52996.00 AS Decimal(18, 2)), N'riverojose@hotmail.com', NULL, NULL, 1, N'Observacion de RIVERO, JOSE')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (40, N'MACCHI, MARIA DE LUJAN', 27572149168, N'CF', 5721491, CAST(N'1947-10-28T00:00:00.000' AS DateTime), N'F', N'V', N'MACCHI', 572, N'SIN ASIGNAR', 105, NULL, NULL, CAST(N'1954-10-26T00:00:00.000' AS DateTime), CAST(N'1958-10-25T00:00:00.000' AS DateTime), 0, 0, 1, CAST(68930.00 AS Decimal(18, 2)), N'macchimariadelujan@gmail.com', NULL, NULL, 1, N'Observacion de MACCHI, MARIA DE LUJAN')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (41, N'SANCHEZ, CRISTINA DORA', 27450689062, N'CF', 4506890, CAST(N'1942-09-15T00:00:00.000' AS DateTime), N'F', N'S', N'SANCHEZ', 450, N'SIN ASIGNAR', 106, NULL, NULL, CAST(N'1949-09-13T00:00:00.000' AS DateTime), CAST(N'1953-09-12T00:00:00.000' AS DateTime), 1, 0, 0, CAST(66952.00 AS Decimal(18, 2)), N'sanchezcristinadora@hotmail.com', NULL, NULL, 1, N'Observacion de SANCHEZ, CRISTINA DORA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (42, N'ARCE, YESICA J', 27281138084, N'CP', 28113808, CAST(N'1980-03-31T00:00:00.000' AS DateTime), N'F', N'S', N'ARCE', 281, N'SIN ASIGNAR', 107, NULL, NULL, CAST(N'1987-03-30T00:00:00.000' AS DateTime), CAST(N'1991-03-29T00:00:00.000' AS DateTime), 0, 0, 0, CAST(64955.00 AS Decimal(18, 2)), N'arceyesicaj@gmail.com', NULL, NULL, 0, N'Observacion de ARCE, YESICA J')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (43, N'URQUIZA, CESAR DANIEL', 22639321708, N'CP', 26393217, CAST(N'1978-01-21T00:00:00.000' AS DateTime), N'M', N'Z', N'URQUIZA', 263, N'CORDOBA CAPITAL', 108, NULL, NULL, CAST(N'1985-01-19T00:00:00.000' AS DateTime), CAST(N'1989-01-18T00:00:00.000' AS DateTime), 1, 0, 1, CAST(52649.00 AS Decimal(18, 2)), N'urquizacesardaniel@hotmail.com', NULL, NULL, 1, N'Observacion de URQUIZA, CESAR DANIEL')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (44, N'RODRIGUEZ, DARIO MARTIN', 20219020007, N'CP', 21902000, CAST(N'1970-11-06T00:00:00.000' AS DateTime), N'M', N'S', N'RODRIGUEZ', 219, N'CORDOBA CAPITAL', 109, NULL, NULL, CAST(N'1977-11-04T00:00:00.000' AS DateTime), CAST(N'1981-11-03T00:00:00.000' AS DateTime), 0, 1, 0, CAST(45952.00 AS Decimal(18, 2)), N'rodriguezdariomartin@gmail.com', NULL, NULL, 1, N'Observacion de RODRIGUEZ, DARIO MARTIN')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (45, N'GULLI, JORGE DANIEL', 20184298942, N'LE', 18429894, CAST(N'1967-10-18T00:00:00.000' AS DateTime), N'M', N'Z', N'GULLI', 184, N'CORDOBA CAPITAL', 110, NULL, NULL, CAST(N'1974-10-16T00:00:00.000' AS DateTime), CAST(N'1978-10-15T00:00:00.000' AS DateTime), 1, 0, 0, CAST(44853.00 AS Decimal(18, 2)), N'gullijorgedaniel@hotmail.com', NULL, NULL, 1, N'Observacion de GULLI, JORGE DANIEL')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (46, N'NAVARRO, DANIEL ALEJANDRO', 20206227870, N'LE', 20622787, CAST(N'1969-02-14T00:00:00.000' AS DateTime), N'M', N'C', N'NAVARRO', 206, N'CORDOBA CAPITAL', 111, NULL, NULL, CAST(N'1976-02-13T00:00:00.000' AS DateTime), CAST(N'1980-02-12T00:00:00.000' AS DateTime), 0, 0, 1, CAST(43926.00 AS Decimal(18, 2)), N'navarrodanielalejandro@gmail.com', NULL, NULL, 1, N'Observacion de NAVARRO, DANIEL ALEJANDRO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (47, N'RAMONDA, FRANCISCA MIGUELA', 27474045041, N'PSP', 4740450, CAST(N'1947-03-02T00:00:00.000' AS DateTime), N'F', N'C', N'RAMONDA', 474, N'LA CESIRA', 112, NULL, NULL, CAST(N'1954-02-28T00:00:00.000' AS DateTime), CAST(N'1958-02-27T00:00:00.000' AS DateTime), 1, 1, 0, CAST(58455.00 AS Decimal(18, 2)), N'ramondafranciscamiguela@hotmail.com', NULL, NULL, 1, N'Observacion de RAMONDA, FRANCISCA MIGUELA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (48, N'BOTTI, AMELIA HAYDEE', 27451000877, N'DNI', 4510008, CAST(N'1942-03-24T00:00:00.000' AS DateTime), N'F', N'C', N'BOTTI', 451, N'LA CESIRA', 113, NULL, NULL, CAST(N'1949-03-22T00:00:00.000' AS DateTime), CAST(N'1953-03-21T00:00:00.000' AS DateTime), 0, 1, 0, CAST(57189.00 AS Decimal(18, 2)), N'bottiameliahaydee@gmail.com', NULL, NULL, 1, N'Observacion de BOTTI, AMELIA HAYDEE')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (49, N'MORENO, DELIA', 27456452616, N'DNI', 4564526, CAST(N'1943-08-06T00:00:00.000' AS DateTime), N'F', N'Z', N'MORENO', 456, N'CORDOBA CAPITAL', 114, NULL, NULL, CAST(N'1950-08-04T00:00:00.000' AS DateTime), CAST(N'1954-08-03T00:00:00.000' AS DateTime), 1, 0, 0, CAST(56033.00 AS Decimal(18, 2)), N'morenodelia@hotmail.com', NULL, NULL, 1, N'Observacion de MORENO, DELIA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (50, N'MAROSTICA, CARLOS ERNESTO', 20769344021, N'DNI', 7693440, CAST(N'1949-04-03T00:00:00.000' AS DateTime), N'M', N'Z', N'MAROSTICA', 769, N'CORDOBA CAPITAL', 115, NULL, NULL, CAST(N'1956-04-01T00:00:00.000' AS DateTime), CAST(N'1960-03-31T00:00:00.000' AS DateTime), 0, 1, 0, CAST(41538.00 AS Decimal(18, 2)), N'marosticacarlosernesto@gmail.com', NULL, NULL, 1, N'Observacion de MAROSTICA, CARLOS ERNESTO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (51, N'LEDESMA, MERCEDES DEL CARMEN', 27370343507, N'DNI', 3703435, CAST(N'1936-11-15T00:00:00.000' AS DateTime), N'F', N'Z', N'LEDESMA', 370, N'VILLA ALLENDE', 116, NULL, NULL, CAST(N'1943-11-14T00:00:00.000' AS DateTime), CAST(N'1947-11-13T00:00:00.000' AS DateTime), 1, 1, 1, CAST(53667.00 AS Decimal(18, 2)), N'ledesmamercedesdelcarmen@hotmail.com', NULL, NULL, 1, N'Observacion de LEDESMA, MERCEDES DEL CARMEN')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (52, N'FLORES, MERCEDES YOLANDA', 27509792916, N'DNI', 5097929, CAST(N'1944-05-20T00:00:00.000' AS DateTime), N'F', N'Z', N'FLORES', 509, N'CORDOBA CAPITAL', 117, NULL, NULL, CAST(N'1951-05-19T00:00:00.000' AS DateTime), CAST(N'1955-05-18T00:00:00.000' AS DateTime), 0, 0, 1, CAST(52903.00 AS Decimal(18, 2)), N'floresmercedesyolanda@gmail.com', NULL, NULL, 1, N'Observacion de FLORES, MERCEDES YOLANDA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (53, N'MENDOZA, MARTIN ELEUTERIO', 20804194236, N'DNI', 8041942, CAST(N'1946-11-03T00:00:00.000' AS DateTime), N'M', N'S', N'MENDOZA', 804, N'CORDOBA CAPITAL', 118, NULL, NULL, CAST(N'1953-11-01T00:00:00.000' AS DateTime), CAST(N'1957-10-31T00:00:00.000' AS DateTime), 1, 0, 0, CAST(39253.00 AS Decimal(18, 2)), N'mendozamartineleuterio@hotmail.com', NULL, NULL, 1, N'Observacion de MENDOZA, MARTIN ELEUTERIO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (54, N'RIVADERO, NILDA MIRTA', 27450779130, N'DNI', 4507791, CAST(N'1942-08-17T00:00:00.000' AS DateTime), N'F', N'C', N'RIVADERO', 450, N'CORDOBA CAPITAL', 119, NULL, NULL, CAST(N'1949-08-15T00:00:00.000' AS DateTime), CAST(N'1953-08-14T00:00:00.000' AS DateTime), 0, 0, 1, CAST(50834.00 AS Decimal(18, 2)), N'rivaderonildamirta@gmail.com', NULL, NULL, 1, N'Observacion de RIVADERO, NILDA MIRTA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (55, N'LUBARY, ELENA DEL CARMEN', 27664750056, N'CF', 6647500, CAST(N'1951-09-14T00:00:00.000' AS DateTime), N'F', N'Z', N'LUBARY', 664, N'ALICIA', 120, NULL, NULL, CAST(N'1958-09-12T00:00:00.000' AS DateTime), CAST(N'1962-09-11T00:00:00.000' AS DateTime), 1, 0, 0, CAST(50299.00 AS Decimal(18, 2)), N'lubaryelenadelcarmen@hotmail.com', NULL, NULL, 1, N'Observacion de LUBARY, ELENA DEL CARMEN')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (56, N'MALDONADO, AURORA DEL VALLE', 27573076394, N'CF', 5730763, CAST(N'1948-07-04T00:00:00.000' AS DateTime), N'F', N'V', N'MALDONADO', 573, N'VILLA CARLOS PAZ', 121, NULL, NULL, CAST(N'1955-07-03T00:00:00.000' AS DateTime), CAST(N'1959-07-02T00:00:00.000' AS DateTime), 0, 0, 1, CAST(49237.00 AS Decimal(18, 2)), N'maldonadoauroradelvalle@gmail.com', NULL, NULL, 1, N'Observacion de MALDONADO, AURORA DEL VALLE')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (57, N'CARRANZA, FRANCISCA CORINA', 27157244850, N'CF', 1572448, CAST(N'1932-10-16T00:00:00.000' AS DateTime), N'F', N'V', N'CARRANZA', 157, N'VILLA CARLOS PAZ', 122, NULL, NULL, CAST(N'1939-10-15T00:00:00.000' AS DateTime), CAST(N'1943-10-14T00:00:00.000' AS DateTime), 1, 0, 0, CAST(47644.00 AS Decimal(18, 2)), N'carranzafranciscacorina@hotmail.com', NULL, NULL, 1, N'Observacion de CARRANZA, FRANCISCA CORINA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (58, N'DELGADO, RAMONA', 27626457982, N'CP', 6264579, CAST(N'1950-01-25T00:00:00.000' AS DateTime), N'F', N'C', N'DELGADO', 626, N'VILLA SANTA CRUZ DEL LAGO', 123, NULL, NULL, CAST(N'1957-01-23T00:00:00.000' AS DateTime), CAST(N'1961-01-22T00:00:00.000' AS DateTime), 0, 0, 1, CAST(47631.00 AS Decimal(18, 2)), N'delgadoramona@gmail.com', NULL, NULL, 1, N'Observacion de DELGADO, RAMONA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (59, N'LUCERO, ROSARIO DEL CARMEN', 27388759522, N'CP', 3887595, CAST(N'1940-07-06T00:00:00.000' AS DateTime), N'F', N'S', N'LUCERO', 388, N'CORDOBA CAPITAL', 124, NULL, NULL, CAST(N'1947-07-05T00:00:00.000' AS DateTime), CAST(N'1951-07-04T00:00:00.000' AS DateTime), 1, 0, 1, CAST(46421.00 AS Decimal(18, 2)), N'lucerorosariodelcarmen@hotmail.com', NULL, NULL, 1, N'Observacion de LUCERO, ROSARIO DEL CARMEN')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (60, N'DOMINGUEZ, MARTHA AMALIA', 27563688988, N'CP', 5636889, CAST(N'1950-06-27T00:00:00.000' AS DateTime), N'F', N'Z', N'DOMINGUEZ', 563, N'SIN ASIGNAR', 125, NULL, NULL, CAST(N'1957-06-25T00:00:00.000' AS DateTime), CAST(N'1961-06-24T00:00:00.000' AS DateTime), 0, 0, 1, CAST(45939.00 AS Decimal(18, 2)), N'dominguezmarthaamalia@gmail.com', NULL, NULL, 1, N'Observacion de DOMINGUEZ, MARTHA AMALIA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (61, N'PIZZUTO, SEBASTIAN', 20841186497, N'LE', 8411864, CAST(N'1950-09-07T00:00:00.000' AS DateTime), N'M', N'Z', N'PIZZUTO', 841, N'CORDOBA CAPITAL', 126, NULL, NULL, CAST(N'1957-09-05T00:00:00.000' AS DateTime), CAST(N'1961-09-04T00:00:00.000' AS DateTime), 1, 1, 0, CAST(34165.00 AS Decimal(18, 2)), N'pizzutosebastian@hotmail.com', NULL, NULL, 1, N'Observacion de PIZZUTO, SEBASTIAN')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (62, N'GIGENA, OFELIA DORA', 27668031772, N'LE', 6680317, CAST(N'1949-04-17T00:00:00.000' AS DateTime), N'F', N'Z', N'GIGENA', 668, N'CORDOBA CAPITAL', 127, NULL, NULL, CAST(N'1956-04-15T00:00:00.000' AS DateTime), CAST(N'1960-04-14T00:00:00.000' AS DateTime), 0, 0, 1, CAST(44625.00 AS Decimal(18, 2)), N'gigenaofeliadora@gmail.com', NULL, NULL, 1, N'Observacion de GIGENA, OFELIA DORA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (63, N'ROMERO, HAYDEE', 27573070353, N'PSP', 5730703, CAST(N'1948-07-11T00:00:00.000' AS DateTime), N'F', N'S', N'ROMERO', 573, N'VILLA CARLOS PAZ', 128, NULL, NULL, CAST(N'1955-07-10T00:00:00.000' AS DateTime), CAST(N'1959-07-09T00:00:00.000' AS DateTime), 1, 1, 1, CAST(43766.00 AS Decimal(18, 2)), N'romerohaydee@hotmail.com', NULL, NULL, 1, N'Observacion de ROMERO, HAYDEE')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (64, N'SAUR, ELENA MARIA', 27524744728, N'DNI', 5247447, CAST(N'1947-01-04T00:00:00.000' AS DateTime), N'F', N'Z', N'SAUR', 524, N'CORRAL DE BUSTOS', 129, NULL, NULL, CAST(N'1954-01-02T00:00:00.000' AS DateTime), CAST(N'1958-01-01T00:00:00.000' AS DateTime), 0, 0, 1, CAST(43007.00 AS Decimal(18, 2)), N'saurelenamaria@gmail.com', NULL, NULL, 1, N'Observacion de SAUR, ELENA MARIA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (65, N'PIRIS, MARGARITA AURELIA', 27524744807, N'DNI', 5247448, CAST(N'1947-04-15T00:00:00.000' AS DateTime), N'F', N'S', N'PIRIS', 524, N'ISLA VERDE', 130, NULL, NULL, CAST(N'1954-04-13T00:00:00.000' AS DateTime), CAST(N'1958-04-12T00:00:00.000' AS DateTime), 1, 1, 0, CAST(42345.00 AS Decimal(18, 2)), N'pirismargaritaaurelia@hotmail.com', NULL, NULL, 1, N'Observacion de PIRIS, MARGARITA AURELIA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (66, N'GUZMAN, ALEJANDRINA', 27279896894, N'DNI', 2798968, CAST(N'1936-02-26T00:00:00.000' AS DateTime), N'F', N'Z', N'GUZMAN', 279, N'VILLA CARLOS PAZ', 131, NULL, NULL, CAST(N'1943-02-24T00:00:00.000' AS DateTime), CAST(N'1947-02-23T00:00:00.000' AS DateTime), 0, 0, 0, CAST(41333.00 AS Decimal(18, 2)), N'guzmanalejandrina@gmail.com', NULL, NULL, 1, N'Observacion de GUZMAN, ALEJANDRINA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (67, N'ZAPATA, ALICIA ANGELICA', 27485027392, N'DNI', 4850273, CAST(N'1944-07-14T00:00:00.000' AS DateTime), N'F', N'Z', N'ZAPATA', 485, N'ISLA VERDE', 132, NULL, NULL, CAST(N'1951-07-13T00:00:00.000' AS DateTime), CAST(N'1955-07-12T00:00:00.000' AS DateTime), 1, 0, 1, CAST(41022.00 AS Decimal(18, 2)), N'zapataaliciaangelica@hotmail.com', NULL, NULL, 1, N'Observacion de ZAPATA, ALICIA ANGELICA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (68, N'RODRIGUEZ, MATEO ELADIO', 20642911154, N'DNI', 6429111, CAST(N'1939-11-08T00:00:00.000' AS DateTime), N'M', N'C', N'RODRIGUEZ', 642, N'CORDOBA CAPITAL', 133, NULL, NULL, CAST(N'1946-11-06T00:00:00.000' AS DateTime), CAST(N'1950-11-05T00:00:00.000' AS DateTime), 0, 0, 1, CAST(30357.00 AS Decimal(18, 2)), N'rodriguezmateoeladio@gmail.com', NULL, NULL, 1, N'Observacion de RODRIGUEZ, MATEO ELADIO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (69, N'AGUIRRE, ANDRES RAFAEL', 20785607987, N'DNI', 7856079, CAST(N'1949-09-09T00:00:00.000' AS DateTime), N'M', N'Z', N'AGUIRRE', 785, N'SIN ASIGNAR', 134, NULL, NULL, CAST(N'1956-09-07T00:00:00.000' AS DateTime), CAST(N'1960-09-06T00:00:00.000' AS DateTime), 1, 1, 1, CAST(30124.00 AS Decimal(18, 2)), N'aguirreandresrafael@hotmail.com', NULL, NULL, 1, N'Observacion de AGUIRRE, ANDRES RAFAEL')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (70, N'FIGUEROA, JUAN SANTOS', 23639408796, N'DNI', 6394087, CAST(N'1943-11-29T00:00:00.000' AS DateTime), N'M', N'C', N'FIGUEROA', 639, N'DEAN FUNES', 135, NULL, NULL, CAST(N'1950-11-27T00:00:00.000' AS DateTime), CAST(N'1954-11-26T00:00:00.000' AS DateTime), 0, 0, 1, CAST(33770.00 AS Decimal(18, 2)), N'figueroajuansantos@gmail.com', NULL, NULL, 1, N'Observacion de FIGUEROA, JUAN SANTOS')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (71, N'MARRONI, MARTA OFELIA', 23497993146, N'CF', 4979931, CAST(N'1944-09-10T00:00:00.000' AS DateTime), N'F', N'Z', N'MARRONI', 497, N'CORDOBA CAPITAL', 136, NULL, NULL, CAST(N'1951-09-09T00:00:00.000' AS DateTime), CAST(N'1955-09-08T00:00:00.000' AS DateTime), 1, 0, 1, CAST(33095.00 AS Decimal(18, 2)), N'marronimartaofelia@hotmail.com', NULL, NULL, 1, N'Observacion de MARRONI, MARTA OFELIA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (72, N'FRONTERA, CARLOS', 20648727461, N'CF', 6487274, CAST(N'1934-05-07T00:00:00.000' AS DateTime), N'M', N'Z', N'FRONTERA', 648, N'CORDOBA CAPITAL', 137, NULL, NULL, CAST(N'1941-05-05T00:00:00.000' AS DateTime), CAST(N'1945-05-04T00:00:00.000' AS DateTime), 0, 1, 0, CAST(28678.00 AS Decimal(18, 2)), N'fronteracarlos@gmail.com', NULL, NULL, 1, N'Observacion de FRONTERA, CARLOS')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (73, N'GUERRERO, RAMONA IMELDA', 27545552245, N'CF', 5455522, CAST(N'1947-01-18T00:00:00.000' AS DateTime), N'F', N'Z', N'GUERRERO', 545, N'CORDOBA CAPITAL', 138, NULL, NULL, CAST(N'1954-01-16T00:00:00.000' AS DateTime), CAST(N'1958-01-15T00:00:00.000' AS DateTime), 1, 1, 0, CAST(37733.00 AS Decimal(18, 2)), N'guerreroramonaimelda@hotmail.com', NULL, NULL, 1, N'Observacion de GUERRERO, RAMONA IMELDA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (74, N'GIMENEZ, GRACIELA BERNARDITA', 27639703236, N'CP', 6397032, CAST(N'1950-08-20T00:00:00.000' AS DateTime), N'F', N'Z', N'GIMENEZ', 639, N'CORDOBA CAPITAL', 139, NULL, NULL, CAST(N'1957-08-18T00:00:00.000' AS DateTime), CAST(N'1961-08-17T00:00:00.000' AS DateTime), 0, 0, 0, CAST(37350.00 AS Decimal(18, 2)), N'gimenezgracielabernardita@gmail.com', NULL, NULL, 1, N'Observacion de GIMENEZ, GRACIELA BERNARDITA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (75, N'SALEVA, ROSA', 27500824967, N'CP', 5008249, CAST(N'1945-01-23T00:00:00.000' AS DateTime), N'F', N'Z', N'SALEVA', 500, N'CORDOBA CAPITAL', 140, NULL, NULL, CAST(N'1952-01-22T00:00:00.000' AS DateTime), CAST(N'1956-01-21T00:00:00.000' AS DateTime), 1, 1, 1, CAST(36667.00 AS Decimal(18, 2)), N'salevarosa@hotmail.com', NULL, NULL, 1, N'Observacion de SALEVA, ROSA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (76, N'CEBALLO, RAMON MARCIANO', 20849888136, N'CP', 8498881, CAST(N'1951-05-09T00:00:00.000' AS DateTime), N'M', N'S', N'CEBALLO', 849, N'CORDOBA CAPITAL', 141, NULL, NULL, CAST(N'1958-05-07T00:00:00.000' AS DateTime), CAST(N'1962-05-06T00:00:00.000' AS DateTime), 0, 0, 1, CAST(27434.00 AS Decimal(18, 2)), N'ceballoramonmarciano@gmail.com', NULL, NULL, 1, N'Observacion de CEBALLO, RAMON MARCIANO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (77, N'FERNANDEZ, IRMA BEATRIZ', 27574818761, N'LE', 5748187, CAST(N'1948-09-15T00:00:00.000' AS DateTime), N'F', N'Z', N'FERNANDEZ', 574, N'CORDOBA CAPITAL', 142, NULL, NULL, CAST(N'1955-09-14T00:00:00.000' AS DateTime), CAST(N'1959-09-13T00:00:00.000' AS DateTime), 1, 1, 1, CAST(35811.00 AS Decimal(18, 2)), N'fernandezirmabeatriz@hotmail.com', NULL, NULL, 1, N'Observacion de FERNANDEZ, IRMA BEATRIZ')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (78, N'ANDRADA, IRMA DEL VALLE', 21287418974, N'LE', 12874189, CAST(N'1955-01-15T00:00:00.000' AS DateTime), N'F', N'Z', N'ANDRADA', 128, N'CORDOBA CAPITAL', 143, NULL, NULL, CAST(N'1962-01-13T00:00:00.000' AS DateTime), CAST(N'1966-01-12T00:00:00.000' AS DateTime), 0, 0, 1, CAST(27291.00 AS Decimal(18, 2)), N'andradairmadelvalle@gmail.com', NULL, NULL, 1, N'Observacion de ANDRADA, IRMA DEL VALLE')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (79, N'PALUMBO, CLAUDIO GERARDO', 21784488704, N'PSP', 17844887, CAST(N'1966-11-19T00:00:00.000' AS DateTime), N'M', N'Z', N'PALUMBO', 178, N'CORDOBA CAPITAL', 144, NULL, NULL, CAST(N'1973-11-17T00:00:00.000' AS DateTime), CAST(N'1977-11-16T00:00:00.000' AS DateTime), 1, 0, 1, CAST(27575.00 AS Decimal(18, 2)), N'palumboclaudiogerardo@hotmail.com', NULL, NULL, 1, N'Observacion de PALUMBO, CLAUDIO GERARDO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (80, N'PERALTA, SILVIA PATRICIA', 22222018877, N'DNI', 22220188, CAST(N'1971-04-08T00:00:00.000' AS DateTime), N'F', N'Z', N'PERALTA', 222, N'CORDOBA CAPITAL', 145, NULL, NULL, CAST(N'1978-04-06T00:00:00.000' AS DateTime), CAST(N'1982-04-05T00:00:00.000' AS DateTime), 0, 1, 0, CAST(27777.00 AS Decimal(18, 2)), N'peraltasilviapatricia@gmail.com', NULL, NULL, 1, N'Observacion de PERALTA, SILVIA PATRICIA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (81, N'BACILOPULOS, CATALINA', 23649356744, N'DNI', 6493567, CAST(N'1954-06-06T00:00:00.000' AS DateTime), N'F', N'Z', N'BACILOPULOS', 649, N'LA FALDA', 146, NULL, NULL, CAST(N'1961-06-04T00:00:00.000' AS DateTime), CAST(N'1965-06-03T00:00:00.000' AS DateTime), 1, 0, 1, CAST(29196.00 AS Decimal(18, 2)), N'bacilopuloscatalina@hotmail.com', NULL, NULL, 1, N'Observacion de BACILOPULOS, CATALINA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (82, N'FARAIG, OLGA ANA', 27734671236, N'DNI', 7346712, CAST(N'1931-10-02T00:00:00.000' AS DateTime), N'F', N'S', N'FARAIG', 734, N'CORDOBA CAPITAL', 147, NULL, NULL, CAST(N'1938-09-30T00:00:00.000' AS DateTime), CAST(N'1942-09-29T00:00:00.000' AS DateTime), 0, 0, 0, CAST(33822.00 AS Decimal(18, 2)), N'faraigolgaana@gmail.com', NULL, NULL, 1, N'Observacion de FARAIG, OLGA ANA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (83, N'GUEVARA, MARTA N', 27624907438, N'DNI', 6249074, CAST(N'1949-11-30T00:00:00.000' AS DateTime), N'F', N'Z', N'GUEVARA', 624, N'LA FALDA', 148, NULL, NULL, CAST(N'1956-11-28T00:00:00.000' AS DateTime), CAST(N'1960-11-27T00:00:00.000' AS DateTime), 1, 0, 0, CAST(33283.00 AS Decimal(18, 2)), N'guevaramartan@hotmail.com', NULL, NULL, 1, N'Observacion de GUEVARA, MARTA N')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (84, N'BARBIERI, YOLANDA ROSA', 27459145484, N'DNI', 4591454, CAST(N'1943-06-16T00:00:00.000' AS DateTime), N'F', N'S', N'BARBIERI', 459, N'CORDOBA CAPITAL', 149, NULL, NULL, CAST(N'1950-06-14T00:00:00.000' AS DateTime), CAST(N'1954-06-13T00:00:00.000' AS DateTime), 0, 0, 0, CAST(32689.00 AS Decimal(18, 2)), N'barbieriyolandarosa@gmail.com', NULL, NULL, 1, N'Observacion de BARBIERI, YOLANDA ROSA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (85, N'ZARATE, NORMA AGUSTINA', 27521555995, N'DNI', 5215559, CAST(N'1971-01-01T00:00:00.000' AS DateTime), N'F', N'S', N'ZARATE', 521, N'LA FALDA', 150, NULL, NULL, CAST(N'1977-12-30T00:00:00.000' AS DateTime), CAST(N'1981-12-29T00:00:00.000' AS DateTime), 1, 1, 1, CAST(32378.00 AS Decimal(18, 2)), N'zaratenormaagustina@hotmail.com', NULL, NULL, 1, N'Observacion de ZARATE, NORMA AGUSTINA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (86, N'LOPEZ, RENE ANTONIO', 20659177866, N'DNI', 6591778, CAST(N'1938-11-30T00:00:00.000' AS DateTime), N'M', N'Z', N'LOPEZ', 659, N'HERNANDO', 151, NULL, NULL, CAST(N'1945-11-28T00:00:00.000' AS DateTime), CAST(N'1949-11-27T00:00:00.000' AS DateTime), 0, 0, 0, CAST(24022.00 AS Decimal(18, 2)), N'lopezreneantonio@gmail.com', NULL, NULL, 1, N'Observacion de LOPEZ, RENE ANTONIO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (87, N'BANEGA, TELEFORO ENRIQUE', 20795397096, N'CF', 7953970, CAST(N'1945-02-20T00:00:00.000' AS DateTime), N'M', N'S', N'BANEGA', 795, N'KILOMETRO 658', 152, NULL, NULL, CAST(N'1952-02-19T00:00:00.000' AS DateTime), CAST(N'1956-02-18T00:00:00.000' AS DateTime), 1, 0, 0, CAST(23902.00 AS Decimal(18, 2)), N'banegateleforoenrique@hotmail.com', NULL, NULL, 1, N'Observacion de BANEGA, TELEFORO ENRIQUE')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (88, N'SUAREZ, VICENTE', 20787085585, N'CF', 7870855, CAST(N'1950-03-27T00:00:00.000' AS DateTime), N'M', N'Z', N'SUAREZ', 787, N'HERNANDO', 153, NULL, NULL, CAST(N'1957-03-25T00:00:00.000' AS DateTime), CAST(N'1961-03-24T00:00:00.000' AS DateTime), 0, 1, 1, CAST(23621.00 AS Decimal(18, 2)), N'suarezvicente@gmail.com', NULL, NULL, 1, N'Observacion de SUAREZ, VICENTE')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (89, N'MURUA, MARIA', 27625678277, N'CF', 6256782, CAST(N'1949-08-20T00:00:00.000' AS DateTime), N'F', N'Z', N'MURUA', 625, N'SIN ASIGNAR', 154, NULL, NULL, CAST(N'1956-08-18T00:00:00.000' AS DateTime), CAST(N'1960-08-17T00:00:00.000' AS DateTime), 1, 1, 0, CAST(31040.00 AS Decimal(18, 2)), N'muruamaria@hotmail.com', NULL, NULL, 1, N'Observacion de MURUA, MARIA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (90, N'PALMIERIS, MIGUEL ANGEL', 20660819916, N'CP', 6608199, CAST(N'1946-12-22T00:00:00.000' AS DateTime), N'M', N'C', N'PALMIERIS', 660, N'HERNANDO', 155, NULL, NULL, CAST(N'1953-12-20T00:00:00.000' AS DateTime), CAST(N'1957-12-19T00:00:00.000' AS DateTime), 0, 0, 1, CAST(22956.00 AS Decimal(18, 2)), N'palmierismiguelangel@gmail.com', NULL, NULL, 1, N'Observacion de PALMIERIS, MIGUEL ANGEL')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (91, N'VIVAS, CRISTINA DEL VALLE', 27568285885, N'CP', 5682858, CAST(N'1948-12-19T00:00:00.000' AS DateTime), N'F', N'S', N'VIVAS', 568, N'CORDOBA CAPITAL', 156, NULL, NULL, CAST(N'1955-12-18T00:00:00.000' AS DateTime), CAST(N'1959-12-17T00:00:00.000' AS DateTime), 1, 1, 0, CAST(30294.00 AS Decimal(18, 2)), N'vivascristinadelvalle@hotmail.com', NULL, NULL, 1, N'Observacion de VIVAS, CRISTINA DEL VALLE')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (92, N'MARQUEZ, NICOLASA Z', 27544334714, N'CP', 5443347, CAST(N'1947-08-24T00:00:00.000' AS DateTime), N'F', N'Z', N'MARQUEZ', 544, N'SIN ASIGNAR', 157, NULL, NULL, CAST(N'1954-08-22T00:00:00.000' AS DateTime), CAST(N'1958-08-21T00:00:00.000' AS DateTime), 0, 0, 1, CAST(29939.00 AS Decimal(18, 2)), N'marqueznicolasaz@gmail.com', NULL, NULL, 1, N'Observacion de MARQUEZ, NICOLASA Z')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (93, N'MONSERRAT, ERNESTO NAZARENO', 20785685782, N'LE', 7856857, CAST(N'1950-03-02T00:00:00.000' AS DateTime), N'M', N'C', N'MONSERRAT', 785, N'CORDOBA CAPITAL', 158, NULL, NULL, CAST(N'1957-02-28T00:00:00.000' AS DateTime), CAST(N'1961-02-27T00:00:00.000' AS DateTime), 1, 0, 1, CAST(22350.00 AS Decimal(18, 2)), N'monserraternestonazareno@hotmail.com', NULL, NULL, 1, N'Observacion de MONSERRAT, ERNESTO NAZARENO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (94, N'CEBALLOS, ETELVINA  SEFERINA', 27115018648, N'LE', 1150186, CAST(N'1942-01-09T00:00:00.000' AS DateTime), N'F', N'S', N'CEBALLOS', 115, N'CORDOBA CAPITAL', 159, NULL, NULL, CAST(N'1949-01-07T00:00:00.000' AS DateTime), CAST(N'1953-01-06T00:00:00.000' AS DateTime), 0, 0, 0, CAST(28845.00 AS Decimal(18, 2)), N'ceballosetelvinaseferina@gmail.com', NULL, NULL, 1, N'Observacion de CEBALLOS, ETELVINA  SEFERINA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (95, N'SOSA, FELIX ENRIQUE', 20669190491, N'PSP', 6691904, CAST(N'1942-03-01T00:00:00.000' AS DateTime), N'M', N'Z', N'SOSA', 669, N'CORDOBA CAPITAL', 160, NULL, NULL, CAST(N'1949-02-27T00:00:00.000' AS DateTime), CAST(N'1953-02-26T00:00:00.000' AS DateTime), 1, 1, 0, CAST(21757.00 AS Decimal(18, 2)), N'sosafelixenrique@hotmail.com', NULL, NULL, 1, N'Observacion de SOSA, FELIX ENRIQUE')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (96, N'CARRIZO, CLARA ROSA', 27321777494, N'DNI', 3217774, CAST(N'1935-03-15T00:00:00.000' AS DateTime), N'F', N'Z', N'CARRIZO', 321, N'DEAN FUNES', 161, NULL, NULL, CAST(N'1942-03-13T00:00:00.000' AS DateTime), CAST(N'1946-03-12T00:00:00.000' AS DateTime), 0, 0, 0, CAST(28460.00 AS Decimal(18, 2)), N'carrizoclararosa@gmail.com', NULL, NULL, 1, N'Observacion de CARRIZO, CLARA ROSA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (97, N'ROSS, LETICIA', 21462617430, N'DNI', 14626174, CAST(N'1961-09-12T00:00:00.000' AS DateTime), N'F', N'Z', N'ROSS', 146, N'CORDOBA CAPITAL', 162, NULL, NULL, CAST(N'1968-09-10T00:00:00.000' AS DateTime), CAST(N'1972-09-09T00:00:00.000' AS DateTime), 1, 0, 0, CAST(22126.00 AS Decimal(18, 2)), N'rossleticia@hotmail.com', NULL, NULL, 1, N'Observacion de ROSS, LETICIA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (98, N'OLMOS, RAMONA', 27497455975, N'DNI', 4974559, CAST(N'1946-01-31T00:00:00.000' AS DateTime), N'F', N'Z', N'OLMOS', 497, N'CORDOBA CAPITAL', 163, NULL, NULL, CAST(N'1953-01-29T00:00:00.000' AS DateTime), CAST(N'1957-01-28T00:00:00.000' AS DateTime), 0, 1, 1, CAST(28058.00 AS Decimal(18, 2)), N'olmosramona@gmail.com', NULL, NULL, 1, N'Observacion de OLMOS, RAMONA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (99, N'BANCHI, MARIA DEL CARMEN', 27588175841, N'DNI', 5881758, CAST(N'1949-02-21T00:00:00.000' AS DateTime), N'F', N'Z', N'BANCHI', 588, N'CORDOBA CAPITAL', 164, NULL, NULL, CAST(N'1956-02-20T00:00:00.000' AS DateTime), CAST(N'1960-02-19T00:00:00.000' AS DateTime), 1, 1, 0, CAST(27866.00 AS Decimal(18, 2)), N'banchimariadelcarmen@hotmail.com', NULL, NULL, 1, N'Observacion de BANCHI, MARIA DEL CARMEN')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (100, N'YONSON, ROBERTO ENRIQUE', 20675461228, N'DNI', 6754612, CAST(N'1935-02-18T00:00:00.000' AS DateTime), N'M', N'S', N'YONSON', 675, N'CORDOBA CAPITAL', 165, NULL, NULL, CAST(N'1942-02-16T00:00:00.000' AS DateTime), CAST(N'1946-02-15T00:00:00.000' AS DateTime), 0, 0, 0, CAST(20675.00 AS Decimal(18, 2)), N'yonsonrobertoenrique@gmail.com', NULL, NULL, 1, N'Observacion de YONSON, ROBERTO ENRIQUE')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (101, N'CORREA, MARIA ISABEL', 27667987187, N'DNI', 6679871, CAST(N'1950-11-15T00:00:00.000' AS DateTime), N'F', N'S', N'CORREA', 667, N'CORDOBA CAPITAL', 166, NULL, NULL, CAST(N'1957-11-13T00:00:00.000' AS DateTime), CAST(N'1961-11-12T00:00:00.000' AS DateTime), 1, 1, 1, CAST(27394.00 AS Decimal(18, 2)), N'correamariaisabel@hotmail.com', NULL, NULL, 1, N'Observacion de CORREA, MARIA ISABEL')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (102, N'VERGARA, HUGO HENRIQUE', 20798967813, N'DNI', 7989678, CAST(N'1946-07-10T00:00:00.000' AS DateTime), N'M', N'C', N'VERGARA', 798, N'CORDOBA CAPITAL', 167, NULL, NULL, CAST(N'1953-07-08T00:00:00.000' AS DateTime), CAST(N'1957-07-07T00:00:00.000' AS DateTime), 0, 1, 0, CAST(20391.00 AS Decimal(18, 2)), N'vergarahugohenrique@gmail.com', NULL, NULL, 1, N'Observacion de VERGARA, HUGO HENRIQUE')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (103, N'ROCHA, CARMEN ILDA', 27245328074, N'CF', 2453280, CAST(N'1937-10-11T00:00:00.000' AS DateTime), N'F', N'Z', N'ROCHA', 245, N'CORDOBA CAPITAL', 168, NULL, NULL, CAST(N'1944-10-09T00:00:00.000' AS DateTime), CAST(N'1948-10-08T00:00:00.000' AS DateTime), 1, 0, 0, CAST(26451.00 AS Decimal(18, 2)), N'rochacarmenilda@hotmail.com', NULL, NULL, 1, N'Observacion de ROCHA, CARMEN ILDA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (104, N'SANCHEZ, RINA', 27606259517, N'CF', 6062595, CAST(N'1948-07-25T00:00:00.000' AS DateTime), N'F', N'S', N'SANCHEZ', 606, N'CORDOBA CAPITAL', 169, NULL, NULL, CAST(N'1955-07-24T00:00:00.000' AS DateTime), CAST(N'1959-07-23T00:00:00.000' AS DateTime), 0, 1, 1, CAST(26544.00 AS Decimal(18, 2)), N'sanchezrina@gmail.com', NULL, NULL, 1, N'Observacion de SANCHEZ, RINA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (105, N'SISTERNA, ANTONIO RAMON', 20650913818, N'CF', 6509138, CAST(N'1939-08-14T00:00:00.000' AS DateTime), N'M', N'S', N'SISTERNA', 650, N'CORDOBA CAPITAL', 170, NULL, NULL, CAST(N'1946-08-12T00:00:00.000' AS DateTime), CAST(N'1950-08-11T00:00:00.000' AS DateTime), 1, 0, 0, CAST(19667.00 AS Decimal(18, 2)), N'sisternaantonioramon@hotmail.com', NULL, NULL, 1, N'Observacion de SISTERNA, ANTONIO RAMON')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (106, N'BENENCIO, TERESA BEATRIZ', 27625715302, N'CP', 6257153, CAST(N'1950-01-06T00:00:00.000' AS DateTime), N'F', N'Z', N'BENENCIO', 625, N'CORDOBA CAPITAL', 171, NULL, NULL, CAST(N'1957-01-04T00:00:00.000' AS DateTime), CAST(N'1961-01-03T00:00:00.000' AS DateTime), 0, 0, 1, CAST(26061.00 AS Decimal(18, 2)), N'benencioteresabeatriz@gmail.com', NULL, NULL, 1, N'Observacion de BENENCIO, TERESA BEATRIZ')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (107, N'FLORES, CARLOS MARTIN', 20794530813, N'CP', 7945308, CAST(N'1945-09-17T00:00:00.000' AS DateTime), N'M', N'C', N'FLORES', 794, N'CORDOBA CAPITAL', 172, NULL, NULL, CAST(N'1952-09-15T00:00:00.000' AS DateTime), CAST(N'1956-09-14T00:00:00.000' AS DateTime), 1, 1, 0, CAST(19434.00 AS Decimal(18, 2)), N'florescarlosmartin@hotmail.com', NULL, NULL, 1, N'Observacion de FLORES, CARLOS MARTIN')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (108, N'HINNI, MARGARITA MARIA', 27639439975, N'CP', 6394399, CAST(N'1951-01-15T00:00:00.000' AS DateTime), N'F', N'Z', N'HINNI', 639, N'CORDOBA CAPITAL', 173, NULL, NULL, CAST(N'1958-01-13T00:00:00.000' AS DateTime), CAST(N'1962-01-12T00:00:00.000' AS DateTime), 0, 1, 1, CAST(25592.00 AS Decimal(18, 2)), N'hinnimargaritamaria@gmail.com', NULL, NULL, 1, N'Observacion de HINNI, MARGARITA MARIA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (109, N'RODRIGUEZ, ROGELIO MANUEL', 20660938255, N'LE', 6609382, CAST(N'1947-04-30T00:00:00.000' AS DateTime), N'M', N'Z', N'RODRIGUEZ', 660, N'CORDOBA CAPITAL', 174, NULL, NULL, CAST(N'1954-04-28T00:00:00.000' AS DateTime), CAST(N'1958-04-27T00:00:00.000' AS DateTime), 1, 1, 0, CAST(18954.00 AS Decimal(18, 2)), N'rodriguezrogeliomanuel@hotmail.com', NULL, NULL, 1, N'Observacion de RODRIGUEZ, ROGELIO MANUEL')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (110, N'MERLO, LEOPOLDO ARGENTINO', 20799258974, N'LE', 7992589, CAST(N'1946-10-25T00:00:00.000' AS DateTime), N'M', N'S', N'MERLO', 799, N'CORDOBA CAPITAL', 175, NULL, NULL, CAST(N'1953-10-23T00:00:00.000' AS DateTime), CAST(N'1957-10-22T00:00:00.000' AS DateTime), 0, 0, 1, CAST(18908.00 AS Decimal(18, 2)), N'merloleopoldoargentino@gmail.com', NULL, NULL, 1, N'Observacion de MERLO, LEOPOLDO ARGENTINO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (111, N'MONGE, TRANSITO ELVIO', 27796426492, N'PSP', 7964264, CAST(N'1941-08-15T00:00:00.000' AS DateTime), N'M', N'C', N'MONGE', 796, N'CORDOBA CAPITAL', 176, NULL, NULL, CAST(N'1948-08-13T00:00:00.000' AS DateTime), CAST(N'1952-08-12T00:00:00.000' AS DateTime), 1, 0, 0, CAST(25041.00 AS Decimal(18, 2)), N'mongetransitoelvio@hotmail.com', NULL, NULL, 1, N'Observacion de MONGE, TRANSITO ELVIO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (112, N'CUELLO, LUIS EDUARDO', 20660789744, N'DNI', 6607897, CAST(N'1946-11-11T00:00:00.000' AS DateTime), N'M', N'Z', N'CUELLO', 660, N'CORDOBA CAPITAL', 177, NULL, NULL, CAST(N'1953-11-09T00:00:00.000' AS DateTime), CAST(N'1957-11-08T00:00:00.000' AS DateTime), 0, 0, 1, CAST(18447.00 AS Decimal(18, 2)), N'cuelloluiseduardo@gmail.com', NULL, NULL, 1, N'Observacion de CUELLO, LUIS EDUARDO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (113, N'GUZMAN, ALFONSO', 20798224784, N'DNI', 7982247, CAST(N'1945-04-02T00:00:00.000' AS DateTime), N'M', N'C', N'GUZMAN', 798, N'CORDOBA CAPITAL', 178, NULL, NULL, CAST(N'1952-03-31T00:00:00.000' AS DateTime), CAST(N'1956-03-30T00:00:00.000' AS DateTime), 1, 0, 1, CAST(18405.00 AS Decimal(18, 2)), N'guzmanalfonso@hotmail.com', NULL, NULL, 1, N'Observacion de GUZMAN, ALFONSO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (114, N'JUHEL, JERONIMO ADRIAN', 20648921354, N'DNI', 6489213, CAST(N'1934-09-30T00:00:00.000' AS DateTime), N'M', N'Z', N'JUHEL', 648, N'CORDOBA CAPITAL', 179, NULL, NULL, CAST(N'1941-09-28T00:00:00.000' AS DateTime), CAST(N'1945-09-27T00:00:00.000' AS DateTime), 0, 0, 1, CAST(18113.00 AS Decimal(18, 2)), N'juheljeronimoadrian@gmail.com', NULL, NULL, 1, N'Observacion de JUHEL, JERONIMO ADRIAN')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (115, N'MONJE, CARLOS HUGO', 20796098477, N'DNI', 7960984, CAST(N'1949-01-11T00:00:00.000' AS DateTime), N'M', N'Z', N'MONJE', 796, N'VILLA ALLENDE', 180, NULL, NULL, CAST(N'1956-01-10T00:00:00.000' AS DateTime), CAST(N'1960-01-09T00:00:00.000' AS DateTime), 1, 1, 0, CAST(18083.00 AS Decimal(18, 2)), N'monjecarloshugo@hotmail.com', NULL, NULL, 1, N'Observacion de MONJE, CARLOS HUGO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (116, N'SALOME, MARIA M', 27500828793, N'DNI', 5008287, CAST(N'1944-12-05T00:00:00.000' AS DateTime), N'F', N'Z', N'SALOME', 500, N'CORDOBA CAPITAL', 181, NULL, NULL, CAST(N'1951-12-04T00:00:00.000' AS DateTime), CAST(N'1955-12-03T00:00:00.000' AS DateTime), 0, 1, 1, CAST(23707.00 AS Decimal(18, 2)), N'salomemariam@gmail.com', NULL, NULL, 1, N'Observacion de SALOME, MARIA M')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (117, N'PUGLIAFITO, BENEDICTA', 27373403542, N'DNI', 3734035, CAST(N'1938-04-05T00:00:00.000' AS DateTime), N'F', N'Z', N'PUGLIAFITO', 373, N'CORDOBA CAPITAL', 182, NULL, NULL, CAST(N'1945-04-03T00:00:00.000' AS DateTime), CAST(N'1949-04-02T00:00:00.000' AS DateTime), 1, 0, 1, CAST(23396.00 AS Decimal(18, 2)), N'pugliafitobenedicta@hotmail.com', NULL, NULL, 1, N'Observacion de PUGLIAFITO, BENEDICTA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (118, N'NUÑEZ, REBECA  INES', 27827215683, N'DNI', 28272156, CAST(N'1980-08-21T00:00:00.000' AS DateTime), N'F', N'Z', N'NUÑEZ', 282, N'SIN ASIGNAR', 183, NULL, NULL, CAST(N'1987-08-20T00:00:00.000' AS DateTime), CAST(N'1991-08-19T00:00:00.000' AS DateTime), 0, 1, 0, CAST(23582.00 AS Decimal(18, 2)), N'nuñezrebecaines@gmail.com', NULL, NULL, 1, N'Observacion de NUÑEZ, REBECA  INES')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (119, N'CAMINO, ELIO PEDRO', 20639061325, N'CF', 6390613, CAST(N'1942-01-28T00:00:00.000' AS DateTime), N'M', N'Z', N'CAMINO', 639, N'SIN ASIGNAR', 184, NULL, NULL, CAST(N'1949-01-26T00:00:00.000' AS DateTime), CAST(N'1953-01-25T00:00:00.000' AS DateTime), 1, 1, 1, CAST(17343.00 AS Decimal(18, 2)), N'caminoeliopedro@hotmail.com', NULL, NULL, 1, N'Observacion de CAMINO, ELIO PEDRO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (120, N'VILLARREAL, MERCEDES EULOGIA', 27463763024, N'CF', 4637630, CAST(N'1943-09-11T00:00:00.000' AS DateTime), N'F', N'S', N'VILLARREAL', 463, N'CORDOBA CAPITAL', 185, NULL, NULL, CAST(N'1950-09-09T00:00:00.000' AS DateTime), CAST(N'1954-09-08T00:00:00.000' AS DateTime), 0, 0, 0, CAST(22886.00 AS Decimal(18, 2)), N'villarrealmercedeseulogia@gmail.com', NULL, NULL, 1, N'Observacion de VILLARREAL, MERCEDES EULOGIA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (121, N'MURUA, NORMA FELIPA', 27569840095, N'CF', 5698400, CAST(N'1948-02-17T00:00:00.000' AS DateTime), N'F', N'Z', N'MURUA', 569, N'CORDOBA CAPITAL', 186, NULL, NULL, CAST(N'1955-02-15T00:00:00.000' AS DateTime), CAST(N'1959-02-14T00:00:00.000' AS DateTime), 1, 1, 0, CAST(22784.00 AS Decimal(18, 2)), N'muruanormafelipa@hotmail.com', NULL, NULL, 1, N'Observacion de MURUA, NORMA FELIPA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (122, N'ROJAS, LUCIA ROSA', 27543148637, N'CP', 5431486, CAST(N'1945-11-13T00:00:00.000' AS DateTime), N'F', N'Z', N'ROJAS', 543, N'CORDOBA CAPITAL', 187, NULL, NULL, CAST(N'1952-11-11T00:00:00.000' AS DateTime), CAST(N'1956-11-10T00:00:00.000' AS DateTime), 0, 1, 0, CAST(22576.00 AS Decimal(18, 2)), N'rojasluciarosa@gmail.com', NULL, NULL, 1, N'Observacion de ROJAS, LUCIA ROSA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (123, N'BULACIO, MARTA ADELMA', 27426143575, N'CP', 4261435, CAST(N'1941-07-22T00:00:00.000' AS DateTime), N'F', N'Z', N'BULACIO', 426, N'CORDOBA CAPITAL', 188, NULL, NULL, CAST(N'1948-07-20T00:00:00.000' AS DateTime), CAST(N'1952-07-19T00:00:00.000' AS DateTime), 1, 1, 1, CAST(22297.00 AS Decimal(18, 2)), N'bulaciomartaadelma@hotmail.com', NULL, NULL, 1, N'Observacion de BULACIO, MARTA ADELMA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (124, N'LOPEZ, JOSE VITALINO', 20796683477, N'CP', 7966834, CAST(N'1941-09-20T00:00:00.000' AS DateTime), N'M', N'Z', N'LOPEZ', 796, N'SIN ASIGNAR', 189, NULL, NULL, CAST(N'1948-09-18T00:00:00.000' AS DateTime), CAST(N'1952-09-17T00:00:00.000' AS DateTime), 0, 1, 0, CAST(16771.00 AS Decimal(18, 2)), N'lopezjosevitalino@gmail.com', NULL, NULL, 1, N'Observacion de LOPEZ, JOSE VITALINO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (125, N'PALMA, GLADYS MAGDALENA', 27639765854, N'LE', 6397658, CAST(N'1950-08-11T00:00:00.000' AS DateTime), N'F', N'Z', N'PALMA', 639, N'SIN ASIGNAR', 190, NULL, NULL, CAST(N'1957-08-09T00:00:00.000' AS DateTime), CAST(N'1961-08-08T00:00:00.000' AS DateTime), 1, 0, 0, CAST(22111.00 AS Decimal(18, 2)), N'palmagladysmagdalena@hotmail.com', NULL, NULL, 1, N'Observacion de PALMA, GLADYS MAGDALENA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (126, N'MOLINA, RUMUALDA MERCEDES', 27528379907, N'LE', 5283799, CAST(N'1946-02-05T00:00:00.000' AS DateTime), N'F', N'S', N'MOLINA', 528, N'CORDOBA CAPITAL', 191, NULL, NULL, CAST(N'1953-02-03T00:00:00.000' AS DateTime), CAST(N'1957-02-02T00:00:00.000' AS DateTime), 0, 1, 1, CAST(21847.00 AS Decimal(18, 2)), N'molinarumualdamercedes@gmail.com', NULL, NULL, 1, N'Observacion de MOLINA, RUMUALDA MERCEDES')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (127, N'MOYA, AMELIA', 27527951204, N'PSP', 5279512, CAST(N'1945-10-12T00:00:00.000' AS DateTime), N'F', N'S', N'MOYA', 527, N'CORDOBA CAPITAL', 192, NULL, NULL, CAST(N'1952-10-10T00:00:00.000' AS DateTime), CAST(N'1956-10-09T00:00:00.000' AS DateTime), 1, 0, 0, CAST(21675.00 AS Decimal(18, 2)), N'moyaamelia@hotmail.com', NULL, NULL, 1, N'Observacion de MOYA, AMELIA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (128, N'SORIA, MANUEL S', 20644479808, N'DNI', 6444798, CAST(N'1946-06-20T00:00:00.000' AS DateTime), N'M', N'Z', N'SORIA', 644, N'CORDOBA CAPITAL', 193, NULL, NULL, CAST(N'1953-06-18T00:00:00.000' AS DateTime), CAST(N'1957-06-17T00:00:00.000' AS DateTime), 0, 0, 0, CAST(16128.00 AS Decimal(18, 2)), N'soriamanuels@gmail.com', NULL, NULL, 1, N'Observacion de SORIA, MANUEL S')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (129, N'FIGUEROA, ANA DEL VALLE', 27572004944, N'DNI', 5720049, CAST(N'1948-06-13T00:00:00.000' AS DateTime), N'F', N'Z', N'FIGUEROA', 572, N'CORDOBA CAPITAL', 194, NULL, NULL, CAST(N'1955-06-12T00:00:00.000' AS DateTime), CAST(N'1959-06-11T00:00:00.000' AS DateTime), 1, 0, 1, CAST(21373.00 AS Decimal(18, 2)), N'figueroaanadelvalle@hotmail.com', NULL, NULL, 1, N'Observacion de FIGUEROA, ANA DEL VALLE')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (130, N'CORVALAN, TOMAS DE LA CRUZ', 20817933107, N'DNI', 8179331, CAST(N'1946-12-28T00:00:00.000' AS DateTime), N'M', N'Z', N'CORVALAN', 817, N'CORDOBA CAPITAL', 195, NULL, NULL, CAST(N'1953-12-26T00:00:00.000' AS DateTime), CAST(N'1957-12-25T00:00:00.000' AS DateTime), 0, 1, 1, CAST(16013.00 AS Decimal(18, 2)), N'corvalantomasdelacruz@gmail.com', NULL, NULL, 1, N'Observacion de CORVALAN, TOMAS DE LA CRUZ')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (131, N'BUSTO, FRANCISCA NILDA', 27602553644, N'DNI', 6025536, CAST(N'1949-10-07T00:00:00.000' AS DateTime), N'F', N'S', N'BUSTO', 602, N'CORDOBA CAPITAL', 196, NULL, NULL, CAST(N'1956-10-05T00:00:00.000' AS DateTime), CAST(N'1960-10-04T00:00:00.000' AS DateTime), 1, 0, 0, CAST(21070.00 AS Decimal(18, 2)), N'bustofranciscanilda@hotmail.com', NULL, NULL, 1, N'Observacion de BUSTO, FRANCISCA NILDA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (132, N'SARMIENTO, MARGARITA DEL MILAGRO', 27525818643, N'DNI', 5258186, CAST(N'1945-10-07T00:00:00.000' AS DateTime), N'F', N'Z', N'SARMIENTO', 525, N'SIN ASIGNAR', 197, NULL, NULL, CAST(N'1952-10-05T00:00:00.000' AS DateTime), CAST(N'1956-10-04T00:00:00.000' AS DateTime), 0, 1, 0, CAST(20852.00 AS Decimal(18, 2)), N'sarmientomargaritadelmilagro@gmail.com', NULL, NULL, 1, N'Observacion de SARMIENTO, MARGARITA DEL MILAGRO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (133, N'GALINDEZ, OSCAR', 20650794118, N'DNI', 6507941, CAST(N'1938-08-17T00:00:00.000' AS DateTime), N'M', N'Z', N'GALINDEZ', 650, N'CORDOBA CAPITAL', 198, NULL, NULL, CAST(N'1945-08-15T00:00:00.000' AS DateTime), CAST(N'1949-08-14T00:00:00.000' AS DateTime), 1, 0, 1, CAST(15526.00 AS Decimal(18, 2)), N'galindezoscar@hotmail.com', NULL, NULL, 1, N'Observacion de GALINDEZ, OSCAR')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (134, N'SCAMPONE, ALEJANDRO PLACIDO', 20779897318, N'DNI', 7798973, CAST(N'1949-05-27T00:00:00.000' AS DateTime), N'M', N'S', N'SCAMPONE', 779, N'CORDOBA CAPITAL', 199, NULL, NULL, CAST(N'1956-05-25T00:00:00.000' AS DateTime), CAST(N'1960-05-24T00:00:00.000' AS DateTime), 0, 0, 1, CAST(15507.00 AS Decimal(18, 2)), N'scamponealejandroplacido@gmail.com', NULL, NULL, 1, N'Observacion de SCAMPONE, ALEJANDRO PLACIDO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (135, N'FERNANDEZ, CARMEN ANIBAL', 20643830280, N'CF', 6438302, CAST(N'1943-11-14T00:00:00.000' AS DateTime), N'M', N'C', N'FERNANDEZ', 643, N'SIN ASIGNAR', 65, NULL, NULL, CAST(N'1950-11-12T00:00:00.000' AS DateTime), CAST(N'1954-11-11T00:00:00.000' AS DateTime), 1, 0, 0, CAST(15291.00 AS Decimal(18, 2)), N'fernandezcarmenanibal@hotmail.com', NULL, NULL, 1, N'Observacion de FERNANDEZ, CARMEN ANIBAL')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (136, N'NIETO, ANGEL ANTONIO', 20669914596, N'CF', 6699145, CAST(N'1944-01-22T00:00:00.000' AS DateTime), N'M', N'Z', N'NIETO', 669, N'CORDOBA CAPITAL', 66, NULL, NULL, CAST(N'1951-01-20T00:00:00.000' AS DateTime), CAST(N'1955-01-19T00:00:00.000' AS DateTime), 0, 0, 1, CAST(15198.00 AS Decimal(18, 2)), N'nietoangelantonio@gmail.com', NULL, NULL, 1, N'Observacion de NIETO, ANGEL ANTONIO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (137, N'VIGNOLA, RUBEN BENITO', 20643159284, N'CF', 6431592, CAST(N'1941-01-12T00:00:00.000' AS DateTime), N'M', N'Z', N'VIGNOLA', 643, N'SAN FRANCISCO', 67, NULL, NULL, CAST(N'1948-01-11T00:00:00.000' AS DateTime), CAST(N'1952-01-10T00:00:00.000' AS DateTime), 1, 0, 0, CAST(15067.00 AS Decimal(18, 2)), N'vignolarubenbenito@hotmail.com', NULL, NULL, 1, N'Observacion de VIGNOLA, RUBEN BENITO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (138, N'ACHERVI, MIGUEL ANGELA7', 20644355407, N'DNI', 64435540, CAST(N'1975-08-01T03:00:00.000' AS DateTime), N'F', N'C', N'ACHERVI', 644, N'santa rosa de calamuchita', 68, 4, 1, CAST(N'1958-03-06T03:00:00.000' AS DateTime), NULL, 0, 1, 0, CAST(14957.00 AS Decimal(18, 2)), N'achervimiguelangel7@gmail.com', NULL, NULL, 1, N'Observacion de ACHERVI, MIGUEL ANGELA7')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (139, N'LENCINAS, CARLOS ALBERTO', 20642881228, N'CP', 6428812, CAST(N'1939-08-29T00:00:00.000' AS DateTime), N'M', N'Z', N'LENCINAS', 642, N'SAN FRANCISCO', 69, NULL, NULL, CAST(N'1946-08-27T00:00:00.000' AS DateTime), CAST(N'1950-08-26T00:00:00.000' AS DateTime), 1, 0, 0, CAST(14850.00 AS Decimal(18, 2)), N'lencinascarlosalberto@hotmail.com', NULL, NULL, 1, N'Observacion de LENCINAS, CARLOS ALBERTO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (140, N'NADALIN, OTILIO JOSE', 20642603253, N'CP', 6426032, CAST(N'1938-08-02T00:00:00.000' AS DateTime), N'M', N'C', N'NADALIN', 642, N'MARULL', 70, NULL, NULL, CAST(N'1945-07-31T00:00:00.000' AS DateTime), CAST(N'1949-07-30T00:00:00.000' AS DateTime), 0, 1, 0, CAST(14744.00 AS Decimal(18, 2)), N'nadalinotiliojose@gmail.com', NULL, NULL, 1, N'Observacion de NADALIN, OTILIO JOSE')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (141, N'MOYANO, RAUL ANTONIO', 23643455591, N'LE', 6434555, CAST(N'1942-04-03T00:00:00.000' AS DateTime), N'M', N'S', N'MOYANO', 643, N'SAN FRANCISCO', 71, NULL, NULL, CAST(N'1949-04-01T00:00:00.000' AS DateTime), CAST(N'1953-03-31T00:00:00.000' AS DateTime), 1, 1, 1, CAST(16768.00 AS Decimal(18, 2)), N'moyanoraulantonio@hotmail.com', NULL, NULL, 1, N'Observacion de MOYANO, RAUL ANTONIO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (142, N'MOYANO, LINA MARGARITA', 27878539487, N'LE', 8785394, CAST(N'1939-12-10T00:00:00.000' AS DateTime), N'F', N'Z', N'MOYANO', 878, N'CORDOBA CAPITAL', 72, NULL, NULL, CAST(N'1946-12-08T00:00:00.000' AS DateTime), CAST(N'1950-12-07T00:00:00.000' AS DateTime), 0, 1, 0, CAST(19632.00 AS Decimal(18, 2)), N'moyanolinamargarita@gmail.com', NULL, NULL, 1, N'Observacion de MOYANO, LINA MARGARITA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (143, N'ABRATE, VICENTE BARTOLO', 20642466677, N'CF', 64246667, CAST(N'1943-08-08T04:00:00.000' AS DateTime), N'M', N'Z', N'ABRATE', 642, N'cordoba capital', 73, 5, 1, CAST(N'1944-08-08T03:00:00.000' AS DateTime), CAST(N'2020-10-01T03:00:00.000' AS DateTime), 1, 1, 1, CAST(123.54 AS Decimal(18, 2)), N'abratevicentebartolo@hotmail.com', NULL, NULL, 1, N'Observacion de ABRATE, VICENTE BARTOLO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (144, N'MEZ, JUAN CARLOS', 20794386868, N'DNI', 7943868, CAST(N'1945-03-03T00:00:00.000' AS DateTime), N'M', N'Z', N'MEZ', 794, N'SAN FRANCISCO', 74, NULL, NULL, CAST(N'1952-03-01T00:00:00.000' AS DateTime), CAST(N'1956-02-29T00:00:00.000' AS DateTime), 0, 0, 0, CAST(14440.00 AS Decimal(18, 2)), N'MEZjuancarlos@gmail.com', NULL, NULL, 1, N'Observacion de MEZ, JUAN CARLOS')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (145, N'HEREDIA, SARA ANTONIA', 27375677155, N'DNI', 3756771, CAST(N'1938-08-10T00:00:00.000' AS DateTime), N'F', N'S', N'HEREDIA', 375, N'CORDOBA CAPITAL', 75, NULL, NULL, CAST(N'1945-08-08T00:00:00.000' AS DateTime), CAST(N'1949-08-07T00:00:00.000' AS DateTime), 1, 1, 1, CAST(18879.00 AS Decimal(18, 2)), N'herediasaraantonia@hotmail.com', NULL, NULL, 1, N'Observacion de HEREDIA, SARA ANTONIA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (146, N'CONTRERAS, NORMA BEATRIZ', 22078512775, N'DNI', 20785127, CAST(N'1969-08-08T00:00:00.000' AS DateTime), N'F', N'S', N'CONTRERAS', 207, N'CORDOBA CAPITAL', 76, NULL, NULL, CAST(N'1976-08-06T00:00:00.000' AS DateTime), CAST(N'1980-08-05T00:00:00.000' AS DateTime), 0, 1, 1, CAST(15122.00 AS Decimal(18, 2)), N'contrerasnormabeatriz@gmail.com', NULL, NULL, 1, N'Observacion de CONTRERAS, NORMA BEATRIZ')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (147, N'ORTEGA, JESUS', 20841075742, N'DNI', 8410757, CAST(N'1948-12-25T00:00:00.000' AS DateTime), N'M', N'Z', N'ORTEGA', 841, N'CORDOBA CAPITAL', 77, NULL, NULL, CAST(N'1955-12-24T00:00:00.000' AS DateTime), CAST(N'1959-12-23T00:00:00.000' AS DateTime), 1, 0, 1, CAST(14177.00 AS Decimal(18, 2)), N'ortegajesus@hotmail.com', NULL, NULL, 1, N'Observacion de ORTEGA, JESUS')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (148, N'DOMINGUEZ, AURORA', 27597976334, N'DNI', 5979763, CAST(N'1949-12-21T00:00:00.000' AS DateTime), N'F', N'C', N'DOMINGUEZ', 597, N'CORDOBA CAPITAL', 78, NULL, NULL, CAST(N'1956-12-19T00:00:00.000' AS DateTime), CAST(N'1960-12-18T00:00:00.000' AS DateTime), 0, 0, 1, CAST(18647.00 AS Decimal(18, 2)), N'dominguezaurora@gmail.com', NULL, NULL, 1, N'Observacion de DOMINGUEZ, AURORA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (149, N'SILVESTRE, CRISTINA BARBARIANA', 27599597651, N'DNI', 5995976, CAST(N'1949-06-25T00:00:00.000' AS DateTime), N'F', N'Z', N'SILVESTRE', 599, N'CORDOBA CAPITAL', 79, NULL, NULL, CAST(N'1956-06-23T00:00:00.000' AS DateTime), CAST(N'1960-06-22T00:00:00.000' AS DateTime), 1, 1, 0, CAST(18523.00 AS Decimal(18, 2)), N'silvestrecristinabarbariana@hotmail.com', NULL, NULL, 1, N'Observacion de SILVESTRE, CRISTINA BARBARIANA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (150, N'SANTOS, ESTELA ELVIRA', 27545233814, N'DNI', 5452338, CAST(N'1946-10-11T00:00:00.000' AS DateTime), N'F', N'Z', N'SANTOS', 545, N'CORDOBA CAPITAL', 80, NULL, NULL, CAST(N'1953-10-09T00:00:00.000' AS DateTime), CAST(N'1957-10-08T00:00:00.000' AS DateTime), 0, 0, 0, CAST(18363.00 AS Decimal(18, 2)), N'santosestelaelvira@gmail.com', NULL, NULL, 1, N'Observacion de SANTOS, ESTELA ELVIRA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (151, N'BARRIONUEVO, AIDA NORA', 27562853226, N'CF', 5628532, CAST(N'1948-01-01T00:00:00.000' AS DateTime), N'F', N'S', N'BARRIONUEVO', 562, N'CORDOBA CAPITAL', 81, NULL, NULL, CAST(N'1954-12-30T00:00:00.000' AS DateTime), CAST(N'1958-12-29T00:00:00.000' AS DateTime), 1, 0, 0, CAST(18253.00 AS Decimal(18, 2)), N'barrionuevoaidanora@hotmail.com', NULL, NULL, 1, N'Observacion de BARRIONUEVO, AIDA NORA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (152, N'GUZMAN, RAMONA  ESPERANZA', 27613276288, N'CF', 6132762, CAST(N'1949-11-14T00:00:00.000' AS DateTime), N'F', N'Z', N'GUZMAN', 613, N'CORDOBA CAPITAL', 82, NULL, NULL, CAST(N'1956-11-12T00:00:00.000' AS DateTime), CAST(N'1960-11-11T00:00:00.000' AS DateTime), 0, 0, 0, CAST(18166.00 AS Decimal(18, 2)), N'guzmanramonaesperanza@gmail.com', NULL, NULL, 1, N'Observacion de GUZMAN, RAMONA  ESPERANZA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (153, N'VIDELA, CONSTANCIA', 27569816976, N'CF', 5698169, CAST(N'1947-11-21T00:00:00.000' AS DateTime), N'F', N'Z', N'VIDELA', 569, N'CORDOBA CAPITAL', 83, NULL, NULL, CAST(N'1954-11-19T00:00:00.000' AS DateTime), CAST(N'1958-11-18T00:00:00.000' AS DateTime), 1, 0, 1, CAST(18019.00 AS Decimal(18, 2)), N'videlaconstancia@hotmail.com', NULL, NULL, 1, N'Observacion de VIDELA, CONSTANCIA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (154, N'CONTRERAS, MIRTA NILDA', 27558722823, N'CP', 5587228, CAST(N'1947-02-08T00:00:00.000' AS DateTime), N'F', N'S', N'CONTRERAS', 558, N'CORDOBA CAPITAL', 84, NULL, NULL, CAST(N'1954-02-06T00:00:00.000' AS DateTime), CAST(N'1958-02-05T00:00:00.000' AS DateTime), 0, 1, 0, CAST(17895.00 AS Decimal(18, 2)), N'contrerasmirtanilda@gmail.com', NULL, NULL, 1, N'Observacion de CONTRERAS, MIRTA NILDA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (155, N'LESCANO, TEODORA ANTONIA', 27568904117, N'CP', 5689041, CAST(N'1948-06-06T00:00:00.000' AS DateTime), N'F', N'Z', N'LESCANO', 568, N'SIN ASIGNAR', 85, NULL, NULL, CAST(N'1955-06-05T00:00:00.000' AS DateTime), CAST(N'1959-06-04T00:00:00.000' AS DateTime), 1, 1, 1, CAST(17786.00 AS Decimal(18, 2)), N'lescanoteodoraantonia@hotmail.com', NULL, NULL, 1, N'Observacion de LESCANO, TEODORA ANTONIA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (156, N'BAIGORRIA, BLANCA ALICIA', 27170068678, N'CP', 1700686, CAST(N'1964-10-25T00:00:00.000' AS DateTime), N'F', N'Z', N'BAIGORRIA', 170, N'CORDOBA CAPITAL', 86, NULL, NULL, CAST(N'1971-10-24T00:00:00.000' AS DateTime), CAST(N'1975-10-23T00:00:00.000' AS DateTime), 0, 0, 0, CAST(17416.00 AS Decimal(18, 2)), N'baigorriablancaalicia@gmail.com', NULL, NULL, 1, N'Observacion de BAIGORRIA, BLANCA ALICIA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (157, N'PERALTA, HILARIA LEONIDES', 23445898345, N'LE', 4458983, CAST(N'1944-01-14T00:00:00.000' AS DateTime), N'F', N'Z', N'PERALTA', 445, N'CORDOBA CAPITAL', 87, NULL, NULL, CAST(N'1951-01-12T00:00:00.000' AS DateTime), CAST(N'1955-01-11T00:00:00.000' AS DateTime), 1, 1, 1, CAST(14933.00 AS Decimal(18, 2)), N'peraltahilarialeonides@hotmail.com', NULL, NULL, 1, N'Observacion de PERALTA, HILARIA LEONIDES')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (158, N'FERREYRA, MARIA INES', 23649485040, N'LE', 6494850, CAST(N'1951-04-01T00:00:00.000' AS DateTime), N'F', N'Z', N'FERREYRA', 649, N'CORDOBA CAPITAL', 88, NULL, NULL, CAST(N'1958-03-30T00:00:00.000' AS DateTime), CAST(N'1962-03-29T00:00:00.000' AS DateTime), 0, 0, 0, CAST(14968.00 AS Decimal(18, 2)), N'ferreyramariaines@gmail.com', NULL, NULL, 1, N'Observacion de FERREYRA, MARIA INES')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (159, N'ROMERO, PETRONA ROSA', 27544339517, N'PSP', 5443395, CAST(N'1947-12-04T00:00:00.000' AS DateTime), N'F', N'Z', N'ROMERO', 544, N'QUILINO', 89, NULL, NULL, CAST(N'1954-12-02T00:00:00.000' AS DateTime), CAST(N'1958-12-01T00:00:00.000' AS DateTime), 1, 1, 1, CAST(17323.00 AS Decimal(18, 2)), N'romeropetronarosa@hotmail.com', NULL, NULL, 1, N'Observacion de ROMERO, PETRONA ROSA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (160, N'SANCHEZ, RAMON ESTEBAN', 20795728972, N'DNI', 7957289, CAST(N'1947-02-13T00:00:00.000' AS DateTime), N'M', N'Z', N'SANCHEZ', 795, N'QUILINO', 90, NULL, NULL, CAST(N'1954-02-11T00:00:00.000' AS DateTime), CAST(N'1958-02-10T00:00:00.000' AS DateTime), 0, 0, 1, CAST(12997.00 AS Decimal(18, 2)), N'sanchezramonesteban@gmail.com', NULL, NULL, 1, N'Observacion de SANCHEZ, RAMON ESTEBAN')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (161, N'DROZCO, RAMONA ELISA', 27595652016, N'DNI', 5956520, CAST(N'1949-12-30T00:00:00.000' AS DateTime), N'F', N'S', N'DROZCO', 595, N'VILLA VALERIA', 91, NULL, NULL, CAST(N'1956-12-28T00:00:00.000' AS DateTime), CAST(N'1960-12-27T00:00:00.000' AS DateTime), 1, 0, 0, CAST(17140.00 AS Decimal(18, 2)), N'drozcoramonaelisa@hotmail.com', NULL, NULL, 1, N'Observacion de DROZCO, RAMONA ELISA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (162, N'SUAREZ, ANGELA LUCRECIA', 27625386148, N'DNI', 6253861, CAST(N'1949-12-18T00:00:00.000' AS DateTime), N'F', N'Z', N'SUAREZ', 625, N'QUILINO', 92, NULL, NULL, CAST(N'1956-12-16T00:00:00.000' AS DateTime), CAST(N'1960-12-15T00:00:00.000' AS DateTime), 0, 0, 1, CAST(17052.00 AS Decimal(18, 2)), N'suarezangelalucrecia@gmail.com', NULL, NULL, 1, N'Observacion de SUAREZ, ANGELA LUCRECIA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (163, N'VILLALBA, BENICIA DEOLINDA', 27496876004, N'DNI', 4968760, CAST(N'1945-09-23T00:00:00.000' AS DateTime), N'F', N'C', N'VILLALBA', 496, N'QUILINO', 93, NULL, NULL, CAST(N'1952-09-21T00:00:00.000' AS DateTime), CAST(N'1956-09-20T00:00:00.000' AS DateTime), 1, 0, 0, CAST(16869.00 AS Decimal(18, 2)), N'villalbabeniciadeolinda@hotmail.com', NULL, NULL, 1, N'Observacion de VILLALBA, BENICIA DEOLINDA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (164, N'ROTH, MARIA ESTELA', 23466879145, N'DNI', 4668791, CAST(N'1945-11-11T00:00:00.000' AS DateTime), N'F', N'S', N'ROTH', 466, N'SUCO', 94, NULL, NULL, CAST(N'1952-11-09T00:00:00.000' AS DateTime), CAST(N'1956-11-08T00:00:00.000' AS DateTime), 0, 1, 1, CAST(14309.00 AS Decimal(18, 2)), N'rothmariaestela@gmail.com', NULL, NULL, 1, N'Observacion de ROTH, MARIA ESTELA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (165, N'GONZALES, OLGA CRISTINA', 27574258522, N'DNI', 5742585, CAST(N'1948-04-24T00:00:00.000' AS DateTime), N'F', N'C', N'GONZALES', 574, N'CRUZ DEL EJE', 95, NULL, NULL, CAST(N'1955-04-23T00:00:00.000' AS DateTime), CAST(N'1959-04-22T00:00:00.000' AS DateTime), 1, 0, 1, CAST(16711.00 AS Decimal(18, 2)), N'gonzalesolgacristina@hotmail.com', NULL, NULL, 1, N'Observacion de GONZALES, OLGA CRISTINA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (166, N'SAINT, GIRONS MIRTA', 27474216546, N'DNI', 4742165, CAST(N'1944-03-08T00:00:00.000' AS DateTime), N'F', N'C', N'SAINT', 474, N'COSQUIN', 96, NULL, NULL, CAST(N'1951-03-07T00:00:00.000' AS DateTime), CAST(N'1955-03-06T00:00:00.000' AS DateTime), 0, 0, 1, CAST(16550.00 AS Decimal(18, 2)), N'saintgironsmirta@gmail.com', NULL, NULL, 1, N'Observacion de SAINT, GIRONS MIRTA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (167, N'GALLARDO, VALERIA ISABEL', 27517863655, N'CF', 5178636, CAST(N'1946-05-05T00:00:00.000' AS DateTime), N'F', N'V', N'GALLARDO', 517, N'LUYABA', 97, NULL, NULL, CAST(N'1953-05-03T00:00:00.000' AS DateTime), CAST(N'1957-05-02T00:00:00.000' AS DateTime), 1, 1, 0, CAST(16477.00 AS Decimal(18, 2)), N'gallardovaleriaisabel@hotmail.com', NULL, NULL, 1, N'Observacion de GALLARDO, VALERIA ISABEL')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (168, N'ALVAREZ, AIDA MIRIAN', 23636575146, N'CF', 6365751, CAST(N'1950-08-01T00:00:00.000' AS DateTime), N'F', N'C', N'ALVAREZ', 636, N'CORDOBA CAPITAL', 98, NULL, NULL, CAST(N'1957-07-30T00:00:00.000' AS DateTime), CAST(N'1961-07-29T00:00:00.000' AS DateTime), 0, 0, 1, CAST(14069.00 AS Decimal(18, 2)), N'alvarezaidamirian@gmail.com', NULL, NULL, 1, N'Observacion de ALVAREZ, AIDA MIRIAN')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (169, N'ALTAMIRA, YOLANDA NELIDA', 27581850731, N'CF', 5818507, CAST(N'1948-09-30T00:00:00.000' AS DateTime), N'F', N'S', N'ALTAMIRA', 581, N'CORDOBA CAPITAL', 99, NULL, NULL, CAST(N'1955-09-29T00:00:00.000' AS DateTime), CAST(N'1959-09-28T00:00:00.000' AS DateTime), 1, 1, 1, CAST(16320.00 AS Decimal(18, 2)), N'altamirayolandanelida@hotmail.com', NULL, NULL, 1, N'Observacion de ALTAMIRA, YOLANDA NELIDA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (170, N'VILLAFAÑE, DAVID CARLOS', 23857867091, N'CP', 8578670, CAST(N'1951-08-09T00:00:00.000' AS DateTime), N'M', N'S', N'VILLAFAÑE', 857, N'RIO TERCERO', 100, NULL, NULL, CAST(N'1958-08-07T00:00:00.000' AS DateTime), CAST(N'1962-08-06T00:00:00.000' AS DateTime), 0, 1, 0, CAST(14034.00 AS Decimal(18, 2)), N'villafañedavidcarlos@gmail.com', NULL, NULL, 1, N'Observacion de VILLAFAÑE, DAVID CARLOS')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (171, N'PAEZ, MARTINA NILDA', 27546182853, N'CP', 5461828, CAST(N'1947-01-30T00:00:00.000' AS DateTime), N'F', N'S', N'PAEZ', 546, N'CORDOBA CAPITAL', 101, NULL, NULL, CAST(N'1954-01-28T00:00:00.000' AS DateTime), CAST(N'1958-01-27T00:00:00.000' AS DateTime), 1, 1, 0, CAST(16108.00 AS Decimal(18, 2)), N'paezmartinanilda@hotmail.com', NULL, NULL, 1, N'Observacion de PAEZ, MARTINA NILDA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (172, N'LAFON, MIGUEL ANGEL', 20799244675, N'CP', 7992446, CAST(N'1946-12-31T00:00:00.000' AS DateTime), N'M', N'Z', N'LAFON', 799, N'CORDOBA CAPITAL', 102, NULL, NULL, CAST(N'1953-12-29T00:00:00.000' AS DateTime), CAST(N'1957-12-28T00:00:00.000' AS DateTime), 0, 1, 0, CAST(12092.00 AS Decimal(18, 2)), N'lafonmiguelangel@gmail.com', NULL, NULL, 1, N'Observacion de LAFON, MIGUEL ANGEL')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (173, N'ROMERO, MARIA ANA', 23634210148, N'LE', 6342101, CAST(N'1941-08-03T00:00:00.000' AS DateTime), N'F', N'Z', N'ROMERO', 634, N'CORDOBA CAPITAL', 103, NULL, NULL, CAST(N'1948-08-01T00:00:00.000' AS DateTime), CAST(N'1952-07-31T00:00:00.000' AS DateTime), 1, 0, 1, CAST(13661.00 AS Decimal(18, 2)), N'romeromariaana@hotmail.com', NULL, NULL, 1, N'Observacion de ROMERO, MARIA ANA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (174, N'ACOSTA, LIDIA', 27606233553, N'LE', 6062335, CAST(N'1949-07-01T00:00:00.000' AS DateTime), N'F', N'Z', N'ACOSTA', 606, N'LA PUERTA', 104, NULL, NULL, CAST(N'1956-06-29T00:00:00.000' AS DateTime), CAST(N'1960-06-28T00:00:00.000' AS DateTime), 0, 1, 1, CAST(15865.00 AS Decimal(18, 2)), N'acostalidia@gmail.com', NULL, NULL, 0, N'Observacion de ACOSTA, LIDIA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (175, N'CHAVEZ, BLANCA GRACIELA', 27160820537, N'PSP', 1608205, CAST(N'1962-10-24T00:00:00.000' AS DateTime), N'F', N'Z', N'CHAVEZ', 160, N'CORDOBA CAPITAL', 105, NULL, NULL, CAST(N'1969-10-22T00:00:00.000' AS DateTime), CAST(N'1973-10-21T00:00:00.000' AS DateTime), 1, 1, 1, CAST(15520.00 AS Decimal(18, 2)), N'chavezblancagraciela@hotmail.com', NULL, NULL, 1, N'Observacion de CHAVEZ, BLANCA GRACIELA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (176, N'GONZALEZ, EDUARDO FELICIANO', 20634808364, N'DNI', 6348083, CAST(N'1940-08-16T00:00:00.000' AS DateTime), N'M', N'C', N'NZALEZ', 634, N'BIALET MASSE', 106, NULL, NULL, CAST(N'1947-08-15T00:00:00.000' AS DateTime), CAST(N'1951-08-14T00:00:00.000' AS DateTime), 0, 0, 1, CAST(11724.00 AS Decimal(18, 2)), N'gonzalezeduardofeliciano@gmail.com', NULL, NULL, 1, N'Observacion de NZALEZ, EDUARDO FELICIANO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (177, N'GONZALEZ, NICOLAS RAMON', 20763904116, N'DNI', 7639041, CAST(N'1949-09-18T00:00:00.000' AS DateTime), N'M', N'Z', N'NZALEZ', 763, N'BIALET MASSE', 107, NULL, NULL, CAST(N'1956-09-16T00:00:00.000' AS DateTime), CAST(N'1960-09-15T00:00:00.000' AS DateTime), 1, 0, 1, CAST(11731.00 AS Decimal(18, 2)), N'gonzaleznicolasramon@hotmail.com', NULL, NULL, 1, N'Observacion de NZALEZ, NICOLAS RAMON')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (178, N'ROBLEDO, ELIDA MABEL', 27426669265, N'DNI', 4266692, CAST(N'1971-01-01T00:00:00.000' AS DateTime), N'F', N'Z', N'ROBLEDO', 426, N'GENERAL LEVALLE', 108, NULL, NULL, CAST(N'1977-12-30T00:00:00.000' AS DateTime), CAST(N'1981-12-29T00:00:00.000' AS DateTime), 0, 1, 0, CAST(15408.00 AS Decimal(18, 2)), N'robledoelidamabel@gmail.com', NULL, NULL, 1, N'Observacion de ROBLEDO, ELIDA MABEL')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (179, N'SALDAÑO, MARIA LIDIA', 27546207783, N'DNI', 5462077, CAST(N'1947-02-19T00:00:00.000' AS DateTime), N'F', N'Z', N'SALDAÑO', 546, N'CORDOBA CAPITAL', 109, NULL, NULL, CAST(N'1954-02-17T00:00:00.000' AS DateTime), CAST(N'1958-02-16T00:00:00.000' AS DateTime), 1, 1, 1, CAST(15388.00 AS Decimal(18, 2)), N'saldañomarialidia@hotmail.com', NULL, NULL, 1, N'Observacion de SALDAÑO, MARIA LIDIA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (180, N'RIVERO, PASTORA', 21400512974, N'DNI', 14005129, CAST(N'1960-10-27T00:00:00.000' AS DateTime), N'F', N'Z', N'RIVERO', 140, N'BIALET MASSE', 110, NULL, NULL, CAST(N'1967-10-26T00:00:00.000' AS DateTime), CAST(N'1971-10-25T00:00:00.000' AS DateTime), 0, 0, 1, CAST(11889.00 AS Decimal(18, 2)), N'riveropastora@gmail.com', NULL, NULL, 1, N'Observacion de RIVERO, PASTORA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (181, N'LAZO, JUAN', 20655653400, N'DNI', 6556534, CAST(N'1945-09-16T00:00:00.000' AS DateTime), N'M', N'Z', N'LAZO', 655, N'CHILIBROSTE', 111, NULL, NULL, CAST(N'1952-09-14T00:00:00.000' AS DateTime), CAST(N'1956-09-13T00:00:00.000' AS DateTime), 1, 0, 0, CAST(11411.00 AS Decimal(18, 2)), N'lazojuan@hotmail.com', NULL, NULL, 1, N'Observacion de LAZO, JUAN')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (182, N'BRUNOTTO, AZUCENA LUCIA', 27133120967, N'DNI', 13312096, CAST(N'1957-04-02T00:00:00.000' AS DateTime), N'F', N'S', N'BRUNOTTO', 133, N'LA FRANCIA', 112, NULL, NULL, CAST(N'1964-03-31T00:00:00.000' AS DateTime), CAST(N'1968-03-30T00:00:00.000' AS DateTime), 0, 1, 0, CAST(14908.00 AS Decimal(18, 2)), N'brunottoazucenalucia@gmail.com', NULL, NULL, 1, N'Observacion de BRUNOTTO, AZUCENA LUCIA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (183, N'DIAZ, CELIA', 27388526264, N'CF', 3885262, CAST(N'1940-02-08T00:00:00.000' AS DateTime), N'F', N'S', N'DIAZ', 388, N'CHILIBROSTE', 113, NULL, NULL, CAST(N'1947-02-06T00:00:00.000' AS DateTime), CAST(N'1951-02-05T00:00:00.000' AS DateTime), 1, 0, 0, CAST(14966.00 AS Decimal(18, 2)), N'diazcelia@hotmail.com', NULL, NULL, 1, N'Observacion de DIAZ, CELIA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (184, N'PAEZ, ROSA', 27478944123, N'CF', 4789441, CAST(N'1944-05-17T00:00:00.000' AS DateTime), N'F', N'Z', N'PAEZ', 478, N'BIALET MASSE', 114, NULL, NULL, CAST(N'1951-05-16T00:00:00.000' AS DateTime), CAST(N'1955-05-15T00:00:00.000' AS DateTime), 0, 1, 1, CAST(14934.00 AS Decimal(18, 2)), N'paezrosa@gmail.com', NULL, NULL, 1, N'Observacion de PAEZ, ROSA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (185, N'PERALTA, TERESA DE JESUS', 27388357648, N'CF', 3883576, CAST(N'1939-10-15T00:00:00.000' AS DateTime), N'F', N'Z', N'PERALTA', 388, N'MARULL', 115, NULL, NULL, CAST(N'1946-10-13T00:00:00.000' AS DateTime), CAST(N'1950-10-12T00:00:00.000' AS DateTime), 1, 0, 0, CAST(14804.00 AS Decimal(18, 2)), N'peraltateresadejesus@hotmail.com', NULL, NULL, 1, N'Observacion de PERALTA, TERESA DE JESUS')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (186, N'JUNCOS, IRMA JOSEFA', 27487279438, N'CP', 4872794, CAST(N'1948-03-19T00:00:00.000' AS DateTime), N'F', N'Z', N'JUNCOS', 487, N'MARULL', 116, NULL, NULL, CAST(N'1955-03-18T00:00:00.000' AS DateTime), CAST(N'1959-03-17T00:00:00.000' AS DateTime), 0, 0, 0, CAST(14778.00 AS Decimal(18, 2)), N'juncosirmajosefa@gmail.com', NULL, NULL, 1, N'Observacion de JUNCOS, IRMA JOSEFA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (187, N'MASOERO, ENRIQUE ALFREDO', 20604056714, N'CP', 6040567, CAST(N'1941-07-15T00:00:00.000' AS DateTime), N'M', N'Z', N'MASOERO', 604, N'VILLA SANTA CRUZ DEL LAGO', 117, NULL, NULL, CAST(N'1948-07-13T00:00:00.000' AS DateTime), CAST(N'1952-07-12T00:00:00.000' AS DateTime), 1, 0, 1, CAST(11018.00 AS Decimal(18, 2)), N'masoeroenriquealfredo@hotmail.com', NULL, NULL, 1, N'Observacion de MASOERO, ENRIQUE ALFREDO')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (188, N'SANCHEZ, TOMASA PETRONA', 27361059985, N'CP', 3610599, CAST(N'1938-09-20T00:00:00.000' AS DateTime), N'F', N'Z', N'SANCHEZ', 361, N'SIN ASIGNAR', 118, NULL, NULL, CAST(N'1945-09-18T00:00:00.000' AS DateTime), CAST(N'1949-09-17T00:00:00.000' AS DateTime), 0, 1, 1, CAST(14553.00 AS Decimal(18, 2)), N'sancheztomasapetrona@gmail.com', NULL, NULL, 1, N'Observacion de SANCHEZ, TOMASA PETRONA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (189, N'ROMERO, ROSA EVA', 27484389336, N'LE', 4843893, CAST(N'1947-08-30T00:00:00.000' AS DateTime), N'F', N'S', N'ROMERO', 484, N'SIN ASIGNAR', 119, NULL, NULL, CAST(N'1954-08-28T00:00:00.000' AS DateTime), CAST(N'1958-08-27T00:00:00.000' AS DateTime), 1, 0, 1, CAST(14542.00 AS Decimal(18, 2)), N'romerorosaeva@hotmail.com', NULL, NULL, 1, N'Observacion de ROMERO, ROSA EVA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (190, N'TORRES, CLAUDINA S', 27412989581, N'LE', 4129895, CAST(N'1941-12-08T00:00:00.000' AS DateTime), N'F', N'Z', N'TORRES', 412, N'SANTA ANA', 120, NULL, NULL, CAST(N'1948-12-06T00:00:00.000' AS DateTime), CAST(N'1952-12-05T00:00:00.000' AS DateTime), 0, 1, 1, CAST(14427.00 AS Decimal(18, 2)), N'torresclaudinas@gmail.com', NULL, NULL, 1, N'Observacion de TORRES, CLAUDINA S')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (191, N'CARRIZO, ELIA DEL VALLE', 23467557745, N'PSP', 4675577, CAST(N'1943-08-12T00:00:00.000' AS DateTime), N'F', N'Z', N'CARRIZO', 467, N'CORDOBA CAPITAL', 121, NULL, NULL, CAST(N'1950-08-10T00:00:00.000' AS DateTime), CAST(N'1954-08-09T00:00:00.000' AS DateTime), 1, 1, 1, CAST(12286.00 AS Decimal(18, 2)), N'carrizoeliadelvalle@hotmail.com', NULL, NULL, 1, N'Observacion de CARRIZO, ELIA DEL VALLE')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (192, N'RODRIGUEZ, MARIA ANTONIA', 22087011071, N'DNI', 20870110, CAST(N'1969-05-03T00:00:00.000' AS DateTime), N'F', N'Z', N'RODRIGUEZ', 208, N'CORDOBA CAPITAL', 122, NULL, NULL, CAST(N'1976-05-01T00:00:00.000' AS DateTime), CAST(N'1980-04-30T00:00:00.000' AS DateTime), 0, 1, 0, CAST(11503.00 AS Decimal(18, 2)), N'rodriguezmariaantonia@gmail.com', NULL, NULL, 1, N'Observacion de RODRIGUEZ, MARIA ANTONIA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (193, N'MUZO, MIRTA EDITH', 27639729112, N'DNI', 6397291, CAST(N'1950-11-08T00:00:00.000' AS DateTime), N'F', N'S', N'MUZO', 639, N'CORDOBA CAPITAL', 123, NULL, NULL, CAST(N'1957-11-06T00:00:00.000' AS DateTime), CAST(N'1961-11-05T00:00:00.000' AS DateTime), 1, 0, 1, CAST(14321.00 AS Decimal(18, 2)), N'muzomirtaedith@hotmail.com', NULL, NULL, 1, N'Observacion de MUZO, MIRTA EDITH')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (194, N'BENETTO, MIRTA DEL VALLE', 27581302798, N'DNI', 5813027, CAST(N'1948-04-03T00:00:00.000' AS DateTime), N'F', N'Z', N'BENETTO', 581, N'SIN ASIGNAR', 124, NULL, NULL, CAST(N'1955-04-02T00:00:00.000' AS DateTime), CAST(N'1959-04-01T00:00:00.000' AS DateTime), 0, 0, 1, CAST(14217.00 AS Decimal(18, 2)), N'benettomirtadelvalle@gmail.com', NULL, NULL, 1, N'Observacion de BENETTO, MIRTA DEL VALLE')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (195, N'OROPEL, NELIDA ROSA', 27568164914, N'DNI', 5681649, CAST(N'1946-08-31T00:00:00.000' AS DateTime), N'F', N'S', N'OROPEL', 568, N'CORDOBA CAPITAL', 125, NULL, NULL, CAST(N'1953-08-29T00:00:00.000' AS DateTime), CAST(N'1957-08-28T00:00:00.000' AS DateTime), 1, 0, 1, CAST(14137.00 AS Decimal(18, 2)), N'oropelnelidarosa@hotmail.com', NULL, NULL, 1, N'Observacion de OROPEL, NELIDA ROSA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (196, N'MOLINA, ROSA ESPERANZA', 27423003521, N'DNI', 4230035, CAST(N'1941-12-08T00:00:00.000' AS DateTime), N'F', N'Z', N'MOLINA', 423, N'CORDOBA CAPITAL', 126, NULL, NULL, CAST(N'1948-12-06T00:00:00.000' AS DateTime), CAST(N'1952-12-05T00:00:00.000' AS DateTime), 0, 1, 1, CAST(13991.00 AS Decimal(18, 2)), N'molinarosaesperanza@gmail.com', NULL, NULL, 1, N'Observacion de MOLINA, ROSA ESPERANZA')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (197, N'BAZAN, ROQUE', 20799556806, N'DNI', 7995568, CAST(N'1947-04-05T00:00:00.000' AS DateTime), N'M', N'Z', N'BAZAN', 799, N'CORDOBA CAPITAL', 127, NULL, NULL, CAST(N'1954-04-03T00:00:00.000' AS DateTime), CAST(N'1958-04-02T00:00:00.000' AS DateTime), 1, 0, 0, CAST(10558.00 AS Decimal(18, 2)), N'bazanroque@hotmail.com', NULL, NULL, 1, N'Observacion de BAZAN, ROQUE')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (198, N'PEREYRA, OLGA BEATRIZ', 27548992923, N'DNI', 5489929, CAST(N'1947-01-09T00:00:00.000' AS DateTime), N'F', N'Z', N'PEREYRA', 548, N'CORDOBA CAPITAL', 128, NULL, NULL, CAST(N'1954-01-07T00:00:00.000' AS DateTime), CAST(N'1958-01-06T00:00:00.000' AS DateTime), 0, 1, 1, CAST(13913.00 AS Decimal(18, 2)), N'pereyraolgabeatriz@gmail.com', NULL, NULL, 1, N'Observacion de PEREYRA, OLGA BEATRIZ')

INSERT [dbo].[Clientes] ([IdCliente], [Nombre], [Cuit], [IdTipoDocumento], [NumeroDocumento], [FechaNacimiento], [IdSexo], [IdEstadoCivil], [Calle], [NumeroCalle], [Localidad], [IdDepartamento], [IdProvincia], [IdPais], [FechaIngreso], [FechaEgreso], [TieneTrabajo], [TieneAuto], [TieneCasa], [CreditoMaximo], [Mail], [Clave], [IdUsuario], [Activo], [Observaciones]) VALUES (199, N'MORENO, MARIA ESTHER', 27625858334, N'CF', 6258583, CAST(N'1950-03-04T00:00:00.000' AS DateTime), N'F', N'Z', N'MORENO', 625, N'CORDOBA CAPITAL', 129, NULL, NULL, CAST(N'1957-03-02T00:00:00.000' AS DateTime), CAST(N'1961-03-01T00:00:00.000' AS DateTime), 1, 0, 1, CAST(13882.00 AS Decimal(18, 2)), N'morenomariaesther@hotmail.com', NULL, NULL, 1, N'Observacion de MORENO, MARIA ESTHER')

SET IDENTITY_INSERT [dbo].[Clientes] OFF


SET IDENTITY_INSERT [dbo].[AuditoriasABM] ON 

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (3, CAST(N'2001-11-15T00:00:00.000' AS DateTime), N'CONTACTOS', 1, N'M', NULL, NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (4, CAST(N'2012-04-20T00:00:00.000' AS DateTime), N'CLIENTES', 775, N'A', NULL, NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (5, CAST(N'2012-04-20T11:03:04.737' AS DateTime), N'CLIENTES', 782, N'A', NULL, NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (6, CAST(N'2012-04-24T11:44:18.017' AS DateTime), N'CLIENTES', 788, N'A', NULL, NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (7, CAST(N'2012-04-24T11:46:53.323' AS DateTime), N'CLIENTES', 789, N'A', NULL, NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (8, CAST(N'2012-04-24T12:00:54.887' AS DateTime), N'CLIENTES', 792, N'A', NULL, NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (9, CAST(N'2012-04-24T12:03:51.860' AS DateTime), N'CLIENTES', 794, N'A', NULL, NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (10, CAST(N'2012-04-24T12:07:32.020' AS DateTime), N'CLIENTES', 796, N'A', NULL, NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (11, CAST(N'2012-04-24T12:10:29.490' AS DateTime), N'CLIENTES', 802, N'A', NULL, NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (12, CAST(N'2012-04-24T12:13:06.673' AS DateTime), N'', 803, N'A', NULL, NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (13, CAST(N'2012-04-24T12:16:11.350' AS DateTime), N'CLIENTES', 807, N'A', NULL, NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (20, CAST(N'2013-04-09T11:59:22.207' AS DateTime), N'CLIENTES', 898, N'B', N'facu', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (21, CAST(N'2013-04-09T12:00:02.600' AS DateTime), N'CLIENTES', 896, N'B', N'facu', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (22, CAST(N'2013-04-09T12:01:55.800' AS DateTime), N'CLIENTES', 912, N'B', N'facu', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (23, CAST(N'2013-04-09T12:01:58.333' AS DateTime), N'CLIENTES', 913, N'B', N'facu', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (24, CAST(N'2013-04-09T12:04:10.020' AS DateTime), N'CLIENTES', 914, N'B', N'facu', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (26, CAST(N'2003-03-03T00:00:00.000' AS DateTime), N'CLIENTES', 900, N'D', NULL, NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (37, CAST(N'2013-04-09T12:16:37.003' AS DateTime), N'CLIENTES', 911, N'D', N'Marcelo', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (38, CAST(N'2013-04-09T12:16:38.190' AS DateTime), N'CLIENTES', 911, N'D', N'Marcelo', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (39, CAST(N'2013-04-09T12:18:21.090' AS DateTime), N'CLIENTES', 915, N'D', N'Marcelo', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (40, CAST(N'2013-04-09T12:32:51.017' AS DateTime), N'CLIENTES', 925, N'B', N'facu', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (41, CAST(N'2013-04-09T12:38:38.717' AS DateTime), N'CLIENTES', 936, N'D', N'Marcelo', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (42, CAST(N'2013-04-09T12:39:54.863' AS DateTime), N'CLIENTES', 938, N'D', N'Marcelo', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (43, CAST(N'2013-04-09T12:41:42.393' AS DateTime), N'CLIENTES', 942, N'D', N'Marcelo', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (44, CAST(N'2013-04-09T12:42:45.700' AS DateTime), N'CLIENTES', 939, N'D', N'Marcelo', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (45, CAST(N'2013-04-09T12:43:57.207' AS DateTime), N'CLIENTES', 947, N'D', N'Marcelo', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (46, CAST(N'2013-04-09T12:48:03.337' AS DateTime), N'CLIENTES', 950, N'D', N'Marcelo950', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (47, CAST(N'2013-04-09T12:48:37.997' AS DateTime), N'CLIENTES', 946, N'B', N'sol', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (48, CAST(N'2013-04-09T12:50:41.410' AS DateTime), N'CLIENTES', 956, N'B', N'sol', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (49, CAST(N'2013-04-12T08:24:03.423' AS DateTime), N'CLIENTES', 959, N'D', N'Marcelo959', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (64, CAST(N'2013-04-12T08:37:03.747' AS DateTime), N'CLIENTES', 966, N'B', N'asd', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (80, CAST(N'2013-04-12T08:55:34.650' AS DateTime), N'CLIENTES', 978, N'B', N'Gonzalo', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (81, CAST(N'2013-04-16T09:55:08.457' AS DateTime), N'CLIENTES', 979, N'D', N'Marcelo979', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (82, CAST(N'2013-04-16T10:11:30.107' AS DateTime), N'CLIENTES', 989, N'D', N'Marcelo989', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (83, CAST(N'2013-04-16T10:19:13.650' AS DateTime), N'CLIENTES', 1005, N'A', N'edt716', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (84, CAST(N'2013-04-16T10:19:22.790' AS DateTime), N'CLIENTES', 1006, N'A', N'edt716', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (85, CAST(N'2013-04-16T10:19:43.980' AS DateTime), N'CLIENTES', 995, N'D', N'Marcelo995', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (86, CAST(N'2013-04-16T10:19:47.677' AS DateTime), N'CLIENTES', 991, N'D', N'pablo991', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (87, CAST(N'2013-04-16T10:19:52.417' AS DateTime), N'CLIENTES', 992, N'D', N'Marcelo992', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (88, CAST(N'2013-04-16T10:19:53.617' AS DateTime), N'CLIENTES', 1005, N'B', N'Gonzalo', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (89, CAST(N'2013-04-16T10:20:08.353' AS DateTime), N'CLIENTES', 997, N'D', N'Marcelo997', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (90, CAST(N'2013-04-16T10:20:12.100' AS DateTime), N'CLIENTES', 994, N'D', N'Marcelo994', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (91, CAST(N'2013-04-16T10:20:13.940' AS DateTime), N'CLIENTES', 998, N'D', N'pablo998', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (92, CAST(N'2013-04-16T10:20:18.493' AS DateTime), N'CLIENTES', 1006, N'D', N'Marcelo1006', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (93, CAST(N'2013-04-16T10:20:19.320' AS DateTime), N'CLIENTES', 1004, N'D', N'Marcelo1004', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (94, CAST(N'2013-04-16T10:20:20.320' AS DateTime), N'CLIENTES', 1000, N'D', N'Marcelo1000', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (95, CAST(N'2013-04-16T10:20:23.003' AS DateTime), N'CLIENTES', 1001, N'D', N'pablo1001', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (96, CAST(N'2013-04-16T10:20:24.063' AS DateTime), N'CLIENTES', 1003, N'D', N'pablo1003', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (97, CAST(N'2013-04-16T10:20:25.227' AS DateTime), N'CLIENTES', 996, N'D', N'Marcelo996', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (98, CAST(N'2013-04-16T10:20:26.253' AS DateTime), N'CLIENTES', 1007, N'D', N'pablo1007', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (99, CAST(N'2013-04-16T10:20:27.313' AS DateTime), N'CLIENTES', 1002, N'D', N'pablo1002', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (100, CAST(N'2013-04-16T10:20:29.110' AS DateTime), N'CLIENTES', 999, N'D', N'pablo999', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (101, CAST(N'2013-04-16T10:21:21.560' AS DateTime), N'CLIENTES', 1008, N'D', N'Marcelo1008', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (102, CAST(N'2013-04-16T10:21:45.587' AS DateTime), N'CLIENTES', 1012, N'A', N'edt716', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (103, CAST(N'2013-04-16T10:22:06.917' AS DateTime), N'CLIENTES', 1014, N'A', N'sebas', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (104, CAST(N'2013-04-16T10:22:55.247' AS DateTime), N'CLIENTES', 1018, N'D', N'Alexis1018', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (105, CAST(N'2013-04-16T10:23:24.353' AS DateTime), N'CLIENTES', 1012, N'D', N'Alexis1012', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (106, CAST(N'2013-04-16T10:26:42.753' AS DateTime), N'CLIENTES', 1019, N'D', N'Alexis1019', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (107, CAST(N'2013-04-16T10:26:44.830' AS DateTime), N'CLIENTES', 1024, N'D', N'Alexis1024', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (108, CAST(N'2013-04-16T10:26:46.047' AS DateTime), N'CLIENTES', 1021, N'D', N'Alexis1021', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (109, CAST(N'2013-04-16T10:26:47.157' AS DateTime), N'CLIENTES', 1009, N'D', N'Alexis1009', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (110, CAST(N'2013-04-16T10:26:48.030' AS DateTime), N'CLIENTES', 1014, N'D', N'Alexis1014', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (111, CAST(N'2013-04-16T10:29:36.590' AS DateTime), N'CLIENTES', 1066, N'A', N'edt716', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (112, CAST(N'2013-04-16T10:35:43.563' AS DateTime), N'CLIENTES', 1087, N'A', N'edt716', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (113, CAST(N'2013-04-16T10:36:25.723' AS DateTime), N'CLIENTES', 1086, N'B', N'Gonzalo', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (114, CAST(N'2013-04-16T10:37:31.337' AS DateTime), N'CLIENTES', 1092, N'B', N'Gonzalo', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (115, CAST(N'2013-04-16T10:38:16.047' AS DateTime), N'CLIENTES', 1102, N'A', N'Emi', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (116, CAST(N'2013-04-16T10:40:31.063' AS DateTime), N'CLIENTES', 1131, N'A', N'sebas', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (117, CAST(N'2013-04-16T10:42:13.783' AS DateTime), N'CLIENTES', 1102, N'B', N'Gonzalo', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (118, CAST(N'2013-04-16T10:42:14.007' AS DateTime), N'CLIENTES', 1089, N'D', N'Marcelo1089', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (119, CAST(N'2013-04-16T10:42:25.647' AS DateTime), N'CLIENTES', 1090, N'B', N'Gonzalo', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (120, CAST(N'2013-04-16T10:44:20.923' AS DateTime), N'CLIENTES', 1145, N'A', N'Emi', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (121, CAST(N'2013-04-16T10:46:03.457' AS DateTime), N'CLIENTES', 1150, N'A', N'edt716', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (122, CAST(N'2013-04-16T10:49:58.160' AS DateTime), N'CLIENTES', 1162, N'A', N'sebas', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (123, CAST(N'2013-04-16T10:52:42.477' AS DateTime), N'CLIENTES', 1167, N'A', N'Emi', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (124, CAST(N'2013-04-16T10:52:56.770' AS DateTime), N'CLIENTES', 1170, N'A', N'sebas', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (125, CAST(N'2013-04-16T10:53:17.240' AS DateTime), N'CLIENTES', 1167, N'B', N'sebas', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (126, CAST(N'2013-04-16T10:53:21.240' AS DateTime), N'CLIENTES', 1166, N'B', N'sebas', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (127, CAST(N'2013-04-16T10:53:25.117' AS DateTime), N'CLIENTES', 1138, N'B', N'sebas', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (128, CAST(N'2013-04-16T10:54:53.367' AS DateTime), N'CLIENTES', 1176, N'A', N'edt716', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (129, CAST(N'2013-04-16T10:59:03.620' AS DateTime), N'CLIENTES', 1192, N'A', N'Emi', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (130, CAST(N'2013-04-16T11:14:11.810' AS DateTime), N'CLIENTES', 1209, N'B', N'sebas', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (131, CAST(N'2013-04-16T11:14:14.530' AS DateTime), N'CLIENTES', 1233, N'B', N'sebas', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (132, CAST(N'2013-04-16T11:53:57.123' AS DateTime), N'CLIENTES', 1285, N'D', N'pablo1285', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (133, CAST(N'2013-04-16T11:55:36.457' AS DateTime), N'CLIENTES', 1287, N'D', N'pablo1287', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (134, CAST(N'2013-04-16T12:17:43.217' AS DateTime), N'CLIENTES', NULL, N'B', N'Santiago', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (135, CAST(N'2013-04-16T12:18:00.030' AS DateTime), N'CLIENTES', NULL, N'B', N'Santiago', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (136, CAST(N'2013-04-16T12:18:08.047' AS DateTime), N'CLIENTES', NULL, N'B', N'Santiago', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (137, CAST(N'2013-04-16T12:33:05.860' AS DateTime), N'CLIENTES', NULL, N'B', N'Santiago', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (138, CAST(N'2013-04-23T12:08:27.150' AS DateTime), N'CLIENTES', 1290, N'D', N'Marcelo1290', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (139, CAST(N'2013-04-23T12:13:43.993' AS DateTime), N'CLIENTES', 1283, N'D', N'Marcelo1283', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (140, CAST(N'2013-04-30T10:00:37.897' AS DateTime), N'CLIENTES', 1282, N'D', N'Marcelo1282', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (141, CAST(N'2013-04-30T10:06:59.473' AS DateTime), N'CLIENTES', 1295, N'D', N'Marcelo1295', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (142, CAST(N'2013-04-30T10:07:19.393' AS DateTime), N'CLIENTES', 1294, N'D', N'Marcelo1294', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (143, CAST(N'2013-04-30T10:45:09.640' AS DateTime), N'CLIENTES', 1298, N'D', N'Marcelo1298', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (144, CAST(N'2013-05-07T10:50:36.753' AS DateTime), N'CLIENTES', 1301, N'D', N'Marcelo1301', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (145, CAST(N'2013-05-07T10:52:53.837' AS DateTime), N'CLIENTES', 1303, N'D', N'Marcelo1303', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (146, CAST(N'2013-05-07T10:59:04.973' AS DateTime), N'CLIENTES', 1300, N'D', N'Marcelo1300', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (147, CAST(N'2013-05-07T10:59:32.843' AS DateTime), N'CLIENTES', 1311, N'D', N'Marcelo1311', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (148, CAST(N'2013-05-07T12:15:13.120' AS DateTime), N'CLIENTES', 1312, N'D', N'Marcelo1312', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (149, CAST(N'2013-05-07T12:32:21.040' AS DateTime), N'CLIENTES', 1318, N'D', N'Marcelo1318', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (150, CAST(N'2013-07-09T16:25:04.407' AS DateTime), N'CLIENTES', 1189, N'D', N'Marcelo1189', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (151, CAST(N'2013-07-09T17:10:23.987' AS DateTime), N'CLIENTES', 1314, N'D', N'Marcelo1314', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (152, CAST(N'2013-07-09T17:10:27.057' AS DateTime), N'CLIENTES', 1302, N'D', N'Marcelo1302', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (153, CAST(N'2013-07-09T17:35:54.337' AS DateTime), N'CLIENTES', 1316, N'D', N'Marcelo1316', NULL)

INSERT [dbo].[AuditoriasABM] ([IdAuditoriaABM], [Fecha], [Tabla], [IdRegistro], [AccionABM], [Observaciones], [IdSession]) VALUES (154, CAST(N'2014-09-30T20:02:06.143' AS DateTime), N'CLIENTES', 9858, N'B', N'', NULL)

SET IDENTITY_INSERT [dbo].[AuditoriasABM] OFF



SET IDENTITY_INSERT [dbo].[Ventas] ON 

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1, 1, CAST(N'2014-10-02T20:29:18.197' AS DateTime), CAST(3317.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (2, 2, CAST(N'2014-10-02T20:37:50.483' AS DateTime), CAST(25136.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (3, 3, CAST(N'2014-10-02T20:54:00.917' AS DateTime), CAST(23482.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (4, 4, CAST(N'2014-10-02T21:41:18.617' AS DateTime), CAST(6801.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (5, 5, CAST(N'2014-10-02T22:32:14.090' AS DateTime), CAST(6272.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (6, 6, CAST(N'2014-10-03T18:43:39.790' AS DateTime), CAST(12685.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (7, 1, CAST(N'2014-10-04T12:06:06.747' AS DateTime), CAST(7848.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (8, 1, CAST(N'2014-10-04T13:40:42.693' AS DateTime), CAST(15532.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (9, 1, CAST(N'2014-10-04T13:52:00.797' AS DateTime), CAST(32503.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1008, 1, CAST(N'2014-10-06T15:45:20.453' AS DateTime), CAST(1844.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1011, 1, CAST(N'2014-10-06T15:47:42.627' AS DateTime), CAST(349.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1012, 3, CAST(N'2020-11-20T20:46:52.923' AS DateTime), CAST(134454.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1013, 5, CAST(N'2020-11-20T20:55:26.170' AS DateTime), CAST(10197.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1014, 144, CAST(N'2020-11-20T21:44:07.347' AS DateTime), CAST(23235.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1015, 119, CAST(N'2020-11-20T22:35:16.707' AS DateTime), CAST(369.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1016, 70, CAST(N'2020-11-20T23:21:17.687' AS DateTime), CAST(5899.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1017, 112, CAST(N'2020-11-21T14:46:13.893' AS DateTime), CAST(649.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1018, 137, CAST(N'2020-11-01T15:00:00.000' AS DateTime), CAST(10399.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1019, 112, CAST(N'2020-11-21T20:20:05.177' AS DateTime), CAST(5899.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1020, 182, CAST(N'2020-11-23T03:00:00.000' AS DateTime), CAST(5899.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1021, 34, CAST(N'2020-11-24T21:47:57.210' AS DateTime), CAST(12498.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1022, 70, CAST(N'2020-11-24T21:50:00.000' AS DateTime), CAST(5899.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1023, 34, CAST(N'2020-11-24T21:52:53.067' AS DateTime), CAST(14998.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1024, 34, CAST(N'2020-11-24T22:17:38.283' AS DateTime), CAST(6548.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1025, 9, CAST(N'2021-02-01T22:50:31.990' AS DateTime), CAST(5597.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1026, 112, CAST(N'2021-02-08T20:37:44.820' AS DateTime), CAST(259.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1027, 112, CAST(N'2021-02-25T18:02:15.507' AS DateTime), CAST(5405.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1028, 100, CAST(N'2021-02-25T18:02:15.507' AS DateTime), CAST(15497.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1029, 119, CAST(N'2021-02-26T15:19:01.590' AS DateTime), CAST(1798.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1030, 82, CAST(N'2021-02-26T15:33:36.843' AS DateTime), CAST(2669.00 AS Decimal(18, 2)))

INSERT [dbo].[Ventas] ([IdVenta], [IdCliente], [Fecha], [Total]) VALUES (1031, 129, CAST(N'2021-02-26T16:32:47.373' AS DateTime), CAST(2669.00 AS Decimal(18, 2)))

SET IDENTITY_INSERT [dbo].[Ventas] OFF



SET IDENTITY_INSERT [dbo].[VentasDetalle] ON 

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1, 1, 1, 1, CAST(299.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (2, 1, 2, 1, CAST(349.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (3, 1, 3, 1, CAST(2669.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (4, 2, 8, 1, CAST(5405.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (5, 2, 8, 1, CAST(5405.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (6, 2, 9, 1, CAST(5290.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (7, 2, 10, 1, CAST(4837.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (8, 2, 11, 1, CAST(4199.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (9, 3, 1, 1, CAST(299.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (10, 3, 4, 1, CAST(2999.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (11, 3, 7, 1, CAST(4830.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (12, 3, 298, 1, CAST(4389.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (13, 3, 301, 1, CAST(2999.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (14, 3, 5, 1, CAST(3129.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (15, 3, 10, 1, CAST(4837.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (16, 4, 198, 1, CAST(122.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (17, 4, 253, 1, CAST(631.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (18, 4, 285, 1, CAST(549.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (19, 4, 88, 1, CAST(5499.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (20, 5, 1, 1, CAST(299.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (21, 5, 4, 1, CAST(2999.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (22, 5, 63, 1, CAST(75.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (23, 5, 70, 1, CAST(2899.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (24, 6, 1, 1, CAST(299.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (25, 6, 2, 1, CAST(349.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (26, 6, 1, 1, CAST(299.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (27, 6, 295, 1, CAST(6239.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (28, 6, 296, 1, CAST(5499.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (29, 7, 2, 1, CAST(349.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (30, 7, 3, 1, CAST(2669.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (31, 7, 7, 1, CAST(4830.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (32, 8, 10, 1, CAST(4837.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (33, 8, 9, 1, CAST(5290.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (34, 8, 8, 1, CAST(5405.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (35, 9, 1, 1, CAST(299.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (36, 9, 2, 1, CAST(349.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (37, 9, 8, 1, CAST(5405.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (38, 9, 9, 5, CAST(5290.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1032, 1008, 2, 1, CAST(349.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1033, 1008, 1, 5, CAST(299.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1036, 1011, 2, 1, CAST(349.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1037, 1012, 107, 1, CAST(123456.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1038, 1012, 140, 2, CAST(5499.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1039, 1013, 74, 1, CAST(199.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1040, 1013, 150, 2, CAST(4999.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1041, 1014, 109, 2, CAST(369.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1042, 1014, 141, 3, CAST(7499.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1043, 1015, 109, 1, CAST(369.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1044, 1016, 139, 1, CAST(5899.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1045, 1017, 112, 1, CAST(649.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1046, 1018, 144, 1, CAST(10399.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1047, 1019, 139, 1, CAST(5899.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1048, 1020, 139, 1, CAST(5899.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1049, 1021, 150, 1, CAST(4999.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1050, 1021, 141, 1, CAST(7499.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1051, 1022, 139, 1, CAST(5899.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1052, 1023, 141, 2, CAST(7499.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1053, 1024, 112, 1, CAST(649.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1054, 1024, 139, 1, CAST(5899.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1055, 1025, 3, 2, CAST(2669.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1056, 1025, 107, 1, CAST(259.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1057, 1026, 107, 1, CAST(259.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1058, 1027, 8, 1, CAST(5405.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1059, 1028, 88, 1, CAST(5499.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1060, 1028, 150, 2, CAST(4999.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1061, 1029, 199, 2, CAST(899.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1062, 1030, 3, 1, CAST(2669.00 AS Decimal(18, 2)))

INSERT [dbo].[VentasDetalle] ([IdVentaDetalle], [IdVenta], [IdArticulo], [Cantidad], [Precio]) VALUES (1063, 1031, 3, 1, CAST(2669.00 AS Decimal(18, 2)))

SET IDENTITY_INSERT [dbo].[VentasDetalle] OFF


END
GO
/****** Object:  StoredProcedure [dbo].[ClientePorId]    Script Date: 05/03/2021 17:46:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ClientePorId]
	
	(
	@IdCliente int
	)
	
AS

select * from clientes where IdCliente = @IdCliente  
	
	RETURN



GO
/****** Object:  StoredProcedure [dbo].[ClientesContar]    Script Date: 05/03/2021 17:46:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ClientesContar]
	
	(
	
	@CantidadDeClientes int = 0 OUTPUT 
	)
	
AS
	select @CantidadDeClientes = count(*) from clientes
	RETURN
GO
